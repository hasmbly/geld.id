<?php
use Nwidart\Modules\Routing\Controller;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('auth/login');
// });
Route::post('/auth/login', 'Auth\LoginController@login2')->name('login');

Route::get('/', 'Pages\HomeController@index')->name('home');

Route::get('/store_list', 'Pages\StoresController@index')->name('stores');


// Route::get('/stores/{code}', 'pages\StoresController@detail')->name('stores');

Route::get('/principal_list', 'Pages\PrincipalsController@index')->name('principals');
// Route::get('/principal/{code}', 'pages\PrincipalsController@detail')->name('principals');

// Route::get('/promo', function () { return view('pages/promo');});
// Route::get('/events', function () { return view('pages/events');});
// Route::get('/merchandise', function () { return view('pages/merchandise');});
// Route::get('/services', function () { return view('pages/services');});
// Route::get('/download/apps', function () { return view('pages/download_apps');});
Route::get('/page/blogs', 'Pages\BlogsController@index')->name('blogs');
Route::get('/page/news', 'Pages\NewsController@index')->name('news');
Route::get('/page/gallery', 'Pages\GalleryController@index')->name('gallery');
Route::get('/page/faq', 'Pages\FaqController@index')->name('faq');
Route::get('/page/contact_us', 'Pages\ContactController@index')->name('contact_us');

Route::get('/page/about_us', 'Pages\AboutUsController@index')->name('about_us');

Route::get('/page/events', 'Pages\EventsController@index')->name('events');
Route::get('/page/events_list', 'Pages\EventsController@index')->name('events');

Route::get('/page/promo', 'Pages\PromoController@index')->name('promo');
Route::get('/page/promo_list', 'Pages\PromoController@index')->name('promo');

Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    return "Cache is cleared";
});
// Route::get('/page/gallery', function () { return view('pages/gallery');});
// Route::get('/page/faq', function () { return view('pages/faq');});
// Route::get('/page/contact_us', function () { return view('pages/contact_us');});
// Route::get('/about_us', function () { return view('pages/about_us');});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

// Route::post('/home', 'Auth\LoginController@showLoginForm2')->name('login');
// $this->get('login', 'Auth\LoginController@showLoginForm2')->name('login');
