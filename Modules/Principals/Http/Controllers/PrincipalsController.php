<?php

namespace Modules\Principals\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
// use Illuminate\Routing\Controller;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Principals;
use App\PrincipalsCategory;

class PrincipalsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        // return view('principals::index');
        return view('principals::index', [
            'principals' => 
            // Principals::all()
            Principals::select('principals_categories.name','principals.code','principals.brand_name','principals.img_logo', 'principals.address','principals.status','principals.img_width')
            ->join('principals_categories', 'principals.category_code', '=', 'principals_categories.code')
                ->get()
            ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('principals::create',['options' => [
            'N/A' => 'N/A', 'No' => 'No', 'Yes' => 'Yes'
        ],
        'categories' => PrincipalsCategory::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
		$this->validate($request, [
            'code' => 'required',
            'brand_name' => 'required',
            'category_code' => 'required',
			'img_logo' => 'required|file|image|mimes:jpeg,png,jpg|max:2048',
        ]);

        $img_logo_name = null;

        $file = $request->file('img_logo');
        if(!empty($file)){
            $img_logo_name = 'LogoPrincipal_'.md5($request->file . microtime()).'_'.$file->getClientOriginalName();
            $file->move(public_path('/public/img/principals/logos/'), $img_logo_name);
        }

        if(!empty($img_logo_name)){
            $img_office_name = null;
            $file_img_office = $request->file('img_office');
            if(!empty($file_img_office)){
                $img_office_name = 'OfficePrincipal_'.md5($request->file . microtime()).'_'.$file_img_office->getClientOriginalName();
                $file_img_office->move(public_path('/public/img/principals/offices/'), $img_office_name);
            }

            if(!empty($request->id)){
                DB::table('principals')->where('id',$request->id)->update([
                    'code' => $request->code,
                    'brand_name' => $request->brand_name,
                    'category_code' => $request->category_code,
                    'content' => $request->content,
                    'address' => $request->address,
                    'url_website' => $request->url_website,
                    'shareholder_legal_name' => $request->shareholder_legal_name,
                    'is_operating_fin_sservice' => $request->is_operating_fin_sservice,
                    'is_owner_our_partner' => $request->is_owner_our_partner,
                    'reference_principal_code' => $request->reference_principal_code,
                    'is_multiple_country' => $request->is_multiple_country,
                    'country_head_office' => $request->country_head_office,
                    'country_located' => $request->country_located,
                    'principal_legal_name' => $request->principal_legal_name,
                    'is_online_trx' => $request->is_online_trx,
                    'url_for_customer' => $request->url_for_customer,
                    'url_for_business' => $request->url_for_business,
                    'call_center_number' => $request->call_center_number,
                    'email_corporate' => $request->email_corporate,
                    'url_annual_report' => $request->url_annual_report,
                    'img_logo' => $img_logo_name,
                    'img_office' => $img_office_name
                ]);
                $request->session()->flash('alert-success', 'Principals `'.$request->name.'` was successful updated!');
            }else{

                DB::table('principals')->insert([
                    'code' => $request->code,
                    'brand_name' => $request->brand_name,
                    'category_code' => $request->category_code,
                    'content' => $request->content,
                    'address' => $request->address,
                    'url_website' => $request->url_website,
                    'shareholder_legal_name' => $request->shareholder_legal_name,
                    'is_operating_fin_sservice' => $request->is_operating_fin_sservice,
                    'is_owner_our_partner' => $request->is_owner_our_partner,
                    'reference_principal_code' => $request->reference_principal_code,
                    'is_multiple_country' => $request->is_multiple_country,
                    'country_head_office' => $request->country_head_office,
                    'country_located' => $request->country_located,
                    'principal_legal_name' => $request->principal_legal_name,
                    'is_online_trx' => $request->is_online_trx,
                    'url_for_customer' => $request->url_for_customer,
                    'url_for_business' => $request->url_for_business,
                    'call_center_number' => $request->call_center_number,
                    'email_corporate' => $request->email_corporate,
                    'url_annual_report' => $request->url_annual_report,
                    'img_logo' => $img_logo_name,
                    'img_office' => $img_office_name
                ]);
                $request->session()->flash('alert-success', 'Principals `'.$request->name.'` was successful added!');
            }
        }
        return redirect('/principals');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('principals::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $principals = DB::table('principals')->where('code',$id)->get();

        return view('principals::edit',['principals' => $principals, 
        'options' => [
            'N/A' => 'N/A', 'No' => 'No', 'Yes' => 'Yes'
        ],'categories' => PrincipalsCategory::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        DB::table('principals')->where('code',$id)->delete();

        return redirect('/principals')->with('alert-success','Principal id.'.$id.' successful deleted!');
    }
}
