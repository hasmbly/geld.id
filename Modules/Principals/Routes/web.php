<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('principals')->group(function() {
    Route::get('/', 'PrincipalsController@index');
    Route::get('/create', 'PrincipalsController@create');
    Route::post('/store', 'PrincipalsController@store');
    Route::get('/destroy/{id}', 'PrincipalsController@destroy');
    Route::get('/edit/{id}', 'PrincipalsController@edit');
});
