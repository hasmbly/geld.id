<?php

namespace Modules\Sites\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class SitesServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('sites.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'sites'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/sites');

        $sitesPath = __DIR__.'/../Resources/views';

        $homePath = __DIR__.'/../Resources/views/pages/home';

        $contactUsPath = __DIR__.'/../Resources/views/pages/contact_us';

        $aboutUsPath = __DIR__.'/../Resources/views/pages/about_us';

        $this->publishes([
            $sitesPath => $viewPath,
            $homePath => $viewPath,
            $contactUsPath => $viewPath,
            $aboutUsPath => $viewPath,
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/sites';
        }, \Config::get('view.paths')), [$sitesPath]), 'sites');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/sites';
        }, \Config::get('view.paths')), [$homePath]), 'home');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/sites';
        }, \Config::get('view.paths')), [$contactUsPath]), 'contact_us'); 

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/sites';
        }, \Config::get('view.paths')), [$aboutUsPath]), 'about_us');                           
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/sites');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'sites');
        } else {
            $this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'sites');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(__DIR__ . '/../Database/factories');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
