<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route::prefix('sites')->group(function() {
//     Route::get('/', 'SitesController@index');
//     Route::get('/home', 'Home\HomeController@index');
// });

/**
 * Route for Sites->Home
 */
Route::prefix('sites/home')->group(function() {

    Route::get('/', 'Home\HomeController@index');
    
    Route::get('/create', 'Home\HomeController@create');

    Route::post('/store', 'Home\HomeController@store');

    Route::post('/stores_images', 'Home\HomeController@stores_images'); // new/update stores_images
    Route::post('/front_images', 'Home\HomeController@front_image'); // new/update front_images

    Route::get('/destroy/{id}', 'Home\HomeController@destroy');
    Route::post('/destroy/image', 'Home\HomeController@destroy_image'); // destroy stores_images

    Route::get('/edit/{id}', 'Home\HomeController@edit');

    Route::post('/is_publish', 'Home\HomeController@update_is_publish');

}); 



/**
 * Route for Sites->Contact Us
 */
Route::prefix('sites/contact_us')->group(function() {
    Route::get('/', 'ContactUs\ContactUsController@index');
    Route::post('/store', 'ContactUs\ContactUsController@store');
    Route::post('/question', 'ContactUs\ContactUsController@question');
}); 

/**
 * Route for Sites->About Us
 */
Route::prefix('sites/about_us')->group(function() {
    Route::get('/', 'AboutUs\AboutUsController@index');
    Route::post('/store', 'AboutUs\AboutUsController@store');

    Route::post('/front_images_large', 'AboutUs\AboutUsController@front_images_large'); 
    Route::post('/front_images_medium', 'AboutUs\AboutUsController@front_images_medium'); 
    Route::post('/front_images_small', 'AboutUs\AboutUsController@front_images_small');
}); 



      

















      