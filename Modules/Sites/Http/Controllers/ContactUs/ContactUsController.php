<?php

namespace Modules\Sites\Http\Controllers\ContactUs;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ContactUsController extends Controller
{   

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {   

        $contact_us = DB::table('contact_us')->get();

        return view('contact_us::index', [
            'contact_us' => $contact_us
            ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {

        /* Validator */
          $rules = array(
            'address'           => 'required',
            'phone'             => 'required',
            'whatsapp'          => 'required',
            'email'             => 'required',
            'get_in_touch'      => 'required',
            'business_hours'     => 'required'
            );  

          $messages = array(
            'address.required'          => 'Please provide your address',
            'phone.required'            => 'Please provide your phone',
            'whatsapp.required'         => 'Please provide your whatsapp',
            'email.required'            => 'Please provide your email',
            'get_in_touch.required'     => 'Please provide your get_in_touch',
            'business_hours.required'     => 'Please provide your get_in_touch'
          );

          $validator = Validator::make( $request->all(), $rules, $messages );

        if ($validator->fails()) {

            return response()->json([
                'error'    => true,
                'message'  => $validator->errors()->first()
            ], 422);

        }          

            if(!empty($request->id)){

                $update = DB::table('contact_us')->where('id',$request->id)->update([
                    'address'         => $request->address,
                    'phone'           => $request->phone,
                    'whatsapp'        => $request->whatsapp,
                    'email'           => $request->email,
                    'get_in_touch'    => $request->get_in_touch,
                    'business_hours'  => $request->business_hours
                ]);

                $request->session()->flash('alert-success', 'Contact Us was successful Updated!');

                 return response()->json([
                    'message' => 'Success Updated'
                 ]);   

            }

    }

    public function question(Request $request)
    {

          $insert = DB::table('question')->where('id',$request->id)->insert([
              'fullname'   => $request->fullname,
              'email'      => $request->email,
              'subject'    => $request->subject,
              'message'    => $request->message
          ]);

          $request->session()->flash('alert-success', 'Your message has been sent to us.');

           return response()->json([
              'response' => 'success'
           ]);   

    }


}

