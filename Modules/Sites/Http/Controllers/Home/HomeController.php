<?php

namespace Modules\Sites\Http\Controllers\Home;

use App\PageHome;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{   
    private $dir_img_stores = '/assets/media/home/';

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('home::index', [
            'home' => PageHome::all()
            ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('home::create');
    }

    /**
     * [stores_images description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function stores_images(Request $request)
    {   


        $file   = $request->file('file');
        $id     = $request->id;

        /* Validator */
          $rules = array(
              'file'             => 'required',
              'id'               => 'required'
                  );    
          $messages = array(
            'file.required'         => 'Please provide your file',
            'id.required'           => 'Please provide your id'
          );

          $validator = Validator::make( $request->all(), $rules, $messages );

        if ($validator->fails()) {


            return response()->json([
                'error'    => true,
                'message'  => $validator->errors()->first()
            ], 422);

        }          
        
        $file->move(public_path($this->dir_img_stores), $file->getClientOriginalName());

        try {
            $InsertImages = DB::table('page_homes_images')->insert([
                                'pages_homes_id'    => $id,
                                'photo'             => $file->getClientOriginalName()
            ]);

        } catch (QueryException $e) {
            return response()->json([
                'error'       => true,
                'code'        => $e->getCode(),
                'message'     => $e->getMessage()
              ], 500);
             }

         return response()->json([
            'error'     => false,
            'message'   => 'Images Uploaded',
            'id'        => $id,
            'Filename'  => $file->getClientOriginalName()
         ], 200);

        
    }

    public function front_image(Request $request)
    {
        $file       = $request->file('file');

        $id         = $request->id;

        /* Validator */

          $rules = array(
              'file'             => 'required',
              'id'               => 'required'
                  );    
          $messages = array(

            'file.required'         => 'Please provide your file',
            'id.required'           => 'Please provide your id'
          );

          $validator = Validator::make( $request->all(), $rules, $messages );

        if ($validator->fails()) {

            return response()->json([
                'error'    => true,
                'message'  => $validator->errors()->first()
            ], 422);

        }        
        
        $file->move(public_path($this->dir_img_stores), $file->getClientOriginalName());

        // $this->photo = $file->getClientOriginalName();
        try {

        DB::table('page_homes')
            ->where('id',$id)
            ->update([
                'img' => $file->getClientOriginalName()
            ]);            
            
        } catch (QueryException $e) {

        return response()->json([
            'error'       => true,
            'code'        => $e->getCode(),
            'message'     => $e->getMessage()
          ], 500);

         } 

         return response()->json([
            'error'     => false,
            'message'   => 'Front Image Uploaded',
            'id'        => $id,
            'Filename'  => $file->getClientOriginalName()
         ], 200);

    }     

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {


        /* Validator */
          $rules = array(
            'title'         => 'required',
            'intro'         => 'required',
            'description'   => 'required'
                  );    
          $messages = array(
            'title.required'        => 'Please provide your Title',
            'intro.required'        => 'Please provide your Intro',
            'description.required'  => 'Please provide your description'
          );

          $validator = Validator::make( $request->all(), $rules, $messages );

        if ($validator->fails()) {

            return response()->json([
                'error'    => true,
                'message'  => $validator->errors()->first()
            ], 422);

        }          

            if(!empty($request->id)){

                $update = DB::table('page_homes')->where('id',$request->id)->update([
                    'title'         => $request->title,
                    'intro'         => $request->intro,
                    'description'   => $request->description
                ]);
                $request->session()->flash('alert-success', 'Page Home `'.$request->title.'` was successful Updated!');
            }else{

                $insertGetId = DB::table('page_homes')->insertGetId([
                                'title'         => $request->title,
                                'intro'         => $request->intro,
                                'description'   => $request->description
                            ]);

                $request->session()->flash('alert-success', 'Page Home `'.$request->title.'` was successful added!');
                 
                 return response()->json([
                    'id'        => $insertGetId,
                    'message'   => 'Success Added'
                 ]);   

            }

                 $request->session()->flash('alert-success', 'Page Home `'.$request->title.'` was successful Updated!');
          
                 return response()->json([
                    'message' => 'Success Updated'
                 ]);                            

    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('home::created');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $home   = PageHome::where('id', $id)->get();
        $getId    = $home[0]->id;

        try {        
        $stores_images = [];
        $stores_images_raw = DB::table('page_homes_images')->where('pages_homes_id', $getId)->get();
        $dir = public_path($this->dir_img_stores);
        foreach ($stores_images_raw as $row) {
            
            $imgs = @exif_read_data($dir.$row->photo);
            // $imgs    = Image::make(public_path($row->photo))->exif();
            array_push(
                $stores_images,[
                'name'      => $imgs['FileName'], 
                'size'      => $imgs['FileSize'], 
                'type'      => $imgs['MimeType'], 
                'status'    => 'queued', 
                'dataURL'   => $this->dir_img_stores.$imgs['FileName']
            ]);
        }
        } catch (QueryException $e) {

            return response()->json([
                'error'       => true,
                'code'        => $e->getCode(),
                'message'     => $e->getMessage()
              ], 500);

             }         

        try {
                $front_image = [];
                $query = DB::table('page_homes')->where('id', $getId)->pluck('img');
                $dir = public_path($this->dir_img_stores);
                $image = @exif_read_data($dir.$query[0]);
                    array_push(
                        $front_image,[
                        'name'      => $image['FileName'], 
                        'size'      => $image['FileSize'], 
                        'type'      => $image['MimeType'], 
                        'status'    => 'queued', 
                        'dataURL'   => $this->dir_img_stores.$image['FileName']
                    ]);                 
            } catch (QueryException $e) {

                return response()->json([
                    'error'       => true,
                    'code'        => $e->getCode(),
                    'message'     => $e->getMessage()
                  ], 500);

                 } 
      

        return view('home::edit',[
            'home' => $home,
            'stores_images' => json_encode($stores_images),
            'front_image' => json_encode($front_image)
            ]); 
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * [destroy_image description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function destroy_image(Request $request)
    {

        $file       = $request->filename;
        $id         = $request->id;

        if($file && $id){

            $getId = DB::table('page_homes')->where('id', $id)->pluck('id');       

            foreach ($file as $value) {

                DB::table('page_homes_images')
                ->where('pages_homes_id', '=', $getId[0])
                ->where('photo', '=', $value)
                ->delete();
            }

            return response('Photo deleted', 200);
            
        }
    }


    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $title = DB::table('page_homes')->where('id', $id)->pluck('title');
        $title = $title[0];

        DB::table('page_homes')->where('id',$id)->delete();

        return redirect('/sites/home')->with('alert-success','Sites Page - Home id.'.$title.' successful deleted!');        
    }

    public function update_is_publish(Request $request){

                $is_publish_code = $request->is_publish;
                $id              = $request->id;

                // check if request publish
                if ($request->is_publish == 1) {

                  $checkPublish = $this->check_existing_publish();

                  if ($checkPublish == false){

                     // return response()->json([
                     //    'error'     => true,
                     //    'message'   => 'Sorry, Only One Content Can be Published'
                     // ]);
                    
                    $set_All_Un_publish = DB::table('page_homes')
                                        ->update([
                                          'is_publish' => 0
                                        ]);                    
                     
                    $update = DB::table('page_homes')->where('id', $id)
                                  ->update([
                                    'is_publish' => $is_publish_code
                                  ]);

                    $getTitle = DB::table('page_homes')->where('id', $id)->pluck('title');

                    $title    = $getTitle[0];
                     
                    $request->session()->flash('alert-success', 'Page Home `'.$title.'` was successful Published');                  

                    return response()->json([
                        'error'     => false,
                        'message'   => 'Published.'
                     ]);                     

                  }  else {

                      $update = DB::table('page_homes')->where('id', $id)
                                  ->update([
                                    'is_publish' => $is_publish_code
                                  ]);

                      $getTitle = DB::table('page_homes')->where('id', $id)->pluck('title');

                      $title    = $getTitle[0];
                     
                     $request->session()->flash('alert-success', 'Page Home `'.$title.'` was successful Published');                  

                     return response()->json([
                        'error'     => false,
                        'message'   => 'Published.'
                     ]);

                  }

                } 

                // check if request un publish
                else {                

                      $update = DB::table('page_homes')->where('id', $id)
                                  ->update([
                                    'is_publish' => $is_publish_code
                                  ]);

                      $getTitle = DB::table('page_homes')->where('id', $id)->pluck('title');

                      $title    = $getTitle[0];

                     $request->session()->flash('alert-success', 'Page Home `'.$title.'` was successful Un Publish');
                    
                     return response()->json([
                        'error'     => false,
                        'message'   => 'Un Publish.'
                     ]); 

                                  
                }
      }

    /**
     * [check_existing_publish description]
     * @return [type] [description]
     */
    public function check_existing_publish(){
        
            $check = DB::table('page_homes')->where('is_publish', 1)->pluck('is_publish');
            
            if (sizeof($check) == 0) {
              return true;
            } elseif ($check[0] === 1) {
              return false;
            }
    }

    public function check_if_no_published_content(){

      $check = DB::table('page_homes')->where('is_publish', 1)->pluck('is_publish');
        if (sizeof($check) == 0) {
              return response()->json([
                  'message' => 'at least one content should be Publish !'
              ]);
            } 
    }
}

