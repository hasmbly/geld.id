<?php

namespace Modules\Sites\Http\Controllers\AboutUs;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class AboutUsController extends Controller
{   
    private $dir_img_stores = '/public/img/about_us/';

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

        public function front_images_large(Request $request)
    {
        $file       = $request->file('file');

        $id         = $request->id;

        /* Validator */

          $rules = array(
              'file'             => 'required',
              'id'               => 'required'
                  );    
          $messages = array(

            'file.required'         => 'Please provide your file',
            'id.required'           => 'Please provide your id'
          );

          $validator = Validator::make( $request->all(), $rules, $messages );

        if ($validator->fails()) {

            return response()->json([
                'error'    => true,
                'message'  => $validator->errors()->first()
            ], 422);

        }        
        
        $file->move(public_path($this->dir_img_stores), $file->getClientOriginalName());

        // $this->photo = $file->getClientOriginalName();
        try {

        DB::table('about_us')
            ->where('id',$id)
            ->update([
                'img_large' => $file->getClientOriginalName()
            ]);            
            
        } catch (QueryException $e) {

        return response()->json([
            'error'       => true,
            'code'        => $e->getCode(),
            'message'     => $e->getMessage()
          ], 500);

         } 

         return response()->json([
            'error'     => false,
            'message'   => 'Front Image Uploaded',
            'id'        => $id,
            'Filename'  => $file->getClientOriginalName()
         ], 200);

    }       

        public function front_images_medium(Request $request)
    {
        $file       = $request->file('file');

        $id         = $request->id;

        /* Validator */

          $rules = array(
              'file'             => 'required',
              'id'               => 'required'
                  );    
          $messages = array(

            'file.required'         => 'Please provide your file',
            'id.required'           => 'Please provide your id'
          );

          $validator = Validator::make( $request->all(), $rules, $messages );

        if ($validator->fails()) {

            return response()->json([
                'error'    => true,
                'message'  => $validator->errors()->first()
            ], 422);

        }        
        
        $file->move(public_path($this->dir_img_stores), $file->getClientOriginalName());

        // $this->photo = $file->getClientOriginalName();
        try {

        DB::table('about_us')
            ->where('id',$id)
            ->update([
                'img_medium' => $file->getClientOriginalName()
            ]);            
            
        } catch (QueryException $e) {

        return response()->json([
            'error'       => true,
            'code'        => $e->getCode(),
            'message'     => $e->getMessage()
          ], 500);

         } 

         return response()->json([
            'error'     => false,
            'message'   => 'Front Image Uploaded',
            'id'        => $id,
            'Filename'  => $file->getClientOriginalName()
         ], 200);

    }       

        public function front_images_small(Request $request)
    {
        $file       = $request->file('file');

        $id         = $request->id;

        /* Validator */

          $rules = array(
              'file'             => 'required',
              'id'               => 'required'
                  );    
          $messages = array(

            'file.required'         => 'Please provide your file',
            'id.required'           => 'Please provide your id'
          );

          $validator = Validator::make( $request->all(), $rules, $messages );

        if ($validator->fails()) {

            return response()->json([
                'error'    => true,
                'message'  => $validator->errors()->first()
            ], 422);

        }        
        
        $file->move(public_path($this->dir_img_stores), $file->getClientOriginalName());

        // $this->photo = $file->getClientOriginalName();
        try {

        DB::table('about_us')
            ->where('id',$id)
            ->update([
                'img_small' => $file->getClientOriginalName()
            ]);            
            
        } catch (QueryException $e) {

        return response()->json([
            'error'       => true,
            'code'        => $e->getCode(),
            'message'     => $e->getMessage()
          ], 500);

         } 

         return response()->json([
            'error'     => false,
            'message'   => 'Front Image Uploaded',
            'id'        => $id,
            'Filename'  => $file->getClientOriginalName()
         ], 200);

    }       



    public function index()
    {   
        $about_us   = DB::table('about_us')->get();
        $getId      = $about_us[0]->id;

        try {
                $front_image_large = [];
                $query = DB::table('about_us')->where('id', $getId)->pluck('img_large');
                $dir = public_path($this->dir_img_stores);
                $image = @exif_read_data($dir.$query[0]);
                    array_push(
                        $front_image_large,[
                        'name'      => $image['FileName'], 
                        'size'      => $image['FileSize'], 
                        'type'      => $image['MimeType'], 
                        'status'    => 'queued', 
                        'dataURL'   => $this->dir_img_stores.$image['FileName']
                    ]);                 
            } catch (QueryException $e) {

                return response()->json([
                    'error'       => true,
                    'code'        => $e->getCode(),
                    'message'     => $e->getMessage()
                  ], 500);

                 }

        try {
                $front_image_medium = [];
                $query = DB::table('about_us')->where('id', $getId)->pluck('img_medium');
                $dir = public_path($this->dir_img_stores);
                $image = @exif_read_data($dir.$query[0]);
                    array_push(
                        $front_image_medium,[
                        'name'      => $image['FileName'], 
                        'size'      => $image['FileSize'], 
                        'type'      => $image['MimeType'], 
                        'status'    => 'queued', 
                        'dataURL'   => $this->dir_img_stores.$image['FileName']
                    ]);                 
            } catch (QueryException $e) {

                return response()->json([
                    'error'       => true,
                    'code'        => $e->getCode(),
                    'message'     => $e->getMessage()
                  ], 500);

                 }     

        try {
                $front_image_small = [];
                $query = DB::table('about_us')->where('id', $getId)->pluck('img_small');
                $dir = public_path($this->dir_img_stores);
                $image = @exif_read_data($dir.$query[0]);
                    array_push(
                        $front_image_small,[
                        'name'      => $image['FileName'], 
                        'size'      => $image['FileSize'], 
                        'type'      => $image['MimeType'], 
                        'status'    => 'queued', 
                        'dataURL'   => $this->dir_img_stores.$image['FileName']
                    ]);                 
            } catch (QueryException $e) {

                return response()->json([
                    'error'       => true,
                    'code'        => $e->getCode(),
                    'message'     => $e->getMessage()
                  ], 500);

                 }                                                

        return view('about_us::index', [

            'about_us'            => $about_us,
            'front_image_large'   => json_encode($front_image_large),
            'front_image_medium'  => json_encode($front_image_medium),
            'front_image_small'   => json_encode($front_image_small)

          ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {

        /* Validator */
          $rules = array(
            'section_one'         => 'required',
            'section_two'         => 'required',
            'section_three'       => 'required',
            'section_four'        => 'required'
            );  

          $messages = array(
            'section_one.required'        => 'Please provide your section_one',
            'section_two.required'        => 'Please provide your section_two',
            'section_three.required'      => 'Please provide your section_three',
            'section_four.required'       => 'Please provide your section_four'
          );

          $validator = Validator::make( $request->all(), $rules, $messages );

        if ($validator->fails()) {

            return response()->json([
                'error'    => true,
                'message'  => $validator->errors()->first()
            ], 422);

        }          

            if(!empty($request->id)){

                $update = DB::table('about_us')->where('id',$request->id)->update([
                    'section_one'   => $request->section_one,
                    'section_two'   => $request->section_two,
                    'section_three' => $request->section_three,
                    'section_four'  => $request->section_four
                ]);

                $request->session()->flash('alert-success', 'About Us was successful Updated!');

                 return response()->json([
                    'message' => 'Success Updated'
                 ]);   

            }

    }




}

