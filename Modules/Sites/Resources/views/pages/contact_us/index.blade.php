@extends('layouts.admin')

@section('content')
<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-container ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">
				<a href="{{ url('sites/contact_us') }}">Contact Us</a> </h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="/dashboard" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Edit</span>
			</div>
		</div>
	</div>
</div>
<!-- end:: Subheader -->
<!-- begin:: Content -->
<div class="kt-container  kt-grid__item kt-grid__item--fluid">

	<!--Begin::Row-->
	<div class="row">
		<div class="col-xl-12 col-lg-12 order-lg-1 order-xl-1">
            <!-- begin .flash-message -->
            <div class="flash-message">
                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))
                <div class="alert alert-outline-{{ $msg }} fade show" role="alert">
                    <div class="alert-icon"><i class="flaticon-warning"></i></div>
                    <div class="alert-text">{{ Session::get('alert-' . $msg) }}</div>
                    <div class="alert-close">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true"><i class="la la-close"></i></span>
                        </button>
                    </div>
                </div>
                @endif
                @endforeach
            </div> 
            <!-- end .flash-message -->			
			<!-- begin:: Content -->
			<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

				<!--begin::Portlet-->
				<div class="kt-portlet">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title">
								Edit Contact Us
							</h3>
						</div>
					</div>
					
					@foreach($contact_us as $row)
					<!--begin::Form-->
					<form class="kt-form kt-form--label-right" action="" id="form" method="POST" >
						{{ csrf_field() }}
						<input type="hidden" name="id" value="{{ $row->id }}">
						<div class="kt-portlet__body">
							<div class="form-group row">
								<label class="col-form-label col-lg-3 col-sm-12">Address *</label>
								<div class="col-lg-4 col-md-9 col-sm-12">
									<input type="text" class="form-control" name="address" placeholder="Title" required="required" value="{{ $row->address }}">
								</div>
							</div>

							<div class="kt-form__seperator kt-form__seperator--dashed kt-form__seperator--space"></div>
							<div class="form-group row">
								<label class="col-form-label col-lg-3 col-sm-12">Phone *</label>
								<div class="col-lg-4 col-md-9 col-sm-12">
									<input type="text" class="form-control" name="phone" placeholder="Title" required="required" value="{{ $row->phone }}">
								</div>
							</div>

							<div class="kt-form__seperator kt-form__seperator--dashed kt-form__seperator--space"></div>
							<div class="form-group row">
								<label class="col-form-label col-lg-3 col-sm-12">Whatsapp *</label>
								<div class="col-lg-4 col-md-9 col-sm-12">
									<input type="text" class="form-control" name="whatsapp" placeholder="Title" required="required" value="{{ $row->whatsapp }}">
								</div>
							</div>

							<div class="kt-form__seperator kt-form__seperator--dashed kt-form__seperator--space"></div>
							<div class="form-group row">
								<label class="col-form-label col-lg-3 col-sm-12">Email *</label>
								<div class="col-lg-4 col-md-9 col-sm-12">
									<input type="text" class="form-control" name="email" placeholder="Title" required="required" value="{{ $row->email }}">
								</div>
							</div>

							<div class="kt-form__seperator kt-form__seperator--dashed kt-form__seperator--space"></div>
							<div class="form-group row">
								<label class="col-form-label col-lg-3 col-sm-12">Get In Touch *</label>
								<div class="col-lg-7 col-md-9 col-sm-12">
									<textarea name="get_in_touch" class="form-control" data-provide="markdown" rows="5" required="required">{{ $row->get_in_touch }}</textarea>		
								</div>
							</div>

							<div class="kt-form__seperator kt-form__seperator--dashed kt-form__seperator--space"></div>
							<div class="form-group row">
								<label class="col-form-label col-lg-3 col-sm-12">Business Hours *</label>
								<div class="col-lg-7 col-md-9 col-sm-12">
									<textarea name="business_hours" class="form-control" data-provide="markdown" rows="5" required="required">{{ $row->business_hours }}</textarea>		
								</div>
							</div>																					

				</form>
				<!--end::Form-->			
	
					<div class="kt-form__seperator kt-form__seperator--dashed kt-form__seperator--space"></div>
						<div class="kt-portlet__foot">
							<div class=" ">
								<div class="row">
									<div class="col-lg-9 ml-lg-auto">
										<button type="submit" class="btn btn-success submitForm" value="update">Submit</button>
										<a href="{{ url('sites/contact_us') }}" class="btn btn-secondary">Cancel</a>
									</div>
								</div>
							</div>
						</div>
					@endforeach
				</div>
				<!--end::Portlet-->
			</div>
		</div>
	</div>

	<!--End::Row-->
</div>

<!--End::Row-->

@stop

@section('js_page')
<script src="{{ URL::asset('assets/js/demo1/pages/crud/forms/validation/form-widgets.js') }}" type="text/javascript"></script>
<script type="text/javascript">

$('.submitForm').click(function() {
    $.ajax({
           type: "POST",
           url: "{{ url('sites/contact_us/store') }}",
           data: $('#form').serialize(), // changed	
           success: function(data) {
			window.location.replace("{{ url('sites/contact_us') }}");
				console.log('Success : ', data);
           }
    });
    return false; // avoid to execute the actual form submission.
});

</script>
@stop