@extends('layouts.admin')

@section('content')
<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-container ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">
				<a href="{{ url('sites/home') }}">{!! config('sites.name') !!}</a> </h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="/dashboard" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Edit</span>
			</div>
		</div>
	</div>
</div>
<!-- end:: Subheader -->
<!-- begin:: Content -->
<div class="kt-container  kt-grid__item kt-grid__item--fluid">

	<!--Begin::Row-->
	<div class="row">
		<div class="col-xl-12 col-lg-12 order-lg-1 order-xl-1">
			<!-- begin:: Content -->
			<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

				<!--begin::Portlet-->
				<div class="kt-portlet">
					@foreach($home as $row)
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title">
								Edit Page - Home `{{ $row->title }}`
							</h3>
						</div>
					</div>

					<!--begin::Form-->
					<form class="kt-form kt-form--label-right" action="{{ url('sites/home/store') }}" id="form" method="post" enctype="multipart/form-data">
						{{ csrf_field() }}
						<input type="hidden" name="id" value="{{ $row->id }}">
						<div class="kt-portlet__body">
							<div class="form-group row">
								<label class="col-form-label col-lg-3 col-sm-12">Title *</label>
								<div class="col-lg-4 col-md-9 col-sm-12">
									<input type="text" class="form-control" name="title" placeholder="Title" required="required" value="{{ $row->title }}">
								</div>
							</div>
							{{-- begin::DummyForm --}}
							<div class="kt-form__seperator kt-form__seperator--dashed kt-form__seperator--space"></div>
							<div class="form-group row">
									<div class="col-lg-9 col-md-9 col-sm-12">
										<form>
										</form>
									</div>
								</div>							
							{{-- end::DummyForm --}}
							<!-- begin::Front_Image_Dropzone -->
							<div class="form-group row">
								<label class="col-form-label col-lg-3 col-sm-12">Display Picture</label>
								<div class="col-lg-4 col-md-9 col-sm-12">
										<form method="post" action="{{url('sites/home/front_images')}}" enctype="multipart/form-data" class="dropzone" id="front_images_dropzone">@csrf
											<input type="hidden" name="id" id="deleteId" value="{{ $row->id }}">
										</form>
								</div>
							</div>
							<!-- end::Front_Dropzone -->							
							<div class="kt-form__seperator kt-form__seperator--dashed kt-form__seperator--space"></div>
							<div class="form-group row">
								<label class="col-form-label col-lg-3 col-sm-12">Intro *</label>
								<div class="col-lg-7 col-md-9 col-sm-12">
									<textarea name="intro" class="form-control" data-provide="markdown" rows="5" required="required">{{ $row->intro }}</textarea>
								</div>
							</div>
							<div class="kt-form__seperator kt-form__seperator--dashed kt-form__seperator--space"></div>
							<div class="form-group row">
								<label class="col-form-label col-lg-3 col-sm-12">Description *</label>
								<div class="col-lg-7 col-md-9 col-sm-12">
									<textarea name="description" class="form-control" data-provide="markdown" rows="5" required="required">{{ $row->description }}</textarea>
								</div>
							</div>

{{-- 							<div class="kt-form__seperator kt-form__seperator--dashed kt-form__seperator--space"></div>

							<div class="form-group row">				
								<label class="col-form-label col-lg-3 col-sm-12">Is Publish ? *</label>
								<div class="col-lg-4 col-md-9 col-sm-12">
									<select class="form-control kt-bootstrap-select" name="is_publish" required="required">
										<option value="1" {{ ( $row->is_publish == 1 ) ? 'selected' : '' }}>Publish</option>
										<option value="0" {{ ( $row->is_publish == 0 ) ? 'selected' : '' }}>Un Publish</option>
									</select>
								</div>
							</div> --}}

				</form>
				<!--end::Form-->

				{{-- begin::Store_Image --}}
							<div class="form-group row">
								<label class="col-form-label col-lg-3 col-sm-12">Images</label>
								<div class="col-lg-7 col-md-9 col-sm-12">
										<form method="post" action="{{url('sites/home/stores_images')}}" enctype="multipart/form-data" class="dropzone" id="store_images_dropzone">@csrf
											<input type="hidden" name="id" id="deleteId" value="{{ $row->id }}">
										</form>
								</div>
							</div>
					</div>
				{{-- end::Store_Image --}}					
	
					<div class="kt-form__seperator kt-form__seperator--dashed kt-form__seperator--space"></div>
						<div class="kt-portlet__foot">
							<div class=" ">
								<div class="row">
									<div class="col-lg-9 ml-lg-auto">
										<button type="submit" class="btn btn-success submitForm" value="update">Submit</button>
										<a href="{{ url('sites/home') }}" class="btn btn-secondary">Cancel</a>
									</div>
								</div>
							</div>
						</div>
					@endforeach
				</div>
				<!--end::Portlet-->
			</div>
		</div>
	</div>

	<!--End::Row-->
</div>

<!--End::Row-->

@stop

@section('js_page')
<script src="{{ URL::asset('assets/js/demo1/pages/crud/forms/validation/form-widgets.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/js/demo1/pages/crud/forms/widgets/dropzone.js') }}" type="text/javascript"></script>
<script type="text/javascript">

		/**
		 * [front_images description]
		 * For Uploading New Image and Also Removing Existing File
		 * @type {object function}
		 */
		var RemovedFrontImage = null;
		Dropzone.autoDiscover = false;
		var front_image = new Dropzone("#front_images_dropzone", 
		{		
			maxFiles: 1,
			removedfile: function(file) {
				RemovedFrontImage = file.name;
				$(document).find(file.previewElement).remove();
				console.log('Dropzone.instance.removedfile() :', 'File Removed');
			},
			init: function() {			
				this.hiddenFileInput.removeAttribute('multiple');
		        var myDropzone = this;
				var mockfiles = {!! $front_image !!};
				var existName = null;
				$.each(mockfiles, function( index, mockFile ) {
					if (mockFile.name != null){
						existName = mockFile.name;
						myDropzone.emit("addedfile", mockFile);
					myDropzone.on('addedfile', function(files) {
					$(document).find(mockFile.previewElement).remove();
				    // if (myDropzone.files.length > 1) {
				    //   myDropzone.removeFile(myDropzone.files[0]);
				    // } 
				  });		
					}

					myDropzone.createThumbnailFromUrl(mockFile,
				    myDropzone.options.thumbnailWidth, 
				    myDropzone.options.thumbnailHeight,
				    myDropzone.options.thumbnailMethod, true, function (thumbnail) 
				        {
				            myDropzone.emit('thumbnail', mockFile, thumbnail);
				        });				 
					myDropzone.emit("complete", mockFile);
				});
				console.log('Dropzone init() :', 'On InitFunction Front Image');
			}
		});	
	
		/**
		 * [store_images description]
		 * For Uploading New Image and Also Removing Existing File
		 * @type {object function}
		 */
		var RemovedFileQueue = [];
		var counter = 0;

		Dropzone.autoDiscover = false;
		var store_images = new Dropzone("#store_images_dropzone", 
		{		
			
			maxFiles: 6,
			removedfile: function(file) {
				counter++;
				RemovedFileQueue[counter] = file.name;
				$(document).find(file.previewElement).remove();			    
			},
			init: function() {
				this.hiddenFileInput.removeAttribute('multiple');				
		        var myDropzone = this;
				var mockfiles = {!! $stores_images !!};
				console.log('Dropzone Load mockfiles :', mockfiles);
				$.each(mockfiles, function( index, mockFile ) {
					myDropzone.emit("addedfile", mockFile);
					myDropzone.createThumbnailFromUrl(mockFile,
				    myDropzone.options.thumbnailWidth, 
				    myDropzone.options.thumbnailHeight,
				    myDropzone.options.thumbnailMethod, true, function (thumbnail) 
				        {
				            myDropzone.emit('thumbnail', mockFile, thumbnail);
				        });				 
					myDropzone.emit("complete", mockFile);
				});
				console.log('Dropzone init() :', 'On InitFunction Stores Image');
			}
		});	


$('.submitForm').click(function() {
	console.log('processQueue() :', 'Uploading Image from Queued');
	store_images.processQueue();


	// Check If RemovedFileQueue != null
	if (RemovedFileQueue != '' || RemovedFileQueue.length) {
		// alert(RemovedFileQueue);
		console.log('Processing RemovedFileQueue :', RemovedFileQueue);
		var id = $("#deleteId").val();
		
		var formData = new FormData();
		formData.append("_token", "{{ csrf_token() }}");
		// destroy stores_image
	    $.ajax({
	           type: "POST",
	           url: "/sites/home/destroy/image",
			   headers: {
			 		'X-CSRF-TOKEN': "{{ csrf_token() }}"
			   },
			   data: {
			   	'id' 		: id,
			   	'filename' 	: RemovedFileQueue 
			   },		           	
	           success: function(data) {
	           	console.log('Deleted Status : ', data);
	           }
	    });		
	}

    $.ajax({
           type: "POST",
           url: "/sites/home/store",
           data: $('#form').serialize(), // changed
           success: function(data) {
           	front_image.processQueue();
			window.location.replace("/sites/home");
				console.log('Success : ', data);
           }
    });
    return false; // avoid to execute the actual form submission.
});		

</script>
@stop