@extends('layouts.admin')

@section('content')
<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
			<a href="{{ url('sites/home') }}">{!! config('sites.name') !!}</a> </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="/dashboard" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">List</span>
            </div>

        </div>
    </div>
</div>
<!-- end:: Subheader -->
<!-- begin:: Content -->
<div class="kt-container  kt-grid__item kt-grid__item--fluid">

    <!--Begin::Row-->
    <div class="row">
        <div class="col-xl-12 col-lg-12 order-lg-1 order-xl-1">
            <!-- begin .flash-message -->
            <div class="flash-message">
                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))
                <div class="alert alert-outline-{{ $msg }} fade show" role="alert">
                    <div class="alert-icon"><i class="flaticon-warning"></i></div>
                    <div class="alert-text">{{ Session::get('alert-' . $msg) }}</div>
                    <div class="alert-close">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true"><i class="la la-close"></i></span>
                        </button>
                    </div>
                </div>
                @endif
                @endforeach
            </div> 
            <!-- end .flash-message -->

            <div class="kt-portlet kt-portlet--mobile">
                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="kt-font-brand flaticon2-line-chart"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            List of Page - Home
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                            <div class="kt-portlet__head-actions">
                                <a href="{{ url('sites/home/create') }}" class="btn btn-brand btn-elevate btn-icon-sm">
                                    <i class="la la-plus"></i>
                                    New Record
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">

                    <!--begin: Datatable -->
                    <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($home as $row)
                            <tr>
                                <td>{{ $row->title }}</td>                              
                                <td>

                                        <div class="dropdown">
                                        <button class="btn btn-flat dropdown-toggle {{ ( $row->is_publish == 1 ) ? 'btn-label-brand' : 'btn-label-danger' }}" type="button" id="dropdownMenu1" name="action" data-toggle="dropdown">
                                          @if($row->is_publish == 1)
                                          Published
                                          @else
                                          Un Publish
                                          @endif
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                           <li role="presentation"><a href="#" role="menuitem" tabindex="-1" onclick="add({{ $row->id }}, 1);event.preventDefault();">Publish</a></li>
                                          <li role="presentation" ><a href="#" role="menuitem" tabindex="-1" onclick="add({{ $row->id }}, 0);event.preventDefault();">Un Publish</a></li>
                                        </ul>
                                        </div> 

                                </td>
                                <td nowrap>
                                    <a href="{{ url('sites/home/edit/'.$row->id.'/') }}" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit `{{$row->name}}`">
                                        <i class="la la-edit"></i>
                                    </a>|
                                    <a href="{{ url('sites/home/destroy/'.$row->id.'/') }}" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Delete `{{$row->name}}`">
                                        <i class="la la-trash"></i>
                                    </a>
                                    <input type="hidden" id="getId" value="{{ $row->id }}">
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <!--end: Datatable -->
                </div>
            </div>
        </div>
    </div>

    <!--End::Row-->
</div>

<!--End::Row-->

@stop

@section('js_page')
<!-- <script src="{{ mix('js/peserta.js') }}"></script> -->
<script type="text/javascript">
    "use strict";
    var KTDatatablesBasicBasic = function() {

        var initTable1 = function() {
            var table = $('#kt_table_1');

            // begin first table
            table.DataTable({
                responsive: true,

                paging: true,
                // DOM Layout settings
                dom: `<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,

                lengthMenu: [5, 10, 25, 50],

                pageLength: 10,

                // Order settings
                order: [
                    [1, 'desc']
                ],
            });

            table.on('change', '.kt-group-checkable', function() {
                var set = $(this).closest('table').find('td:first-child .kt-checkable');
                var checked = $(this).is(':checked');

                $(set).each(function() {
                    if (checked) {
                        $(this).prop('checked', true);
                        $(this).closest('tr').addClass('active');
                    } else {
                        $(this).prop('checked', false);
                        $(this).closest('tr').removeClass('active');
                    }
                });
            });

            table.on('change', 'tbody tr .kt-checkbox', function() {
                $(this).parents('tr').toggleClass('active');
            });
        };

        return {

            //main function to initiate the module
            init: function() {
                initTable1();
            },

        };

    }();

    jQuery(document).ready(function() {
        KTDatatablesBasicBasic.init();
    });

function add(id, code)
{
    
    console.log( 'id is : ' + id );
    console.log( 'is_publish is : ' + code );

    var formData = new FormData();
    formData.append("_token", "{{ csrf_token() }}");

    $.ajax({
           type: "POST",
           url: "/sites/home/is_publish",
           headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
           },
           data: {
                'id'            : id,
                'is_publish'    : code 
           },           
           success: function(data) {
            window.location.replace("/sites/home");
                console.log('Info : ', data.message);
           }
    });

}

$('.submitPublish').click(function() {
});

</script>
@stop