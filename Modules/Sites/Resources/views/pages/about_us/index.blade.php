@extends('layouts.admin')

@section('content')
<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-container ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">
				<a href="{{ url('sites/about_us') }}">About Us</a> </h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="/dashboard" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Edit</span>
			</div>
		</div>
	</div>
</div>
<!-- end:: Subheader -->
<!-- begin:: Content -->
<div class="kt-container  kt-grid__item kt-grid__item--fluid">

	<!--Begin::Row-->
	<div class="row">
		<div class="col-xl-12 col-lg-12 order-lg-1 order-xl-1">
            <!-- begin .flash-message -->
            <div class="flash-message">
                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))
                <div class="alert alert-outline-{{ $msg }} fade show" role="alert">
                    <div class="alert-icon"><i class="flaticon-warning"></i></div>
                    <div class="alert-text">{{ Session::get('alert-' . $msg) }}</div>
                    <div class="alert-close">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true"><i class="la la-close"></i></span>
                        </button>
                    </div>
                </div>
                @endif
                @endforeach
            </div> 
            <!-- end .flash-message -->			
			<!-- begin:: Content -->
			<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

				<!--begin::Portlet-->
				<div class="kt-portlet">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title">
								Edit About Us
							</h3>
						</div>
					</div>
					
					@foreach($about_us as $row)
					<!--begin::Form-->
					<form class="kt-form kt-form--label-right" action="" id="form" method="POST" >
						{{ csrf_field() }}
						<input type="hidden" name="id" value="{{ $row->id }}">
						<div class="kt-portlet__body">

							<div class="kt-form__seperator kt-form__seperator--dashed kt-form__seperator--space"></div>
							<div class="form-group row">
								<label class="col-form-label col-lg-3 col-sm-12">Section One *</label>
								<div class="col-lg-7 col-md-9 col-sm-12">
									<textarea name="section_one" class="form-control" data-provide="markdown" rows="5" required="required">{{ $row->section_one }}</textarea>		
								</div>
							</div>

							<div class="kt-form__seperator kt-form__seperator--dashed kt-form__seperator--space"></div>
							<div class="form-group row">
								<label class="col-form-label col-lg-3 col-sm-12">Section Two *</label>
								<div class="col-lg-7 col-md-9 col-sm-12">
									<textarea name="section_two" class="form-control" data-provide="markdown" rows="5" required="required">{{ $row->section_two }}</textarea>		
								</div>
							</div>

							<div class="kt-form__seperator kt-form__seperator--dashed kt-form__seperator--space"></div>
							<div class="form-group row">
								<label class="col-form-label col-lg-3 col-sm-12">Section Three *</label>
								<div class="col-lg-7 col-md-9 col-sm-12">
									<textarea name="section_three" class="form-control" data-provide="markdown" rows="5" required="required">{{ $row->section_three }}</textarea>		
								</div>
							</div>

							<div class="kt-form__seperator kt-form__seperator--dashed kt-form__seperator--space"></div>
							<div class="form-group row">
								<label class="col-form-label col-lg-3 col-sm-12">Section Four *</label>
								<div class="col-lg-7 col-md-9 col-sm-12">
									<textarea name="section_four" class="form-control" data-provide="markdown" rows="5" required="required">{{ $row->section_four }}</textarea>		
								</div>
							</div>

							{{-- begin::DummyForm --}}
							<div class="kt-form__seperator kt-form__seperator--dashed kt-form__seperator--space"></div>
							<div class="form-group row">
									<div class="col-lg-9 col-md-9 col-sm-12">
										<form>
										</form>
									</div>
								</div>							
							{{-- end::DummyForm --}}

							<!-- begin::Image_Dropzone -->
							<div class="form-group row">
								<label class="col-form-label col-lg-3 col-sm-12"></label>

								<div class="col-lg-2 col-md-9 col-sm-12">
									<label>Large Image *</label>
										<form method="post" action="{{url('sites/about_us/front_images_large')}}" enctype="multipart/form-data" class="dropzone" id="front_large_images_dropzone">@csrf
											<input type="hidden" name="id" id="deleteId" value="{{ $row->id }}">
										</form>
								</div>

								<div class="col-lg-2 col-md-9 col-sm-12">
									<label>Medium Image *</label>
										<form method="post" action="{{url('sites/about_us/front_images_medium')}}" enctype="multipart/form-data" class="dropzone" id="front_medium_images_dropzone">@csrf
											<input type="hidden" name="id" id="deleteId" value="{{ $row->id }}">
										</form>
								</div>

								<div class="col-lg-2 col-md-9 col-sm-12">
									<label>Small Image *</label>
										<form method="post" action="{{url('sites/about_us/front_images_small')}}" enctype="multipart/form-data" class="dropzone" id="front_small_images_dropzone">@csrf
											<input type="hidden" name="id" id="deleteId" value="{{ $row->id }}">
										</form>
								</div>

							</div>																										
				</form>
				<!--end::Form-->			
	
					<div class="kt-form__seperator kt-form__seperator--dashed kt-form__seperator--space"></div>
						<div class="kt-portlet__foot">
							<div class=" ">
								<div class="row">
									<div class="col-lg-9 ml-lg-auto">
										<button type="submit" class="btn btn-success submitForm" value="update">Submit</button>
										<a href="{{ url('sites/about_us') }}" class="btn btn-secondary">Cancel</a>
									</div>
								</div>
							</div>
						</div>
					@endforeach
				</div>
				<!--end::Portlet-->
			</div>
		</div>
	</div>

	<!--End::Row-->
</div>

<!--End::Row-->

@stop

@section('js_page')
<script src="{{ URL::asset('assets/js/demo1/pages/crud/forms/validation/form-widgets.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/js/demo1/pages/crud/forms/widgets/dropzone.js') }}" type="text/javascript"></script>
<script type="text/javascript">

		/**
		 * [front_images description]
		 * For Uploading New Image and Also Removing Existing File
		 * @type {object function}
		 */
		var RemovedLargeImage = null;
		Dropzone.autoDiscover = false;
		var front_image_large = new Dropzone("#front_large_images_dropzone", 
		{		
			maxFiles: 1,
			removedfile: function(file) {
				RemovedLargeImage = file.name;
				$(document).find(file.previewElement).remove();
				console.log('Dropzone.instance.removedfile() :', 'File Removed');
			},
			init: function() {			
				this.hiddenFileInput.removeAttribute('multiple');
		        var myDropzone = this;
				var mockfiles = {!! $front_image_large !!};
				var existName = null;
				$.each(mockfiles, function( index, mockFile ) {
					if (mockFile.name != null){
						existName = mockFile.name;
						myDropzone.emit("addedfile", mockFile);
					myDropzone.on('addedfile', function(files) {
					$(document).find(mockFile.previewElement).remove();
				  });		
					}

					myDropzone.createThumbnailFromUrl(mockFile,
				    myDropzone.options.thumbnailWidth, 
				    myDropzone.options.thumbnailHeight,
				    myDropzone.options.thumbnailMethod, true, function (thumbnail) 
				        {
				            myDropzone.emit('thumbnail', mockFile, thumbnail);
				        });				 
					myDropzone.emit("complete", mockFile);
				});
				console.log('Dropzone init() :', 'On InitFunction Large Image');
			}
		});	

		/**
		 * [front_images description]
		 * For Uploading New Image and Also Removing Existing File
		 * @type {object function}
		 */
		var RemovedMediumImage = null;
		Dropzone.autoDiscover = false;
		var front_image_medium = new Dropzone("#front_medium_images_dropzone", 
		{		
			maxFiles: 1,
			removedfile: function(file) {
				RemovedMediumImage = file.name;
				$(document).find(file.previewElement).remove();
				console.log('Dropzone.instance.removedfile() :', 'File Removed');
			},
			init: function() {			
				this.hiddenFileInput.removeAttribute('multiple');
		        var myDropzone = this;
				var mockfiles = {!! $front_image_medium !!};
				var existName = null;
				$.each(mockfiles, function( index, mockFile ) {
					if (mockFile.name != null){
						existName = mockFile.name;
						myDropzone.emit("addedfile", mockFile);
					myDropzone.on('addedfile', function(files) {
					$(document).find(mockFile.previewElement).remove();
				  });		
					}

					myDropzone.createThumbnailFromUrl(mockFile,
				    myDropzone.options.thumbnailWidth, 
				    myDropzone.options.thumbnailHeight,
				    myDropzone.options.thumbnailMethod, true, function (thumbnail) 
				        {
				            myDropzone.emit('thumbnail', mockFile, thumbnail);
				        });				 
					myDropzone.emit("complete", mockFile);
				});
				console.log('Dropzone init() :', 'On InitFunction Medium Image');
			}
		});		

		/**
		 * [front_images description]
		 * For Uploading New Image and Also Removing Existing File
		 * @type {object function}
		 */
		var RemovedSmallImage = null;
		Dropzone.autoDiscover = false;
		var front_image_small = new Dropzone("#front_small_images_dropzone", 
		{		
			maxFiles: 1,
			removedfile: function(file) {
				RemovedSmallImage = file.name;
				$(document).find(file.previewElement).remove();
				console.log('Dropzone.instance.removedfile() :', 'File Removed');
			},
			init: function() {			
				this.hiddenFileInput.removeAttribute('multiple');
		        var myDropzone = this;
				var mockfiles = {!! $front_image_small !!};
				var existName = null;
				$.each(mockfiles, function( index, mockFile ) {
					if (mockFile.name != null){
						existName = mockFile.name;
						myDropzone.emit("addedfile", mockFile);
					myDropzone.on('addedfile', function(files) {
					$(document).find(mockFile.previewElement).remove();
				  });		
					}

					myDropzone.createThumbnailFromUrl(mockFile,
				    myDropzone.options.thumbnailWidth, 
				    myDropzone.options.thumbnailHeight,
				    myDropzone.options.thumbnailMethod, true, function (thumbnail) 
				        {
				            myDropzone.emit('thumbnail', mockFile, thumbnail);
				        });				 
					myDropzone.emit("complete", mockFile);
				});
				console.log('Dropzone init() :', 'On InitFunction Small Image');
			}
		});				

$('.submitForm').click(function() {
    $.ajax({
           type: "POST",
           url: "{{ url('sites/about_us/store') }}",
           data: $('#form').serialize(), // changed	
           success: function(data) {
           	front_image_large.processQueue();
           	front_image_medium.processQueue();
           	front_image_small.processQueue();
			window.location.replace("{{ url('sites/about_us') }}");
				console.log('Success : ', data);
           }
    });
    return false; // avoid to execute the actual form submission.
});

</script>
@stop