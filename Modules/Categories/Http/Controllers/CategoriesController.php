<?php

namespace Modules\Categories\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
// use Illuminate\Routing\Controller;
use App\Http\Controllers\Controller;
use App\PrincipalsCategory;
use Illuminate\Support\Facades\DB;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        // return view('categories::index');
        return view('categories::index', [
            'categories' => PrincipalsCategory::all()
            ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('categories::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
		$this->validate($request, [
            'code' => 'required',            
            'name' => 'required',
            'description' => 'required',
			'is_eligible' => 'required',
			'logo' => 'required|file|image|mimes:jpeg,png,jpg|max:2048',
        ]);

        $name = null;

        $file = $request->file('logo');
        if(!empty($file)){
            $name = md5(microtime()).'_'.$file->getClientOriginalName();
            $file->move(public_path('/public/img/principals/icons/'), $name);
        }

        if(!empty($name)){
            if(!empty($request->id)){
                DB::table('principals_categories')->where('id',$request->id)->update([
                    'code' => $request->code,
                    'name' => $request->name,
                    'description' => $request->description,
                    'is_eligible' => $request->is_eligible,
                    'logo' => $name
                ]);
                $request->session()->flash('alert-success', 'Service `'.$request->name.'` was successful updated!');
            }else{
                DB::table('principals_categories')->insert([
                    'code' => $request->code,
                    'name' => $request->name,
                    'description' => $request->description,
                    'is_eligible' => $request->is_eligible,
                    'logo' => $name
                ]);
                $request->session()->flash('alert-success', 'Service `'.$request->name.'` was successful added!');
            }
        }
        return redirect('/categories');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('categories::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $categories = DB::table('principals_categories')->where('id',$id)->get();

        return view('categories::edit',['principals_categories' => $categories, 'avaibility_list' => ['in_scope' => 'In Scope', 'out_scope' => 'Out Scope']]);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        DB::table('principals_categories')->where('id',$id)->delete();

        return redirect('/categories')->with('alert-success','Category id.'.$id.' successful deleted!');
    }
}
