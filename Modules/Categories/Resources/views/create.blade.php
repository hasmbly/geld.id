@extends('layouts.admin')

@section('content')
<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-container ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">
			<a href="/categories">{!! config('categories.name') !!}</a> </h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="/dashboard" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<!-- <a href="" class="kt-subheader__breadcrumbs-link">
                    Dashboards </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">
                    Default Dashboard </a> -->
				<span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">New</span>
			</div>

		</div>
	</div>
</div>
<!-- end:: Subheader -->
<!-- begin:: Content -->
<div class="kt-container  kt-grid__item kt-grid__item--fluid">

	<!--Begin::Row-->
	<div class="row">
		<div class="col-xl-12 col-lg-12 order-lg-1 order-xl-1">
			<!-- begin:: Content -->
			<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

				<!--begin::Portlet-->
				<div class="kt-portlet">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title">
								New Category
							</h3>
						</div>
					</div>

					<!--begin::Form-->
					<form class="kt-form kt-form--label-right" action="/categories/store" method="post"  enctype="multipart/form-data">
					{{ csrf_field() }}
						<div class="kt-portlet__body">
						<div class="form-group row">
								<label class="col-form-label col-lg-3 col-sm-12">Code *</label>
								<div class="col-lg-4 col-md-9 col-sm-12">
									<input type="text" class="form-control" name="code" placeholder="Unique category code" required="required">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-form-label col-lg-3 col-sm-12">Name *</label>
								<div class="col-lg-4 col-md-9 col-sm-12">
									<input type="text" class="form-control" name="name" placeholder="Name of category" required="required">
								</div>
							</div>
							<div class="kt-form__seperator kt-form__seperator--dashed kt-form__seperator--space"></div>
							<div class="form-group row">
								<label class="col-form-label col-lg-3 col-sm-12">Is Eligible ? *</label>
								<div class="col-lg-4 col-md-9 col-sm-12">
									<select class="form-control kt-bootstrap-select" name="is_eligible" required="required">
										<option value="in_scope">In Scope</option>
										<option value="out_scope">Out Scope</option>
									</select>
								</div>
							</div>
							<div class="kt-form__seperator kt-form__seperator--dashed kt-form__seperator--space"></div>
							<div class="form-group row">
								<label class="col-form-label col-lg-3 col-sm-12">Description *</label>
								<div class="col-lg-7 col-md-9 col-sm-12">
									<textarea name="description" class="form-control" data-provide="markdown" rows="5" required="required"></textarea>
								</div>
							</div>
							<div class="kt-form__seperator kt-form__seperator--dashed kt-form__seperator--space"></div>
							<div class="form-group row">
								<label class="col-form-label col-lg-3 col-sm-12">Upload logo *</label>
								<div class="col-lg-7 col-md-9 col-sm-12">
									<div></div>
									<div class="custom-file">
										<input type="file" class="custom-file-input" id="customFile" name="logo">
										<label class="custom-file-label" for="customFile">Choose file</label>
									</div>
								</div>
							</div>
						</div>
						<div class="kt-portlet__foot">
							<div class=" ">
								<div class="row">
									<div class="col-lg-9 ml-lg-auto">
										<button type="submit" class="btn btn-success" value="submit">Submit</button>
										<a href="/categories" class="btn btn-secondary">Cancel</a>
									</div>
								</div>
							</div>
						</div>
					</form>

					<!--end::Form-->
				</div>

				<!--end::Portlet-->
			</div>
		</div>
	</div>

	<!--End::Row-->
</div>

<!--End::Row-->

@stop

@section('js_page')
<script src="./assets/js/demo1/pages/crud/forms/validation/form-widgets.js" type="text/javascript"></script>
@stop