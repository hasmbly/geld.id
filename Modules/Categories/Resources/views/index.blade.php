@extends('layouts.admin')

@section('content')
<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-container ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">
				{!! config('categories.name') !!} </h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<!-- <a href="" class="kt-subheader__breadcrumbs-link">
                    Dashboards </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">
                    Default Dashboard </a> -->
				<span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">List</span>
			</div>

		</div>
	</div>
</div>
<!-- end:: Subheader -->
<!-- begin:: Content -->
<div class="kt-container  kt-grid__item kt-grid__item--fluid">

	<!--Begin::Row-->
	<div class="row">
		<div class="col-xl-12 col-lg-12 order-lg-1 order-xl-1">
			<div class="flash-message">
				@foreach (['danger', 'warning', 'success', 'info'] as $msg)
				@if(Session::has('alert-' . $msg))
				<div class="alert alert-outline-{{ $msg }} fade show" role="alert">
					<div class="alert-icon"><i class="flaticon-warning"></i></div>
					<div class="alert-text">{{ Session::get('alert-' . $msg) }}</div>
					<div class="alert-close">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true"><i class="la la-close"></i></span>
						</button>
					</div>
				</div>
				@endif
				@endforeach
			</div> <!-- end .flash-message -->
			<div class="kt-portlet kt-portlet--mobile">
				<div class="kt-portlet__head kt-portlet__head--lg">
					<div class="kt-portlet__head-label">
						<span class="kt-portlet__head-icon">
							<i class="kt-font-brand flaticon2-line-chart"></i>
						</span>
						<h3 class="kt-portlet__head-title">
							List of principal categories
						</h3>
					</div>
					<div class="kt-portlet__head-toolbar">
						<div class="kt-portlet__head-wrapper">
							<div class="kt-portlet__head-actions">
								<div class="dropdown dropdown-inline">
									<button type="button" class="btn btn-default btn-icon-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<i class="la la-download"></i> Export
									</button>
									<div class="dropdown-menu dropdown-menu-right">
										<ul class="kt-nav">
											<li class="kt-nav__section kt-nav__section--first">
												<span class="kt-nav__section-text">Choose an option</span>
											</li>
											<li class="kt-nav__item">
												<a href="#" class="kt-nav__link">
													<i class="kt-nav__link-icon la la-file-excel-o"></i>
													<span class="kt-nav__link-text">Excel</span>
												</a>
											</li>
											<li class="kt-nav__item">
												<a href="#" class="kt-nav__link">
													<i class="kt-nav__link-icon la la-file-pdf-o"></i>
													<span class="kt-nav__link-text">PDF</span>
												</a>
											</li>
										</ul>
									</div>
								</div>
								&nbsp;
								<a href="/categories/create" class="btn btn-brand btn-elevate btn-icon-sm">
									<i class="la la-plus"></i>
									New Record
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="kt-portlet__body">

					<!--begin: Datatable -->
					<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
						<thead>
							<tr>
								<th>Code</th>
								<th>Logo</th>
								<th>Name</th>
								<th>Description</th>
								<th>Is eligible?</th>
								<!-- <th>Status</th> -->
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($categories as $category)
							<tr>
								<td><strong>{{ $category->code }}</strong></td>
								<td><img class="img-fluid border-radius-0" src="./public/img/principals/icons/{{ $category->logo }}" width="24"> </td>
								<td>{{ $category->name }}</td>
								<td>{{ $category->description }}</td>
								<td>{{ $category->is_eligible }}</td>
								<!-- <td>{{ $category->status }}</td> -->
								<td nowrap>
									<a href="/categories/edit/{{ $category->id }}" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit `{{$category->name}}`">
										<i class="la la-edit"></i>
									</a>|
									<a href="/categories/destroy/{{ $category->id }}" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Delete `{{$category->name}}`">
										<i class="la la-trash"></i>
									</a>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
					<!--end: Datatable -->
				</div>
			</div>
		</div>
	</div>

	<!--End::Row-->
</div>

<!--End::Row-->

@stop

@section('js_page')
<!-- <script src="{{ mix('js/peserta.js') }}"></script> -->
<script type="text/javascript">
	"use strict";
	var KTDatatablesBasicBasic = function() {

		var initTable1 = function() {
			var table = $('#kt_table_1');

			// begin first table
			table.DataTable({
				responsive: true,

				paging: true,
				// DOM Layout settings
				dom: `<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,

				lengthMenu: [5, 10, 25, 50],

				pageLength: 10,

				language: {
					'lengthMenu': 'Display _MENU_',
				},

				// Order settings
				order: [
					[1, 'desc']
				],

			});

			table.on('change', '.kt-group-checkable', function() {
				var set = $(this).closest('table').find('td:first-child .kt-checkable');
				var checked = $(this).is(':checked');

				$(set).each(function() {
					if (checked) {
						$(this).prop('checked', true);
						$(this).closest('tr').addClass('active');
					} else {
						$(this).prop('checked', false);
						$(this).closest('tr').removeClass('active');
					}
				});
			});

			table.on('change', 'tbody tr .kt-checkbox', function() {
				$(this).parents('tr').toggleClass('active');
			});
		};

		return {

			//main function to initiate the module
			init: function() {
				initTable1();
			},

		};

	}();

	jQuery(document).ready(function() {
		KTDatatablesBasicBasic.init();
	});
</script>
@stop