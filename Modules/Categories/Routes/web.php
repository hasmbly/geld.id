<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('categories')->group(function() {
    Route::get('/', 'CategoriesController@index');
    Route::get('/create', 'CategoriesController@create');
    Route::post('/store', 'CategoriesController@store');
    Route::get('/destroy/{id}', 'CategoriesController@destroy');
    Route::get('/edit/{id}', 'CategoriesController@edit');
});
