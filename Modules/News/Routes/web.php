<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('news')->group(function() {
    Route::get('/', 'NewsController@index');
    Route::post('/store', 'NewsController@store');
    Route::get('/create', 'NewsController@create');
    Route::get('/destroy/{id}', 'NewsController@destroy');
    Route::get('/edit/{id}', 'NewsController@edit');
});
