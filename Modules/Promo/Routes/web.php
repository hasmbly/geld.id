<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('promo')->group(function() {

    Route::get('/', 'PromoController@index');

    Route::get('/create', 'PromoController@create');

    Route::post('/store', 'PromoController@store');

    Route::post('/stores_images', 'PromoController@stores_images'); // new/update stores_images
    Route::post('/front_images', 'PromoController@front_image'); // new/update front_images

    Route::get('/destroy/{id}', 'PromoController@destroy');
    Route::post('/destroy/image', 'PromoController@destroy_image'); // destroy stores_images

    Route::get('/edit/{id}', 'PromoController@edit');

});