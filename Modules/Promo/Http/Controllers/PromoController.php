<?php

namespace Modules\Promo\Http\Controllers;

use App\Promo;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class PromoController extends Controller
{   
    private $dir_img_stores = '/public/img/promo/'; 

    /**
     * Display a listing of the resource.
     * @return Response
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        return view('promo::index', [
            'promo' => Promo::all()
            ]);        
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {

        return view('promo::create');
    }

    public function stores_images(Request $request)
    {   


        $file   = $request->file('file');
        $id     = $request->id;

        /* Validator */
          $rules = array(
              'file'             => 'required',
              'id'               => 'required'
                  );    
          $messages = array(
            'file.required'         => 'Please provide your file',
            'id.required'           => 'Please provide your id'
          );

          $validator = Validator::make( $request->all(), $rules, $messages );

        if ($validator->fails()) {


            return response()->json([
                'error'    => true,
                'message'  => $validator->errors()->first()
            ], 422);

        }          
        
        $file->move(public_path($this->dir_img_stores), $file->getClientOriginalName());

        try {
            $InsertImages = DB::table('promo_images')->insert([
                                'promo_id'    => $id,
                                'photo'       => $file->getClientOriginalName()
            ]);

        } catch (QueryException $e) {
            return response()->json([
                'error'       => true,
                'code'        => $e->getCode(),
                'message'     => $e->getMessage()
              ], 500);
             }

         return response()->json([
            'error'     => false,
            'message'   => 'Images Uploaded',
            'id'        => $id,
            'Filename'  => $file->getClientOriginalName()
         ], 200);
    
    }

    public function front_image(Request $request)
    {
        $file       = $request->file('file');

        $id         = $request->id;

        /* Validator */

          $rules = array(
              'file'             => 'required',
              'id'               => 'required'
                  );    
          $messages = array(

            'file.required'         => 'Please provide your file',
            'id.required'           => 'Please provide your id'
          );

          $validator = Validator::make( $request->all(), $rules, $messages );

        if ($validator->fails()) {

            return response()->json([
                'error'    => true,
                'message'  => $validator->errors()->first()
            ], 422);

        }        
        
        $file->move(public_path($this->dir_img_stores), $file->getClientOriginalName());

        // $this->photo = $file->getClientOriginalName();
        try {

        DB::table('promos')
            ->where('id',$id)
            ->update([
                'img' => $file->getClientOriginalName()
            ]);            
            
        } catch (QueryException $e) {

        return response()->json([
            'error'       => true,
            'code'        => $e->getCode(),
            'message'     => $e->getMessage()
          ], 500);

         } 

         return response()->json([
            'error'     => false,
            'message'   => 'Front Image Uploaded',
            'id'        => $id,
            'Filename'  => $file->getClientOriginalName()
         ], 200);

    }     

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {


        /* Validator */
          $rules = array(
            'name'         => 'required',
            'description'   => 'required',
            'start_date'    => 'required',
            'end_date'      => 'required'
            );

          $messages = array(
            'name.required'        => 'Please provide your Name of Promo',
            'description.required'  => 'Please provide your description',
            'start_date.required'   => 'Please provide your start_date',
            'end_date.required'     => 'Please provide your end_date'
          );

          $validator = Validator::make( $request->all(), $rules, $messages );

        if ($validator->fails()) {

            return response()->json([
                'error'    => true,
                'message'  => $validator->errors()->first()
            ], 422);

        }          

            if(!empty($request->id)){

                $update = DB::table('promos')->where('id',$request->id)->update([
                    'name'          => $request->name,
                    'description'   => $request->description,
                    'start_date'    => date('Y-m-d',strtotime($request->start_date)),
                    'end_date'      => date('Y-m-d',strtotime($request->end_date))                    

                ]);

                $request->session()->flash('alert-success', 'Promo `'.$request->name.'` was successful Updated!');
            
            }else{

                $insertGetId = DB::table('promos')->insertGetId([
                                'name'          => $request->name,
                                'description'   => $request->description,
                                'start_date'    => date('Y-m-d',strtotime($request->start_date)),
                                'end_date'      => date('Y-m-d',strtotime($request->end_date)),
                            ]);

                $request->session()->flash('alert-success', 'Promo `'.$request->name.'` was successful Added!');

                 return response()->json([
                    'id'        => $insertGetId,
                    'message'   => 'Success Added'
                 ]);   

            }

                 $request->session()->flash('alert-success', 'Promo `'.$request->name.'` was successful Updated!');
          
                 return response()->json([
                    'message' => 'Success Updated'
                 ]);                            

    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('promo::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $promo   = Promo::where('id', $id)->get();
        $getId    = $promo[0]->id;

        try {        
        $stores_images = [];
        $stores_images_raw = DB::table('promo_images')->where('promo_id', $getId)->get();
        $dir = public_path($this->dir_img_stores);
        foreach ($stores_images_raw as $row) {
            
            $imgs = @exif_read_data($dir.$row->photo);
            // $imgs    = Image::make(public_path($row->photo))->exif();
            array_push(
                $stores_images,[
                'name'      => $imgs['FileName'], 
                'size'      => $imgs['FileSize'], 
                'type'      => $imgs['MimeType'], 
                'status'    => 'queued', 
                'dataURL'   => $this->dir_img_stores.$imgs['FileName']
            ]);
        }
        } catch (QueryException $e) {

            return response()->json([
                'error'       => true,
                'code'        => $e->getCode(),
                'message'     => $e->getMessage()
              ], 500);

             }         

        try {
                $front_image = [];
                $query = DB::table('promos')->where('id', $getId)->pluck('img');
                $dir = public_path($this->dir_img_stores);
                $image = @exif_read_data($dir.$query[0]);
                    array_push(
                        $front_image,[
                        'name'      => $image['FileName'], 
                        'size'      => $image['FileSize'], 
                        'type'      => $image['MimeType'], 
                        'status'    => 'queued', 
                        'dataURL'   => $this->dir_img_stores.$image['FileName']
                    ]);                 
            } catch (QueryException $e) {

                return response()->json([
                    'error'       => true,
                    'code'        => $e->getCode(),
                    'message'     => $e->getMessage()
                  ], 500);

                 } 
      

        return view('promo::edit',[
            'promo' => $promo,
            'stores_images' => json_encode($stores_images),
            'front_image' => json_encode($front_image)
            ]); 
    }


    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

  /**
     * [destroy_image description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function destroy_image(Request $request)
    {

        $file       = $request->filename;
        $id         = $request->id;

        if($file && $id){

            $getId = DB::table('promos')->where('id', $id)->pluck('id');       

            foreach ($file as $value) {

                DB::table('promo_images')
                ->where('promo_id', '=', $getId[0])
                ->where('photo', '=', $value)
                ->delete();
            }

            return response('Photo deleted', 200);
            
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $name = DB::table('promos')->where('id', $id)->pluck('name');
        $name = $name[0];
        
        DB::table('promos')->where('id',$id)->delete();


        return redirect('promo')->with('alert-success','Promo '.$name.' successful deleted!');        
    }
}
