@extends('layouts.admin')

@section('content')
<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-container ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">
				<a href="/stores">{!! config('stores.name') !!}</a> </h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="/dashboard" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				
				<span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">New</span>
			</div>

		</div>
	</div>
</div>
<!-- end:: Subheader -->
<!-- begin:: Content -->
<div class="kt-container  kt-grid__item kt-grid__item--fluid">

	<!--Begin::Row-->
	<div class="row">
		<div class="col-xl-12 col-lg-12 order-lg-1 order-xl-1">
			<!-- begin:: Content -->
			<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

				<!--begin::Portlet-->
				<div class="kt-portlet">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title">
								New Store
							</h3>
						</div>
					</div>

					<!--begin::Form-->
					<form class="kt-form kt-form--label-right dropzone1 files-container1" action="/stores/store" id="form" method="post" enctype="multipart/form-data">
						{{ csrf_field() }}
						<div class="kt-portlet__body">
						<div class="row">
							<div class="col-xl-6 col-lg-6 order-lg-1 order-xl-1">
									<div class="form-group row">
										<label class="col-form-label col-lg-3 col-sm-12">Code *</label>
										<div class="col-lg-9 col-md-9 col-sm-12">
											<input type="text" class="form-control" name="code" placeholder="Code of store" required="required">
										</div>
									</div>
									<div class="form-group row">
										<label class="col-form-label col-lg-3 col-sm-12">Name *</label>
										<div class="col-lg-9 col-md-9 col-sm-12">
											<input type="text" class="form-control" name="name" placeholder="Name of store" required="required">
										</div>
									</div>

									<div class="kt-form__seperator kt-form__seperator--dashed kt-form__seperator--space"></div>
									<div class="form-group row">
											<div class="col-lg-9 col-md-9 col-sm-12">
												<form>
												</form>
											</div>
										</div>
									<div class="kt-form__seperator kt-form__seperator--dashed kt-form__seperator--space"></div>
									<div class="form-group row">
											<label class="col-form-label col-lg-3 col-sm-12">Front Image</label>
											<div class="col-lg-9 col-md-9 col-sm-12">
												<form method="post" action="{{url('stores/front_image')}}" enctype="multipart/form-data" class="dropzone" id="front_images_dropzone">@csrf
												<div id="front_image_id"></div>
												</form>
											</div>
										</div>

									<div class="kt-form__seperator kt-form__seperator--dashed kt-form__seperator--space"></div>
									<div class="form-group row">
										<label class="col-form-label col-lg-3 col-sm-12">Building Name</label>
										<div class="col-lg-9 col-md-9 col-sm-12">
											<input type="text" class="form-control" name="building_name" placeholder="Building Name of store">
										</div>
									</div>
									<div class="kt-form__seperator kt-form__seperator--dashed kt-form__seperator--space"></div>
									<div class="form-group row">
										<label class="col-form-label col-lg-3 col-sm-12">Address</label>
										<div class="col-lg-9 col-md-9 col-sm-12">
											<textarea name="address" class="form-control" data-provide="markdown" rows="2"></textarea>
										</div>
									</div>
									<div class="kt-form__seperator kt-form__seperator--dashed kt-form__seperator--space"></div>
									<div class="form-group row">
										<label class="col-form-label col-lg-3 col-sm-12">City</label>
										<div class="col-lg-9 col-md-9 col-sm-12">
											<input type="text" class="form-control" name="city" placeholder="City name">
										</div>
									</div>
									<div class="kt-form__seperator kt-form__seperator--dashed kt-form__seperator--space"></div>
									<div class="form-group row">
										<label class="col-form-label col-lg-3 col-sm-12">Province Name</label>
										<div class="col-lg-9 col-md-9 col-sm-12">
											<input type="text" class="form-control" name="province" placeholder="Province Name of store">
										</div>
									</div>
									<div class="kt-form__seperator kt-form__seperator--dashed kt-form__seperator--space"></div>
									<div class="form-group row">
										<label class="col-form-label col-lg-3 col-sm-12">Country Name *</label>
										<div class="col-lg-9 col-md-9 col-sm-12">
											<input type="text" class="form-control" name="country" placeholder="Country Name of store">
										</div>
									</div>
									<div class="kt-form__seperator kt-form__seperator--dashed kt-form__seperator--space"></div>
									<div class="form-group row">
										<label class="col-form-label col-lg-3 col-sm-12">Postal *</label>
										<div class="col-lg-9 col-md-9 col-sm-12">
											<input type="text" class="form-control" name="postal" placeholder="Postal">
										</div>
									</div>
									<div class="kt-form__seperator kt-form__seperator--dashed kt-form__seperator--space"></div>
									<div class="form-group row">
										<label class="col-form-label col-lg-3 col-sm-12">Phone Number</label>
										<div class="col-lg-9 col-md-9 col-sm-12">
											<input type="text" class="form-control" name="phone_number" placeholder="Phone Number">
										</div>
									</div>
									<div class="kt-form__seperator kt-form__seperator--dashed kt-form__seperator--space"></div>
									<div class="form-group row">
										<label class="col-form-label col-lg-3 col-sm-12">Open Date</label>
										<div class="col-lg-9 col-md-9 col-sm-12">
											<input type="text" class="form-control" name="open_date" placeholder="Open date of store">
										</div>
									</div>
									<div class="kt-form__seperator kt-form__seperator--dashed kt-form__seperator--space"></div>
									<div class="form-group row">
										<label class="col-form-label col-lg-3 col-sm-12">Opening Date</label>
										<div class="col-lg-9 col-md-9 col-sm-12">
											<input type="text" class="form-control" name="opening_date" placeholder="Opening Date of store">
										</div>
									</div>
									<div class="kt-form__seperator kt-form__seperator--dashed kt-form__seperator--space"></div>
									<div class="form-group row">
										<label class="col-form-label col-lg-3 col-sm-12">Opening Hour</label>
										<div class="col-lg-9 col-md-9 col-sm-12">
											<input type="text" class="form-control" name="opening_hour" placeholder="Opening Hour of store">
										</div>
									</div>
								

							</div>
							<div class="col-xl-6 col-lg-6 order-lg-1 order-xl-1">
							<div class="form-group row">
									<label class="col-form-label col-lg-3 col-sm-12">Amenities</label>
									<div class="col-lg-9 col-md-9 col-sm-12">
										<select class="form-control kt-select2" id="stores_amenities" multiple name="stores_amenities[]">
											@foreach($amenities as $amenity)
											<option value="{{ $amenity->id }}">{{ $amenity->name }}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-lg-3 col-sm-12">Principals ATM</label>
									<div class="col-lg-9 col-md-9 col-sm-12">
										<select class="form-control kt-select2" id="stores_principals_banks" multiple name="stores_principals_banks[]" rows="3">
											@foreach($stores_principals_bank as $row)
											<option value="{{ $row->code }}">{{ $row->brand_name }}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-lg-3 col-sm-12">Principals Bank</label>
									<div class="col-lg-9 col-md-9 col-sm-12">
									<select class="form-control kt-select2" id="stores_principals_atms" multiple name="stores_principals_atms[]" rows="3">
											@foreach($stores_principals_bank as $row)
											<option value="{{ $row->code }}">{{ $row->brand_name }}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-lg-3 col-sm-12">Principals Other</label>
									<div class="col-lg-9 col-md-9 col-sm-12">
									<select class="form-control kt-select2" id="stores_principals_others" multiple name="stores_principals_others[]" rows="3">
											@foreach($stores_principals_other as $row)
											<option value="{{ $row->code }}">{{ $row->brand_name }}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-lg-3 col-sm-12">Services</label>
									<div class="col-lg-9 col-md-9 col-sm-12">
									<select class="form-control kt-select2" id="stores_services" multiple name="stores_services[]" rows="3">
											@foreach($stores_services as $row)
											<option value="{{ $row->id }}" data-image="./public/img/principals/logos/{{ $row->logo }}">{{ $row->name }}</option>
											@endforeach
										</select>
									</div>
								</div>

							<div class="form-group row">
									<label class="col-form-label col-lg-3 col-sm-12">Promo </label>
									<div class="col-lg-9 col-md-9 col-sm-12">
										<select class="form-control kt-select2" id="stores_promos" multiple name="stores_promos[]">
											@foreach($stores_promos as $promo)
											<option value="{{ $promo->id }}">{{ $promo->name }}</option>
											@endforeach
										</select>
									</div>
								</div>

							<div class="form-group row">
									<label class="col-form-label col-lg-3 col-sm-12">Events </label>
									<div class="col-lg-9 col-md-9 col-sm-12">
										<select class="form-control kt-select2" id="stores_events" multiple name="stores_events[]">
											@foreach($stores_events as $event)
											<option value="{{ $event->id }}">{{ $event->title }}</option>
											@endforeach
										</select>
									</div>
								</div>																
								
							</div>
						</div>
						</form>

						<div class="row">
							<div class="col-xl-6 col-lg-6 order-lg-1 order-xl-1">
							<div class="form-group row">
									<label class="col-form-label col-lg-3 col-sm-12">Store Images</label>
									<div class="col-lg-9 col-md-9 col-sm-12">
										<form method="post" action="{{url('stores/stores_images')}}" enctype="multipart/form-data" class="dropzone" id="store_images_dropzone">@csrf
										<div id="stores_image_id"></div>
										</form>
									</div>
								</div>
							</div>
						</div>	

						</div>
						
						<div class="kt-portlet__foot">
							<div class=" ">
								<div class="row">
									<div class="col-lg-9 ml-lg-auto">
										<button type="submit" class="btn btn-success submitForm" value="update">Submit</button>
										<a href="/stores" class="btn btn-secondary">Cancel</a>
									</div>
								</div>
							</div>
						</div>
					<!--end::Form-->
				</div>
				<!--end::Portlet-->
			</div>
		</div>
	</div>
	<!--End::Row-->
</div>

<!--End::Row-->

@stop

@section('js_page')
<!-- <link rel="stylesheet" href="{{ URL::asset('/css/dropzone.css') }}"> -->
<script src="{{ URL::asset('assets/js/demo1/pages/crud/forms/validation/form-widgets.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/js/demo1/pages/crud/forms/widgets/dropzone.js') }}" type="text/javascript"></script>
<script type="text/javascript">


		/**
		 * Put this on create.blade.php
		 * [front_images description]
		 * For Uploading New Image and Also Removing Existing File
		 * @type {object function}
		 */
		Dropzone.autoDiscover = false;
		var front_image = new Dropzone("#front_images_dropzone", 
		{		
			maxFiles: 1,
			removedfile: function(file) {
				$(document).find(file.previewElement).remove();			    
			},
			init: function() {			
				this.hiddenFileInput.removeAttribute('multiple');
		        var myDropzone = this;
				myDropzone.on('addedfile', function(files) {
			    if (myDropzone.files.length > 1) {
			      myDropzone.removeFile(myDropzone.files[0]);
			    } 
			  });		
				console.log('Dropzone init() :', 'On InitFunction Front_Image');
			}
		});	
	
		/**
		 * Put this on create.blade.php
		 * [store_images description]
		 * For Uploading New Image and Also Removing Existing File
		 * @type {object function}
		 */

		Dropzone.autoDiscover = false;
		var store_images = new Dropzone("#store_images_dropzone", 
		{		
			maxFiles: 6,
			removedfile: function(file) {
				$(document).find(file.previewElement).remove();			    
			},
			init: function() {
				this.hiddenFileInput.removeAttribute('multiple');		
				console.log('Dropzone init() :', 'On InitFunction store_images');
			}
		});	

var newId = null;
$('.submitForm').click(function() {

	console.log('processQueue() :', 'Uploading Image from Queued');

	var formData = new FormData();
	formData.append("_token", "{{ csrf_token() }}");

    $.ajax({
           type: "POST",
           url: "/stores/store",
           data: $('#form').serialize(), // changed
           success: function(data) {

           	newId = data.id

			var inputFrontImage = document.createElement("input");
			inputFrontImage.setAttribute("type", "hidden");
			inputFrontImage.setAttribute("name", "id");
			inputFrontImage.setAttribute("value", newId);

			var inputStoresImages = document.createElement("input");
			inputStoresImages.setAttribute("type", "hidden");
			inputStoresImages.setAttribute("name", "id");
			inputStoresImages.setAttribute("value", newId);				

			document.getElementById("front_image_id").appendChild(inputFrontImage);
			document.getElementById("stores_image_id").appendChild(inputStoresImages);

           	front_image.processQueue();
           	store_images.processQueue();         	
			window.location.replace("/stores");
				console.log('data', data);

           }
    });
    return false; // avoid to execute the actual form submission.
});		
	
	$(document).ready(function() {
		
		$('#stores_amenities').select2({
            placeholder: "Add Amenities",
            tags: true
		});
        $('#stores_principals_atms').select2({
            placeholder: "Add Principal ATM",
            tags: true
		});
        $('#stores_principals_banks').select2({
            placeholder: "Add Principal Bank",
            tags: true
		});
        $('#stores_principals_others').select2({
            placeholder: "Add Principal Other",
            tags: true
		});
        $('#stores_services').select2({
            placeholder: "Add Services",
			tags: true,
        });
		$('#stores_promos').select2({
            placeholder: "Add Promo",
            tags: true
		});    
		$('#stores_events').select2({
            placeholder: "Add Events",
            tags: true
		});  		    

		var max_fields = 10; //maximum input boxes allowed
		var wrapper = $(".input_fields_wrap"); //Fields wrapper
		var add_button = $(".add_field_button"); //Add button ID

		var x = 1; //initlal text box count
		$(add_button).click(function(e) { //on add input button click
			e.preventDefault();
			if (x < max_fields) { //max input box allowed
				x++; //text box increment
				$(wrapper).append('<div class="row" style="margin-bottom: 1rem;"><div class="col-lg-10 col-md-10 col-sm-10"><input type="text" class="form-control" name="mytext[]" placeholder="Amenities"></div><div class="col-lg-2 col-md-2 col-sm-2"><a href="#" class="remove_field">Remove</a></div></div>'); //add input box
			}
		});

		$(wrapper).on("click", ".remove_field", function(e) { //user click on remove text
			e.preventDefault();
			$(this).parent('div').parent('div').remove();
			x--;
		})
	});
</script>
@stop