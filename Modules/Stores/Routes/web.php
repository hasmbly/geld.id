<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('stores')->group(function() {
    Route::get('/', 'StoresController@index');
    Route::get('/create', 'StoresController@create');
    Route::post('/store', 'StoresController@store');
    Route::post('/stores_images', 'StoresController@stores_images');
    Route::post('/front_image', 'StoresController@front_image');
    Route::get('/destroy/{id}', 'StoresController@destroy');
    Route::post('/destroy/image', 'StoresController@destroy_image');
    Route::post('/destroy/image/front/{filename}', 'StoresController@destroy_front_image');
    Route::get('/edit/{id}', 'StoresController@edit');
});
