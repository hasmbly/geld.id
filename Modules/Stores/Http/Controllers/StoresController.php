<?php

namespace Modules\Stores\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
// use Illuminate\Routing\Controller;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;

use App\Stores;
use App\Amenities;
use App\Promo;
use App\Event;
use App\StoresAmenities;
use App\StoresImage;
use App\StoresPrincipalsAtm;
use App\StoresPrincipalsBank;
use App\StoresPrincipalsOther;
use App\StoresService;
use App\StoresPromo;
use App\StoresEvent;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class StoresController extends Controller
{
    private $dir_img_stores = '/public/img/stores/';
    private $photo = "";

    private $storeId = 0;
    private $affectedPromo = [];
    private $StoresCode = "";

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('stores::index', [
            'stores' => Stores::all(),
            ]);
        // return view('stores::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $stores_principals_bank = DB::table('principals')->where('category_code', '111BNK')->get();
        $stores_principals_other = DB::table('principals')->where('category_code', '!=', '111BNK')->get();
        $stores_services = DB::table('services')->where('status', '=', '1')->get();
        $stores_promos = DB::table('promos')->where('status', '1')->get();
        $stores_events = DB::table('events')->where('status', '1')->get();
                
        return view('stores::create', [
            'avaibility_list' => ['in_scope' => 'In Scope', 'out_scope' => 'Out Scope'], 
            'amenities' => Amenities::all(),
            'stores_principals_bank' => $stores_principals_bank,
            'stores_principals_other' => $stores_principals_other,
            'stores_services' => $stores_services,
            'stores_promos' => $stores_promos,
            'stores_events' => $stores_events

        ]);
    }

    public function stores_images(Request $request)
    {
        $file   = $request->file('file');

        $id     = $request->id;

        /* Validator */

          $rules = array(
              'file'             => 'required',
              'id'               => 'required'
                  );    
          $messages = array(

            'file.required'         => 'Please provide your file',
            'id.required'           => 'Please provide your id'
          );

          $validator = Validator::make( $request->all(), $rules, $messages );

        if ($validator->fails()) {

            return response()->json([
                'error'    => true,
                'message'  => $validator->errors()->first()
            ], 422);

        }          

        $store_code = DB::table('stores')->where('id', $id)->pluck('code');
        
        $file->move(public_path($this->dir_img_stores), $file->getClientOriginalName());

        try {

            $InsertStoresImages = DB::table('stores_images')->insert([
                                'store_code' => $store_code[0],
                                'photo' => $file->getClientOriginalName()
            ]);
        } catch (QueryException $e) {

            return response()->json([
                'error'       => true,
                'code'        => $e->getCode(),
                'message'     => $e->getMessage()
              ], 500);

             }

         return response()->json([
            'error'     => false,
            'message'   => 'Back End Image Uploaded',
            'id'        => $id,
            'Filename'  => $file->getClientOriginalName()
         ], 200);

    }

    public function front_image(Request $request)
    {
        $file       = $request->file('file');

        $id         = $request->id;

        /* Validator */

          $rules = array(
              'file'             => 'required',
              'id'               => 'required'
                  );    
          $messages = array(

            'file.required'         => 'Please provide your file',
            'id.required'           => 'Please provide your id'
          );

          $validator = Validator::make( $request->all(), $rules, $messages );

        if ($validator->fails()) {

            return response()->json([
                'error'    => true,
                'message'  => $validator->errors()->first()
            ], 422);

        }        
        
        $file->move(public_path($this->dir_img_stores), $file->getClientOriginalName());

        // $this->photo = $file->getClientOriginalName();
        try {

        DB::table('stores')
            ->where('id',$id)
            ->update([
                'photo' => $file->getClientOriginalName()
            ]);            
            
    } catch (QueryException $e) {

        return response()->json([
            'error'       => true,
            'code'        => $e->getCode(),
            'message'     => $e->getMessage()
          ], 500);

         } 

         return response()->json([
            'error'     => false,
            'message'   => 'Front Image Uploaded',
            'id'        => $id,
            'Filename'  => $file->getClientOriginalName()
         ], 200);

    } 


    public function destroy_image(Request $request)
    {

        $file       = $request->filename;
        $id         = $request->id;

        if($file && $id){

            $store_code = DB::table('stores')->where('id', $id)->pluck('code');            

            foreach ($file as $value) {

                DB::table('stores_images')
                ->where('store_code', '=', $store_code[0])
                ->where('photo', '=', $value)
                ->delete();

            }
            
            return response('Photo deleted', 200);
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $getId = $request->id;

        $this->StoresCode = $request->code;

		$this->validate($request, [
            'code' => 'required',            
            'name' => 'required',
			// 'photo' => 'required|file|image|mimes:jpeg,png,jpg|max:2048',
        ]);

            if(!empty($request->id)){
                // print_r($request->all());die;
                // save on table stores
                DB::table('stores')->where('id',$request->id)->update([
                    'code' => $request->code,
                    'name' => $request->name,
                    'building_name' => $request->building_name,
                    'address' => $request->address,
                    'city' => $request->city,
                    'province' => $request->province,
                    'postal' => $request->postal,
                    'phone_number' => $request->phone_number,
                    'open_date' => $request->open_date,
                    'opening_date' => $request->opening_date,
                    'opening_hour' => $request->opening_hour
                ]);        

                // save on table stores_amenities
                if($request->has('stores_amenities') ){
                    
                    // check for create & update
                    $affectedStoresstores_amenities = StoresAmenities::where('store_code', '=', $request->code)->get();
        
                        if(count($affectedStoresstores_amenities) != 0){ // for update if not empty

                            StoresAmenities::where('store_code', '=', $request->code)->delete();

                            foreach ($request->stores_amenities as $stores_amenities_id) {
                                DB::table('stores_amenities')->insert([
                                    'store_code'    => $request->code,
                                    'amenities_id'    => $stores_amenities_id
                                ]);
                            }
                        
                        } elseif (count($affectedStoresstores_amenities) == 0) { // for create if empty

                            foreach ($request->stores_amenities as $stores_amenities_id) { // create if empty
                                DB::table('stores_amenities')->insert([
                                    'store_code'        => $request->code,
                                    'amenities_id'    => $stores_amenities_id
                                ]);
                            }                        
                        }

                } else {
                    StoresAmenities::where('store_code', '=', $request->code)->delete();
                } 

                // save on table stores_principals_atms
                if($request->has('stores_principals_atms') ){
                    
                    // check for create & update
                    $affectedStoresstores_principals_atms = StoresPrincipalsAtm::where('store_code', '=', $request->code)->get();
        
                        if(count($affectedStoresstores_principals_atms) != 0){ // for update if not empty

                            StoresPrincipalsAtm::where('store_code', '=', $request->code)->delete();

                            foreach ($request->stores_principals_atms as $principal_code) {
                                DB::table('stores_principals_atms')->insert([
                                    'store_code'    => $request->code,
                                    'principal_code'    => $principal_code
                                ]);
                            }
                        
                        } elseif (count($affectedStoresstores_principals_atms) == 0) { // for create if empty

                            foreach ($request->stores_principals_atms as $principal_code) { // create if empty
                                DB::table('stores_principals_atms')->insert([
                                    'store_code'        => $request->code,
                                    'principal_code'    => $principal_code
                                ]);
                            }                        
                        }

                } else {
                    StoresPrincipalsAtm::where('store_code', '=', $request->code)->delete();
                }                 

                // save on table stores_principals_banks
                if($request->has('stores_principals_banks') ){
                    
                    // check for create & update
                    $affectedStoresstores_principals_banks = StoresPrincipalsBank::where('store_code', '=', $request->code)->get();
        
                        if(count($affectedStoresstores_principals_banks) != 0){ // for update if not empty

                            StoresPrincipalsBank::where('store_code', '=', $request->code)->delete();

                            foreach ($request->stores_principals_banks as $principal_code) {
                                DB::table('stores_principals_banks')->insert([
                                    'store_code'    => $request->code,
                                    'principal_code'    => $principal_code
                                ]);
                            }
                        
                        } elseif (count($affectedStoresstores_principals_banks) == 0) { // for create if empty

                            foreach ($request->stores_principals_banks as $principal_code) { // create if empty
                                DB::table('stores_principals_banks')->insert([
                                    'store_code'        => $request->code,
                                    'principal_code'    => $principal_code
                                ]);
                            }                        
                        }

                } else {
                    StoresPrincipalsBank::where('store_code', '=', $request->code)->delete();
                }                


                // save on table stores_principals_others
                if($request->has('stores_principals_others') ){
                    
                    // check for create & update
                    $affectedStoresStores_principals_others = StoresPrincipalsOther::where('store_code', '=', $request->code)->get();
        
                        if(count($affectedStoresStores_principals_others) != 0){ // for update if not empty

                            StoresPrincipalsOther::where('store_code', '=', $request->code)->delete();

                            foreach ($request->stores_principals_others as $principal_code) {
                                DB::table('stores_principals_others')->insert([
                                    'store_code'    => $request->code,
                                    'principal_code'    => $principal_code
                                ]);
                            }
                        
                        } elseif (count($affectedStoresStores_principals_others) == 0) { // for create if empty

                            foreach ($request->stores_principals_others as $principal_code) { // create if empty
                                DB::table('stores_principals_others')->insert([
                                    'store_code'        => $request->code,
                                    'principal_code'    => $principal_code
                                ]);
                            }                        
                        }

                } else {
                    StoresPrincipalsOther::where('store_code', '=', $request->code)->delete();
                }


                // save on table stores_services
                if($request->has('stores_services') ){
                    
                    // check for create & update
                    $affectedStoresStores_services = StoresService::where('store_code', '=', $request->code)->get();
        
                        if(count($affectedStoresStores_services) != 0){ // for update if not empty

                            StoresService::where('store_code', '=', $request->code)->delete();

                            foreach ($request->stores_services as $service_id) {
                                DB::table('stores_services')->insert([
                                    'store_code'    => $request->code,
                                    'service_id'    => $service_id
                                ]);
                            }
                        
                        } elseif (count($affectedStoresStores_services) == 0) { // for create if empty

                            foreach ($request->stores_services as $service_id) { // create if empty
                                DB::table('stores_services')->insert([
                                    'store_code'    => $request->code,
                                    'service_id'    => $service_id
                                ]);
                            }                        
                        }

                } else {
                    StoresService::where('store_code', '=', $request->code)->delete();
                }

            
                // save on table stores_promo
                if($request->has('stores_promos') ){
                    
                    // check for create & update
                    $affectedPromo = StoresPromo::where('stores_id', '=', $request->id)->get();
        
                    // $this->affectedPromo = $affectedPromo; // for debuging
                    
                        if(count($affectedPromo) != 0){ // for update if not empty
                            StoresPromo::where('stores_id', '=', $request->id)->delete();
                            foreach ($request->stores_promos as $promo_id) {
                                DB::table('stores_promos')->insert([
                                    'promo_id'  => $promo_id,
                                    'stores_id' => $getId 
                                ]);
                            }
                        
                        } elseif (count($affectedPromo) == 0) { // for create if empty
                            foreach ($request->stores_promos as $promo_id) { // create if empty
                                DB::table('stores_promos')->insert([
                                    'promo_id'  => $promo_id,
                                    'stores_id' => $getId
                                ]);
                            }                        
                        }

                } else {
                    StoresPromo::where('stores_id', '=', $request->id)->delete();
                }

                // save on table stores_events
                if($request->has('stores_events') ){
                    
                    // check for create & update
                    $affectedEvent = StoresEvent::where('stores_id', '=', $request->id)->get();
        
                    // $this->affectedEvent = $affectedEvent; // for debuging
                    
                        if(count($affectedEvent) != 0){ // for update if not empty
                            StoresEvent::where('stores_id', '=', $request->id)->delete();
                            foreach ($request->stores_events as $event_id) {
                                DB::table('stores_events')->insert([
                                    'event_id'  => $event_id,
                                    'stores_id' => $getId 
                                ]);
                            }
                        
                        } elseif (count($affectedEvent) == 0) { // for create if empty
                            foreach ($request->stores_events as $event_id) { // create if empty
                                DB::table('stores_events')->insert([
                                    'event_id'  => $event_id,
                                    'stores_id' => $getId
                                ]);
                            }                        
                        }

                } else {
                    StoresEvent::where('stores_id', '=', $request->id)->delete();
                }                

            
            $request->session()->flash('alert-success', 'Store `'.$request->name.'` was successful updated!');
            
            // $objPromo = (object) $request->stores_promos; // for debuging

            return response()->json([
                    'id'        => $getId,
                    'message'   => 'Success Updated'
                    // 'affectedPromo' => count($this->affectedPromo) // for debuging
            ]);            


            }else{ // create NEW

             $insertStoreGetId = DB::table('stores')->insertGetId([
                        'code' => $request->code,
                        'name' => $request->name,
                        'building_name' => $request->building_name,
                        'address' => $request->address,
                        'city' => $request->city,
                        'province' => $request->province,
                        'postal' => $request->postal,
                        'phone_number' => $request->phone_number,
                        'open_date' => $request->open_date,
                        'opening_date' => $request->opening_date,
                        'opening_hour' => $request->opening_hour,                    
                        'photo' => null
                ]);

             // insert stores_events
             if($request->has('stores_events')){
                foreach ($request->stores_events as $event_id) {
                    DB::table('stores_events')->insert([
                        'event_id'  => $event_id,
                        'stores_id' => $insertStoreGetId
                    ]);
                }  
            } 

             // insert stores_promos
             if($request->has('stores_promos')){
                foreach ($request->stores_promos as $promo_id) {
                    DB::table('stores_promos')->insert([
                        'promo_id'  => $promo_id,
                        'stores_id' => $insertStoreGetId
                    ]);
                }  
            }             

            // insert stores_services
            if($request->has('stores_services')){    
                foreach ($request->stores_services as $service_id) {
                    DB::table('stores_services')->insert([
                        'store_code' => $this->StoresCode,
                        'service_id' => $service_id
                    ]);
                }
            }

            // insert stores_principals_others
            if($request->has('stores_principals_others')){
                foreach ($request->stores_principals_others as $principal_code) {
                    DB::table('stores_principals_others')->insert([
                        'store_code' => $this->StoresCode,
                        'principal_code' => $principal_code
                    ]);
                }
            }

            // insert stores_principals_banks
            if($request->has('stores_principals_banks')){
                foreach ($request->stores_principals_banks as $principal_code) {
                    DB::table('stores_principals_banks')->insert([
                        'store_code' => $this->StoresCode,
                        'principal_code' => $principal_code
                    ]);
                }
            }                    
            
            // insert stores_principals_atms
            if($request->has('stores_principals_atms')){
                foreach ($request->stores_principals_atms as $principal_code) {
                    DB::table('stores_principals_atms')->insert([
                        'store_code' => $this->StoresCode,
                        'principal_code' => $principal_code
                    ]);
                }
            }            

            // insert stores_amenities
            if($request->has('stores_amenities')){
                    foreach ($request->stores_amenities as $stores_amenities_id) {
                        DB::table('stores_amenities')->insert([
                            'store_code' => $this->StoresCode,
                            'amenities_id' => $stores_amenities_id
                        ]);
                    }
            }            


                $request->session()->flash('alert-success', 'Store `'.$request->name.'` was successful added!');
                 
                 return response()->json([
                    'id'               => $insertStoreGetId,
                    'message_stores'   => 'Success Added'
                 ]);

            }


        // }
        // return redirect('/stores');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('stores::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id, Request $request)
    {
        $store_id = $id;
        if ($request->session()->has('stores_images')) {
            $request->session()->forget('stores_images');
        }

        $stores = DB::table('stores')->where('code',$id)->get();

        $store = $stores[0];

        $this->storeId = $store->id;

        $amenities_selected = [];
        $amenities_selected_row = DB::table('stores_amenities')->where('store_code', $store->code)->get();
        foreach ($amenities_selected_row as $row) {
            array_push($amenities_selected, (string) $row->amenities_id);
        }

        $principal_atm_selected = [];
        $principal_atm_selected_row = DB::table('stores_principals_atms')->where('store_code', $store->code)->get();
        foreach ($principal_atm_selected_row as $row) {
            array_push($principal_atm_selected, (string) $row->principal_code);
        }

        $principal_bank_selected = [];
        $principal_bank_selected_row = DB::table('stores_principals_banks')->where('store_code', $store->code)->get();
        foreach ($principal_bank_selected_row as $row) {
            array_push($principal_bank_selected, (string) $row->principal_code);
        }

        $principal_other_selected = [];
        $principal_other_selected_row = DB::table('stores_principals_others')->where('store_code', $store->code)->get();
        foreach ($principal_other_selected_row as $row) {
            array_push($principal_other_selected, (string) $row->principal_code);
        }

        $stores_services_selected = [];
        $stores_services_selected_row = DB::table('stores_services')->where('store_code', $store->code)->get();
        foreach ($stores_services_selected_row as $row) {
            array_push($stores_services_selected, (string) $row->service_id);
        }

        $stores_promos_selected = [];
        $stores_promos_selected_row = DB::table('stores_promos')->where('stores_id', $this->storeId)->get();
        foreach ($stores_promos_selected_row as $row) {
            array_push($stores_promos_selected, (string) $row->promo_id);
        }

        $stores_events_selected = [];
        $stores_events_selected_row = DB::table('stores_events')->where('stores_id', $this->storeId)->get();
        foreach ($stores_events_selected_row as $row) {
            array_push($stores_events_selected, (string) $row->event_id);
        }        

        //////////////// **** \\\\\\\\\\\\

        $stores_principals_bank = DB::table('principals')->where('category_code', '111BNK')->get();
        $stores_principals_other = DB::table('principals')->where('category_code', '!=', '111BNK')->get();
        $stores_services = DB::table('services')->where('status', '=', '1')->get();
        $stores_promos = DB::table('promos')->where('status', '=', '1')->get();
        $stores_events = DB::table('events')->where('status', '=', '1')->get();

        $stores_images = [];
        $stores_images_raw = DB::table('stores_images')->where('store_code', $store->code)->get();
        $dir = public_path($this->dir_img_stores);
        foreach ($stores_images_raw as $row) {
            
            $imgs = @exif_read_data($dir.$row->photo);
            // $imgs    = Image::make(public_path($row->photo))->exif();
            array_push(
                $stores_images,[
                'name'   => $imgs['FileName'], 
                'size'   => $imgs['FileSize'], 
                'type'   => $imgs['MimeType'], 
                'status' => 'queued', 
                'dataURL'    => $this->dir_img_stores.$imgs['FileName']
            ]);
        }

        $front_image = [];
        $query = DB::table('stores')->where('id', $store->id)->pluck('photo');
        $dir = public_path($this->dir_img_stores);
        $image = @exif_read_data($dir.$query[0]);
            array_push(
                $front_image,[
                'name'      => $image['FileName'], 
                'size'      => $image['FileSize'], 
                'type'      => $image['MimeType'], 
                'status'    => 'queued', 
                'dataURL'   => $this->dir_img_stores.$image['FileName']
            ]);      

        return view('stores::edit',[
            'stores' => $stores, 

            'avaibility_list' => ['in_scope' => 'In Scope', 'out_scope' => 'Out Scope'], 
            'amenities' => Amenities::all(),
            'stores_principals_bank' => $stores_principals_bank,
            'stores_principals_other' => $stores_principals_other,
            'stores_services' => $stores_services,
            'stores_promos' => $stores_promos,
            'stores_events' => $stores_events,

            'amenities_selected' => json_encode($amenities_selected),
            'principal_atm_selected' => json_encode($principal_atm_selected),
            'principal_bank_selected' => json_encode($principal_bank_selected),
            'principal_other_selected' => json_encode($principal_other_selected),
            'stores_services_selected' => json_encode($stores_services_selected),
            'stores_promos_selected' => json_encode($stores_promos_selected),
            'stores_events_selected' => json_encode($stores_events_selected),

            'stores_images' => json_encode($stores_images),
            'front_image' => json_encode($front_image)
        ]);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        DB::table('stores')->where('code',$id)->delete();

        return redirect('/stores')->with('alert-success','Store id.'.$id.' successful deleted!');
    }
}
