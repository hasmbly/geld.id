<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('blogs')->group(function() {
    Route::get('/', 'BlogsController@index');
    Route::post('/store', 'BlogsController@store');
    Route::get('/create', 'BlogsController@create');
    Route::get('/destroy/{id}', 'BlogsController@destroy');
    Route::get('/edit/{id}', 'BlogsController@edit');
});
