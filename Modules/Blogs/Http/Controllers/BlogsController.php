<?php

namespace Modules\Blogs\Http\Controllers;

use App\PageBlogs;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;


class BlogsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('blogs::index', [
            'blogs' => PageBlogs::all()
            ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('blogs::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
		$this->validate($request, [
            'title' => 'required',
            'intro' => 'required',
            'content' => 'required',
            'tags' => 'required',
			// 'img' => 'required|file|image|mimes:jpeg,png,jpg|max:2048',
        ]);

        $img = null;

        $file = $request->file('img');
        if(!empty($file)){
            $img = 'blogs_'.md5($request->file . microtime()).'_'.$file->getClientOriginalName();
            $file->move(public_path('/assets/media/blog/'), $img);
        }

        // if(!empty($name)){
            if(!empty($request->id)){
                DB::table('page_blogs')->where('id',$request->id)->update([
                    'title' => $request->title,
                    'intro' => $request->intro,
                    'content' => $request->content,
                    'tags' => $request->tags,
                    'created_by' => 'admin',
                    'is_publish' => $request->is_publish
                    // 'img_tumbnail' => $img,
                    // 'img_full' => $img,
                ]);
                $request->session()->flash('alert-success', 'Blog `'.$request->title.'` was successful updated!');
            }else{
                DB::table('page_blogs')->insert([
                    'title' => $request->title,
                    'intro' => $request->intro,
                    'content' => $request->content,
                    'tags' => $request->tags,
                    'created_by' => 'admin',
                    'img_tumbnail' => $img,
                    'img_full' => $img,
                    'is_publish' => $request->is_publish
                ]);
                $request->session()->flash('alert-success', 'Blog `'.$request->title.'` was successful added!');
            }
        // }
        return redirect('/blogs');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('blogs::created');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $blogs = DB::table('page_blogs')->where('id',$id)->get();

        return view('blogs::edit',[
            'blogs' => $blogs, 
            'avaibility_list' => [
                'Public' => 'Public', 
                'Visitor' => 'Visitor', 
                'Member' => 'Member'
                ]
            ]);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        DB::table('page_blogs')->where('id',$id)->delete();

        return redirect('/blogs')->with('alert-success','Blog id.'.$id.' successful deleted!');
    }
}
