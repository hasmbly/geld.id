<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('event')->group(function() {

    Route::get('/', 'EventController@index');
    
    Route::get('/create', 'EventController@create');

    Route::post('/store', 'EventController@store');

    Route::post('/stores_images', 'EventController@stores_images'); // new/update stores_images
    Route::post('/front_images', 'EventController@front_image'); // new/update front_images

    Route::get('/destroy/{id}', 'EventController@destroy');
    Route::post('/destroy/image', 'EventController@destroy_image'); // destroy stores_images

    Route::get('/edit/{id}', 'EventController@edit');

});