<?php

namespace Modules\Services\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
// use Illuminate\Routing\Controller;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Services;

class ServicesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        // return view('services::index');
        return view('services::index', [
            'services' => Services::all()
            ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('services::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
		$this->validate($request, [
            'name' => 'required',
            'description' => 'required',
			'avaibility_for' => 'required',
			'logo' => 'required|file|image|mimes:jpeg,png,jpg|max:2048',
        ]);

        $name = null;

        $file = $request->file('logo');
        if(!empty($file)){
            $name = 'LogoServices_'.md5($request->file . microtime()).'_'.$file->getClientOriginalName();
            $file->move(public_path('/public/img/icons/'), $name);
        }

        if(!empty($name)){
            if(!empty($request->id)){
                DB::table('services')->where('id',$request->id)->update([
                    'name' => $request->name,
                    'description' => $request->description,
                    'avaibility_for' => $request->avaibility_for,
                    'logo' => $name
                ]);
                $request->session()->flash('alert-success', 'Service `'.$request->name.'` was successful updated!');
            }else{
                DB::table('services')->insert([
                    'name' => $request->name,
                    'description' => $request->description,
                    'avaibility_for' => $request->avaibility_for,
                    'logo' => $name
                ]);
                $request->session()->flash('alert-success', 'Service `'.$request->name.'` was successful added!');
            }
        }
        return redirect('/services');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('services::create');
        // return view('services::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $services = DB::table('services')->where('id',$id)->get();

        return view('services::edit',['services' => $services, 'avaibility_list' => [
            'Public' => 'Public', 'Visitor' => 'Visitor', 'Member' => 'Member']
            ]);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        DB::table('services')->where('id',$id)->delete();

        return redirect('/services')->with('alert-success','Service id.'.$id.' successful deleted!');
    }
}
