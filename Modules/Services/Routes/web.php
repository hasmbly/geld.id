<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('services')->group(function() {
    Route::get('/', 'ServicesController@index');
    Route::get('/create', 'ServicesController@create');
    Route::post('/store', 'ServicesController@store');
    Route::get('/destroy/{id}', 'ServicesController@destroy');
    Route::get('/edit/{id}', 'ServicesController@edit');
});
