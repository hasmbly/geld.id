@extends('layouts.admin')

@section('content')
<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                {!! config('dashboard.name') !!} </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">
                    Dashboards </a>
                <!-- <span class="kt-subheader__breadcrumbs-separator"></span> -->
                <!-- <a href="" class="kt-subheader__breadcrumbs-link">
                    Default Dashboard </a> -->
                <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
            </div>
        </div>
    </div>
</div>
<!-- end:: Subheader -->
<!-- begin:: Content -->
<div class="kt-container  kt-grid__item kt-grid__item--fluid">


</div>

<!--End::Row-->

@stop



@section('js_page')
<script src="./assets/js/demo5/pages/dashboard.js" type="text/javascript"></script>
@stop