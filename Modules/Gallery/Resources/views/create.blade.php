@extends('layouts.admin')

@section('content')
<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-container ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">
				<a href="{{ url('gallery') }}">{!! config('gallery.name') !!}</a> </h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="/dashboard" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Create</span>
			</div>
		</div>
	</div>
</div>
<!-- end:: Subheader -->
<!-- begin:: Content -->
<div class="kt-container  kt-grid__item kt-grid__item--fluid">

	<!--Begin::Row-->
	<div class="row">
		<div class="col-xl-12 col-lg-12 order-lg-1 order-xl-1">
			<!-- begin:: Content -->
			<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

				<!--begin::Portlet-->
				<div class="kt-portlet">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title">
								New Gallery
							</h3>
						</div>
					</div>

					<!--begin::Form-->
					<form class="kt-form kt-form--label-right" action="{{ url('gallery/store') }}" id="form" method="post" enctype="multipart/form-data">
						{{ csrf_field() }}
						<div class="kt-portlet__body">
							<div class="form-group row">
								<label class="col-form-label col-lg-3 col-sm-12">Title Of Gallery *</label>
								<div class="col-lg-4 col-md-9 col-sm-12">
									<input type="text" class="form-control" name="title" placeholder="Title" required="required">
								</div>
							</div>
 
							{{-- begin::DummyForm --}}
							<div class="kt-form__seperator kt-form__seperator--dashed kt-form__seperator--space"></div>
							<div class="form-group row">
									<div class="col-lg-9 col-md-9 col-sm-12">
										<form>
										</form>
									</div>
								</div>							
							{{-- end::DummyForm --}}
							<!-- begin::Front_Image_Dropzone -->
							<div class="form-group row">
								<label class="col-form-label col-lg-3 col-sm-12">Display Picture</label>
								<div class="col-lg-4 col-md-9 col-sm-12">
										<form method="post" action="{{url('gallery/front_images')}}" enctype="multipart/form-data" class="dropzone" id="front_images_dropzone">@csrf
											<div id="front_image_id"></div>
										</form>
								</div>
							</div>
							<!-- end::Front_Dropzone -->

							<div class="kt-form__seperator kt-form__seperator--dashed kt-form__seperator--space"></div>
							<div class="form-group row">
								<label class="col-form-label col-lg-3 col-sm-12">Description *</label>
								<div class="col-lg-7 col-md-9 col-sm-12">
									<textarea name="description" class="form-control" data-provide="markdown" rows="5" required="required"></textarea>
								</div>
							</div>

						<div class="kt-form__seperator kt-form__seperator--dashed kt-form__seperator--space"></div>
							<div class="form-group row">
								<label class="col-form-label col-lg-3 col-sm-12">Is Publish ? *</label>
								<div class="col-lg-4 col-md-9 col-sm-12">
									<select class="form-control kt-bootstrap-select" name="is_publish" required="required">
										<option value="1">Publish</option>
										<option value="0">Un Publish</option>
									</select>
								</div>
							</div>

				</form>
				<!--end::Form-->
				{{-- begin::Store_Image --}}
							<div class="form-group row">
								<label class="col-form-label col-lg-3 col-sm-12">Images</label>
								<div class="col-lg-7 col-md-9 col-sm-12">
										<form method="post" action="{{url('gallery/stores_images')}}" enctype="multipart/form-data" class="dropzone" id="store_images_dropzone">@csrf
											<div id="stores_image_id"></div>
										</form>
								</div>
							</div>
					</div>
				{{-- end::Store_Image --}}					
	
					<div class="kt-form__seperator kt-form__seperator--dashed kt-form__seperator--space"></div>
						<div class="kt-portlet__foot">
							<div class=" ">
								<div class="row">
									<div class="col-lg-9 ml-lg-auto">
										<button type="submit" class="btn btn-success submitForm" value="update">Submit</button>
										<a href="{{ url('gallery') }}" class="btn btn-secondary">Cancel</a>
									</div>
								</div>	
							</div>
						</div>
				</div>
				<!--end::Portlet-->
			</div>
		</div>
	</div>

	<!--End::Row-->
</div>

<!--End::Row-->

@stop

@section('js_page')
<script src="{{ URL::asset('assets/js/demo1/pages/crud/forms/validation/form-widgets.js') }}" type="text/javascript"></script>

<script src="{{ URL::asset('assets/js/demo1/pages/crud/forms/widgets/dropzone.js') }}" type="text/javascript"></script>
<script type="text/javascript">

		/**
		 * [front_images description]
		 * For Uploading New Image and Also Removing Existing File
		 * @type {object function}
		 */
		Dropzone.autoDiscover = false;
		var front_image = new Dropzone("#front_images_dropzone", 
		{		
			maxFiles: 1,
			removedfile: function(file) {
				$(document).find(file.previewElement).remove();			    
			},
			init: function() {			
				this.hiddenFileInput.removeAttribute('multiple');
		        var myDropzone = this;
				console.log('Dropzone init() :', 'On InitFunction Front_Image');
			}
		});	
	
		/**
		 * [store_images description]
		 * For Uploading New Image and Also Removing Existing File
		 * @type {object function}
		 */
		Dropzone.autoDiscover = false;
		var store_images = new Dropzone("#store_images_dropzone", 
		{		
			maxFiles: 6,
			removedfile: function(file) {
				$(document).find(file.previewElement).remove();			    
			},
			init: function() {
				this.hiddenFileInput.removeAttribute('multiple');		
				console.log('Dropzone init() :', 'On InitFunction store_images');
			}
		});	


var newId = null;
$('.submitForm').click(function() {

	console.log('processQueue() :', 'Uploading Image from Queued');

	var formData = new FormData();
	formData.append("_token", "{{ csrf_token() }}");

    $.ajax({
           type: "POST",
           url: "{{ url('gallery/store') }}",
           data: $('#form').serialize(), // changed
           success: function(data) {

           	newId = data.id

			var inputFrontImage = document.createElement("input");
			inputFrontImage.setAttribute("type", "hidden");
			inputFrontImage.setAttribute("name", "id");
			inputFrontImage.setAttribute("value", newId);

			var inputStoresImages = document.createElement("input");
			inputStoresImages.setAttribute("type", "hidden");
			inputStoresImages.setAttribute("name", "id");
			inputStoresImages.setAttribute("value", newId);				

			document.getElementById("front_image_id").appendChild(inputFrontImage);
			document.getElementById("stores_image_id").appendChild(inputStoresImages);

           	front_image.processQueue();
           	store_images.processQueue();         	
			window.location.replace("{{ url('gallery') }}");
				console.log('data', data);

           }
    });
    return false; // avoid to execute the actual form submission.
});		

// for datepicker
$.fn.datepicker.defaults.format = "dd-mm-yyyy";
$.fn.datepicker.defaults.autoclose = true;
$.fn.datepicker.defaults.forceParse = true;
$.fn.datepicker.defaults.startDate = '0d';

</script>
@stop