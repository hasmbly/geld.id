<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('gallery')->group(function() {

    Route::get('/', 'GalleryController@index');
    
    Route::get('/create', 'GalleryController@create');

    Route::post('/store', 'GalleryController@store');

    Route::post('/stores_images', 'GalleryController@stores_images'); // new/update stores_images
    Route::post('/front_images', 'GalleryController@front_image'); // new/update front_images

    Route::get('/destroy/{id}', 'GalleryController@destroy');
    Route::post('/destroy/image', 'GalleryController@destroy_image'); // destroy stores_images

    Route::get('/edit/{id}', 'GalleryController@edit');

});
