<?php

namespace Modules\Gallery\Http\Controllers;

use App\Gallery;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class GalleryController extends Controller
{   
    private $dir_img_stores = '/public/img/gallery/'; 

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('gallery::index', [
            'gallery' => Gallery::all()
            ]);           
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('gallery::create');
    }

    public function stores_images(Request $request)
    {   


        $file   = $request->file('file');
        $id     = $request->id;

        /* Validator */
          $rules = array(
              'file'             => 'required',
              'id'               => 'required'
                  );    
          $messages = array(
            'file.required'         => 'Please provide your file',
            'id.required'           => 'Please provide your id'
          );

          $validator = Validator::make( $request->all(), $rules, $messages );

        if ($validator->fails()) {


            return response()->json([
                'error'    => true,
                'message'  => $validator->errors()->first()
            ], 422);

        }          
        
        $file->move(public_path($this->dir_img_stores), $file->getClientOriginalName());

        try {
            $InsertImages = DB::table('galleries_images')->insert([
                                'gallery_id'    => $id,
                                'photo'         => $file->getClientOriginalName()
            ]);

        } catch (QueryException $e) {
            return response()->json([
                'error'       => true,
                'code'        => $e->getCode(),
                'message'     => $e->getMessage()
              ], 500);
             }

         return response()->json([
            'error'     => false,
            'message'   => 'Images Uploaded',
            'id'        => $id,
            'Filename'  => $file->getClientOriginalName()
         ], 200);
    
    }

    public function front_image(Request $request)
    {
        $file       = $request->file('file');

        $id         = $request->id;

        /* Validator */

          $rules = array(
              'file'             => 'required',
              'id'               => 'required'
                  );    
          $messages = array(

            'file.required'         => 'Please provide your file',
            'id.required'           => 'Please provide your id'
          );

          $validator = Validator::make( $request->all(), $rules, $messages );

        if ($validator->fails()) {

            return response()->json([
                'error'    => true,
                'message'  => $validator->errors()->first()
            ], 422);

        }        
        
        $file->move(public_path($this->dir_img_stores), $file->getClientOriginalName());

        // $this->photo = $file->getClientOriginalName();
        try {

        DB::table('galleries')
            ->where('id',$id)
            ->update([
                'img' => $file->getClientOriginalName()
            ]);            
            
        } catch (QueryException $e) {

        return response()->json([
            'error'       => true,
            'code'        => $e->getCode(),
            'message'     => $e->getMessage()
          ], 500);

         } 

         return response()->json([
            'error'     => false,
            'message'   => 'Front Image Uploaded',
            'id'        => $id,
            'Filename'  => $file->getClientOriginalName()
         ], 200);

    }   

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {

        /* Validator */
          $rules = array(
            'title'         => 'required',
            'description'   => 'required'
            );

          $messages = array(
            'title.required'        => 'Please provide your title of Promo',
            'description.required'  => 'Please provide your description'
          );

          $validator = Validator::make( $request->all(), $rules, $messages );

        if ($validator->fails()) {

            return response()->json([
                'error'    => true,
                'message'  => $validator->errors()->first()
            ], 422);

        }          

            if(!empty($request->id)){

                $update = DB::table('galleries')->where('id',$request->id)->update([
                    'title'         => $request->title,
                    'description'   => $request->description,
                    'is_publish' => $request->is_publish                 

                ]);

                $request->session()->flash('alert-success', 'Gallery `'.$request->title.'` was successful Updated!');
            
            }else{

                $insertGetId = DB::table('galleries')->insertGetId([
                                'title'          => $request->title,
                                'description'   => $request->description,
                                'is_publish' => $request->is_publish 
                    ]);

                $request->session()->flash('alert-success', 'Gallery `'.$request->title.'` was successful Added!');

                 return response()->json([
                    'id'        => $insertGetId,
                    'message'   => 'Success Added'
                 ]);   

            }

                 $request->session()->flash('alert-success', 'Gallery `'.$request->title.'` was successful Updated!');
          
                 return response()->json([
                    'message' => 'Success Updated'
                 ]);                            

    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('gallery::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $gallery   = Gallery::where('id', $id)->get();
        $getId   = $gallery[0]->id;

        try {        
        $stores_images = [];
        $stores_images_raw = DB::table('galleries_images')->where('gallery_id', $getId)->get();
        $dir = public_path($this->dir_img_stores);
        foreach ($stores_images_raw as $row) {
            
            $imgs = @exif_read_data($dir.$row->photo);
            // $imgs    = Image::make(public_path($row->photo))->exif();
            array_push(
                $stores_images,[
                'name'      => $imgs['FileName'], 
                'size'      => $imgs['FileSize'], 
                'type'      => $imgs['MimeType'], 
                'status'    => 'queued', 
                'dataURL'   => $this->dir_img_stores.$imgs['FileName']
            ]);
        }
        } catch (QueryException $e) {

            return response()->json([
                'error'       => true,
                'code'        => $e->getCode(),
                'message'     => $e->getMessage()
              ], 500);

             }         

        try {
                $front_image = [];
                $query = DB::table('galleries')->where('id', $getId)->pluck('img');
                $dir = public_path($this->dir_img_stores);
                $image = @exif_read_data($dir.$query[0]);
                    array_push(
                        $front_image,[
                        'name'      => $image['FileName'], 
                        'size'      => $image['FileSize'], 
                        'type'      => $image['MimeType'], 
                        'status'    => 'queued', 
                        'dataURL'   => $this->dir_img_stores.$image['FileName']
                    ]);                 
            } catch (QueryException $e) {

                return response()->json([
                    'error'       => true,
                    'code'        => $e->getCode(),
                    'message'     => $e->getMessage()
                  ], 500);

                 } 
      

        return view('gallery::edit',[
            'gallery' => $gallery,
            'stores_images' => json_encode($stores_images),
            'front_image' => json_encode($front_image)
            ]); 
    }
    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

  /**
     * [destroy_image description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function destroy_image(Request $request)
    {

        $file       = $request->filename;
        $id         = $request->id;

        if($file && $id){

            $getId = DB::table('galleries')->where('id', $id)->pluck('id');       

            foreach ($file as $value) {

                DB::table('galleries_images')
                ->where('gallery_id', '=', $getId[0])
                ->where('photo', '=', $value)
                ->delete();
            }

            return response('Photo deleted', 200);
            
        }
    } 

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $title = DB::table('galleries')->where('id', $id)->pluck('title');
        $title = $title[0];
        
        DB::table('galleries')->where('id',$id)->delete();


        return redirect('gallery')->with('alert-success','Gallery '.$title.' successful deleted!');            
    }
}
