<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('amenities')->group(function() {
    Route::get('/', 'AmenitiesController@index');
    Route::get('/create', 'AmenitiesController@create');
    Route::post('/store', 'AmenitiesController@store');
    Route::get('/destroy/{id}', 'AmenitiesController@destroy');
    Route::get('/edit/{id}', 'AmenitiesController@edit');
});
