<?php

namespace Modules\Amenities\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
// use Illuminate\Routing\Controller;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Amenities;

class AmenitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        // return view('amenities::index');
        return view('amenities::index', [
            'amenities' => Amenities::all(),
            ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('amenities::create',['avaibility_list' => [
            'Public' => 'Public', 
            'Visitor' => 'Visitor', 
            'Member' => 'Member',
            'Pay to Third Party' => 'Pay to Third Party',
            'Pay for Use' => 'Pay for Use',
            'With Appointment' => 'With Appointment'
            ]]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
		$this->validate($request, [
            'name' => 'required',
            'description' => 'required',
			'avaibility_for' => 'required',
			'logo' => 'required|file|image|mimes:jpeg,png,jpg|max:2048',
        ]);

        $name = null;

        $file = $request->file('logo');
        if(!empty($file)){
            $name = 'LogoAmenities_'.md5($request->file . microtime()).'_'.$file->getClientOriginalName();
            $file->move(public_path('/public/img/icons/'), $name);
        }

        if(!empty($name)){
            if(!empty($request->id)){
                DB::table('amenities')->where('id',$request->id)->update([
                    'name' => $request->name,
                    'description' => $request->description,
                    'avaibility_for' => $request->avaibility_for,
                    'logo' => $name
                ]);
                $request->session()->flash('alert-success', 'Amenities `'.$request->name.'` was successful updated!');
            }else{
                DB::table('amenities')->insert([
                    'name' => $request->name,
                    'description' => $request->description,
                    'avaibility_for' => $request->avaibility_for,
                    'logo' => $name
                ]);
                $request->session()->flash('alert-success', 'Amenities `'.$request->name.'` was successful added!');
            }
        }
        return redirect('/amenities');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('amenities::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $amenities = DB::table('amenities')->where('id',$id)->get();

        return view('amenities::edit',['amenities' => $amenities, 'avaibility_list' => [
            'Public' => 'Public', 
            'Visitor' => 'Visitor', 
            'Member' => 'Member',
            'Pay to Third Party' => 'Pay to Third Party',
            'Pay for Use' => 'Pay for Use',
            'With Appointment' => 'With Appointment'
            ]]);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        DB::table('amenities')->where('id',$id)->delete();

        return redirect('/amenities')->with('alert-success','Amenities id.'.$id.' successful deleted!');
    }
}
