<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('faq')->group(function() {

    Route::get('/', 'FaqController@index');

    Route::get('/create', 'FaqController@create');

    Route::post('/store', 'FaqController@store');

    Route::post('/detailfaq', 'FaqController@detailfaq');    

    Route::get('/destroy/{id}', 'FaqController@destroy');

    Route::get('/edit/{id}', 'FaqController@edit');
});
