<?php

namespace Modules\Faq\Http\Controllers;

use App\Faq;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class FaqController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('faq::index', [
            'faq'           => Faq::all(),
            'faq_question'  => DB::table('faqs_question')->get()
            ]);               
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('faq::create');
    }

    public function detailfaq(Request $request)
    {
        /* Validator */
          $rules = array(
            'description'           => 'required'
            );  

          $messages = array(
            'description.required'          => 'Please provide your description'
          );

          $validator = Validator::make( $request->all(), $rules, $messages );

        if ($validator->fails()) {

            return response()->json([
                'error'    => true,
                'message'  => $validator->errors()->first()
            ], 422);

        }          

            if(!empty($request->id)){

                $update = DB::table('faqs')->where('id',$request->id)->update([
                    'description'      => $request->description
                ]);

                $request->session()->flash('alert-success', 'FAQ Header Detail was successful Updated!');

                 return response()->json([
                    'message' => 'Success Updated'
                 ]);   

            }
        
    }    

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {

        /* Validator */
          $rules = array(
            'question'         => 'required',
            'reply'            => 'required'
            );

          $messages = array(
            'question.required'  => 'Please provide your question',
            'reply.required'  => 'Please provide your reply'
          );

          $validator = Validator::make( $request->all(), $rules, $messages );

        if ($validator->fails()) {

            return response()->json([
                'error'    => true,
                'message'  => $validator->errors()->first()
            ], 422);

        }          

            if(!empty($request->id)){

                $update = DB::table('faqs_question')->where('id',$request->id)->update([
                    'question'         => $request->question,
                    'reply'            => $request->reply
                ]);

                $request->session()->flash('alert-success', 'Faq `'.$request->question.'` was successful Updated!');
            
            }else{

                $insertGetId = DB::table('faqs_question')->insertGetId([
                                'question'  => $request->question,
                                'reply'     => $request->reply
                    ]);

                $request->session()->flash('alert-success', 'Faq `'.$request->question.'` was successful Added!');

                 return response()->json([
                    'id'        => $insertGetId,
                    'message'   => 'Success Added'
                 ]);   

            }

                 $request->session()->flash('alert-success', 'Faq `'.$request->question.'` was successful Updated!');
          
                 return response()->json([
                    'message' => 'Success Updated'
                 ]);                            

    }


    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('faq::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $question   = DB::table('faqs_question')->where('id', $id)->get();

        return view('faq::edit',[
            'question'         => $question
            ]); 
    }


    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $question = DB::table('faqs_question')->where('id', $id)->pluck('question');
        $question = $question[0];
        
        DB::table('faqs_question')->where('id',$id)->delete();

        return redirect('faq')->with('alert-success','FAQ '.$question.' successful deleted!');            
    }
}
