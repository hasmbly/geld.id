@extends('layouts.admin')

@section('content')
<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
			<a href="{{ url('faq') }}">{!! config('faq.name') !!}</a> </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="/dashboard" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">List</span>
            </div>
        </div>
    </div>
</div>
<!-- end:: Subheader -->
<!-- begin:: Content -->
<div class="kt-container  kt-grid__item kt-grid__item--fluid">

    <!--Begin::Row-->
    <div class="row">
        <div class="col-xl-12 col-lg-12 order-lg-1 order-xl-1">
            <!-- begin .flash-message -->
            <div class="flash-message">
                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))
                <div class="alert alert-outline-{{ $msg }} fade show" role="alert">
                    <div class="alert-icon"><i class="flaticon-warning"></i></div>
                    <div class="alert-text">{{ Session::get('alert-' . $msg) }}</div>
                    <div class="alert-close">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true"><i class="la la-close"></i></span>
                        </button>
                    </div>
                </div>
                @endif
                @endforeach
            </div> 
            <!-- end .flash-message -->

            <div class="kt-portlet kt-portlet--mobile">
                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="kt-font-brand flaticon2-line-chart"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            List of Faq
                        </h3>
                    </div>               
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                            <div class="kt-portlet__head-actions">
                                <a href="#" id="showFaq" class="btn btn-bold btn-elevate btn-icon-sm">
                                    <i class="la la-edit"></i>
                                    Show Detail Faq
                                </a>                              	
                                <a href="{{ url('faq/create') }}" class="btn btn-brand btn-elevate btn-icon-sm">
                                    <i class="la la-plus"></i>
                                    New Record
                                </a>                              
                            </div>
                        </div>
                    </div>
                </div>

					<form class="kt-form kt-form--label-right" style="display:none" action="{{ url('faq/detailfaq') }}" id="form" method="post" enctype="multipart/form-data">
						{{ csrf_field() }}
						<input type="hidden" name="id" value="{{ $faq[0]->id }}">
						<div class="kt-portlet__body">	
							<div class="form-group row">
								<div class="col-lg-12 col-md-9 col-sm-12">
									<label>Detail FAQ on Header *</label>
									<textarea name="description" class="form-control" data-provide="markdown" rows="5" required="required">{{ $faq[0]->description }}</textarea>
								</div>								
							</div>
								<div class="row">
									<div class="col-lg-12 ml-lg-auto">
										<button type="submit" class="btn btn-success submitForm" value="update">Save</button>
										<a href="/faq" id="hideFaq" class="btn btn-secondary">Cancel</a>
									</div>
								</div>							
						</div>						
					</form>				

                <div class="kt-portlet__body">

                    <!--begin: Datatable -->
                    <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                        <thead>
                            <tr>
                                <th width="20%">Question</th>
                                <th>Reply</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($faq_question as $row)
                            <tr>
                                <td>{{ $row->question }}</td> 
                                <td>{{ $row->reply }}</td>
                                <td nowrap>
                                    <a href="{{ url('faq/edit/'.$row->id.'/') }}" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit `{{$row->question}}`">
                                        <i class="la la-edit"></i>
                                    </a>|
                                    <a href="{{ url('faq/destroy/'.$row->id.'/') }}" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Delete `{{$row->question}}`">
                                        <i class="la la-trash"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <!--end: Datatable -->
                </div>
            </div>
        </div>
    </div>

    <!--End::Row-->
</div>

<!--End::Row-->

@stop

@section('js_page')
<!-- <script src="{{ mix('js/peserta.js') }}"></script> -->
<script type="text/javascript">
    "use strict";
    var KTDatatablesBasicBasic = function() {

        var initTable1 = function() {
            var table = $('#kt_table_1');

            // begin first table
            table.DataTable({
                responsive: true,

                paging: true,
                // DOM Layout settings
                dom: `<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,

                lengthMenu: [5, 10, 25, 50],

                pageLength: 10,

                // Order settings
                order: [
                    [1, 'desc']
                ],
            });

            table.on('change', '.kt-group-checkable', function() {
                var set = $(this).closest('table').find('td:first-child .kt-checkable');
                var checked = $(this).is(':checked');

                $(set).each(function() {
                    if (checked) {
                        $(this).prop('checked', true);
                        $(this).closest('tr').addClass('active');
                    } else {
                        $(this).prop('checked', false);
                        $(this).closest('tr').removeClass('active');
                    }
                });
            });

            table.on('change', 'tbody tr .kt-checkbox', function() {
                $(this).parents('tr').toggleClass('active');
            });
        };

        return {

            //main function to initiate the module
            init: function() {
                initTable1();
            },

        };

    }();

    jQuery(document).ready(function() {
        KTDatatablesBasicBasic.init();
    });

// function IDClick() {
// 	return false;
// }

$('.submitForm').click(function() {

    $.ajax({
    	   type: "POST",
           url: "/faq/detailfaq",
           data: $('#form').serialize(), // changed
           success: function(data) {
			window.location.replace("/faq");
				console.log('data', data);
           }
    });
    return false; // avoid to execute the actual form submission.
});	

$(document).ready(function() {

	$('#showFaq').click(function(e) {
		e.preventDefault();
		$('#form').slideToggle(function() {
			$('#showFaq').html($('#form').is(':visible')?'<i class="la la-edit"></i>Close Detail Faq':'<i class="la la-edit"></i>Show Detail Faq');
		 });
                                    

	});	
});

</script>
@stop