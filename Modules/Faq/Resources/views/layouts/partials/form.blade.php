					<form class="kt-form kt-form--label-right" action="{{ url('faq/detailfaq') }}" id="form" method="post" enctype="multipart/form-data">
						{{ csrf_field() }}
						<input type="hidden" name="id" value="{{ $faq[0]->id }}">
						<div class="kt-portlet__body">	
							<div class="form-group row">
								<div class="col-lg-12 col-md-9 col-sm-12">
									<label>Detail FAQ on Header *</label>
									<textarea name="description" class="form-control" data-provide="markdown" rows="5" required="required">{{ $faq[0]->description }}</textarea>
								</div>								
							</div>
								<div class="row">
									<div class="col-lg-12 ml-lg-auto">
										<button type="submit" class="btn btn-success submitForm" value="update">Save</button>
										<a href="/faq" id="hideFaq" class="btn btn-secondary">Cancel</a>
									</div>
								</div>							
						</div>						
					</form>