@extends('layouts.default')

@section('content')
<div class="container mb-5">
		<div class="col-md-12">
		<h2 class="font-weight-normal text-6 ml-4 mb-4 mt-4">Our <strong class="font-weight-extra-bold">Promo</strong></h2>
		</div>	
	<div class="row pt-4">
		<div class="owl-carousel owl-theme1 stage-margin rounded-nav" data-plugin-options="{'margin': 10, 'loop': true, 'nav': true, 'dots': false, 'stagePadding': 20}">
				
				{{-- begin::foreach --}}
				@foreach ( $promos as $row )
				<div class="masonry-item no-default-style col-md-12">
					<article class="post post-medium border-0 pb-0 mb-5">
						<div class="post-image">
							<a href="/page/promo_list?name={{ $row->name }}">
								<img src="/public/img/promo/{{ $row->img }}" class="img-fluid img-thumbnail img-thumbnail-no-borders rounded-4" alt="" />
							</a>
						</div>
						<div class="post-content">
						<h3 class="font-weight-bold text-5 line-height-6 mt-5 mb-2">{{ $row->name }}</h3>
							<div class="font-weight-bold text-4 line-height-5 mt-2 mb-1">{{ date('d F',strtotime($row->start_date)) }} - {{ date('d F Y',strtotime($row->end_date)) }}</div>
						</div>
					</article>
				</div>
				@endforeach
				{{-- end::foreach --}}

		</div>
	</div>
</div>
	@endsection