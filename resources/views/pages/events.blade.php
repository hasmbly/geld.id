@extends('layouts.default')

@section('content')
<div class="container">
		<div class="col-md-12">
		<h2 class="font-weight-normal text-6 ml-4 mb-4 mt-4">Our <strong class="font-weight-extra-bold">Events</strong></h2>
		</div>	
	<div class="row pt-4">
		<div class="owl-carousel owl-theme1 stage-margin rounded-nav" data-plugin-options="{'margin': 10, 'loop': true, 'nav': true, 'dots': false, 'stagePadding': 20}">
				
				{{-- begin::foreach --}}
				@foreach ( $events as $row )
				<div class="masonry-item no-default-style col-md-12">
					<article class="post post-medium border-0 pb-0 mb-5">
						<div class="post-image">
							<a href="/page/events_list?title={{ $row->title }}">
								<img src="/public/img/events/{{ $row->img }}" class="img-fluid img-thumbnail img-thumbnail-no-borders rounded-4" alt="" />
							</a>
						</div>
						<div class="post-content">
							<div class="font-weight-bold text-4 line-height-5 mt-5 mb-2 ">{{ date('d F Y',strtotime($row->start_date)) }}</div><div class="font-weight-bold text-4 line-height-5 mt-2 mb-1">{{ date('h:i A', strtotime($row->start_time)) }} - {{ date('h:i A', strtotime($row->end_time)) }}</div>
						<h3 class="font-weight-bold text-5 line-height-6 mt-3 mb-2">{{ $row->title }}</h3>
						</div>
					</article>
				</div>
				@endforeach
				{{-- end::foreach --}}

		</div>
	</div>
</div>
	@endsection