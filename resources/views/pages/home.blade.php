@extends('layouts.default')

@section('content')

{{-- begin::page_home_section_1 --}}
<section class="section section-primary section-no-border section-height-0 my-0">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <h1 class="font-weight-bold line-height-7 text-2 negative-ls-5 mb-1 mt-3 big">{{$homes->title}}</h1>
                <p class="text-color-dark opacity-8 text-4 mb-4">{{$homes->intro}}</p>
                <!-- <a href="#" class="btn btn-dark font-weight-semibold btn-px-3 btn-py-2 text-2 rounded-0 mb-2">MORE</a> -->
            </div>
            <div class="col-lg-8 mt-5 mt-lg-0">
                <div class="owl-carousel owl-theme nav-inside nav-inside-edge nav-squared nav-with-transparency nav-light mb-0" data-plugin-options="{'items': 1, 'margin': 10, 'loop': true, 'nav': true, 'dots': false}" style="height: auto;">
                    {{-- begin::front_image --}}
                    <div>
                        <div class="img-thumbnail border-0 p-0 d-block">
                            <img class="img-fluid border-radius-0" style="height:475px;" src="./assets/media/home/{{$homes->img}}" alt="" />
                        </div>
                    </div>
                    {{-- end::front_image --}}
                    
                    {{-- begin::stores_image --}}
                    @foreach ($photos as $photo)
                    <div>
                        <div class="img-thumbnail border-0 p-0 d-block">
                            <img class="img-fluid border-radius-0" style="height:475px;" src="./assets/media/home/{{$photo}}" alt="" />
                        </div>
                    </div>
                    @endforeach
                    {{-- end::stores_image --}}

                </div>
            </div>
        </div>
    </div>
</section>
{{-- end::page_home_section_1 --}}

{{-- begin::page_home_section_2 --}}
<section class="section section-white section-no-border section-height-0 my-0">
    <div class="container">
        <div class="row pt-4">
            <div class="col text-center">
                <p class="font-weight-normal text-5 mb-4">{{$homes->description}}</p>
            </div>
        </div>
    </div>
</section>
{{-- end::page_home_section_2 --}}

{{-- begin::promo --}}
<section class="section section-primary section-no-border section-height-2">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!-- <div class="mb-0"> -->
                <h2 class="font-weight-normal text-6 mb-2">Our <strong class="font-weight-extra-bold">Promo</strong></h2>
                    <div class="owl-carousel owl-theme nav-inside nav-inside-edge nav-squared nav-with-transparency nav-light mb-3" data-plugin-options="{'items': 1, 'margin': 10, 'loop': false, 'nav': true, 'dots': false}" style="height: auto;">
                        {{-- begin::fetch_one_slide_two_image --}}
                        @php
                        $sum    = count($promos)-1;
                        $first  = 1;
                        $second = 0;
                        $break  = false;
                        @endphp
                        @for ( $y = 0; $y <= $sum; $y++ )
                            @php 
                            if ( $break == true ) {
                                break;
                            }

                            @endphp
                        <div class="row pt-1">   
                            @for ( $x = 0; $x < 2; $x++ )
                                <div class="col-lg-6">
                                    <img class="img-fluid border-radius-0" src="./public/img/promo/{{ $promos[$second]->img }}" alt="" />                            
                                </div>
                                @php 
                                $second++;           
                                if ( $first > $sum ) {
                                    $break = true;
                                    break;
                                } elseif ( $second == $first) {
                                    $first = $first + 1;
                                }
                                @endphp
                            @endfor
                        </div>      
                        @endfor
                        {{-- end::fetch_one_slide_two_image --}}
                    </div>
                <!-- </div> -->
            </div>
        </div>
    </div>
</section>
{{-- end::promo --}}

{{-- begin::store_list --}}
<section class="section section-white section-no-border section-height-0 my-0">
    <div class="container">
        <div class="row border-bottom-dark">
            <div class="col-lg-12">
                <div class="recent-posts mb-5">
                    <h2 class="font-weight-normal text-6 mb-4 mt-4">Our <strong class="font-weight-extra-bold">Store</strong></h2>
                    <div class="owl-carousel owl-theme nav-inside nav-inside-edge nav-squared nav-with-transparency nav-light mb-0" data-plugin-options="{'items': 1, 'margin': 10, 'loop': true, 'nav': false, 'dots': true}" style="height: auto;">
                    <!-- <div class="owl-carousel owl-theme dots-title mb-0" data-plugin-options="{'items': 1, 'autoHeight': true, 'autoplay': true, 'autoplayTimeout': 8000}"> -->

                        @foreach ($stores as $store)
                            <div class="row mt-4 mb-4">
                                <div class="col-lg-6">
                                    <a href="/stores?code={{ $store->code }}"><img class="img-fluid border-radius-0" style="width: 488px; height: 350px;" src="./public/img/stores/{{ $store->photo }}" /></a>
                                </div>
                                <div class="col-lg-6">
                                    <div class="row border-bottom-dark">
                                        <div class="col-lg-10">
                                            <a href="/stores?code={{ $store->code }}">
                                                <h3 class="font-weight-extra-bold mb-0">{{ $store->name }}</h3>
                                            </a>
                                        </div>
                                        <div class="col-lg-2">
                                            <a href="/stores?code={{ $store->code }}"><span class="badge badge-dark badge-sm badge-pill btn-primary text-uppercase mb-3">VISIT</span></a>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12 mb-1 border-bottom-dark">
                                            {{ $store->address }}<br />{{ $store->city }}, {{ $store->province }}, {{ $store->country }} {{ $store->postal }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12 mb-1 border-bottom-dark">
                                            <a href="tel:{{ $store->phone_number }}"><i class="fa fa-phone text-color-primary"></i>{{ $store->phone_number }}</a> | <i class="fa fa-calendar text-color-primary mr-2"></i>{{ $store->opening_date }} | <i class="fa fa-clock text-color-primary mr-2"></i>{{ $store->opening_hour }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12 mb-1">
                                        <h6 class="mb-0 text-color-primary">Our Principal</h6>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <span class="mb-1">Bank Office Available</span>
                                            <div class="service_list">
                                            @foreach (\App\StoresPrincipalsBank::where(['store_code' => $store->code])->join('principals', 'principals.code', '=', 'principal_code')->get() as $office)
                                            <img class="border-radius-0 mb-2 mt-2 mr-1 ml-1" src="./public/img/principals/logos/{{ $office->img_logo }}" width="38%" style="width: 50% !important;" />
                                            @endforeach
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <span class="mb-1">Bank ATM Available</span>
                                            <div class="service_list" id="style-3">
                                            @foreach (\App\StoresPrincipalsAtm::where(['store_code' => $store->code])->join('principals', 'principals.code', '=', 'principal_code')->get() as $office)
                                            <img class="border-radius-0 mb-2 mt-2 mr-1 ml-1" src="./public/img/principals/logos/{{ $office->img_logo }}" width="38%" style="width: 50% !important;" />
                                            @endforeach
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <span>Promo on Store</span>
                                            <div class="service_list" id="style-3">
                                            @foreach (\App\StoresPromo::where(['stores_id' => $store->id])->join('promos', 'promos.id', '=', 'promo_id')->get() as $promo)
                                            <img class="border-radius-0 mb-2 mt-2 mr-1 ml-1" src="./public/img/promo/{{ $promo->img }}" width="38%" style="width: 50% !important;" />
                                            @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- end::store_list --}}

{{-- begin::principal --}}
<div class="container">
    <div class="row pt-4">
    <h2 class="font-weight-normal text-6 mb-4 mt-4">Our <strong class="font-weight-extra-bold">Principal</strong></h2>
        <div class="owl-carousel owl-theme1 stage-margin rounded-nav" data-plugin-options="{'margin': 10, 'loop': true, 'nav': true, 'dots': false, 'stagePadding': 20}">
            @foreach ($principals as $principal)
                <div class="text-center">
                <a href="/principals?code={{$principal->code}}"><h4 class="text-center1">{{$principal->brand_name}}</h4></a>
                    <a href="/principals?code={{$principal->code}}"><center><img class="img-fluid" src="./public/img/principals/logos/{{$principal->img_logo}}" alt="" style="width: {{$principal->img_width}}px !important;" /></center></a>
                </div>
            @endforeach
        </div>
    </div>
</div>
{{-- end::principal --}}

@endsection