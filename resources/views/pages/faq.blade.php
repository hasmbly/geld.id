@extends('layouts.default')

@section('content')


<section class="page-header page-header-classic">
					<div class="container">
						<div class="row">
							<div class="col">
								<ul class="breadcrumb">
									<li><a href="#">Home</a></li>
									<li class="active">Pages</li>
								</ul>
							</div>
						</div>
						<div class="row">
							<div class="col p-static">
								<h1 data-title-border>FAQ</h1>

							</div>
						</div>
					</div>
				</section>

				<div class="container py-4">

					<div class="row">
						<div class="col">

							<h2 class="font-weight-normal text-7 mb-2">Frequently Asked <strong class="font-weight-extra-bold">Questions</strong></h2>
							{!! $faq[0]->description !!}

							<hr class="solid my-5">
							@foreach ($faq_question as $row)
							<div class="toggle toggle-primary" data-plugin-toggle>

								<section class="toggle active">
									<label>{{ $row->question }}</label>
									{!! $row->reply !!}
								</section>

							</div>
							@endforeach

						</div>

					</div>

				</div>

			</div>

@endsection