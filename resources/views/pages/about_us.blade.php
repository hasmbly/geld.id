@extends('layouts.default')

{{-- begin::foreach --}}
@foreach ( $about_us as $row )
@section('content')
<section class="page-header page-header-modern page-header-background page-header-background-md overlay overlay-color-dark overlay-show overlay-op-7" style="background-image: url(/public/img/page-header/page-header-about-us.jpg);">

	<div class="container">
		<div class="row mt-5">
			<div class="col-md-12 align-self-center p-static order-2 text-center">
				{!! $row->section_one !!}
			</div>
		</div>
	</div>
</section>

<div class="container">

	<div class="row pt-5">
		<div class="col">

			<div class="row text-center pb-5">
				<div class="col-md-9 mx-md-auto">
					{!! $row->section_two !!}
				</div>
			</div>

			<div class="row mt-3 mb-5">
				{!! $row->section_three !!}
			</div>

		</div>
	</div>

</div>

<section class="section section-height-3 bg-color-white-scale-1 m-0 border-0">
	<div class="container">
		<div class="row align-items-center justify-content-center">
			<div class="col-lg-6 pb-sm-4 pb-lg-0 pr-lg-5 mb-sm-5 mb-lg-0">
				{!! $row->section_four !!}
			</div>
			<div class="col-sm-8 col-md-6 col-lg-4 offset-sm-4 offset-md-4 offset-lg-2 mt-sm-5" style="top: 1.7rem;">
				<img src="/public/img/about_us/{{ $row->img_small }}" class="img-fluid position-absolute d-none d-sm-block appear-animation rounded-10" data-appear-animation="expandIn" data-appear-animation-delay="300" style="top: 10%; left: -50%; width: 164px; height: 148px;" alt="" />
				<img src="/public/img/about_us/{{ $row->img_medium }}" class="img-fluid position-absolute d-none d-sm-block appear-animation rounded-10" data-appear-animation="expandIn" style="top: -33%; left: -29%; width: 212px; height: 186px;" alt="" />
				<img src="/public/img/about_us/{{ $row->img_large }}" class="img-fluid position-relative appear-animation mb-2 rounded-10" data-appear-animation="expandIn" data-appear-animation-delay="600" style="width: 318px; height: 288px;" alt="" />
			</div>
		</div>
	</div>
</section>
@endforeach
{{-- end::foreach --}}

<!-- <div class="container appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="300">
	<div class="row pt-5 pb-4 my-5">
		<div class="col-md-6 order-2 order-md-1 text-center text-md-left">
			<div class="owl-carousel owl-theme nav-style-1 nav-center-images-only stage-margin mb-0" data-plugin-options="{'responsive': {'576': {'items': 1}, '768': {'items': 1}, '992': {'items': 2}, '1200': {'items': 2}}, 'margin': 25, 'loop': false, 'nav': true, 'dots': false, 'stagePadding': 40}">
				<div>
					<img class="img-fluid rounded-0 mb-4" src="/public/img/team/team-1.jpg" alt="" />
					<h3 class="font-weight-bold text-color-dark text-4 mb-0">John Doe</h3>
					<p class="text-2 mb-0">CEO</p>
				</div>
				<div>
					<img class="img-fluid rounded-0 mb-4" src="/public/img/team/team-2.jpg" alt="" />
					<h3 class="font-weight-bold text-color-dark text-4 mb-0">Jessica Doe</h3>
					<p class="text-2 mb-0">CEO</p>
				</div>
				<div>
					<img class="img-fluid rounded-0 mb-4" src="/public/img/team/team-3.jpg" alt="" />
					<h3 class="font-weight-bold text-color-dark text-4 mb-0">Chris Doe</h3>
					<p class="text-2 mb-0">DEVELOPER</p>
				</div>
				<div>
					<img class="img-fluid rounded-0 mb-4" src="/public/img/team/team-4.jpg" alt="" />
					<h3 class="font-weight-bold text-color-dark text-4 mb-0">Julie Doe</h3>
					<p class="text-2 mb-0">SEO ANALYST</p>
				</div>
				<div>
					<img class="img-fluid rounded-0 mb-4" src="/public/img/team/team-5.jpg" alt="" />
					<h3 class="font-weight-bold text-color-dark text-4 mb-0">Robert Doe</h3>
					<p class="text-2 mb-0">DESIGNER</p>
				</div>
			</div>
		</div>
		<div class="col-md-6 order-1 order-md-2 text-center text-md-left mb-5 mb-md-0">
			<h2 class="text-color-dark font-weight-normal text-6 mb-2 pb-1">Meet <strong class="font-weight-extra-bold">Our Team</strong></h2>
			<p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit massa enim. Nullam id varius nunc.</p>
			<p class="mb-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus blandit massa enim. Nullam id varius nunc. Vivamus bibendum magna ex, et faucibus lacus venenatis eget.</p>
		</div>
	</div>
</div> -->

<!-- <section class="section section-height-3 border-0 m-0">
	<div class="container pb-2">
		<div class="row">
			<div class="col-lg-6 text-center text-md-left mb-5 mb-lg-0">
				<h2 class="text-color-dark font-weight-normal text-6 mb-2">About <strong class="font-weight-extra-bold">Our Clients</strong></h2>
				<p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit massa enim. Nullam id varius nunc.</p>
				<div class="row justify-content-center my-5">
					<div class="col-8 text-center col-md-4">
						<img src="/public/img/client_logos/logo-1.png" class="img-fluid hover-effect-3" alt="" />
					</div>
					<div class="col-8 text-center col-md-4 my-3 my-md-0">
						<img src="/public/img/client_logos/logo-2.png" class="img-fluid hover-effect-3" alt="" />
					</div>
					<div class="col-8 text-center col-md-4">
						<img src="/public/img/client_logos/logo-3.png" class="img-fluid hover-effect-3" alt="" />
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="owl-carousel owl-theme nav-style-1 stage-margin" data-plugin-options="{'responsive': {'576': {'items': 1}, '768': {'items': 1}, '992': {'items': 1}, '1200': {'items': 1}}, 'loop': true, 'nav': false, 'dots': false, 'stagePadding': 40, 'autoplay': true, 'autoplayTimeout': 6000, 'loop': true}">
					<div>
						<div class="testimonial testimonial-style-2 testimonial-with-quotes testimonial-quotes-dark testimonial-remove-right-quote pl-md-4 mb-0">
							<div class="testimonial-author">
								<img src="img/clients/client-1.jpg" class="img-fluid rounded-circle mb-0" alt="">
							</div>
							<blockquote>
								<p class="text-color-dark text-4 line-height-5 mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi vitae metus tellus. Curabitur non lorem at odio tempus consectetur vel eu lacus. Morbi.</p>
							</blockquote>
							<div class="testimonial-author">
								<p><strong class="font-weight-extra-bold text-2">John Smith</strong><span>Okler</span></p>
							</div>
						</div>
					</div>
					<div>
						<div class="testimonial testimonial-style-2 testimonial-with-quotes testimonial-quotes-dark testimonial-remove-right-quote pl-md-4 mb-0">
							<div class="testimonial-author">
								<img src="img/clients/client-1.jpg" class="img-fluid rounded-circle mb-0" alt="">
							</div>
							<blockquote>
								<p class="text-color-dark text-4 line-height-5 mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi vitae metus tellus. Curabitur non lorem at odio tempus consectetur vel eu lacus. Morbi.</p>
							</blockquote>
							<div class="testimonial-author">
								<p><strong class="font-weight-extra-bold text-2">John Smith</strong><span>Okler</span></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section> -->

@endsection
