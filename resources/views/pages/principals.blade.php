@extends('layouts.default')

@section('content')

<section class="section section-white section-no-border section-height-0 my-0">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 mt-5">
            <h3 class="font-weight-extra-bold line-height-2 text-2 negative-ls-5 mb-2 big_red" style="text-transform: none !important;">Principals</h3>
                <h4 class="font-weight-bold line-height-2 text-2 negative-ls-5 mb-1 mt-1 big">The most complete principals at Project Titan </h4>
            </div>
            <div class="col-lg-7 mt-5 mt-lg-0">
                <div class="owl-carousel owl-theme nav-inside nav-inside-edge nav-squared nav-with-transparency nav-light mb-0" data-plugin-options="{'items': 1, 'margin': 10, 'loop': false, 'nav': true, 'dots': false}" style="height: auto;">
                    <div>
                        <div class="img-thumbnail border-0 p-0 d-block"><a href="/stores">
                            <img class="img-fluid border-radius-0" src="./public/img/stores/Hero-Image-Mall-Kelapa-Gading-GS-IDN-JKT-JKTURT-KEGA-1910A.jpg" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="section section-white section-no-border section-height-0 my-0">
    <div class="container">
        <div class="row mb-0">
        
        @foreach ($principals as $principal)
            <div class="col-lg-3 mt-5">
                <div class="card border-radius-2 bg-color-light border-1 box-shadow-2">
                    <div class="card-body text-center">
                        <img class="img-fluid border-radius-2 mb-3" src="./public/img/icons/{{ $principal->logo }}" alt="">
                        <h4 class="card-title mb-1 text-4 font-weight-bold">{{ $principal->name }}</h4>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>


<section class="section section-white section-no-border section-height-0 mt-5">
    <div class="container">
        <h4 class="font-weight-bold mb-3 mt-5">OTHER SERVICES</h4>
        <div class="row mb-0">
            <div class="col-lg-3">
                <div class="row">
                    <div class="col-lg-2">
                        <img class="img-fluid border-radius-0" src="./public/img/icons/Logo Principal Category - Bank.png" alt="">
                    </div>
                    <div class="col-lg-10">
                        <h4>Bank</h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-9">
                <ul class="social-icons logo mt-0">
                    @foreach (\App\Principals::where(['category_code' => '111BNK'])->get() as $principal)
                    <li><a href="/principal_list?code={{ $principal->code }}"><img class="img-fluid border-radius-0" src="./public/img/principals/logos/{{ $principal->img_logo }}" alt=""></a></li>
                    @endforeach
                </ul>
            </div>
        </div>
        <hr class="mt-2 mb-2" />
        <div class="row mb-0">
            <div class="col-lg-3">
                <div class="row">
                    <div class="col-lg-2">
                        <img class="img-fluid border-radius-0" src="./public/img/icons/Logo Principal Category - Multifinance companies.png" alt="">
                    </div>
                    <div class="col-lg-10">
                        <h4>Multifinance companies</h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-9">
                <ul class="social-icons logo mt-0">
                @foreach (\App\Principals::where(['category_code' => '211MFN'])->get() as $principal)
                    <li><a href="/principal_list?code={{ $principal->code }}"><img class="img-fluid border-radius-0" src="./public/img/principals/logos/{{ $principal->img_logo }}" alt=""></a></li>
                    @endforeach
                </ul>
            </div>
        </div>

        <hr class="mt-2 mb-2" />
        <div class="row mb-0">
            <div class="col-lg-3">
                <div class="row">
                    <div class="col-lg-2">
                        <img class="img-fluid border-radius-0" src="./public/img/icons/Logo Principal Category - Securities broker.png" alt="">
                    </div>
                    <div class="col-lg-10">
                        <h4>Securities broker</h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-9">
                <ul class="social-icons logo mt-0">
                @foreach (\App\Principals::where(['category_code' => '431SCB'])->get() as $principal)
                    <li><a href="/principal_list?code={{ $principal->code }}"><img class="img-fluid border-radius-0" src="./public/img/principals/logos/{{ $principal->img_logo }}" alt=""></a></li>
                    @endforeach
                </ul>
            </div>
        </div>

        <hr class="mt-2 mb-2" />
        <div class="row mb-0">
            <div class="col-lg-3">
                <div class="row">
                    <div class="col-lg-2">
                        <img class="img-fluid border-radius-0" src="./public/img/icons/Logo Principal Category - Insurance broker companies.png" alt="">
                    </div>
                    <div class="col-lg-10">
                        <h4>Insurance broker companies</h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-9">
                <ul class="social-icons logo mt-0">
                @foreach (\App\Principals::where(['category_code' => '551IRB'])->get() as $principal)
                <li><a href="/principal_list?code={{ $principal->code }}"><img class="img-fluid border-radius-0" src="./public/img/principals/logos/{{ $principal->img_logo }}" width="{{ $principal->img_width }}"></a></li>
                    @endforeach
                </ul>
            </div>
        </div>

        <hr class="mt-2 mb-2" />
        <div class="row mb-0">
            <div class="col-lg-3">
                <div class="row">
                    <div class="col-lg-2">
                        <img class="img-fluid border-radius-0" src="./public/img/icons/Logo Principal Category - Forex companies.png" alt="">
                    </div>
                    <div class="col-lg-10">
                        <h4>Forex companies</h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-9">
                <ul class="social-icons logo mt-0">
                @foreach (\App\Principals::where(['category_code' => '711FRX'])->get() as $principal)
                <li><a href="/principal_list?code={{ $principal->code }}"><img class="img-fluid border-radius-0" src="./public/img/principals/logos/{{ $principal->img_logo }}" width="{{ $principal->img_width }}"></a></li>
                    @endforeach
                </ul>
            </div>
        </div>

        <hr class="mt-2 mb-2" />
        <div class="row mb-0">
            <div class="col-lg-3">
                <div class="row">
                    <div class="col-lg-2">
                        <img class="img-fluid border-radius-0" src="./public/img/icons/Logo Principal Category - Remittance companies.png" alt="">
                    </div>
                    <div class="col-lg-10">
                        <h4>Remittance companies</h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-9">
                <ul class="social-icons logo mt-0">
                @foreach (\App\Principals::where(['category_code' => '721RMT'])->get() as $principal)
                <li><a href="/principal_list?code={{ $principal->code }}"><img class="img-fluid border-radius-0" src="./public/img/principals/logos/{{ $principal->img_logo }}" width="{{ $principal->img_width }}"></a></li>
                    @endforeach
                </ul>
            </div>
        </div>

        <hr class="mt-2 mb-2" />
        <div class="row mb-0">
            <div class="col-lg-3">
                <div class="row">
                    <div class="col-lg-2">
                        <img class="img-fluid border-radius-0" src="./public/img/icons/Logo Principal Category - E-wallet platform.png" alt="">
                    </div>
                    <div class="col-lg-10">
                        <h4>E-wallet platform</h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-9">
                <ul class="social-icons logo mt-0">
                @foreach (\App\Principals::where(['category_code' => '622EWP'])->get() as $principal)
                    <li><a href="/principal_list?code={{ $principal->code }}"><img class="img-fluid border-radius-0" src="./public/img/principals/logos/{{ $principal->img_logo }}" width="{{ $principal->img_width }}"></a></li>
                    @endforeach
                </ul>
            </div>
        </div>

    </div>
</section>
@endsection