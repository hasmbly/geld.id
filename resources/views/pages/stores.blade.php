@extends('layouts.default')

@section('content')

<section class="section section-white section-no-border section-height-0 mt-4 section-store-find">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <h5 class="text-3 mb-1 pb-1">Find Our Store</h5>
                <p>Our search filter can help you find the right category, principal, merchandise, or service.</p>
            </div>
            <div class="col-lg-1 pad-5"></div>
            <div class="col-lg-1 pad-5">
                <select class="form-control form-control-sm mb-3">
                    <option>Country</option>
                </select>
            </div>
            <div class="col-lg-1 pad-5">
                <select class="form-control form-control-sm mb-3">
                    <option>City</option>
                </select>
            </div>
            <div class="col-lg-1 pad-5">
                <select class="form-control form-control-sm mb-3">
                    <option>Category</option>
                </select>
            </div>
            <div class="col-lg-1 pad-5">
                <select class="form-control form-control-sm mb-3">
                    <option>Principal</option>
                </select>
            </div>
            <div class="col-lg-1 pad-5">
                <select class="form-control form-control-sm mb-3">
                    <option>Merchandise</option>
                </select>
            </div>
            <div class="col-lg-1 pad-5">
                <select class="form-control form-control-sm mb-4">
                    <option>Services</option>
                </select>
            </div>
            <div class="col-lg-1 pad-5 text-center">
                <button type="button" class="btn btn-sm btn-primary mb-1">FIND</button>
            </div>
        </div>
    </div>
</section>

<section class="section section-white section-no-border section-height-0 my-0 mb-4">
    <div class="container">
        @foreach ($stores as $store)
        <div class="row mt-4 mb-4">
            <div class="col-lg-6">
                <a href="/store_list?code={{ $store->code }}"><img class="img-fluid border-radius-0" src="./public/img/stores/{{ $store->photo }}" /></a>
            </div>
            <div class="col-lg-6">
                <div class="row border-bottom-dark">
                    <div class="col-lg-10">
                        <a href="/store_list?code={{ $store->code }}">
                            <h3 class="font-weight-extra-bold mb-0">{{ $store->name }}</h3>
                        </a>
                    </div>
                    <div class="col-lg-2">
                        <a href="/store_list?code={{ $store->code }}"><span class="badge badge-dark badge-sm badge-pill btn-primary text-uppercase mb-3">VISIT</span></a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 mb-1 border-bottom-dark">
                        {{ $store->address }}<br />{{ $store->city }}, {{ $store->province }}, {{ $store->country }} {{ $store->postal }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 mb-1 border-bottom-dark">
                        <a href="tel:{{ $store->phone_number }}"><i class="fa fa-phone text-color-primary"></i>{{ $store->phone_number }}</a> | <i class="fa fa-calendar text-color-primary mr-2"></i>{{ $store->opening_date }} | <i class="fa fa-clock text-color-primary mr-2"></i>{{ $store->opening_hour }}
                    </div>
                </div>

                <div class="row">
                                        <div class="col-lg-12 mb-1">
                                        <h6 class="mb-0 text-color-primary">Our Principal</h6>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <span class="mb-1">Bank Office Available</span>
                                            <div class="service_list">
                                            @foreach (\App\StoresPrincipalsBank::where(['store_code' => $store->code])->join('principals', 'principals.code', '=', 'principal_code')->get() as $office)
                                            <img class="border-radius-0 mb-2 mt-2 mr-1 ml-1" src="./public/img/principals/logos/{{ $office->img_logo }}" width="38%" style="width: 50% !important;" />
                                            @endforeach
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <span class="mb-1">Bank ATM Available</span>
                                            <div class="service_list" id="style-3">
                                            @foreach (\App\StoresPrincipalsAtm::where(['store_code' => $store->code])->join('principals', 'principals.code', '=', 'principal_code')->get() as $office)
                                            <img class="border-radius-0 mb-2 mt-2 mr-1 ml-1" src="./public/img/principals/logos/{{ $office->img_logo }}" width="38%" style="width: 50% !important;" />
                                            @endforeach
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <span>Promo on Store</span>
                                            <div class="service_list" id="style-3">
                                            @foreach (\App\StoresPromo::where(['stores_id' => $store->id])->join('promos', 'promos.id', '=', 'promo_id')->get() as $promo)
                                            <img class="border-radius-0 mb-2 mt-2 mr-1 ml-1" src="./public/img/promo/{{ $promo->img }}" width="38%" style="width: 50% !important;" />
                                            @endforeach
                                            </div>
                                        </div>
                                    </div>
            </div>
        </div>
        <hr />
        @endforeach
    </div>
</section>

@endsection