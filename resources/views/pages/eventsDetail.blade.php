@extends('layouts.default')

@section('content')

{{-- begin::page_home_section_1 --}}
<section class="section section-primary section-no-border section-height-0 my-0">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
            </div>
        </div>
    </div>
</section>
{{-- end::page_home_section_1 --}}

{{-- begin::page_home_section_2 --}}
<section class="section section-white section-no-border section-height-0 my-0">

<div class="container">
    <div class="row pt-4 justify-content-center">
        <div class="col-md-5 px-0">
            <a href="/page/events_list?title={{ $events->title }}">
                <img src="/public/img/events/{{ $events->img }}" class="img-fluid img-thumbnail img-thumbnail-no-borders rounded-0"  alt="" />
            </a>
        </div>
    </div>
</div>

<div class="container">
    <div class="row pt-4 justify-content-center">
        <div class="col text-center">            
            <h2 class="font-weight-bold text-7 line-height-5 mt-3 mb-1">{{ $events->title }}</h2>
                <div class="font-weight-bold text-6 line-height-5 mt-3 mb-1">{{ date('d F Y',strtotime($events->start_date)) }}</div>
                <div class="font-weight-bold text-5 line-height-5 mt-3 mb-1">{{ date('h:i A', strtotime($events->start_time)) }} - {{ date('h:i A', strtotime($events->end_time)) }}</div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row pt-5 justify-content-center">
        <div class="col-lg-9 ">
            <div class="font-weight-normal text-5 line-height-5 mt-1 mb-1">
                {!! $events->description !!}
            </div>
        </div>
    </div>
</div>

</section>
{{-- end::page_home_section_2 --}}

@endsection