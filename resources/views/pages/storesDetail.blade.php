@extends('layouts.default')

@section('content')
<section class="page-header bg-color-light-scale-1 mt-0">
					<div class="container">
						<div class="row">

							<div class="col">
								<div class="row">
									
									<div class="col-md-12 order-1 mt-2 mb-2">
										<ul data-appear-animation="fadeIn" data-appear-animation-delay="300" style="animation-delay: 300ms;" class="breadcrumb d-block">
											<!-- <li><a href="#"><</a></li> -->
											<li><span class="mr-2" style="font-size:1rem;"><</span><a href="/store_list">All Store</a> <span style="font-size:1rem;">/</span> {{ $store->name }}</li>
											<!-- <li class="active">Extra</li> -->
										</ul>
									</div>
								</div>
							</div>

						</div>
					</div>
				</section>

<section class="section section-white section-no-border section-height-0 my-0 mb-4">
    <div class="container">
        <div class="row pt-4">
            <div class="col-lg-6">
                <div class="owl-carousel owl-theme nav-inside nav-inside-edge nav-squared nav-with-transparency nav-light mb-0"
                    data-plugin-options="{'items': 1, 'margin': 10, 'loop': false, 'nav': true, 'dots': false}"
                    style="height: auto;">
                    <div>
                        <div class="img-thumbnail border-0 p-0 d-block">
                                <img class="img-fluid border-radius-0" src="./public/img/stores/{{ $store->photo }}" alt="" />
                        </div>
                    </div>
                    @foreach (\App\StoresImage::where(['store_code' => $store->code])->get() as $img)
                    <div>
                        <div class="img-thumbnail border-0 p-0 d-block">
                                <img class="img-fluid border-radius-0" src="./public/img/stores/{{ $img->photo }}" alt="" />
                        </div>
                    </div>
                    @endforeach
                </div>

                <h4 class="font-weight-bold mt-4 mb-2">PRINCIPAL</h4>
                <div class="row mb-2 mt-2">
                <div class="col-lg-12 border-bottom-light">
                    <div class="row">
                    <div class="col-lg-2"><h5 class="mb-1 mt-2">Bank</h3></div>
                    <div class="col-lg-10">
                    @foreach (\App\StoresPrincipalsBank::where(['store_code' => $store->code])->join('principals', 'principals.code', '=', 'principal_code')->get() as $office)
                    <img class="border-radius-0 mb-2 mt-2 mr-1 ml-1" src="./public/img/principals/logos/{{ $office->img_logo }}" width="{{ $office['img_width']}}" />
                        @endforeach
                    </div>                    
                </div>
                </div>
                </div>


                <div class="row mb-2 mt-2">
                <div class="col-lg-12 border-bottom-light">
                    <div class="row">
                    <div class="col-lg-2"><h5>ATM</h3></div>
                    <div class="col-lg-10">
                    @foreach (\App\StoresPrincipalsAtm::where(['store_code' => $store->code])->join('principals', 'principals.code', '=', 'principal_code')->get() as $office)
                    <img class="border-radius-0 mb-2 mt-2 mr-1 ml-1" src="./public/img/principals/logos/{{ $office->img_logo }}" width="{{ $office['img_width']}}" />
                        @endforeach
                    </div>                    
                    </div>
                </div>
                </div>

                <div class="row mb-2 mt-2">
                <div class="col-lg-12 border-bottom-light">
                    <div class="row">
                    <div class="col-lg-2"><h5 class="mb-1 mt-2">Others</h3></div>
                    <div class="col-lg-10">
                    @foreach (\App\StoresPrincipalsOther::where(['store_code' => $store->code])->join('principals', 'principals.code', '=', 'principal_code')->get() as $office)
                        <img class="border-radius-0 mb-2 mt-2 mr-1 ml-1" src="./public/img/principals/logos/{{ $office->img_logo }}" width="{{ $office['img_width']}}" />
                        @endforeach
                    </div>                    
                </div>
                </div>
                </div>

                <h4 class="font-weight-bold mt-5 mb-2">SERVICES</h4>
                @foreach (\App\StoresService::where(['store_code' => $store->code])->join('services', 'services.id', '=', 'service_id')->get() as $service)
                <div class="row border-bottom-light mt-2">
                    
                    <div class="col-lg-1">
                        <img class="img-fluid border-radius-0"
                            src="./public/img/icons/{{ $service->logo }}" width="80">
                    </div>
                    <div class="col-lg-11">
                        <h6 class="mb-2">{{ $service->name }}</h4>
                            <p class="mb-1">{{ $service->description }}</p>
                    </div>
                </div>
                @endforeach


                <h4 class="font-weight-bold mt-5 mb-2">AMENITIES</h4>
                @foreach (\App\StoresAmenities::where(['store_code' => $store->code])->join('amenities', 'amenities.id', '=', 'amenities_id')->get() as $amenities)
                <div class="row border-bottom-light mt-2">
                    
                    <div class="col-lg-1">
                        <img class="img-fluid border-radius-0"
                            src="./public/img/icons/{{ $amenities->logo }}" width="80">
                    </div>
                    <div class="col-lg-11">
                        <h6 class="mb-2">{{ $amenities->name }}</h6>
                            <p class="mb-1">{{ $amenities->description }}</p>
                    </div>
                </div>
                @endforeach

                <h2 class="font-weight-normal text-6 mb-1 mt-4">Others <strong class="font-weight-extra-bold">Store</strong></h2>
                <div class="owl-carousel owl-theme nav-inside nav-inside-edge nav-squared nav-with-transparency nav-light mb-2 mt-4"
                    data-plugin-options="{'items': 1, 'margin': 10, 'loop': true, 'nav': true, 'dots': true}"
                    style="height: auto;">
                    @foreach (\App\Stores::Where('code', '!=', "'$store->code'")->get() as $other_stores)
					<div class="mb-4">
						<a href="/store_list?code={{$other_stores->code}}">
							<span class="thumb-info thumb-info-centered-info thumb-info-no-borders">
								<span class="thumb-info-wrapper">
                                    <img class="img-fluid border-radius-0" src="./public/img/stores/{{ $other_stores->photo }}" alt="" />    
									<span class="thumb-info-title">
										<span class="thumb-info-inner">{{ $other_stores->name }}</span>
									</span>
								</span>
							</span>
						</a>
					</div>
                    @endforeach
                </div>
            </div>
            <div class="col-lg-6">
                <h3 class="font-weight-extra-bold mb-2 border-bottom-dark">{{ $store->name }}</h3>
                <div id="googlemapsFullWidthInside " class="google-map mt-0 mb-0" style="height: 280px;"></div>
                <div class="row">
                    <div class="col-lg-12 mb-1 border-bottom-dark">
                        {{ $store->address }}<br />{{ $store->city }}, {{ $store->province }}, {{ $store->country }} {{ $store->postal }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 mb-1">
                        <a href="tel:{{ $store->phone_number }}"><i class="fa fa-phone text-color-primary"></i>{{ $store->phone_number }}</a> | <i class="fa fa-calendar text-color-primary mr-2"></i>{{ $store->opening_date }} | <i class="fa fa-clock text-color-primary mr-2"></i>{{ $store->opening_hour }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection