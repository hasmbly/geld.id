@extends('layouts.default')

@section('content')

{{-- begin::page_home_section_1 --}}
<section class="section section-primary section-no-border section-height-0 my-0">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
            </div>
        </div>
    </div>
</section>
{{-- end::page_home_section_1 --}}

{{-- begin::page_home_section_2 --}}
<section class="section section-white section-no-border section-height-0 my-0">

<div class="container">
    <div class="row pt-4 justify-content-center">
        <div class="col-md-5 px-0">
            <a href="/page/promo_list?title={{ $promos->name }}">
                <img src="/public/img/promo/{{ $promos->img }}" class="img-fluid img-thumbnail img-thumbnail-no-borders rounded-0"  alt="" />
            </a>
        </div>
    </div>
</div>

<div class="container">
    <div class="row pt-4 justify-content-center">
        <div class="col text-center">            
            <h2 class="font-weight-bold text-7 line-height-5 mt-3 mb-1">{{ $promos->name }}</h2>
                <div class="font-weight-bold text-6 line-height-5 mt-3 mb-1">{{ date('d F',strtotime($promos->start_date)) }} - {{ date('d F Y',strtotime($promos->end_date)) }}</div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row pt-5 justify-content-center">
        <div class="col-lg-9 ">
            <div class="font-weight-normal text-5 line-height-5 mt-1 mb-5">
                {!! $promos->description !!}
            </div>
        </div>
    </div>
</div>

</section>
{{-- end::page_home_section_2 --}}

@endsection