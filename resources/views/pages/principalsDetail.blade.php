@extends('layouts.default')

@section('content')
<section class="page-header bg-color-light-scale-1 mt-0">
    <div class="container">
        <div class="row">

            <div class="col">
                <div class="row">

                    <div class="col-md-12 order-1 mt-2 mb-2">
                        <ul data-appear-animation="fadeIn" data-appear-animation-delay="300" style="animation-delay: 300ms;" class="breadcrumb d-block">
                            <!-- <li><a href="#"><</a></li> -->
                            <li><span class="mr-2" style="font-size:1rem;">
                                    <</span> <a href="/principal_list">All Principals</a> <span style="font-size:1rem;">/</span> {{ $principal->brand_name }}</li>
                                        <!-- <li class="active">Extra</li> -->
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section class="section section-white section-no-border section-height-0 my-0 mb-4">
    <div class="container">
        <div class="row pt-4">
            <div class="col-lg-2">
                <img class="img-fluid border-radius-0" src="./public/img/principals/logos/{{ $principal->img_logo }}" alt="" />
            </div>
            <div class="col-lg-8" style="margin: auto !important;">
                <h3 class="font-weight-extra-bold mt-0 mb-0">{{ $principal->brand_name }}</h3>
            </div>
        </div>
        <div class="row pt-4">
            <div class="col-lg-12">
                <img class="img-fluid border-radius-0 ml-4 mb-4 mt-4" style="float: right;" src="./public/img/principals/offices/{{ $principal->img_office }}" width="250" />
                {!! $principal->content !!}
            </div>
        </div>
    </div>
</section>


<section class="section section-white section-no-border section-height-0 my-0 mb-4">
    <div class="container">
        <h3 class="mt-5 mb-4 text-color-primary" style="text-transform: none !important;">This Principal is available in the following stores</h3>
        @foreach ($stores as $store)
        <div class="row mt-3 mb-3">
            <div class="col-lg-5">
                <div class="row mb-2">
                    <div class="col-lg-12">
                        <a href="/store_list?code={{ $store->code }}">
                            <h4 class="font-weight-extra-bold mb-0">{{ $store->name }}</h4>
                        </a>
                    </div>
                    <!-- <div class="col-lg-2">
                        <a href="/stores?code={{ $store->code }}"><span class="badge badge-dark badge-sm badge-pill btn-primary text-uppercase mb-3">VISIT</span></a>
                    </div> -->
                </div>
                <div class="row">
                    <div class="col-lg-12 mb-1 border-bottom-dark">
                        {{ $store->address }}<br />{{ $store->city }}, {{ $store->province }}, {{ $store->country }} {{ $store->postal }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 mb-1">
                        <a href="tel:{{ $store->phone_number }}"><i class="fa fa-phone text-color-primary"></i>{{ $store->phone_number }}</a> <br /><i class="fa fa-calendar text-color-primary mr-2"></i>{{ $store->opening_date }} | <i class="fa fa-clock text-color-primary mr-2"></i>{{ $store->opening_hour }}
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <h4 class="mt-1 mb-1" style="text-transform: none !important;">Service Type: </h4>
                <div class="service_list">
                <ul class="list list-icons list-icons-lg">
                @foreach (\App\StoresService::where(['store_code' => $store->code])->join('services', 'services.id', '=', 'service_id')->get() as $service)
                    <li class="mb-0 ml-null border-bottom-light"><img class="border-radius-0 mb-0 mt-0 mr-1 ml-1" width="20" src="./public/img/icons/{{ $service->logo }}" />{{ $service->name }}</li>
                @endforeach
                </ul>
                </div>
            </div>
            <div class="col-lg-4">
                <a href="/store_list?code={{ $store->code }}"><img class="img-fluid border-radius-0" src="./public/img/stores/{{ $store->photo }}" /></a>
            </div>
        </div>
        <hr class="solid my-2">
        @endforeach
    </div>
</section>

<section class="section section-white section-no-border section-height-0 my-0 mb-4">
    <div class="container">
<div class="row pt-4 mt-2 mb-5">
<div class="col-md-12">
							<h2 class="text-color-dark font-weight-normal text-5 mb-2">Principal <strong class="font-weight-extra-bold">Details</strong></h2>
							<ul class="list list-icons list-primary list-borders text-2">
                                <li><i class="fas fa-caret-right left-10"></i> <strong class="text-color-primary">Principal Legal Name:</strong> {{ $principal->principal_legal_name }}</li>
								<li><i class="fas fa-caret-right left-10"></i> <strong class="text-color-primary">Head Office Address:</strong> {{ $principal->address }}</li>
								<li><i class="fas fa-caret-right left-10"></i> <strong class="text-color-primary">Website:</strong> <a href="{{ $principal->url_website }}" target="_blank"> {{ $principal->url_website }}</a></li>
								<!-- <li><i class="fas fa-caret-right left-10"></i> <strong class="text-color-primary">Majority Shareholder Legal Name:</strong> {{ $principal->shareholder_legal_name }}</li>
                                <li><i class="fas fa-caret-right left-10"></i> <strong class="text-color-primary">Majority Shareholder is an Operating Financial Service Company :</strong> {{ $principal->is_operating_fin_sservice }}</li>
                                <li><i class="fas fa-caret-right left-10"></i> <strong class="text-color-primary">If Majority Shareholder is an Operating Financial Service Company, is Owner a Partner of GeldStadt:</strong> {{ $principal->is_owner_our_partner }}</li>
                                <li><i class="fas fa-caret-right left-10"></i> <strong class="text-color-primary">if Majority Shareholder is a Partner of GeldStadt, what is Owner's Principal Code:</strong> {{ $principal->reference_principal_code }}</li>
                                <li><i class="fas fa-caret-right left-10"></i> <strong class="text-color-primary">Principal Located in Multiple Countries:</strong> {{ $principal->is_multiple_country }}</li>
                                <li><i class="fas fa-caret-right left-10"></i> <strong class="text-color-primary">Principal Head Office Country:</strong> {{ $principal->country_head_office }}</li>
                                <li><i class="fas fa-caret-right left-10"></i> <strong class="text-color-primary">Principal Country:</strong> {{ $principal->country_located }}</li> -->
							</ul>
						</div>
						<!-- <div class="col-md-6">
							<h2 class="text-color-dark font-weight-normal text-5 mb-2">&nbsp;</h2>
							<ul class="list list-icons list-primary list-borders text-2">
                            <li><i class="fas fa-caret-right left-10"></i> <strong class="text-color-primary">Principal Has Online Transaction Service:</strong> {{ $principal->is_online_trx }}</li>
                                <li><i class="fas fa-caret-right left-10"></i> <strong class="text-color-primary">Principal Online Transaction Service Web URL for Customers:</strong> <a href="{{ $principal->url_for_customer }}" target="_blank">{{ $principal->url_for_customer }}</a></li>
                                <li><i class="fas fa-caret-right left-10"></i> <strong class="text-color-primary">Principal Online Transaction Service Web URL for Businesses:</strong> <a href="{{ $principal->url_for_business }}" target="_blank">{{ $principal->url_for_business }}</a></li>
                                <li><i class="fas fa-caret-right left-10"></i> <strong class="text-color-primary">Principal Call Center Number:</strong> {{ $principal->call_center_number }}</li>
                                <li><i class="fas fa-caret-right left-10"></i> <strong class="text-color-primary">Principal Corporate E-mail:</strong> {{ $principal->email_corporate }}</li>
                                <li><i class="fas fa-caret-right left-10"></i> <strong class="text-color-primary">Source of Annual Report:</strong> <a href="{{ $principal->url_annual_report }}" target="_blank">{{ $principal->url_annual_report }}</a></li>
							</ul>
						</div> -->
                    </div>
</div>
</div>
@endsection