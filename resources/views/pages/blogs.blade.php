@extends('layouts.default')

@section('content')

<div class="container py-4">

    <div class="row">
        <div class="col">
            <div class="blog-posts">

                <div class="masonry-loader masonry-loader-showing">
                    <div class="masonry row" data-plugin-masonry data-plugin-options="{'itemSelector': '.masonry-item'}">

                    @foreach ($blogs as $blog)
                        <div class="masonry-item no-default-style col-md-4">
                            <article class="post post-medium border-0 pb-0 mb-5">
                                <div class="post-image">
                                    <a href="/page/blogs/{{ $blog->id }}">
                                        <img src="{{ url('/assets/media/blog/') }}/{{ $blog->img_tumbnail }}" class="img-fluid img-thumbnail img-thumbnail-no-borders rounded-0" alt="" />
                                    </a>
                                </div>

                                <div class="post-content">

                                    <h2 class="font-weight-semibold text-5 line-height-6 mt-3 mb-2"><a href="blog-post.html">{{ $blog->title }}</a></h2>
                                    <p>{!! $blog->intro !!}</p>

                                    <div class="post-meta">
                                        <span><i class="far fa-user"></i> By <a href="#">{{ $blog->created_by }}</a> </span>
                                        <span><i class="far fa-folder"></i> <a href="#">News</a>, <a href="#">{{ $blog->tags }}</a> </span>
                                        <span class="d-block mt-2"><a href="/page/blogs/{{ $blog->id }}" class="btn btn-xs btn-light text-1 text-uppercase">Read More</a></span>
                                    </div>

                                </div>
                            </article>
                        </div>
                        @endforeach

                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <ul class="pagination float-right">
                            <li class="page-item"><a class="page-link" href="#"><i class="fas fa-angle-left"></i></a></li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <a class="page-link" href="#"><i class="fas fa-angle-right"></i></a>
                        </ul>
                    </div>
                </div>

            </div>
        </div>

    </div>

</div>

@endsection