<footer id="footer" class="mt-0">
        <div class="container">
            <div class="row py-1 my-1">
            <div class="col-md-12 col-lg-12 mb-2 mb-lg-0 pt-3">
            <img alt="TitanSpace" class="mb-2" src="{{ url('/public/img/logo.png') }}" width="75" />
            </div>
                <div class="col-md-7 col-lg-3 mb-0 mb-lg-0 pt-3">
                        @foreach ($contact_us as $contact)
                        <ul class="list list-icons list-icons-lg">
                                <li class="mb-1"><a href="tel:+62 811-1681-180"><i class="fa fa-phone text-color-primary"></i><p class="m-0">{{ $contact->phone }}</p></a></li>
                                <li class="mb-1"><a href="tel:+62 811-1681-180"><i class="fab fa-whatsapp text-5 text-color-primary"></i><p class="m-0">{{ $contact->whatsapp }}</p></a></li>
                                <li class="mb-1"><a href="mailto:contact.us@geldstadt.com"><i class="fa fa-envelope text-5 text-color-primary"></i><p class="m-0">{{ $contact->email }}</p></a></li>
                            </ul>
                            <h5 class="text-3 mb-1 pb-1">Business Hours</h5>
                                {!! $contact->business_hours !!}
                            @endforeach
                </div>
                <div class="col-md-5 col-lg-2 mb-0 mb-lg-0 pt-3 mt-0">
                        <ul class="list list-icons">
                                <li class="mb-1 ml-null"><a href="/page/blogs">BLOG</a></li>
                                <li class="mb-1 ml-null"><a href="/page/news">NEWS</a></li>
                                <li class="mb-1 ml-null"><a href="/page/gallery">GALLERY</a></li>
                                <li class="mb-1 ml-null"><a href="/page/faq">FAQ</a></li>
                                <li class="mb-1 ml-null"><a href="/page/contact_us">CONTACT US</a></li>
                            </ul>
                </div>
                <div class="col-md-5 col-lg-2 mb-0 mb-lg-0 pt-3 mt-0">
                        <ul class="list list-icons">
                                <li class="mb-1 ml-null"><a href="#">FOR SITE OWNER</a></li>
                                <li class="mb-1 ml-null"><a href="#">FOR PRINCIPALS</a></li>
                                <li class="mb-1 ml-null"><a href="#">CAREER</a></li>
                                <li class="mb-1 ml-null"><a href="#">TEAM</a></li>
                                <li class="mb-1 ml-null"><a href="/page/about_us">ABOUT US</a></li>
                            </ul>
                </div>
                <div class="col-md-1 col-lg-1 mb-0 mb-lg-0 pt-3 mt-0"></div>
                <div class="col-md-5 col-lg-4 mb-0 mb-lg-0 pt-3 mt-0">
                        <p class="text-3 mb-1">Subscribe for updates</p>
                        <form id="newsletterForm" action="php/newsletter-subscribe.php" method="POST" class="mr-4 mb-3 mb-md-0" novalidate="novalidate">
                            <div class="input-group input-group-rounded">
                                <input class="form-control form-control-sm bg-light" placeholder="Your email address" name="newsletterEmail" id="newsletterEmail" type="text">
                                <span class="input-group-append">
                                    <button class="btn btn-light text-color-dark" type="submit"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
                                </span>
                            </div>
                        </form>
                        <ul class="social-icons mt-4">
                                <li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
                                <li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
                                <li class="social-icons-instagram"><a href="http://www.instagram.com/" target="_blank" title="Instagram"><i class="fab fa-instagram"></i></a></li>
                                <li class="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fab fa-linkedin-in"></i></a></li>
                            </ul>
                            <div class="row mt-3">
                                <div class="col-12">
                                    <a href="#" target="_blank" id="btn-google-play" class="btn-app"><img src="{{ url('/public/img/btn-dl-google-play.svg') }}" alt="Google Play" width="98" height="30"></a>
                                    <a href="#" target="_blank" id="btn-app-store" class="btn-app"><img src="{{ url('/public/img/btn-dl-app-store.svg') }}" alt="App Store" width="108" height="30"></a>
                                </div>
                                </div>
                        <p class="mt-2">&copy; Copyright 2019 GeldStadt, a brand of Trast Distribusi Digital. Financial Center for All</p>
                </div>
            </div>
        </div>
    </footer>