<section class="section section-white section-no-border section-height-0 mt-5">
    <div class="container">
        <h4 class="font-weight-bold mb-3 mt-5">OTHER SERVICES</h4>
        <div class="row mb-0">
            <div class="col-lg-3">
                <div class="row">
                    <div class="col-lg-2">
                        <img class="img-fluid border-radius-0"
                            src="{{ 'assets/public/img/icon/Logo-Amenities-Cleaning-Team.png'|theme }}" alt="">
                    </div>
                    <div class="col-lg-10">
                        <h4>Bank</h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-9">
                <ul class="social-icons logo mt-0">
                    <li><a href="/principal/detail_1"><img class="img-fluid border-radius-0"
                                src="{{ 'assets/public/img/logo/Logo-Bank-Amar.jpg'|theme }}" alt=""></a></li>
                    <li><a href="#"><img class="img-fluid border-radius-0"
                                src="{{ 'assets/public/img/logo/Logo-Bank-Amar.jpg'|theme }}" alt=""></a></li>
                    <li><a href="#"><img class="img-fluid border-radius-0"
                                src="{{ 'assets/public/img/logo/Logo-Bank-Amar.jpg'|theme }}" alt=""></a></li>
                    <li><a href="#"><img class="img-fluid border-radius-0"
                                src="{{ 'assets/public/img/logo/Logo-Bank-Amar.jpg'|theme }}" alt=""></a></li>
                    <li><a href="#"><img class="img-fluid border-radius-0"
                                src="{{ 'assets/public/img/logo/Logo-Bank-Amar.jpg'|theme }}" alt=""></a></li>
                    <li><a href="#"><img class="img-fluid border-radius-0"
                                src="{{ 'assets/public/img/logo/Logo-Bank-Amar.jpg'|theme }}" alt=""></a></li>
                    <li><a href="#"><img class="img-fluid border-radius-0"
                                src="{{ 'assets/public/img/logo/Logo-Bank-Amar.jpg'|theme }}" alt=""></a></li>
                    <li><a href="#"><img class="img-fluid border-radius-0"
                                src="{{ 'assets/public/img/logo/Logo-Bank-Amar.jpg'|theme }}" alt=""></a></li>
                    <li><a href="#"><img class="img-fluid border-radius-0"
                                src="{{ 'assets/public/img/logo/Logo-Bank-Amar.jpg'|theme }}" alt=""></a></li>
                    <li><a href="#"><img class="img-fluid border-radius-0"
                                src="{{ 'assets/public/img/logo/Logo-Bank-Amar.jpg'|theme }}" alt=""></a></li>
                    <li><a href="#"><img class="img-fluid border-radius-0"
                                src="{{ 'assets/public/img/logo/Logo-Bank-Amar.jpg'|theme }}" alt=""></a></li>
                </ul>
            </div>
        </div>
        <hr class="mt-2 mb-2" />
        <div class="row mb-0">
            <div class="col-lg-3">
                <div class="row">
                    <div class="col-lg-2">
                        <img class="img-fluid border-radius-0"
                            src="{{ 'assets/public/img/icon/Logo-Amenities-Cleaning-Team.png'|theme }}" alt="">
                    </div>
                    <div class="col-lg-10">
                        <h4>Bank</h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-9">
                <ul class="social-icons logo mt-0">
                    <li><a href="#"><img class="img-fluid border-radius-0"
                                src="{{ 'assets/public/img/logo/Logo-Bank-Amar.jpg'|theme }}" alt=""></a></li>
                    <li><a href="#"><img class="img-fluid border-radius-0"
                                src="{{ 'assets/public/img/logo/Logo-Bank-Amar.jpg'|theme }}" alt=""></a></li>
                    <li><a href="#"><img class="img-fluid border-radius-0"
                                src="{{ 'assets/public/img/logo/Logo-Bank-Amar.jpg'|theme }}" alt=""></a></li>
                    <li><a href="#"><img class="img-fluid border-radius-0"
                                src="{{ 'assets/public/img/logo/Logo-Bank-Amar.jpg'|theme }}" alt=""></a></li>
                    <li><a href="#"><img class="img-fluid border-radius-0"
                                src="{{ 'assets/public/img/logo/Logo-Bank-Amar.jpg'|theme }}" alt=""></a></li>
                    <li><a href="#"><img class="img-fluid border-radius-0"
                                src="{{ 'assets/public/img/logo/Logo-Bank-Amar.jpg'|theme }}" alt=""></a></li>
                    <li><a href="#"><img class="img-fluid border-radius-0"
                                src="{{ 'assets/public/img/logo/Logo-Bank-Amar.jpg'|theme }}" alt=""></a></li>
                    <li><a href="#"><img class="img-fluid border-radius-0"
                                src="{{ 'assets/public/img/logo/Logo-Bank-Amar.jpg'|theme }}" alt=""></a></li>
                    <li><a href="#"><img class="img-fluid border-radius-0"
                                src="{{ 'assets/public/img/logo/Logo-Bank-Amar.jpg'|theme }}" alt=""></a></li>
                    <li><a href="#"><img class="img-fluid border-radius-0"
                                src="{{ 'assets/public/img/logo/Logo-Bank-Amar.jpg'|theme }}" alt=""></a></li>
                    <li><a href="#"><img class="img-fluid border-radius-0"
                                src="{{ 'assets/public/img/logo/Logo-Bank-Amar.jpg'|theme }}" alt=""></a></li>
                </ul>
            </div>
        </div>

        <hr class="mt-2 mb-2" />
        <div class="row mb-0">
            <div class="col-lg-3">
                <div class="row">
                    <div class="col-lg-2">
                        <img class="img-fluid border-radius-0"
                            src="{{ 'assets/public/img/icon/Logo-Amenities-Cleaning-Team.png'|theme }}" alt="">
                    </div>
                    <div class="col-lg-10">
                        <h4>Bank</h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-9">
                <ul class="social-icons logo mt-0">
                    <li><a href="#"><img class="img-fluid border-radius-0"
                                src="{{ 'assets/public/img/logo/Logo-Bank-Amar.jpg'|theme }}" alt=""></a></li>
                    <li><a href="#"><img class="img-fluid border-radius-0"
                                src="{{ 'assets/public/img/logo/Logo-Bank-Amar.jpg'|theme }}" alt=""></a></li>
                    <li><a href="#"><img class="img-fluid border-radius-0"
                                src="{{ 'assets/public/img/logo/Logo-Bank-Amar.jpg'|theme }}" alt=""></a></li>
                    <li><a href="#"><img class="img-fluid border-radius-0"
                                src="{{ 'assets/public/img/logo/Logo-Bank-Amar.jpg'|theme }}" alt=""></a></li>
                    <li><a href="#"><img class="img-fluid border-radius-0"
                                src="{{ 'assets/public/img/logo/Logo-Bank-Amar.jpg'|theme }}" alt=""></a></li>
                    <li><a href="#"><img class="img-fluid border-radius-0"
                                src="{{ 'assets/public/img/logo/Logo-Bank-Amar.jpg'|theme }}" alt=""></a></li>
                    <li><a href="#"><img class="img-fluid border-radius-0"
                                src="{{ 'assets/public/img/logo/Logo-Bank-Amar.jpg'|theme }}" alt=""></a></li>
                    <li><a href="#"><img class="img-fluid border-radius-0"
                                src="{{ 'assets/public/img/logo/Logo-Bank-Amar.jpg'|theme }}" alt=""></a></li>
                    <li><a href="#"><img class="img-fluid border-radius-0"
                                src="{{ 'assets/public/img/logo/Logo-Bank-Amar.jpg'|theme }}" alt=""></a></li>
                    <li><a href="#"><img class="img-fluid border-radius-0"
                                src="{{ 'assets/public/img/logo/Logo-Bank-Amar.jpg'|theme }}" alt=""></a></li>
                    <li><a href="#"><img class="img-fluid border-radius-0"
                                src="{{ 'assets/public/img/logo/Logo-Bank-Amar.jpg'|theme }}" alt=""></a></li>
                </ul>
            </div>
        </div>
    </div>
</section>