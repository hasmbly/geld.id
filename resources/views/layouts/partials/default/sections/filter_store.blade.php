<section class="section section-white section-no-border section-height-0 my-0">
    <div class="container">
        <div class="row pt-4">
            <div class="col-lg-4">
                <h5 class="text-3 mb-1 pb-1">Find Our Store</h5>
                <p>Our search filter can help you find the right category, principal, merchandise, or service.</p>
            </div>
            <div class="col-lg-1 pad-5"></div>
            <div class="col-lg-1 pad-5">
                <select class="form-control form-control-sm mb-3">
                    <option>Country</option>
                </select>
            </div>
            <div class="col-lg-1 pad-5">
                <select class="form-control form-control-sm mb-3">
                    <option>City</option>
                </select>
            </div>
            <div class="col-lg-1 pad-5">
                <select class="form-control form-control-sm mb-3">
                    <option>Category</option>
                </select>
            </div>
            <div class="col-lg-1 pad-5">
                <select class="form-control form-control-sm mb-3">
                    <option>Principal</option>
                </select>
            </div>
            <div class="col-lg-1 pad-5">
                <select class="form-control form-control-sm mb-3">
                    <option>Merchandise</option>
                </select>
            </div>
            <div class="col-lg-1 pad-5">
                <select class="form-control form-control-sm mb-4">
                    <option>Services</option>
                </select>
            </div>
            <div class="col-lg-1 pad-5 text-center">
                <button type="button" class="btn btn-sm btn-primary mb-1">FIND</button>
            </div>
        </div>
    </div>
</section>