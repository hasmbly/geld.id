<section class="section section-primary section-no-border section-height-0 my-0">
        <div class="container">
                <div class="row pt-4">
                        <div class="col-lg-12">
                                <div class="mb-2">
                                        <div class="owl-carousel owl-theme nav-inside nav-inside-edge nav-squared nav-with-transparency nav-light mb-0" 
                                        data-plugin-options="{'items': 1, 'margin': 10, 'loop': false, 'nav': true, 'dots': false}" style="height: auto;">
                                                <div class="row pt-1">
                                                        <div class="col-lg-6">
                                                            <img class="img-fluid border-radius-0" src="{{ 'assets/public/img/stores/Hero-Image-Grand-Indo-GS-IDN-JKT-JKTPST-GRID-1910A.jpg'|theme }}" alt="" />
                                                        </div>
                                                        <div class="col-lg-6">
                                                                <img class="img-fluid border-radius-0" src="{{ 'assets/public/img/stores/Hero-Image-Mall-Kelapa-Gading-GS-IDN-JKT-JKTURT-KEGA-1910A.jpg'|theme }}" alt="" />
                                                        </div>
                                                    </div>
                                                    <div class="row pt-4">
                                                            <div class="col-lg-6">
                                                                    <img class="img-fluid border-radius-0" src="{{ 'assets/public/img/stores/Hero-Image-Grand-Indo-GS-IDN-JKT-JKTPST-GRID-1910A.jpg'|theme }}" alt="" />
                                                                </div>
                                                                <div class="col-lg-6">
                                                                        <img class="img-fluid border-radius-0" src="{{ 'assets/public/img/stores/Hero-Image-Mall-Kelapa-Gading-GS-IDN-JKT-JKTURT-KEGA-1910A.jpg'|theme }}" alt="" />
                                                                </div>
                                                        </div>
										</div>
									</div>
                        </div>
                    </div>
            </div>
</section>
