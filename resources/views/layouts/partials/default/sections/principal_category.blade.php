<section class="section section-white section-no-border section-height-0 my-0">
    <div class="container">
        <div class="row mb-0">
            <div class="col-lg-3 mt-5">
                    <div class="card border-radius-2 bg-color-light border-1 box-shadow-2">
                            <div class="card-body text-center">
                                    <img class="img-fluid border-radius-2 mb-3" src="{{ 'assets/public/img/icon/Logo-Services-Bank-Office.png'|theme }}" alt="">
                                <h4 class="card-title mb-1 text-4 font-weight-bold">Bank</h4>
                            </div>
                        </div>
            </div>
            <div class="col-lg-3 mt-5">
                    <div class="card border-radius-2 bg-color-light border-1 box-shadow-2">
                            <div class="card-body text-center">
                                    <img class="img-fluid border-radius-2 mb-3" src="{{ 'assets/public/img/icon/Logo-Services-Bank-Office.png'|theme }}" alt="">
                                <h4 class="card-title mb-1 text-4 font-weight-bold">Multifinance</h4>
                            </div>
                        </div>
            </div>
            <div class="col-lg-3 mt-5">
                    <div class="card border-radius-2 bg-color-light border-1 box-shadow-2">
                            <div class="card-body text-center">
                                    <img class="img-fluid border-radius-2 mb-3" src="{{ 'assets/public/img/icon/Logo-Services-Bank-Office.png'|theme }}" alt="">
                                <h4 class="card-title mb-1 text-4 font-weight-bold">P2P Platform</h4>
                            </div>
                        </div>
            </div>
            <div class="col-lg-3 mt-5">
                    <div class="card border-radius-2 bg-color-light border-1 box-shadow-2">
                            <div class="card-body text-center">
                                    <img class="img-fluid border-radius-2 mb-3" src="{{ 'assets/public/img/icon/Logo-Services-Bank-Office.png'|theme }}" alt="">
                                <h4 class="card-title mb-1 text-4 font-weight-bold">Securities Broker</h4>
                            </div>
                        </div>
            </div>
        </div>

        <div class="row mb-5">
                <div class="col-lg-3 mt-5">
                        <div class="card border-radius-2 bg-color-light border-1 box-shadow-2">
                                <div class="card-body text-center">
                                        <img class="img-fluid border-radius-2 mb-3" src="{{ 'assets/public/img/icon/Logo-Services-Bank-Office.png'|theme }}" alt="">
                                    <h4 class="card-title mb-1 text-4 font-weight-bold">Insurance Broker</h4>
                                </div>
                            </div>
                </div>
                <div class="col-lg-3 mt-5">
                        <div class="card border-radius-2 bg-color-light border-1 box-shadow-2">
                                <div class="card-body text-center">
                                        <img class="img-fluid border-radius-2 mb-3" src="{{ 'assets/public/img/icon/Logo-Services-Bank-Office.png'|theme }}" alt="">
                                    <h4 class="card-title mb-1 text-4 font-weight-bold">Money Changer</h4>
                                </div>
                            </div>
                </div>
                <div class="col-lg-3 mt-5">
                        <div class="card border-radius-2 bg-color-light border-1 box-shadow-2">
                                <div class="card-body text-center">
                                        <img class="img-fluid border-radius-2 mb-3" src="{{ 'assets/public/img/icon/Logo-Services-Bank-Office.png'|theme }}" alt="">
                                    <h4 class="card-title mb-1 text-4 font-weight-bold">Remittance</h4>
                                </div>
                            </div>
                </div>
                <div class="col-lg-3 mt-5">
                        <div class="card border-radius-2 bg-color-light border-1 box-shadow-2">
                                <div class="card-body text-center">
                                        <img class="img-fluid border-radius-2 mb-3" src="{{ 'assets/public/img/icon/Logo-Services-Bank-Office.png'|theme }}" alt="">
                                    <h4 class="card-title mb-1 text-4 font-weight-bold">E-Wallet</h4>
                                </div>
                            </div>
                </div>
            </div>
    </div>
</section>