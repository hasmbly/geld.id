<section class="section section-white section-no-border section-height-0 my-0 mb-4">
        <div class="container">
                <div class="row pt-4">
                        <div class="col-lg-6">
                                <img class="img-fluid border-radius-0" src="./public/img/stores/Hero-Image-Grand-Indo-GS-IDN-JKT-JKTPST-GRID-1910A.jpg" />
                        </div>
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-10">
                                    <ul class="list list-icons">
                                        <li class="no-padding"><h4 class="font-weight-extra-bold">Menara Topas</h4></li>
                                        <li class="no-padding">Jenderal Sudirman Kav 21 Level 24 Jakarta Pusat</li>
                                        <li class="no-padding">021-841 922 213</li>
                                    </ul>
                                </div>
                                <div class="col-lg-2">
                                    <a href="/stores/detail_1"><span class="badge badge-dark badge-sm badge-pill text-uppercase px-2 py-1 mr-1">VISIT</span></a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    <h5 class="text-3 mb-1 pb-1">Bank Office Available</h5>
                                    <img class="img-fluid1 border-radius-0" src="./public/img/logo/Logo-Bank-Amar.jpg" width="70" />
                                    <img class="img-fluid1 border-radius-0" src="./public/img/logo/Logo-Bank-BJB.jpg" width="80" />
                                    <img class="img-fluid1 border-radius-0" src="./public/img/logo/Logo-Bank-BTPN.jpeg" width="80" />
                                    <img class="img-fluid1 border-radius-0" src="./public/img/logo/Logo-Bank-Bukopin.jpg" width="70" />
                                </div>
                                <div class="col-lg-4">
                                    <h5 class="text-3 mb-1 pb-1">Bank ATM Available</h5>
                                    <img class="img-fluid1 border-radius-0" src="./public/img/logo/Logo-Bank-Amar.jpg" width="70" />
                                    <img class="img-fluid1 border-radius-0" src="./public/img/logo/Logo-Bank-BJB.jpg" width="80" />
                                    <img class="img-fluid1 border-radius-0" src="./public/img/logo/Logo-Bank-BTPN.jpeg" width="80" />
                                    <img class="img-fluid1 border-radius-0" src="./public/img/logo/Logo-Bank-Bukopin.jpg" width="70" />
                                </div>
                                <div class="col-lg-4">
                                    <h5 class="text-3 mb-1 pb-1">Promo on Store</h5>
                                    <img class="img-fluid1 border-radius-0" src="./public/img/promo/promo_1.png" width="150" />
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
</section>
