<section class="section section-primary section-no-border section-height-0 my-0">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <h1 class="font-weight-bold line-height-7 text-2 negative-ls-5 mb-1 mt-3 big">AGE OF</h1>
                    <h1 class="font-weight-bold line-height-2 text-2 negative-ls-5 mb-2 big_red">COLLABORATION</h1>
                    <p class="text-color-dark opacity-8 text-4 mb-4">A more connected world means a more collaborative world, we are making it happen for financial services</p>
                    <a href="#" class="btn btn-dark font-weight-semibold btn-px-3 btn-py-2 text-2 rounded-0 mb-2">MORE</a>
                </div>
                <div class="col-lg-8 mt-5 mt-lg-0">
                        <div class="owl-carousel owl-theme nav-inside nav-inside-edge nav-squared nav-with-transparency nav-light mb-0" 
                        data-plugin-options="{'items': 1, 'margin': 10, 'loop': false, 'nav': true, 'dots': false}" style="height: auto;">
                                <div>
                                    <div class="img-thumbnail border-0 p-0 d-block">
                                        <img class="img-fluid border-radius-0" src="{{ 'assets/public/img/stores/Hero-Image-Grand-Indo-GS-IDN-JKT-JKTPST-GRID-1910A.jpg'|theme }}" alt="" />
                                    </div>
                                </div>
                                <div>
                                    <div class="img-thumbnail border-0 p-0 d-block">
                                        <img class="img-fluid border-radius-0" src="{{ 'assets/public/img/stores/Hero-Image-Mall-Kelapa-Gading-GS-IDN-JKT-JKTURT-KEGA-1910A.jpg'|theme }}" alt="" />
                                    </div>
                                </div>
                            </div>
                </div>
            </div>
        </div>
    </section>