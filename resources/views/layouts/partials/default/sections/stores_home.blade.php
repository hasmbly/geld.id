<section class="section section-white section-no-border section-height-0 my-0">
        <div class="container">
                <div class="row pt-4">
                        <div class="col-lg-12">
                                <div class="recent-posts mb-5">
										<h2 class="font-weight-normal text-6 mb-4">Our <strong class="font-weight-extra-bold">Store</strong></h2>
										<div class="owl-carousel owl-theme dots-title mb-0" data-plugin-options="{'items': 1, 'autoHeight': true, 'autoplay': true, 'autoplayTimeout': 8000}">
                                                <div class="row pt-4">
                                                        <div class="col-lg-6">
                                                            <img class="img-fluid border-radius-0" src="{{ 'assets/public/img/stores/Hero-Image-Mall-Puri-Indah-GS-IDN-JKT-JKTBRT-PUIN-1912A.jpg'|theme }}" alt="" />
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="row">
                                                                <div class="col-lg-10">
                                                                    <ul class="list list-icons">
                                                                        <li class="no-padding">Menara Topas</li>
                                                                        <li class="no-padding">Jenderal Sudirman Kav 21 Level 24 Jakarta Pusat</li>
                                                                        <li class="no-padding">021-841 922 213</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="col-lg-2">
                                                                    <a href="/"><span class="badge badge-dark badge-sm badge-pill text-uppercase px-2 py-1 mr-1">VISIT</span></a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-4">
                                                                    <h5 class="text-3 mb-1 pb-1">Bank Office Available</h5>
                                        
                                                                </div>
                                                                <div class="col-lg-4">
                                                                    <h5 class="text-3 mb-1 pb-1">Bank ATM Available</h5>
                                        
                                                                </div>
                                                                <div class="col-lg-4">
                                                                    <h5 class="text-3 mb-1 pb-1">Promo on Store</h5>
                                        
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row pt-4">
                                                            <div class="col-lg-6">
                                                                    <img class="img-fluid border-radius-0" src="{{ 'assets/public/img/stores/Hero-Image-Mall-Puri-Indah-GS-IDN-JKT-JKTBRT-PUIN-1912A.jpg'|theme }}" alt="" />
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="row">
                                                                    <div class="col-lg-10">
                                                                        <ul class="list list-icons">
                                                                            <li class="no-padding">Menara Topas</li>
                                                                            <li class="no-padding">Jenderal Sudirman Kav 21 Level 24 Jakarta Pusat</li>
                                                                            <li class="no-padding">021-841 922 213</li>
                                                                        </ul>
                                                                    </div>
                                                                    <div class="col-lg-2">
                                                                        <a href="/"><span class="badge badge-dark badge-sm badge-pill text-uppercase px-2 py-1 mr-1">VISIT</span></a>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-lg-4">
                                                                        <h5 class="text-3 mb-1 pb-1">Bank Office Available</h5>
                                            
                                                                    </div>
                                                                    <div class="col-lg-4">
                                                                        <h5 class="text-3 mb-1 pb-1">Bank ATM Available</h5>
                                            
                                                                    </div>
                                                                    <div class="col-lg-4">
                                                                        <h5 class="text-3 mb-1 pb-1">Promo on Store</h5>
                                            
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
										</div>
									</div>
                        </div>
                    </div>
            </div>
</section>
