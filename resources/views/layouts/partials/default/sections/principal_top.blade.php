<section class="section section-white section-no-border section-height-0 my-0">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 mt-5">
                    <h3 class="mt-5 font-weight-bold line-height-2 text-2 negative-ls-5 mb-1 mt-3 big">THE MOST COMPLETE PRINCIPALS AT </h3>
                    <h3 class="font-weight-extra-bold line-height-2 text-2 negative-ls-5 mb-2 big_red">GELT</h3>
                </div>
                <div class="col-lg-7 mt-5 mt-lg-0">
                        <div class="owl-carousel owl-theme nav-inside nav-inside-edge nav-squared nav-with-transparency nav-light mb-0" 
                        data-plugin-options="{'items': 1, 'margin': 10, 'loop': false, 'nav': true, 'dots': false}" style="height: auto;">
                                <div>
                                    <div class="img-thumbnail border-0 p-0 d-block">
                                        <img class="img-fluid border-radius-0" src="{{ 'assets/public/img/principal_header.png'|theme }}" alt="">
                                    </div>
                                </div>
                            </div>
                </div>
            </div>
        </div>
    </section>