<!DOCTYPE html>
</body>
<html>

<head>

	<!-- Basic -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>{{ config('app.name', 'GELD') }}</title>
	<meta name="keywords" content="Working space" />
	<meta name="description" content="Geld.id">

	<!-- Favicon -->
	<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
	<link rel="apple-touch-icon" href="img/apple-touch-icon.png') }}">

	<!-- Mobile Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

	<!-- Web Fonts  -->
	<!-- <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light%7CPlayfair+Display:400" rel="stylesheet" type="text/css"> -->
	<!-- <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700,800,900&display=swap" rel="stylesheet"> -->
	<link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,400,600,700&display=swap" rel="stylesheet">
	<!-- Vendor CSS -->

	<link rel="stylesheet" href="{{ url('/public/vendor/bootstrap/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ url('/public/vendor/fontawesome-free/css/all.min.css') }}">
	<!-- <link rel="stylesheet" href="{{ url('/public/vendor/animate/animate.min.css') }}"> -->
	<!-- <link rel="stylesheet" href="{{ url('/public/vendor/simple-line-icons/css/simple-line-icons.min.css') }}"> -->
	<link rel="stylesheet" href="{{ url('/public/vendor/owl.carousel/assets/owl.carousel.min.css') }}">
	<link rel="stylesheet" href="{{ url('/public/vendor/owl.carousel/assets/owl.theme.default.min.css') }}">
	<!-- <link rel="stylesheet" href="{{ url('/public/vendor/magnific-popup/magnific-popup.min.css') }}"> -->

	<!-- Theme CSS -->
	<link rel="stylesheet" href="{{ url('/public/css/theme.css') }}">
	<link rel="stylesheet" href="{{ url('/public/css/theme-elements.css') }}">
	<!-- <link rel="stylesheet" href="{{ url('/public/css/theme-blog.css') }}"> -->
	<!-- <link rel="stylesheet" href="{{ url('/public/css/theme-shop.css') }}"> -->

	<!-- Current Page CSS -->
	<!-- <link rel="stylesheet" href="{{ url('/public/vendor/rs-plugin/css/settings.css') }}"> -->
	<!-- <link rel="stylesheet" href="{{ url('/public/vendor/rs-plugin/css/layers.css') }}"> -->
	<!-- <link rel="stylesheet" href="{{ url('/public/vendor/rs-plugin/css/navigation.css') }}"> -->

	<!-- Skin CSS -->
	<!-- <link rel="stylesheet" href="{{ url('/public/css/skins/skin-corporate-6.css') }}"> -->
 
	<!-- Theme Custom CSS -->
	<link rel="stylesheet" href="{{ url('/public/css/custom.css') }}">

	<!-- Head Libs -->
	<script src="{{ url('/public/vendor/modernizr/modernizr.min.js') }}"></script>

</head>
<!-- <body class="loading-overlay-showing" data-plugin-page-transition data-loading-overlay data-plugin-options="{'hideDelay': 200}"> -->

<body>

	<div class="body">
		<header id="header" class="header-effect-shrink" data-plugin-options="{'stickyEnabled': true, 'stickyEffect': 'shrink', 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyChangeLogo': true, 'stickyStartAt': 120, 'stickyHeaderContainerHeight': 70}">
			<div class="header-body">
				<div class="header-container container">
					<div class="header-row">
						<div class="header-column">
							<div class="header-row">
								<div class="header-logo">
									<a href="/">
										<img alt="GELT" src="{{ url('/public/img/logo.png') }}">
									</a>
								</div>
							</div>
						</div>
						<div class="header-column justify-content-end">
							<div class="header-row">
								<div class="header-nav header-nav-links order-2 order-lg-1">
									<div class="header-nav-main header-nav-main-square header-nav-main-effect-2 header-nav-main-sub-effect-1">
										<nav class="collapse">
											<ul class="nav nav-pills" id="mainNav">
												<li><a class="" href="/store_list">STORES LIST</a></li>
												<li><a class="" href="/page/promo">PROMO</a></li>
												<li><a class="" href="/page/events">EVENTS</a></li>
												<li><a class="" href="/principal_list">PRINCIPALS</a></li>
												<li><a class="" href="/page/merchandise">MERCHANDISE</a></li>
												<li><a class="" href="/page/services">SERVICES</a></li>
												<li><a class="" href="/page/download">DOWNLOAD APP</a></li>
												<li><a class="" href="#"><span class="badge badge-dark badge-sm badge-pill text-uppercase px-2 py-1 mr-1">BOOK APPOINTMENT</span></a></li>
											</ul>
										</nav>
									</div>
									<button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
										<i class="fas fa-bars"></i>
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>

		<div role="main" class="main">

			@yield('content')

		</div>
		@extends('layouts.partials.default.footer')

	</div>

	<!-- Vendor -->
	<script src="{{ url('/public/vendor/jquery/jquery.min.js') }}"></script>
	<script src="{{ url('/public/vendor/jquery.appear/jquery.appear.min.js') }}"></script>
	<script src="{{ url('/public/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
	<script src="{{ url('/public/vendor/jquery.cookie/jquery.cookie.min.js') }}"></script>
	<script src="{{ url('/public/vendor/popper/umd/popper.min.js') }}"></script>
	<script src="{{ url('/public/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
	<script src="{{ url('/public/vendor/common/common.min.js') }}"></script>
	<script src="{{ url('/public/vendor/jquery.validation/jquery.validate.min.js') }}"></script>
	<!-- <script src="{{ url('/public/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js') }}"></script> -->
	<script src="{{ url('/public/vendor/jquery.gmap/jquery.gmap.min.js') }}"></script>
	<!-- <script src="{{ url('/public/vendor/jquery.lazyload/jquery.lazyload.min.js') }}"></script> -->
	<script src="{{ url('/public/vendor/isotope/jquery.isotope.min.js') }}"></script>
	<script src="{{ url('/public/vendor/owl.carousel/owl.carousel.min.js') }}"></script>
	<!-- <script src="{{ url('/public/vendor/magnific-popup/jquery.magnific-popup.min.js') }}"></script> -->
	<!-- <script src="{{ url('/public/vendor/vide/jquery.vide.min.js') }}"></script> -->
	<!-- <script src="{{ url('/public/vendor/vivus/vivus.min.js') }}"></script> -->

	<!-- Theme Base, Components and Settings -->
	<script src="{{ url('/public/js/theme.js') }}"></script>

	<!-- Current Page Vendor and Views -->
	<script src="{{ url('/public/vendor/rs-plugin/js/jquery.themepunch.tools.min.js') }}"></script>
	<script src="{{ url('/public/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js') }}"></script>
	<!-- <script src="{{ url('/public/vendor/circle-flip-slideshow/js/jquery.flipshow.min.js') }}"></script> -->
	<script src="{{ url('/public/js/views/view.home.js') }}"></script>

	<!-- Current Page Vendor and Views -->
	<script src="{{ url('/public/js/views/view.contact.js') }}"></script>

	<!-- Theme Custom -->
	<script src="{{ url('/public/js/custom.js') }}"></script>

	<!-- Theme Initialization Files -->
	<script src="{{ url('/public/js/theme.init.js') }}"></script>

	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCzI4DhUeFCTWk-V1VziPO8muLYWWUD-WA"></script>
	<!-- <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCzI4DhUeFCTWk-V1VziPO8muLYWWUD-WA" type="text/javascript"></script> -->
	<script>
		// Full Width (inside Container)
		$("#googlemapsFullWidthInside").gMap({
			controls: {
				// draggable: (($.browser.mobile) ? false : true),
				panControl: true,
				zoomControl: true,
				mapTypeControl: true,
				scaleControl: true,
				streetViewControl: true,
				overviewMapControl: true
			},
			scrollwheel: false,
			markers: [{
				address: "217 Summit Boulevard, Birmingham, AL 35243",
				html: "<strong>Alabama Office</strong><br>217 Summit Boulevard, Birmingham, AL 35243",
				icon: {
					image: "img/pin.png') }}",
					iconsize: [26, 46],
					iconanchor: [12, 46]
				}
			}, {
				address: "645 E. Shaw Avenue, Fresno, CA 93710",
				html: "<strong>California Office</strong><br>645 E. Shaw Avenue, Fresno, CA 93710",
				icon: {
					image: "img/pin.png') }}",
					iconsize: [26, 46],
					iconanchor: [12, 46]
				}
			}, {
				address: "New York, NY 10017",
				html: "<strong>New York Office</strong><br>New York, NY 10017",
				icon: {
					image: "img/pin.png') }}",
					iconsize: [26, 46],
					iconanchor: [12, 46]
				}
			}],
			latitude: 37.09024,
			longitude: -95.71289,
			zoom: 3
		});
	</script>
	<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.
                <script>
                    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                    })(window,document,'script','//www.google-analytics.com/analytics.js') }}','ga');
                
                    ga('create', 'UA-12345678-1', 'auto');
                    ga('send', 'pageview');
                </script>
                 -->

</body>

</html>