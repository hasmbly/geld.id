@extends('layouts.login')

@section('content')
							<div class="kt-login__signin">
								<div class="kt-login__head">
									<!-- <h3 class="kt-login__title">{{ __('Masuk Ke Akun GELD') }}</h3> -->
								</div>
								<div class="kt-login__form">
                                    <form method="POST" action="{{ route('login') }}" class="kt-form">
                                        @csrf
                                        <!-- {{csrf_field()}} -->
										<div class="form-group">
                                            <input class="form-control" type="text" placeholder="{{ __('Username') }}" name="username" autocomplete="off">
                                            @error('username')
                                                <div class="error invalid-feedback">
                                                    <strong>{{ $message }}</strong>
                                                </div>
                                            @enderror
										</div>
										<div class="form-group">
                                            <input class="form-control form-control-last" type="Password" placeholder="{{ __('Password') }}" name="password">
                                            @error('password')
                                                <div class="error invalid-feedback">
                                                    <strong>{{ $message }}</strong>
                                                </div>
                                            @enderror
										</div>
										<div class="row kt-login__extra">
											<div class="col kt-align-left">
												<label class="kt-checkbox">
                                                    <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Ingat password saya') }}
													<span></span>
												</label>
											</div>
											<div class="col kt-align-right">
												<a href="javascript:;" id="kt_login_forgot" class="kt-link">{{ __('Lupa Password ?') }}</a>
											</div>
										</div>
										<div class="kt-login__actions">
											<button type="submit" id="kt_login_signin_submit" class="btn btn-brand btn-pill btn-elevate">{{ __('Masuk') }}</button>
										</div>
									</form>
								</div>
							</div>
							<div class="kt-login__forgot">
								<div class="kt-login__head">
									<h3 class="kt-login__title">{{ __('Lupa Password ?') }}</h3>
									<div class="kt-login__desc">{{ __('Masukkan email Anda untuk mengatur ulang kata password:') }}</div>
								</div>
								<div class="kt-login__form">
									<form class="kt-form" action="">
										<div class="form-group">
											<input class="form-control" type="text" placeholder="Username" name="username" id="kt_username" autocomplete="off">
										</div>
										<div class="kt-login__actions">
											<button id="kt_login_forgot_submit" class="btn btn-brand btn-pill btn-elevate">{{ __('Reset') }}</button>
											<button id="kt_login_forgot_cancel" class="btn btn-outline-brand btn-pill">{{ __('Batal') }}</button>
										</div>
									</form>
								</div>
                            </div>
                            
<!-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> -->
@endsection
