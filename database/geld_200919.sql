-- MySQL dump 10.16  Distrib 10.1.38-MariaDB, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: geld_2019
-- ------------------------------------------------------
-- Server version	10.1.38-MariaDB-0ubuntu0.18.10.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `about_us`
--

DROP TABLE IF EXISTS `about_us`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `about_us` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section_one` text,
  `section_two` text,
  `section_three` text,
  `section_four` text,
  `img_large` varchar(200) DEFAULT NULL,
  `img_medium` varchar(200) DEFAULT NULL,
  `img_small` varchar(200) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `about_us`
--

LOCK TABLES `about_us` WRITE;
/*!40000 ALTER TABLE `about_us` DISABLE KEYS */;
INSERT INTO `about_us` VALUES (2,'<h1 class=\"text-9 font-weight-bold\">About Us</h1>\r\n<span class=\"sub-title\">The perfect choice for your next project</span>','<div class=\"overflow-hidden mb-3\">\r\n<h1 class=\"word-rotator slide font-weight-bold text-8 mb-0 appear-animation\" data-appear-animation=\"maskUp\">\r\n<span>We are Geld, We </span>\r\n<span class=\"word-rotator-words bg-primary\">\r\n<b class=\"is-visible\">Create</b>\r\n<b>Build</b>\r\n<b>Develop</b>\r\n</span>\r\n<span> Solutions</span>\r\n</h1>\r\n</div>\r\n<div class=\"overflow-hidden mb-3\">\r\n<p class=\"lead mb-0 appear-animation\" data-appear-animation=\"maskUp\" data-appear-animation-delay=\"200\">\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce elementum, nulla vel pellentesque consequat, ante nulla hendrerit arcu, ac tincidunt mauris lacus sed leo.\r\n</p>\r\n</div>','<div class=\"col-md-4 appear-animation\" data-appear-animation=\"fadeInLeftShorter\" data-appear-animation-delay=\"800\">\r\n<h3 class=\"font-weight-bold text-4 mb-2\">Our Mission</h3>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce elementum, nulla vel pellentesque consequat, ante nulla hendrerit arcu.</p>\r\n</div>\r\n<div class=\"col-md-4 appear-animation\" data-appear-animation=\"fadeIn\" data-appear-animation-delay=\"600\">\r\n<h3 class=\"font-weight-bold text-4 mb-2\">Our Vision</h3>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce nulla vel pellentesque consequat, ante nulla hendrerit arcu.</p>\r\n</div>\r\n<div class=\"col-md-4 appear-animation\" data-appear-animation=\"fadeInRightShorter\" data-appear-animation-delay=\"800\">\r\n<h3 class=\"font-weight-bold text-4 mb-2\">Why Us</h3>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce elementum, nulla vel consequat, ante nulla hendrerit arcu.</p>\r\n</div>','<h2 class=\"text-color-dark font-weight-normal text-6 mb-2\">Who <strong class=\"font-weight-extra-bold\">We Are</strong></h2>\r\n<p class=\"lead\">Lorem ipsum dolor sit amet, consectetur adipiscing elit massa enim. Nullam id varius nunc. </p>\r\n<p class=\"pr-5 mr-5\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus blandit massa enim. Nullam id varius nunc. Vivamus bibendum magna ex, et faucibus lacus venenatis eget</p>','1568896479068_124910_1568002605524_124910_blog5.jpg','1568896479068_5255_1568002607580_5255_vimeo-1.jpg','1568896672599_96560_1568002606768_96560_Logo-E-Money-Link.jpg','2019-09-19 16:56:54');
/*!40000 ALTER TABLE `about_us` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `amenities`
--

DROP TABLE IF EXISTS `amenities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amenities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `avaibility_for` varchar(50) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `update_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `amenities`
--

LOCK TABLES `amenities` WRITE;
/*!40000 ALTER TABLE `amenities` DISABLE KEYS */;
INSERT INTO `amenities` VALUES (1,'Financial Event Space\r\n','Conduct your financial seminar, training, sharing session, press event and other events here\r\n','Logo Amenities - Financial Event Space.png','With Appointment\r\n',1,'2019-08-04 23:17:51'),(2,'Remote Queue\r\n','Schedule a service appointment from our app at select our financial center\r\n','Logo Amenities - Remote Queue.png','Member\r\n',1,'2019-08-04 23:17:51'),(3,'Integrated Queue\r\n','Conveniently get a queue for all service at our financal center\r\n','Logo Amenities - Integrated Queue.png','Visitor\r\n',1,'2019-08-04 23:17:51'),(4,'Service Staff\r\n','Our staff ready to help you with member service, product consultation, or other services\r\n','Logo Amenities - Service Staff.png','Visitor\r\n',1,'2019-08-04 23:17:51'),(5,'Security & Safety\r\n','On site security and comprehensive CCTV ensure your safety at our financial center\r\n','Logo Amenities - Security & Safety.png','Public\r\n',1,'2019-08-04 23:17:51'),(6,'Cleaning Team\r\n','Our cleaning team maintains a comfortable environment for your financial activity\r\n','Logo Amenities - Cleaning Team.png','Public\r\n',1,'2019-08-04 23:17:51'),(7,'Relaxed Waiting Area\r\n','Enjoy our waiting room with space, furnishing, sound, and fragrant designer for your comfort\r\n','Logo Amenities - Relaxed Waiting Area.png','Visitor\r\n',1,'2019-08-04 23:17:51'),(8,'Discussion Desk\r\n','Inquire, understand, and make a decision on the available financial product with our team here\r\n','Logo Amenities - Discussion Desk.png','Visitor\r\n',1,'2019-08-04 23:17:51'),(9,'Financial Product Tablet\r\n','Discover, compare, and evaluate available financial products with our easy to use tablet\r\n','Logo Amenities - Financial Product Tablet.png','Visitor\r\n',1,'2019-08-04 23:17:51'),(10,'On Site Payment Counter\r\n','Complete your purchase of financial service product and get the benefit you want\r\n','Logo Amenities - On Site Payment Counter.png','Visitor\r\n',1,'2019-08-04 23:17:51'),(11,'Mobile Phone Charger\r\n','Be a member and enjoy the ability to charge your phone while you wait\r\n','Logo Amenities - Mobile Phone Charger.png','Member\r\n',1,'2019-08-04 23:17:51'),(12,'Power Bank Rental\r\n','Rent a power bank when you complete your service and still need power for your device\r\n','Logo Amenities - Power Bank Rental.png','Pay to Third Party\r\n',1,'2019-08-04 23:17:51'),(13,'WiFi\r\n','Be a member and enjoy secure wifi at our financial center\r\n','Logo Amenities - WiFi.png','Member\r\n',1,'2019-08-04 23:17:51'),(14,'Print and Photocopy Service\r\n','Print or get copies of your document on site\r\n','Logo Amenities - Print and Photocopy Service.png','Pay for Use\r\n',1,'2019-08-04 23:17:51'),(15,'Phone Booth\r\n','Be a member and enjoy making that important call in private\r\n','Logo Amenities - Phone Booth.png','Member\r\n',1,'2019-08-04 23:17:51'),(16,'Mineral Water\r\n','Keep yourself hidrated with our mineral water, prepared every day\r\n','Logo Amenities - Mineral Water.png','Visitor\r\n',1,'2019-08-04 23:17:51'),(17,'Premium Coffee\r\n','Enjoy fresh hot and cold coffee drinks from our partner\r\n','Logo Amenities - Premium Coffee.png','Pay to Third Party\r\n',1,'2019-08-04 23:17:51'),(18,'Premium Tea\r\n','Enjoy refreshing hot and cold tea drinks from our partner\r\n','Logo Amenities - Premium Tea.png','Pay to Third Party\r\n',1,'2019-08-04 23:17:51'),(19,'Premium Juice\r\n','Enjoy premium juice made fresh from our partner\r\n','Logo Amenities - Premium Juice.png','Pay to Third Party\r\n',1,'2019-08-04 23:17:51');
/*!40000 ALTER TABLE `amenities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contact_us`
--

DROP TABLE IF EXISTS `contact_us`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact_us` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(200) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `whatsapp` varchar(50) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `get_in_touch` varchar(255) DEFAULT NULL,
  `business_hours` text,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact_us`
--

LOCK TABLES `contact_us` WRITE;
/*!40000 ALTER TABLE `contact_us` DISABLE KEYS */;
INSERT INTO `contact_us` VALUES (1,'1234 Street Name, City Name, United States,','(123) 456-789','+62 811 824 1689','geld.id@mail.com','<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur eget leo at velit imperdiet varius. In eu ipsum vitae velit congue iaculis vitae at risus. Lorem ipsum dolor sit amet, consectetur adipiscing elit.\r\n</p>','<ul class=\"list list-icons list-dark mt-2\">\r\n<li><i class=\"far fa-clock top-6\"></i> Monday - Friday - 9am to 5pm</li>\r\n<li><i class=\"far fa-clock top-6\"></i> Saturday - 9am to 2pm</li>\r\n<li><i class=\"far fa-clock top-6\"></i> Sunday - Closed</li>\r\n</ul>','2019-09-18 07:08:55');
/*!40000 ALTER TABLE `contact_us` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_images`
--

DROP TABLE IF EXISTS `event_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(10) unsigned NOT NULL,
  `photo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `event_id` (`event_id`),
  CONSTRAINT `event_images_ibfk_1` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_images`
--

LOCK TABLES `event_images` WRITE;
/*!40000 ALTER TABLE `event_images` DISABLE KEYS */;
INSERT INTO `event_images` VALUES (3,17,'1568886785730_411760_1568712936428_411760_1568696926698_411760_1568471368865_411760_1568002606980_411760_HERO PHOTO CAROUSEL 001 - CopyRight Trast Digital.jpg'),(4,17,'1568886845456_1922953_1568697889376_1922953_1568610826377_1922953_1568002607496_1922953_Image B Mall Central Park - GS-IDN-JKT-JKTBRT-CEPA-1910A.jpg'),(5,18,'1568002606768_16671_Logo-Bank-Resona-Perdania.jpg'),(6,18,'1568002606768_324028_Logo-Bank-Woori.jpg'),(10,21,'1568095005968_208002_agile-cicd-devops-difference.jpg.imgo.jpg'),(11,21,'1568002607484_767912_Image B Grand Indo - GS-IDN-JKT-JKTPST-GRID-1910A.jpg'),(12,22,'1568002607240_588033_Image A Grand Indo - GS-IDN-JKT-JKTPST-GRID-1910A.jpg');
/*!40000 ALTER TABLE `event_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` int(1) NOT NULL DEFAULT '1',
  `title` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `img` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_date` date NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
INSERT INTO `events` VALUES (17,1,'Event Geloo','1568002607580_5255_vimeo-1.jpg','2019-09-21','10:00:00','21:08:00','niceeeee','2019-09-19 10:08:09','2019-09-19 10:08:09'),(18,1,'Event maknyusss','1566525695363_121146_666194.jpg','2019-09-30','20:00:00','10:00:00','weeewwwww','2019-09-19 10:10:53','2019-09-19 10:10:53'),(21,1,'Promo amazing','1568896672599_96560_1568002606768_96560_Logo-E-Money-Link.jpg','2019-09-30','13:00:00','03:00:00','adf','2019-09-19 20:57:13','2019-09-19 20:57:13'),(22,1,'Promo Gokilll','1568002606992_598641_HERO PHOTO CAROUSEL 002 - CopyRight Trast Digital.jpg','2019-09-29','00:00:00','22:00:00','a','2019-09-20 03:47:56','2019-09-20 03:47:57');
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `faqs`
--

DROP TABLE IF EXISTS `faqs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `faqs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `faqs`
--

LOCK TABLES `faqs` WRITE;
/*!40000 ALTER TABLE `faqs` DISABLE KEYS */;
INSERT INTO `faqs` VALUES (2,'<p class=\"lead\">hello bro *_*</p>','2019-09-19 20:32:07');
/*!40000 ALTER TABLE `faqs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `faqs_question`
--

DROP TABLE IF EXISTS `faqs_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `faqs_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` varchar(255) DEFAULT NULL,
  `reply` text,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `faqs_question`
--

LOCK TABLES `faqs_question` WRITE;
/*!40000 ALTER TABLE `faqs_question` DISABLE KEYS */;
INSERT INTO `faqs_question` VALUES (2,'How to Booking Geld.id ?','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur eget leo at velit imperdiet varius. In eu ipsum vitae velit congue iaculis vitae at risus. Nullam tortor nunc, bibendum vitae semper a, volutpat eget massa. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer fringilla, orci sit amet posuere auctor, orci eros pellentesque odio, nec pellentesque erat ligula nec massa. Aenean consequat lorem ut felis ullamcorper posuere gravida tellus faucibus. Maecenas dolor elit, pulvinar eu vehicula eu, consequat et lacus. Duis et purus ipsum. In auctor mattis ipsum id molestie. Donec risus nulla, fringilla a rhoncus vitae, semper a massa. Vivamus ullamcorper, enim sit amet consequat laoreet, tortor tortor dictum urna, ut egestas urna ipsum nec libero. Nulla justo leo, molestie vel tempor nec, egestas at massa. Aenean pulvinar, felis porttitor iaculis pulvinar, odio orci sodales odio, ac pulvinar felis quam sit.</p>','2019-09-19 20:35:27'),(3,'How to be SuperMan ?','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur eget leo at velit imperdiet varius. In eu ipsum vitae velit congue iaculis vitae at risus. Nullam tortor nunc, bibendum vitae semper a, volutpat eget massa. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer fringilla, orci sit amet posuere auctor, orci eros pellentesque odio, nec pellentesque erat ligula nec massa. Aenean consequat lorem ut felis</p>','2019-09-19 20:36:53'),(4,'adfadafasdfd ?','<p>asdfasdfasdfadsf</p>','2019-09-19 20:37:51');
/*!40000 ALTER TABLE `faqs_question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `galleries`
--

DROP TABLE IF EXISTS `galleries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galleries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `is_publish` tinyint(4) NOT NULL DEFAULT '0',
  `title` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `img` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `galleries`
--

LOCK TABLES `galleries` WRITE;
/*!40000 ALTER TABLE `galleries` DISABLE KEYS */;
INSERT INTO `galleries` VALUES (17,1,'Gallery-01','1568896479068_124910_1568002605524_124910_blog5.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur lectus lacus, rutrum sit amet placerat et, bibendum nec mauris. Duis molestie, purus eget placerat viverra, nisi odio gravida sapien, congue tincidunt nisl ante nec tellus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit','2019-09-19 12:40:58','2019-09-19 12:40:58');
/*!40000 ALTER TABLE `galleries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `galleries_images`
--

DROP TABLE IF EXISTS `galleries_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galleries_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gallery_id` int(10) unsigned NOT NULL,
  `photo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `gallery_id` (`gallery_id`),
  CONSTRAINT `gallery_images_ibfk_1` FOREIGN KEY (`gallery_id`) REFERENCES `galleries` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `galleries_images`
--

LOCK TABLES `galleries_images` WRITE;
/*!40000 ALTER TABLE `galleries_images` DISABLE KEYS */;
INSERT INTO `galleries_images` VALUES (4,17,'1568896479068_5255_1568002607580_5255_vimeo-1.jpg'),(5,17,'1568896672599_96560_1568002606768_96560_Logo-E-Money-Link.jpg'),(6,17,'1568896479132_515003_1568710909130_515003_1568697889352_515003_1568471368777_515003_1568002607020_515003_HERO PHOTO CAROUSEL 003 - CopyRight Trast Digital.jpg');
/*!40000 ALTER TABLE `galleries_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(6,'2019_05_06_025859_create_peserta_table',2),(7,'2019_05_06_030049_create_peserta_alamat_table',2),(8,'2019_05_06_030129_create_desa_table',2),(9,'2019_05_06_030156_create_alamat_table',2),(10,'2019_05_06_030225_create_propinsi_table',2),(11,'2019_05_06_030252_create_kecamatan_table',2),(12,'2019_05_06_030319_create_kabupaten_table',2),(13,'2019_05_06_030357_create_pelatihan_table',2),(14,'2019_05_06_030425_create_pelatihan_fasilitator_table',2),(15,'2019_05_06_030454_create_pelatihan_penyelenggara_table',2),(16,'2019_05_06_030529_create_penyelenggara_table',2),(17,'2019_05_06_030611_create_pelatihan_peserta_table',2),(18,'2019_05_06_030647_create_reff_pendidikan_table',2),(19,'2019_05_06_030721_create_reff_jenis_pelatihan_table',2),(20,'2019_05_06_030747_create_reff_status_table',2),(21,'2019_05_06_030820_create_fasilitator_table',2),(22,'2019_05_06_030842_create_fasilitator_dinas_table',2),(23,'2019_05_06_030908_create_dinas_table',2),(24,'2019_05_06_030933_create_balai_table',2),(25,'2019_05_06_030957_create_balai_wilayah_table',2),(26,'2019_05_06_031501_create_roles_table',2),(27,'2019_05_06_080400_create_user_balai_table',2),(33,'2019_09_12_161733_create_page_homes_table',3);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_blogs`
--

DROP TABLE IF EXISTS `page_blogs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_blogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_publish` int(1) NOT NULL DEFAULT '0',
  `title` varchar(255) DEFAULT NULL,
  `intro` text,
  `content` text,
  `img_tumbnail` varchar(255) DEFAULT NULL,
  `img_full` varchar(255) DEFAULT NULL,
  `tags` varchar(255) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_blogs`
--

LOCK TABLES `page_blogs` WRITE;
/*!40000 ALTER TABLE `page_blogs` DISABLE KEYS */;
INSERT INTO `page_blogs` VALUES (1,1,'Class aptent taciti sociosqu','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur lectus lacus, rutrum sit amet placerat et, bibendum nec mauris. Duis molestie, purus eget placerat viverra, nisi odio gravida sapien, congue tincidunt nisl ante nec tellus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur lectus lacus, rutrum sit amet placerat et, bibendum nec mauris. Duis molestie, purus eget placerat viverra, nisi odio gravida sapien, congue tincidunt nisl ante nec tellus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sagittis, massa fringilla consequat blandit, mauris ligula porta nisi, non tristique enim sapien vel nisl. Suspendisse vestibulum lobortis dapibus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent nec tempus nibh. Donec mollis commodo metus et fringilla. Etiam venenatis, diam id adipiscing convallis, nisi eros lobortis tellus, feugiat adipiscing ante ante sit amet dolor. Vestibulum vehicula scelerisque facilisis. Sed faucibus placerat bibendum. Maecenas sollicitudin commodo justo, quis hendrerit leo consequat ac. Proin sit amet risus sapien, eget interdum dui. Proin justo sapien, varius sit amet hendrerit id, egestas quis mauris.</p>\r\n\r\n<p>Ut ac elit non mi pharetra dictum nec quis nibh. Pellentesque ut fringilla elit. Aliquam non ipsum id leo eleifend sagittis id a lorem. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam massa mauris, viverra et rhoncus a, feugiat ut sem. Quisque ultricies diam tempus quam molestie vitae sodales dolor sagittis. Praesent commodo sodales purus. Maecenas scelerisque ligula vitae leo adipiscing a facilisis nisl ullamcorper. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>\r\n\r\n<p>Curabitur non erat quam, id volutpat leo. Nullam pretium gravida urna et interdum. Suspendisse in dui tellus. Cras luctus nisl vel risus adipiscing aliquet. Phasellus convallis lorem dui. Quisque hendrerit, lectus ut accumsan gravida, leo tellus porttitor mi, ac mattis eros nunc vel enim. Nulla facilisi. Nam non nulla sed nibh sodales auctor eget non augue. Pellentesque sollicitudin consectetur mauris, eu mattis mi dictum ac. Etiam et sapien eu nisl dapibus fermentum et nec tortor.</p>\r\n\r\n<p>Curabitur nec nulla lectus, non hendrerit lorem. Quisque lorem risus, porttitor eget fringilla non, vehicula sed tortor. Proin enim quam, vulputate at lobortis quis, condimentum at justo. Phasellus nec nisi justo. Ut luctus sagittis nulla at dapibus. Aliquam ullamcorper commodo elit, quis ornare eros consectetur a. Curabitur nulla dui, fermentum sed dapibus at, adipiscing eget nisi. Aenean iaculis vehicula imperdiet. Donec suscipit leo sed metus vestibulum pulvinar. Phasellus bibendum magna nec tellus fringilla faucibus. Phasellus mollis scelerisque volutpat. Ut sed molestie turpis. Phasellus ultrices suscipit tellus, ac vehicula ligula condimentum et.</p>\r\n\r\n<p>Aenean metus nibh, molestie at consectetur nec, molestie sed nulla. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam nec euismod urna. Donec gravida pharetra ipsum, non volutpat ipsum sagittis a. Phasellus ut convallis ipsum. Sed nec dui orci, nec hendrerit massa. Curabitur at risus suscipit massa varius accumsan. Proin eu nisi id velit ultrices viverra nec condimentum magna. Ut porta orci quis nulla aliquam at dictum mi viverra. Maecenas ultricies elit in tortor scelerisque facilisis. Mauris vehicula porttitor lacus, vel pretium est semper non. Ut accumsan rhoncus metus non pharetra. Quisque luctus blandit nisi, id tempus tellus pulvinar eu. Nam ornare laoreet mi a molestie. Donec sodales scelerisque congue.</p>','blog1.jpg','blog1.jpg','Blogs, Bank, Space','admin','2019-08-18 13:26:27'),(2,1,'Class aptent taciti sociosqu ad litora torquent','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur lectus lacus, rutrum sit amet placerat et, bibendum nec mauris. Duis molestie, purus eget placerat viverra, nisi odio gravida sapien, congue tincidunt nisl ante nec tellus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur lectus lacus, rutrum sit amet placerat et, bibendum nec mauris. Duis molestie, purus eget placerat viverra, nisi odio gravida sapien, congue tincidunt nisl ante nec tellus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sagittis, massa fringilla consequat blandit, mauris ligula porta nisi, non tristique enim sapien vel nisl. Suspendisse vestibulum lobortis dapibus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent nec tempus nibh. Donec mollis commodo metus et fringilla. Etiam venenatis, diam id adipiscing convallis, nisi eros lobortis tellus, feugiat adipiscing ante ante sit amet dolor. Vestibulum vehicula scelerisque facilisis. Sed faucibus placerat bibendum. Maecenas sollicitudin commodo justo, quis hendrerit leo consequat ac. Proin sit amet risus sapien, eget interdum dui. Proin justo sapien, varius sit amet hendrerit id, egestas quis mauris.</p>\r\n\r\n<p>Ut ac elit non mi pharetra dictum nec quis nibh. Pellentesque ut fringilla elit. Aliquam non ipsum id leo eleifend sagittis id a lorem. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam massa mauris, viverra et rhoncus a, feugiat ut sem. Quisque ultricies diam tempus quam molestie vitae sodales dolor sagittis. Praesent commodo sodales purus. Maecenas scelerisque ligula vitae leo adipiscing a facilisis nisl ullamcorper. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>\r\n\r\n<p>Curabitur non erat quam, id volutpat leo. Nullam pretium gravida urna et interdum. Suspendisse in dui tellus. Cras luctus nisl vel risus adipiscing aliquet. Phasellus convallis lorem dui. Quisque hendrerit, lectus ut accumsan gravida, leo tellus porttitor mi, ac mattis eros nunc vel enim. Nulla facilisi. Nam non nulla sed nibh sodales auctor eget non augue. Pellentesque sollicitudin consectetur mauris, eu mattis mi dictum ac. Etiam et sapien eu nisl dapibus fermentum et nec tortor.</p>\r\n\r\n<p>Curabitur nec nulla lectus, non hendrerit lorem. Quisque lorem risus, porttitor eget fringilla non, vehicula sed tortor. Proin enim quam, vulputate at lobortis quis, condimentum at justo. Phasellus nec nisi justo. Ut luctus sagittis nulla at dapibus. Aliquam ullamcorper commodo elit, quis ornare eros consectetur a. Curabitur nulla dui, fermentum sed dapibus at, adipiscing eget nisi. Aenean iaculis vehicula imperdiet. Donec suscipit leo sed metus vestibulum pulvinar. Phasellus bibendum magna nec tellus fringilla faucibus. Phasellus mollis scelerisque volutpat. Ut sed molestie turpis. Phasellus ultrices suscipit tellus, ac vehicula ligula condimentum et.</p>\r\n\r\n<p>Aenean metus nibh, molestie at consectetur nec, molestie sed nulla. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam nec euismod urna. Donec gravida pharetra ipsum, non volutpat ipsum sagittis a. Phasellus ut convallis ipsum. Sed nec dui orci, nec hendrerit massa. Curabitur at risus suscipit massa varius accumsan. Proin eu nisi id velit ultrices viverra nec condimentum magna. Ut porta orci quis nulla aliquam at dictum mi viverra. Maecenas ultricies elit in tortor scelerisque facilisis. Mauris vehicula porttitor lacus, vel pretium est semper non. Ut accumsan rhoncus metus non pharetra. Quisque luctus blandit nisi, id tempus tellus pulvinar eu. Nam ornare laoreet mi a molestie. Donec sodales scelerisque congue.</p>','blog2.jpg','blog2.jpg','Blogs, Bank, Space','Admin','2019-08-18 13:27:47'),(3,1,'Class aptent taciti sociosqu ad litora torquent','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur lectus lacus, rutrum sit amet placerat et, bibendum nec mauris. Duis molestie, purus eget placerat viverra, nisi odio gravida sapien, congue tincidunt nisl ante nec tellus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur lectus lacus, rutrum sit amet placerat et, bibendum nec mauris. Duis molestie, purus eget placerat viverra, nisi odio gravida sapien, congue tincidunt nisl ante nec tellus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sagittis, massa fringilla consequat blandit, mauris ligula porta nisi, non tristique enim sapien vel nisl. Suspendisse vestibulum lobortis dapibus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent nec tempus nibh. Donec mollis commodo metus et fringilla. Etiam venenatis, diam id adipiscing convallis, nisi eros lobortis tellus, feugiat adipiscing ante ante sit amet dolor. Vestibulum vehicula scelerisque facilisis. Sed faucibus placerat bibendum. Maecenas sollicitudin commodo justo, quis hendrerit leo consequat ac. Proin sit amet risus sapien, eget interdum dui. Proin justo sapien, varius sit amet hendrerit id, egestas quis mauris.</p>\r\n\r\n<p>Ut ac elit non mi pharetra dictum nec quis nibh. Pellentesque ut fringilla elit. Aliquam non ipsum id leo eleifend sagittis id a lorem. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam massa mauris, viverra et rhoncus a, feugiat ut sem. Quisque ultricies diam tempus quam molestie vitae sodales dolor sagittis. Praesent commodo sodales purus. Maecenas scelerisque ligula vitae leo adipiscing a facilisis nisl ullamcorper. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>\r\n\r\n<p>Curabitur non erat quam, id volutpat leo. Nullam pretium gravida urna et interdum. Suspendisse in dui tellus. Cras luctus nisl vel risus adipiscing aliquet. Phasellus convallis lorem dui. Quisque hendrerit, lectus ut accumsan gravida, leo tellus porttitor mi, ac mattis eros nunc vel enim. Nulla facilisi. Nam non nulla sed nibh sodales auctor eget non augue. Pellentesque sollicitudin consectetur mauris, eu mattis mi dictum ac. Etiam et sapien eu nisl dapibus fermentum et nec tortor.</p>\r\n\r\n<p>Curabitur nec nulla lectus, non hendrerit lorem. Quisque lorem risus, porttitor eget fringilla non, vehicula sed tortor. Proin enim quam, vulputate at lobortis quis, condimentum at justo. Phasellus nec nisi justo. Ut luctus sagittis nulla at dapibus. Aliquam ullamcorper commodo elit, quis ornare eros consectetur a. Curabitur nulla dui, fermentum sed dapibus at, adipiscing eget nisi. Aenean iaculis vehicula imperdiet. Donec suscipit leo sed metus vestibulum pulvinar. Phasellus bibendum magna nec tellus fringilla faucibus. Phasellus mollis scelerisque volutpat. Ut sed molestie turpis. Phasellus ultrices suscipit tellus, ac vehicula ligula condimentum et.</p>\r\n\r\n<p>Aenean metus nibh, molestie at consectetur nec, molestie sed nulla. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam nec euismod urna. Donec gravida pharetra ipsum, non volutpat ipsum sagittis a. Phasellus ut convallis ipsum. Sed nec dui orci, nec hendrerit massa. Curabitur at risus suscipit massa varius accumsan. Proin eu nisi id velit ultrices viverra nec condimentum magna. Ut porta orci quis nulla aliquam at dictum mi viverra. Maecenas ultricies elit in tortor scelerisque facilisis. Mauris vehicula porttitor lacus, vel pretium est semper non. Ut accumsan rhoncus metus non pharetra. Quisque luctus blandit nisi, id tempus tellus pulvinar eu. Nam ornare laoreet mi a molestie. Donec sodales scelerisque congue.</p>','blog3.jpg','blog3.jpg','Blogs, Bank, Space','Admin','2019-08-18 13:41:28');
/*!40000 ALTER TABLE `page_blogs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_galleries`
--

DROP TABLE IF EXISTS `page_galleries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_galleries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_publish` int(1) NOT NULL DEFAULT '0',
  `title` varchar(255) DEFAULT NULL,
  `descriptoin` text,
  `img_tumbnail` varchar(255) DEFAULT NULL,
  `img_full` varchar(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_galleries`
--

LOCK TABLES `page_galleries` WRITE;
/*!40000 ALTER TABLE `page_galleries` DISABLE KEYS */;
INSERT INTO `page_galleries` VALUES (1,1,'Class aptent taciti sociosqu ad litora torquent','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur lectus lacus, rutrum sit amet placerat et, bibendum nec mauris. Duis molestie, purus eget placerat viverra, nisi odio gravida sapien, congue tincidunt nisl ante nec tellus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit','blog1.jpg','blog1.jpg','Bank','Admin','2019-08-18 13:26:27'),(2,1,'Class aptent taciti sociosqu ad litora torquent','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur lectus lacus, rutrum sit amet placerat et, bibendum nec mauris. Duis molestie, purus eget placerat viverra, nisi odio gravida sapien, congue tincidunt nisl ante nec tellus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit','blog2.jpg','blog2.jpg','Bank','Admin','2019-08-18 13:27:47'),(3,1,'Class aptent taciti sociosqu ad litora torquent','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur lectus lacus, rutrum sit amet placerat et, bibendum nec mauris. Duis molestie, purus eget placerat viverra, nisi odio gravida sapien, congue tincidunt nisl ante nec tellus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit','blog3.jpg','blog3.jpg','Bank','Admin','2019-08-18 13:41:28');
/*!40000 ALTER TABLE `page_galleries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_homes`
--

DROP TABLE IF EXISTS `page_homes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_homes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `is_publish` tinyint(4) NOT NULL DEFAULT '0',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `intro` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `img` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_homes`
--

LOCK TABLES `page_homes` WRITE;
/*!40000 ALTER TABLE `page_homes` DISABLE KEYS */;
INSERT INTO `page_homes` VALUES (2,1,'AGE OF COLLABORATION','A more connected world means a more collaborative world, we are making it happen for financial services','1568002606992_598641_HERO PHOTO CAROUSEL 002 - CopyRight Trast Digital.jpg','Project Titan seeks to helps financial service companies to focus on manufacturing and let go of captive distribution \r\nmodels that was useful when information and connectivity was limited','2019-09-14 12:13:58','2019-09-19 12:21:25'),(3,0,'Age of Artificial Intelegence','Goooooddddddddddddddddddddd','1568002607464_1882880_Image A Surabaya Town Square - GS-IDN-JTM-SURBAY-SUTO-1911A.jpg','Goooooddddddddddddddddddddd','2019-09-16 05:23:35','2019-09-19 12:21:24');
/*!40000 ALTER TABLE `page_homes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_homes_images`
--

DROP TABLE IF EXISTS `page_homes_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_homes_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pages_homes_id` int(10) unsigned NOT NULL,
  `photo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pages_homes_id` (`pages_homes_id`),
  CONSTRAINT `page_homes_images_ibfk_1` FOREIGN KEY (`pages_homes_id`) REFERENCES `page_homes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_homes_images`
--

LOCK TABLES `page_homes_images` WRITE;
/*!40000 ALTER TABLE `page_homes_images` DISABLE KEYS */;
INSERT INTO `page_homes_images` VALUES (17,2,'1568002607020_515003_HERO PHOTO CAROUSEL 003 - CopyRight Trast Digital.jpg'),(18,2,'1568002606980_411760_HERO PHOTO CAROUSEL 001 - CopyRight Trast Digital.jpg'),(19,3,'1568180376953_208002_1565239331000_208002_agile-cicd-devops-difference.jpg.imgo.jpg'),(20,3,'1568185287939_218569_1566462746000_218569_WhatsApp Image 2019-08-22 at 3.27.40 PM.jpeg'),(21,3,'1568002607032_1800905_Hero-Image-Grand-Indo-GS-IDN-JKT-JKTPST-GRID-1910A.jpg');
/*!40000 ALTER TABLE `page_homes_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_news`
--

DROP TABLE IF EXISTS `page_news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_publish` int(1) NOT NULL DEFAULT '0',
  `title` varchar(255) DEFAULT NULL,
  `intro` text,
  `content` text,
  `img_tumbnail` varchar(255) DEFAULT NULL,
  `img_full` varchar(255) DEFAULT NULL,
  `tags` varchar(255) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_news`
--

LOCK TABLES `page_news` WRITE;
/*!40000 ALTER TABLE `page_news` DISABLE KEYS */;
INSERT INTO `page_news` VALUES (1,1,'Class aptent taciti sociosqu ad litora torquent','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur lectus lacus, rutrum sit amet placerat et, bibendum nec mauris. Duis molestie, purus eget placerat viverra, nisi odio gravida sapien, congue tincidunt nisl ante nec tellus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur lectus lacus, rutrum sit amet placerat et, bibendum nec mauris. Duis molestie, purus eget placerat viverra, nisi odio gravida sapien, congue tincidunt nisl ante nec tellus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sagittis, massa fringilla consequat blandit, mauris ligula porta nisi, non tristique enim sapien vel nisl. Suspendisse vestibulum lobortis dapibus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent nec tempus nibh. Donec mollis commodo metus et fringilla. Etiam venenatis, diam id adipiscing convallis, nisi eros lobortis tellus, feugiat adipiscing ante ante sit amet dolor. Vestibulum vehicula scelerisque facilisis. Sed faucibus placerat bibendum. Maecenas sollicitudin commodo justo, quis hendrerit leo consequat ac. Proin sit amet risus sapien, eget interdum dui. Proin justo sapien, varius sit amet hendrerit id, egestas quis mauris.</p>\r\n\r\n<p>Ut ac elit non mi pharetra dictum nec quis nibh. Pellentesque ut fringilla elit. Aliquam non ipsum id leo eleifend sagittis id a lorem. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam massa mauris, viverra et rhoncus a, feugiat ut sem. Quisque ultricies diam tempus quam molestie vitae sodales dolor sagittis. Praesent commodo sodales purus. Maecenas scelerisque ligula vitae leo adipiscing a facilisis nisl ullamcorper. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>\r\n\r\n<p>Curabitur non erat quam, id volutpat leo. Nullam pretium gravida urna et interdum. Suspendisse in dui tellus. Cras luctus nisl vel risus adipiscing aliquet. Phasellus convallis lorem dui. Quisque hendrerit, lectus ut accumsan gravida, leo tellus porttitor mi, ac mattis eros nunc vel enim. Nulla facilisi. Nam non nulla sed nibh sodales auctor eget non augue. Pellentesque sollicitudin consectetur mauris, eu mattis mi dictum ac. Etiam et sapien eu nisl dapibus fermentum et nec tortor.</p>\r\n\r\n<p>Curabitur nec nulla lectus, non hendrerit lorem. Quisque lorem risus, porttitor eget fringilla non, vehicula sed tortor. Proin enim quam, vulputate at lobortis quis, condimentum at justo. Phasellus nec nisi justo. Ut luctus sagittis nulla at dapibus. Aliquam ullamcorper commodo elit, quis ornare eros consectetur a. Curabitur nulla dui, fermentum sed dapibus at, adipiscing eget nisi. Aenean iaculis vehicula imperdiet. Donec suscipit leo sed metus vestibulum pulvinar. Phasellus bibendum magna nec tellus fringilla faucibus. Phasellus mollis scelerisque volutpat. Ut sed molestie turpis. Phasellus ultrices suscipit tellus, ac vehicula ligula condimentum et.</p>\r\n\r\n<p>Aenean metus nibh, molestie at consectetur nec, molestie sed nulla. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam nec euismod urna. Donec gravida pharetra ipsum, non volutpat ipsum sagittis a. Phasellus ut convallis ipsum. Sed nec dui orci, nec hendrerit massa. Curabitur at risus suscipit massa varius accumsan. Proin eu nisi id velit ultrices viverra nec condimentum magna. Ut porta orci quis nulla aliquam at dictum mi viverra. Maecenas ultricies elit in tortor scelerisque facilisis. Mauris vehicula porttitor lacus, vel pretium est semper non. Ut accumsan rhoncus metus non pharetra. Quisque luctus blandit nisi, id tempus tellus pulvinar eu. Nam ornare laoreet mi a molestie. Donec sodales scelerisque congue.</p>','blog1.jpg','blog1.jpg','Blogs, Bank, Space','admin','2019-08-18 13:26:27'),(2,1,'Class aptent taciti sociosqu ad litora torquent','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur lectus lacus, rutrum sit amet placerat et, bibendum nec mauris. Duis molestie, purus eget placerat viverra, nisi odio gravida sapien, congue tincidunt nisl ante nec tellus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur lectus lacus, rutrum sit amet placerat et, bibendum nec mauris. Duis molestie, purus eget placerat viverra, nisi odio gravida sapien, congue tincidunt nisl ante nec tellus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sagittis, massa fringilla consequat blandit, mauris ligula porta nisi, non tristique enim sapien vel nisl. Suspendisse vestibulum lobortis dapibus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent nec tempus nibh. Donec mollis commodo metus et fringilla. Etiam venenatis, diam id adipiscing convallis, nisi eros lobortis tellus, feugiat adipiscing ante ante sit amet dolor. Vestibulum vehicula scelerisque facilisis. Sed faucibus placerat bibendum. Maecenas sollicitudin commodo justo, quis hendrerit leo consequat ac. Proin sit amet risus sapien, eget interdum dui. Proin justo sapien, varius sit amet hendrerit id, egestas quis mauris.</p>\r\n\r\n<p>Ut ac elit non mi pharetra dictum nec quis nibh. Pellentesque ut fringilla elit. Aliquam non ipsum id leo eleifend sagittis id a lorem. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam massa mauris, viverra et rhoncus a, feugiat ut sem. Quisque ultricies diam tempus quam molestie vitae sodales dolor sagittis. Praesent commodo sodales purus. Maecenas scelerisque ligula vitae leo adipiscing a facilisis nisl ullamcorper. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>\r\n\r\n<p>Curabitur non erat quam, id volutpat leo. Nullam pretium gravida urna et interdum. Suspendisse in dui tellus. Cras luctus nisl vel risus adipiscing aliquet. Phasellus convallis lorem dui. Quisque hendrerit, lectus ut accumsan gravida, leo tellus porttitor mi, ac mattis eros nunc vel enim. Nulla facilisi. Nam non nulla sed nibh sodales auctor eget non augue. Pellentesque sollicitudin consectetur mauris, eu mattis mi dictum ac. Etiam et sapien eu nisl dapibus fermentum et nec tortor.</p>\r\n\r\n<p>Curabitur nec nulla lectus, non hendrerit lorem. Quisque lorem risus, porttitor eget fringilla non, vehicula sed tortor. Proin enim quam, vulputate at lobortis quis, condimentum at justo. Phasellus nec nisi justo. Ut luctus sagittis nulla at dapibus. Aliquam ullamcorper commodo elit, quis ornare eros consectetur a. Curabitur nulla dui, fermentum sed dapibus at, adipiscing eget nisi. Aenean iaculis vehicula imperdiet. Donec suscipit leo sed metus vestibulum pulvinar. Phasellus bibendum magna nec tellus fringilla faucibus. Phasellus mollis scelerisque volutpat. Ut sed molestie turpis. Phasellus ultrices suscipit tellus, ac vehicula ligula condimentum et.</p>\r\n\r\n<p>Aenean metus nibh, molestie at consectetur nec, molestie sed nulla. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam nec euismod urna. Donec gravida pharetra ipsum, non volutpat ipsum sagittis a. Phasellus ut convallis ipsum. Sed nec dui orci, nec hendrerit massa. Curabitur at risus suscipit massa varius accumsan. Proin eu nisi id velit ultrices viverra nec condimentum magna. Ut porta orci quis nulla aliquam at dictum mi viverra. Maecenas ultricies elit in tortor scelerisque facilisis. Mauris vehicula porttitor lacus, vel pretium est semper non. Ut accumsan rhoncus metus non pharetra. Quisque luctus blandit nisi, id tempus tellus pulvinar eu. Nam ornare laoreet mi a molestie. Donec sodales scelerisque congue.</p>','blog2.jpg','blog2.jpg','Blogs, Bank, Space','Admin','2019-08-18 13:27:47');
/*!40000 ALTER TABLE `page_news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
INSERT INTO `password_resets` VALUES ('ibnu.munandar@gmail.com','$2y$10$ouA3mU10cpyj27GHBVzLX.nf7St3evNWLgVLrL.LyegCizUoAhIey','2019-08-02 11:48:53');
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `principals`
--

DROP TABLE IF EXISTS `principals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `principals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) DEFAULT NULL,
  `category_code` varchar(50) DEFAULT NULL,
  `brand_name` varchar(50) DEFAULT NULL,
  `img_logo` varchar(100) DEFAULT NULL,
  `content` text,
  `img_office` varchar(200) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `url_website` varchar(255) DEFAULT NULL,
  `shareholder_legal_name` varchar(100) DEFAULT NULL,
  `is_operating_fin_sservice` enum('Yes','No','N/A') DEFAULT NULL,
  `is_owner_our_partner` enum('Yes','No','N/A') DEFAULT NULL,
  `reference_principal_code` varchar(50) DEFAULT NULL,
  `is_multiple_country` enum('N/A','Yes','No') DEFAULT NULL,
  `country_head_office` varchar(50) DEFAULT NULL,
  `country_located` varchar(50) DEFAULT NULL,
  `principal_legal_name` varchar(100) DEFAULT NULL,
  `is_online_trx` enum('N/A','Yes','No') DEFAULT NULL,
  `url_for_customer` varchar(200) DEFAULT NULL,
  `url_for_business` varchar(200) DEFAULT NULL,
  `call_center_number` varchar(200) DEFAULT NULL,
  `email_corporate` varchar(200) DEFAULT NULL,
  `url_annual_report` varchar(200) DEFAULT NULL,
  `img_width` int(6) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `code` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `principals`
--

LOCK TABLES `principals` WRITE;
/*!40000 ALTER TABLE `principals` DISABLE KEYS */;
INSERT INTO `principals` VALUES (1,'GP-BNK-IDN-CMNG','111BNK','CIMB Niaga\r\n','GP-BNK-IDN-CMNG LOGO\r\n.png','<p>PT Bank CIMB Niaga Tbk atau yang lebih dikenal dengan CIMB Niaga adalah sebuah bank yang berdiri pada tahun 1955. Saat ini CIMB Niaga merupakan bank terbesar keempat di Indonesia dilihat dari sisi aset, dan diakui prestasi dan keunggulannya di bidang pelayanan nasabah dan pengembangan manajemen. \r\nSaat ini mayoritas saham Bank CIMB Niaga dimiliki oleh CIMB Group. </p>\r\n<p>Bank CIMB Niaga merupakan bank pembayar (payment bank) KSEI terbesar dari nilai transaksi, dan dengan pangsa pasar 11%, saat ini CIMB Niaga adalah bank penyedia kredit pemilikan rumah terbesar ketiga di Indonesia</p>\r\n','GP-BNK-IDN-CMNG HEAD OFFICE.jpg','Graha CIMB Niaga. Jl. Jend. Sudirman Kav. 58. Jakarta 12190. Indonesia','https://www.cimbniaga.com','CIMB Group Sdn Bhd','Yes','No','N/A','Yes','Malaysia','Indonesia','PT. Bank CIMB Niaga TBk\r\n','Yes','https://www.cimbclicks.co.id/ib-cimbniaga/Login.html\r\n','https://bizchannel.cimbniaga.co.id/corp/common2/login.do?action=loginRequest\r\n','14041\r\n','14041@cimbniaga.co.id\r\n','http://investor.cimbniaga.co.id/ar.html\r\n',100,1),(2,'GP-BNK-IDN-PANN','111BNK','Bank Panin\r\n','GP-BNK-IDN-PANN LOGO.png','<p>PT Bank Pan Indonesia Tbk (PaninBank) merupakan salah satu perbankan komersial terbesar di Indonesia. Didirikan pada 1971 dari hasil penggabungan usaha Bank Kemakmuran, Bank Industri Djaja, serta Bank Industri dan Dagang Indonesia, PaninBank memperoleh izin sebagai bank devisa pada 1972. Selanjutnya, pada 1982, PaninBank melakukan penawaran saham perdana sekaligus menjadi bank pertama di Indonesia yang mencatatkan sahamnya di lantai bursa.</p>\r\n<p>Dengan ditopang fondasi fundamental yang kuat, PaninBank mampu melewati berbagai periode sulit dalam perekonomian Indonesia.  Pada 1998, saat dilanda krisis ekonomi sebagai dampak resesi ekonomi Asia satu tahun sebelumnya, PaninBank masih bisa bertahan sebagai Bank Kategori “A”. Pada periode-periode setelahnya, PaninBank terus melaju mengembangkan berbagai produk dan layanan di bidang perbankan ritel dan komersial.</p>\r\n<p>PaninBank terus tumbuh menjadi salah satu bank Small Medium Enterprise (SME) terdepan di Indonesia dengan didukung Sumber Daya Manusia (SDM) yang andal. Melalui beragam produk dan layanan di segmen perbankan konsumer, SME dan mikro, komersial, korporat, dan tresuri, PaninBank terus menjaga komitmen untuk tumbuh dengan kompetensi yang telah teruji dalam menciptakan nilai sejalan dengan prinsip kehati-hatian.</p>\r\n<p>PaninBank memiliki jaringan perusahaan yang merata di seluruh Nusantara. Hingga 2017, kami telah memiliki lebih dari 562 kantor cabang di seluruh Indonesia, belum termasuk kantor perwakilan di Singapura. Pelayanan prima kami juga didukung dengan lebih dari 967 Automatic Teller Machine (ATM) yang tersebar dari Aceh di ujung barat hingga Papua di pelosok timur Nusantara.</p>\r\n<p>Hingga Per 31 Desember 2017, PaninBank memiliki total asset senilai Rp213,54 triliun. Pada tahun ini, Penyaluran Kredit juga tumbuh 2,88% menjadi Rp128,65 triliun sementara Simpanan nasabah juga tumbuh 2,11% menjadi Rp145,67 triliun. Dalam perkembangannya hingga saat ini, PaninBank juga terus meningkatkan penerapan proses tata kelola perusahaan yang baik, dan secara efektif memanfaatkan teknologi informasi untuk menjawab tuntutan pertumbuhan bisnis dan perkembangan zaman.</p>\r\n','GP-BNK-IDN-PANN HEAD OFFICE.jpg','Jl. Jendral Sudirman Kav. 1 - (Senayan), Jakarta 10270, Indonesia','https://www.panin.co.id/\r\n','PT Panin Financial Tbk','No','N/A','N/A','No','Indonesia','Indonesia','PT Bank Pan Indonesia Tbk\r\n','Yes','https://www.internetpanin.com/PaninWeb/Core/Login.aspx\r\n','https://www.panbib.com/paninbib/\r\n','1-500-678\r\n','panin@panin.co.id\r\n','https://www.panin.co.id/download/26/laporan-tahunan-\r\n',100,1),(3,'GP-BNK-IDN-DNMN','111BNK','Bank Danamon\r\n','GP-BNK-IDN-DNMN LOGO.png','<p>PT Bank Danamon Indonesia Tbk yang berdiri sejak 1956, per 31 Maret 2019 mengelola aset sebesar Rp190 triliun bersama anak perusahaannya, yaitu PT Adira Dinamika Multi Finance Tbk. (Adira Finance) dan PT Asuransi Adira Dinamika (Adira Insurance). Dalam hal kepemilikan saham, 40,00% saham Bank Danamon dimiliki oleh MUFG Bank, Ltd., 33,83% oleh Asia Financial (Indonesia) Pte. Ltd., dan 26,17% dimiliki oleh publik.</p>\r\n<p>Bank Danamon didukung oleh 1.106 jaringan kantor cabang konvensional, unit Syariah dan kantor cabang anak perusahaannya serta lebih dari 60.000 jaringan ATM Danamon, ATM Bersama, PRIMA dan ALTO yang tersebar di 34 provinsi. Selain jaringan fisik, layanan Danamon juga dapat diakses melalui Danamon Online Banking, aplikasi D-Bank, D-Card, serta SMS Banking.</p>\r\n<p>Bank Danamon merupakan penerbit kartu debit, kartu ATM dan Kartu Kredit Manchester United di Indonesia serta penerbit kartu kredit American Express. Dengan beragam produk keuangan seperti Danamon Lebih, FlexiMax, Tabungan Bisa iB, Dana Pinter 50, KAB Bisa, dan Asuransi Primajaga, Danamon siap melayani kebutuhan nasabah dari berbagai segmen.</p>\r\n<p>Bank Danamon dinobatkan sebagai Best Digital Bank: Indonesia dari Asiamoney pada ajang Asiamoney Best Bank Award 2018 di Beijing, Republik Rakyat Tiongkok. Bank Danamon juga meraih penghargaan Best Bank in Digital Service dan Best Sharia Business Unit pada ajang Indonesia Banking Award 2018 yang diselenggarakan oleh Tempo Media Group dan Indonesia Banking School. Dalam hal tanggung jawab sosial, Bank Danamon menerima TOP CSR Award 2018 untuk kategori “Pengelolaan Program CSR” dan “TOP Leader in CSR Commitment” dari majalah Top Business.</p>','GP-BNK-IDN-DNMN HEAD OFFICE.jpg','MENARA BANK DANAMON\nJl. HR. Rasuna Said Blok C No. 10\nKaret - Setiabudi\nJakarta - Indonesia 12920','https://www.danamon.co.id/\r\n','Mitsubishi UFJ Financial Group','Yes','No','N/A','No','Indonesia','Indonesia','PT Bank Danamon Indonesia Tbk \r\n','Yes','https://www.danamonline.com/\r\n','https://icashatwork.danamon.co.id/\r\n','1-500-900\r\n','hellodanamon@danamon.co.id \r\n','https://www.danamon.co.id/en/Tentang-Danamon/InformasiInvestor/Informasi-Keuangan/Laporan-Tahunan\r\n',100,1),(4,'GP-BNK-IDN-OCNI','111BNK','Bank OCBC NISP\r\n','GP-BNK-IDN-OCNI LOGO.png','<p>Bank OCBC NISP (dahulu bernama Bank NISP) merupakan bank tertua keempat di Indonesia, yang didirikan pada tanggal 4 April 1941 di Bandung dengan nama NV Nederlandsch Indische Spaar En Deposito Bank.</p>\r\n\r\n<p>Bank NISP berkembang menjadi Bank yang solid dan handal, terutama melayani segmen Usaha Kecil dan Menengah (UKM). Bank NISP resmi menjadi bank komersial pada tahun 1967, bank devisa pada tahun 1990, dan perusahaan publik di Bursa Efek Indonesia pada tahun 1994.</p>\r\n\r\n<p>Pada akhir tahun 1990-an, Bank NISP berhasil melewati krisis keuangan Asia dan jatuhnya sektor perbankan di Indonesia tanpa dukungan pemerintah. Saat itu, Bank NISP menjadi salah satu bank pertama yang segera melanjutkan penyaluran kreditnya dalam masa krisis.</p>\r\n\r\n<p>Reputasi Bank NISP yang baik di industrinya dan pertumbuhannya yang menjanjikan, telah menarik perhatian berbagai institusi internasional antara lain International Finance Corporation (IFC), bagian dari Grup Bank Dunia, yang memberikan pinjaman jangka panjang pada tahun 1999 dan kemudian menjadi pemegang saham pada tahun 2001 – 2010. Selain itu, sejak awal tahun 1990-an the Netherlands Development Finance Company (FMO) memberikan berbagai pinjaman jangka panjang dengan bunga menarik yang digunakan untuk penyaluran kredit pada segmen UKM.</p>\r\n\r\n<p>Selanjutnya, OCBC Bank - Singapura menjadi pemegang saham mayoritas Bank OCBC NISP melalui serangkaian akuisisi dan penawaran tender sejak tahun 2004. OCBC Bank - Singapura saat ini memiliki saham Bank OCBC NISP sebesar 85,1%.</p>\r\n\r\n<p>Dengan dukungan dari OCBC Bank - Singapura, Bank NISP telah menetapkan program yang sangat dinamis untuk memperkuat infrastruktur, termasuk sumber daya manusia, teknologi informasi dan jaringan kantor. Program ini kemudian memicu kepindahan kantor pusat Bank NISP ke OCBC NISP Tower di pusat Jakarta pada tahun 2006, yang memungkinkan akses langsung ke pusat bisnis di Indonesia.</p>\r\n\r\n<p>Sebagai bagian dari strategi jangka panjang, Bank NISP menggunakan nama dan logo baru “OCBC NISP” sejak akhir tahun 2008, diikuti dengan transformasi besar di seluruh organisasi. Transformasi ini telah dilaksanakan dengan semangat menjadi “Your Partner for Life” bagi seluruh stakeholder.</p>\r\n\r\n<p>Pada tahun 2011, Bank OCBC NISP genap berusia 70 tahun sekaligus memasuki tonggak sejarah penting, dimana Bank OCBC Indonesia resmi bergabung (merger) dengan Bank OCBC NISP. Penggabungan ini menunjukkan komitmen penuh dari OCBC Bank - Singapura sebagai pemegang saham mayoritas, untuk memusatkan dukungannya hanya pada satu bank di Indonesia, yaitu Bank OCBC NISP.</p>\r\n\r\n<p>Sejalan dengan pengembangan bisnisnya, pada tahun 2012 Bank OCBC NISP juga memperbaharui budaya perusahaan yang disebut ONe PIC, untuk menjadi pedoman bagi seluruh karyawan dalam berperilaku dan bekerja. ONe PIC merupakan singkatan dari OCBC NISP one, Professionalism, Integrity, dan Customer Focus. Kini, Bank OCBC NISP memiliki 6.654 karyawan dengan motivasi tinggi untuk melayani nasabah di 337 kantor di 59 kota di Indonesia</p>\r\n','GP-BNK-IDN-OCNI HEAD OFFICE.jpg','OCBC NISP Tower\n, Jl. Prof. Dr. Satrio Kav. 25 Jakarta 12940','http://www.ocbcnisp.com/Home.aspx\r\n','OCBC OVERSEAS INVESTMENTS PTE. LTD','No','N/A','N/A','Yes','Singapore','Indonesia','PT Bank OCBC NISP Tbk\r\n','Yes','https://online.ocbcnisp.com/\r\n','https://newvelocity.ocbcnisp.com/\r\n','1-500-999\r\n','callcenter@ocbcnisp.com\r\n','https://www.ocbcnisp.com/Groups/Hubungan-Investor/Laporan/Laporan-Tahunan.aspx?viewmode=0&devicename=&langobjectlifetime=request&loaddevice=1&lang=en-US\r\n',100,1),(5,'GP-BNK-IDN-BTPN','111BNK','Bank BTPN\r\n','GP-BNK-IDN-BTPN LOGO.png','<p>T Bank Maybank Indonesia Tbk (“Maybank Indonesia” atau “Bank”) adalah salah satu bank swasta terkemuka di Indonesia yang merupakan bagian dari grup Malayan Banking Berhad (Maybank), salah satu  grup penyedia layanan keuangan terbesar di ASEAN. Sebelumnya, Maybank Indonesia bernama PT Bank Internasional Indonesia Tbk (BII) yang didirikan pada 15 Mei 1959, mendapatkan ijin sebagai bank devisa pada 1988 dan mencatatkan sahamnya sebagai perusahaan terbuka di Bursa Efek Jakarta dan Surabaya (sekarang telah merger menjadi Bursa Efek Indonesia) pada 1989.</p>\r\n<p>Maybank Indonesia menyediakan serangkaian produk dan jasa komprehensif bagi nasabah individu maupun korporasi melalui layanan Community Financial Services (Perbankan Ritel dan Perbankan Non-Ritel) dan Perbankan Global, serta pembiayaan otomotif melalui entitas anak yaitu WOM Finance untuk kendaraan roda dua dan Maybank Finance untuk kendaraan roda empat. Maybank Indonesia juga terus mengembangkan layanan dan kapasitas digital banking melalui Mobile Banking, Internet Banking, Maybank2U (mobile banking berbasis internet banking dan berbagai saluran lainnya.</p> \r\n<p>Per 31 Desember 2018, Maybank Indonesia memiliki 386 cabang termasuk cabang Syariah yang tersebar di Indonesia serta satu cabang luar negeri (Mumbai, India), 21 Mobil Kas Keliling dan 1.609 ATM termasuk CDM (Cash Deposit Machine) yang terkoneksi dengan lebih dari 20.000 ATM tergabung dalam jaringan ATM PRIMA, ATM BERSAMA, ALTO, CIRRUS dan terhubung dengan 3.500 ATM Maybank di Singapura, Malaysia dan Brunei. Hingga akhir tahun 2018, Maybank Indonesia mengelola simpanan nasabah sebesar Rp116,8 triliun dan memiliki total aset senilai Rp177,5 triliun.</p>\r\n','GP-BNK-IDN-MYBN HEAD OFFICE.jpg','Sentral Senayan III, \nJl. Asia Afrika No.8\n, Kelurahan Gelora, Kecamatan Tanah Abang\n, Jakarta Pusat\n, DKI Jakarta Raya 10270','https://www.maybank.co.id/Pages/Home.aspx\r\n','Malayan Banking Bhd','Yes','No','N/A','Yes','Malaysia','Indonesia','PT Bank Maybank Indonesia Tbk\r\n','Yes','https://m2u.maybank.co.id/common/Login.do\r\n','https://m2e.maybank.co.id/m2e/portal/portal.view\r\n','69811\r\n','customercare@maybank.co.id\r\n','https://www.maybank.co.id/investor/annual0report/Pages/Annual-Report.aspx\r\n',100,1),(6,'GP-BNK-IDN-PRMT','111BNK','Bank Permata\r\n','GP-BNK-IDN-PRMT LOGO.png','<p>PermataBank dibentuk sebagai hasil merger dari lima bank di bawah pengawasan Badan Penyehatan Perbankan Nasional (BPPN) yakni, PT Bank Bali Tbk, PT Bank Universal Tbk, PT Bank Prima Express, PT Bank Artamedia dan PT Bank Patriot pada tahun 2002. Di tahun 2004, Standard Chartered Bank dan PT Astra International Tbk mengambilalih PermataBank dan memulai proses transformasi secara besar-besaran di dalam organisasi. Selanjutnya, sebagai wujud komitmennya terhadap PermataBank, kepemilikan gabungan saham utama ini meningkat menjadi 89,12% sejak tahun 2006.</p>\r\n\r\n<p>Saat ini, PermataBank telah berkembang menjadi sebuah bank swasta utama yang menawarkan produk dan jasa inovatif yang dapat memberikan layanan keuangan menyeluruh secara sederhana, cepat, dan dapat diandalkan. Sebagai pelopor dalam teknologi mobile banking dan mobile cash di pasar Indonesia, PermataBank kembali memimpin inovasi melalui peluncuran produk E-Bond yang pertama di pasar dan merupakan Bank pertama yang memperkenalkan TouchID & FaceID di aplikasi mobile banking-nya. Di tahun 2018, Bank meluncurkan aplikasi PermataMobile X dengan 200 fitur andalan terkini.</p>\r\n\r\n<p>Melayani hampir 2 juta nasabah di 62 kota di Indonesia, per Des 2018 PermataBank memiliki 323 kantor cabang, 16 cabang bergerak (Mobile Branch), 1 payment point, 1005 ATM dengan akses di lebih dari 100.000 ATM (VisaPlus, Visa Electron, MasterCard, Alto, ATM Bersama dan ATM Prima) dan jutaan ATM di seluruh dunia yang terhubung dengan jaringan Visa, Mastercard, Cirrus. Selama 2018 ini, PermataBank menerima berbagai macam penghargaan atas beberapa pencapaian. Di area CSR, PermataBank menerima “Apresiasi CSR dalam bidang pendidikan dari Sindo Media (Jan 2018). Untuk kampanye produk PermataMe #CountMEin dari The Marketing Event Awards, PermataBank memenangkan 5 piala emas dan 1 piala silver (Gold Award untuk kategory Best Event Press / Media, Best Event Consumer, Best Event Digital Integration, Best Event Ambience, Best Event Product Launch / Relaunch serta Silver Award untuk kategory Best Event Multi Channel). Single lagu MoneyHoney #CountMEin yang dibawakan oleh Dipha Barus & Monica Karina, juga menyabet dua penghargaan sebagai Karya Produksi Terbaik dan Karya Produksi Elektronika Terbaik di AMI (Anugerah Musik Indonesia) awards 2018 (26 Sep 2018).</p>\r\n\r\n<p>PermataBank memiliki rangkaian produk untuk nasabah retail, SME, komersial hingga korporasi, baik konvensional maupun syariah. Fokus PermataBank ke arah digitalisasi diperkuat dengan layanan Permata e-Banking diantaranya PermataATM, PermataMobile X, PermataNet, Permata E-Form, Permata E-Statement, Permata E-Wealth (E-Bonds, E-Mutual Funds), Permata E-Business (Cash, E-Trade, dan Financial Supply Chain) dan API Banking.</p>\r\n','GP-BNK-IDN-PRMT HEAD OFFICE.jpg','PermataBank, WTC II\n, Jl. Jend. Sudirman Kav. 29 - 31, Jakarta 12920','https://www.permatabank.com/\r\n','Astra International & Standard Chartered Bank','Yes','No','N/A','No','Indonesia','Indonesia','PT Bank Permata, Tbk \r\n','Yes','https://new.permatanet.com/permatanet/retail/logon\r\n','https://www.permatae-business.com/corp/common/login.do?action=loginRequest\r\n','1-500-111\r\n','care@permatabank.co.id\r\n','https://www.permatabank.com/TentangKami/HubunganInvestor/Laporan-Tahunan/\r\n',100,1),(7,'GP-BNK-IDN-JBBT','111BNK','Bank BJB\r\n','GP-BNK-IDN-JBBT LOGO.png','<p>Bank BJB (IDX: BJBR) (dahulu dikenal dengan Bank Jabar Banten) adalah bank BUMD milik Pemerintah Provinsi Jawa Barat dan Banten yang berkantor pusat di Bandung. Bank ini didirikan pada tanggal 20 Mei 1961 dengan bentuk perseroan terbatas (PT), kemudian dalam perkembangannya berubah status menjadi Badan Usaha Milik Daerah (BUMD). Saat ini Bank BJB memiliki 63 Kantor Cabang, 311 Kantor Cabang Pembantu, 330 Kantor Kas, 1202 ATM BJB, 103 Payment Point, 4 Kantor Wilayah, dan 473 Waroeng BJB.</p>\r\n\r\n<p>PT Bank Pembangunan Daerah Jawa Barat dan Banten Tbk menjadi bank devisa sejak tanggal 2 Agustus 1990</p>\r\n','GP-BNK-IDN-JBBT HEAD OFFICE.jpg','Menara bank bjb\n, JL. Naripan No. 12-14\n, Bandung 40111','http://www.bankbjb.co.id/\r\n','Pemerintah Propinsi Jawa Barat','No','N/A','N/A','No','Indonesia','Indonesia','PT Bank Pembangunan Daerah Jawa Barat & Banten, Tbk','Yes','https://ib.bankbjb.co.id/bjb.net\r\n','https://bjbcorp.bankbjb.co.id/corporate/\r\n','14049\r\n','bjbcare@bankbjb.co.id\r\n','http://www.bankbjb.co.id/en/corporate-website/investor-relations/financial-and-non-financial-information/annual-reports.html\r\n',100,1),(8,'GP-BNK-IDN-HSBC','111BNK','Bank HSBC Indonesia\r\n','GP-BNK-IDN-HSBC LOGO.png','<p>Grup HSBC memiliki riwayat panjang dan beragam di Indonesia. Kami membuka cabang pertama di Indonesia pada tahun 1884 dengan nama The Hongkong and Shanghai Banking Corporation Limited, Cabang Indonesia (“Kantor Cabang Bank Asing HSBC di Indonesia”). Awalnya, HSBC membantu pembiayaan dan memfasilitasi perdagangan gula. Kemudian HSBC memperluas operasinya dengan membuka kantor keduanya di Surabaya pada tahun 1896. Di tahun-tahun dan dekade-dekade berikutnya, HSBC terus berkembang dan memperkuat kehadirannya di pasar Indonesia yang berkembang dan dinamis.</p>\r\n\r\n<p>Tahun 2009, HSBC Group mengakuisisi PT Bank Ekonomi Raharja (“Bank Ekonomi”), sebuah bank lokal dengan jaringan luas di Indonesia. Di bulan Oktober 2016, Bank Ekonomi berubah nama menjadi PT Bank HSBC Indonesia.</p>\r\n\r\n<p>Pada tanggal 17 April 2017, Grup HSBC telah menggabungkan operasi Kantor Cabang Bank Asing HSBC di Indonesia dengan PT Bank HSBC Indonesia dan menjadi PT Bank HSBC Indonesia, yang mendapatkan izin melakukan usaha sebagai bank umum berdasarkan Keputusan Dewan Komisioner Otoritas Jasa Keuangan No.15/KDK.03/2016 tanggal 4 Oktober 2016, Surat Bank Indonesia No.22/1221/UPPS/PSbD tanggal 2 Maret 1990 dan Keputusan Menteri Keuangan Republik Indonesia No.104/KMK.013/1990 tanggal 12 Februari 1990. Ini adalah salah satu tonggak sejarah penting bagi sejarah HSBC Group di Indonesia. Bank yang terintegrasi ini memungkinkan HSBC memperluas proposisi perbankannya, sekaligus berkontribusi lebih bagi perekonomian Indonesia dengan menghubungkan nasabah ke berbagai peluang melalui kapabilitas dan kehadiran geografis yang meluas di Indonesia.</p>\r\n\r\n<p>PT Bank HSBC Indonesia menawarkan beragam produk dan layanan di bidang komersial, korporasi, perbankan ritel dan wealth management. Jaringan domestik PT Bank HSBC Indonesia yang luas dan jaringan global Grup HSBC memungkinkan PT Bank HSBC Indonesia mendukung para nasabah dengan solusi keuangan tepat baik di Indonesia maupun di berbagai negara tempat HSBC Group beroperasi.</p>\r\n','GP-BNK-IDN-HSBC HEAD OFFICE.jpg','Gedung WTC 1, Lt. 8-9 Jl. Jendral Sudirman Kav.29-31, Jakarta 12920 \r\n','https://www.hsbc.co.id/1/2/\r\n','HSBC Asia Pacific Holdings (UK) Limited','Yes','No','N/A','Yes','United Kingdom','Indonesia','PT Bank HSBC Indonesia\r\n','Yes','https://www.hsbc.co.id/1/2/!ut/p/z1/04_Sj9CPykssy0xPLMnMz0vMAfIjo8ziDdyNQ_1CDQ2CXQx8PUI8zR39DQ0gQD84NS8-NFi_INtREQDuZoZi/?__Destination=HUB_IDV_CUSTOMER_MIGRATION&__IWLang=id&__IWCountry=ID&__registra','https://www2.secure.hsbcnet.com/uims/portal/IDV_CAM10_AUTHENTICATION;jsessionid=0000Yv0upagUfX1FGk5p4gzF9CV:12rfcdfs9?__trats=&__arxasnkucl=729a3adc9c305d55332e7464e223e3a80d87b8b94d16556519312707783a','1-500-700\r\n','N/A','https://www.about.hsbc.co.id/id-id/hsbc-in-indonesia/financial-and-regulatory-reports\r\n',100,1),(9,'GP-BNK-IDN-DBSI','111BNK','Bank DBS Indonesia\r\n','GP-BNK-IDN-DBSI LOGO.png','<p>Didirikan pada 1989, dan menjadi bagian dari kelompok usaha DBS Group di Singapura, PT Bank DBS Indonesia merupakan salah satu bank yang telah berdiri lama di Asia. Dengan 44 cabang dan 1.600 karyawan aktif di 11 kota besar di Indonesia, Bank DBS Indonesia menyediakan layanan perbankan menyeluruh untuk korporasi, usaha kecil dan menengah (SME), dan aktivitas perbankan konsumen. Diakui sebagai “Best Wealth Manager in Indonesia” oleh The Asset dan “Best Foreign Exchange Bank in Indonesia” oleh Global Finance, DBS Indonesia juga merupakan penerima predikat ‘Sangat Baik’ untuk kategori Aset Rp50 Triliun sampai dengan di bawah Rp100 Triliun dari Infobank. Pada tanggal 10 Februari 2018, Bank DBS Indonesia mengambil alih bisnis Retail dan Wealth dari Bank ANZ Indonesia</p>','GP-BNK-IDN-DBSI HEAD OFFICE.jpg','DBS Bank Tower, Jl. Prof.Dr. Satrio Kav. 3-5, Jakarta 12940\r\n','https://www.dbs.com/indonesia-bh/default.page\r\n','DBS Bank Ltd','Yes','No','N/A','Yes','Singapore','Indonesia','PT Bank DBS Indonesia\r\n','Yes','https://secure.id.dbsdigibank.com/login\r\n','https://ideal.dbs.com/loginSubscriberv2/login/pin\r\n','1-500-327\r\n','N/A','https://www.dbs.com/indonesia-bh/investor/financial-results.page\r\n',60,1),(10,'GP-BNK-IDN-BKPN','111BNK','Bank Bukopin\r\n','GP-BNK-IDN-BKPN LOGO.png','<p>Bank Bukopin didirikan pada tanggal 10 Juli 1970, sebelumnya dikenal sebagai Bank Umum Koperasi Indonesia. Pada 1989, perusahaan berganti nama menjadi Bank Bukopin. Selanjutnya, pada 1993 status perusahaan berubah menjadi perseroan terbatas.</p>\r\n\r\n<p>Bank Bukopin menfokuskan diri pada segmen UMKMK, saat ini telah tumbuh dan berkembang menjadi bank yang masuk ke kelompok bank menengah di Indonesia dari sisi aset. Seiring dengan terbukanya kesempatan dan peningkatan kemampuan melayani kebutuhan masyarakat yang lebih luas, Bank Bukopin telah mengembangkan usahanya ke segmen komersial dan konsumer.</p>\r\n\r\n<p>Ketiga segmen ini merupakan pilar bisnis Bank Bukopin, dengan pelayanan secara konvensional maupun syariah, yang didukung oleh sistem pengelolaan dana yang optimal, kehandalan teknologi informasi, kompetensi sumber daya manusia dan praktik tata kelola perusahaan yang baik. Landasan ini memungkinkan Bank Bukopin melangkah maju dan menempatkannya sebagai suatu bank yang kredibel.</p>\r\n\r\n<p>Berkantor pusat di Gedung Bank Bukopin, Jl MT Haryono Kav 50-51 Jakarta Selatan, operasional Bank Bukopin kini didukung oleh lebih dari 425 outlet yang tersebar di 22 provinsi di seluruh Indonesia yang terhubung secara real time online. Bank Bukopin juga telah membangun jaringan micro-banking yang diberi nama “Swamitra”, yang kini berjumlah 543 outlet, sebagai wujud program kemitraan dengan koperasi dan lembaga keuangan mikro.</p>\r\n\r\n<p>Dengan struktur permodalan yang semakin kokoh sebagai hasil pelaksanaan Initial Public Offering (IPO) pada bulan Juli 2006, Bank Bukopin terus mengembangkan program operasionalnya dengan menerapkan skala prioritas sesuai strategi jangka pendek yang telah disusun dengan matang. Penerapan strategi tersebut ditujukan untuk menjamin dipenuhinya layanan perbankan yang komprehensif kepada nasabah melalui jaringan yang terhubung secara nasional maupun internasional, produk yang beragam serta mutu pelayanan dengan standar yang tinggi. Keseluruhan kegiatan dan program yang dilaksanakan pada akhirnya berujung pada sasaran terciptanya citra Bank Bukopin sebagai lembaga perbankan yang terpercaya dengan struktur keuangan yang kokoh, sehat dan efisien. Keberhasilan membangun kepercayaan tersebut akan mampu membuat Bank Bukopin tetap tumbuh memberi hasil terbaik secara berkelanjutan.</p>\r\n','GP-BNK-IDN-BKPN HEAD OFFICE.jpg','Jl. MT. Haryono Kav. 50-51\n, Jakarta 12770','https://www.bukopin.co.id/\r\n','PT Bosowa Corporindo','No','N/A','N/A','No','Indonesia','Indonesia','PT Bank Bukopin Tbk\r\n','Yes','https://ib.bukopin.co.id/business/\r\n','https://bcm.bukopin.co.id/corp/common2/login.do?action=loginRequest\r\n','14005\r\n','N/A','https://www.bukopin.co.id/page/laporan-tahunan\r\n',100,1),(11,'GP-BNK-IDN-MYPD','111BNK','Bank Mayapada\r\n','GP-BNK-IDN-MYPD LOGO.png','<p>Pada tanggal 07 September 1989 di Jakarta, dibentuklah PT Bank Mayapada International melalui Akta Pendirian Bank yang disahkan pada tanggal 10 Januari 1990 oleh Menteri Kehakiman Republik Indonesia. Bank mulai beroperasi secara komersial pada tanggal 16 Maret 1990 dan sejak 23 Maret 1990 menjadi bank umum. Izin dari Bank Indonesia sebagai bank devisa  diperoleh pada tahun 1993. Pada tahun 1995 Bank berubah nama menjadi PT Bank Mayapada Internasional. Dari tahun 1997 hingga saat ini kami menjadi bank publik dengan nama PT. Bank Mayapada Internasional Tbk.</p>\r\n\r\n<p>Kegiatan usaha perusahaan yang utama adalah menghimpun dana masyarakat dan menyalurkan dana tersebut melalui pinjaman dalam bentuk fasilitas kredit. Sesuai dengan rencana bisnis bank, kegiatan perusahaan berfokus pada usaha retail dan consumerdan melayani dengan komitmen demi kepuasan para nasabah.</p>\r\n\r\n<p>Sejak didirikan, kinerja Bank Mayapada senantiasa mengalami perbaikan, mulai dari menjadi Perusahaan Terbuka (Tbk) , melalui penawaran obligasi, serta penambahan kantor cabang. Hingga saat ini Bank Mayapada memiliki jaringan pelayanan perbankan yang terdiri dari 36 Kantor Cabang, 67 kantor Capem, 75 kantor Fungsional, 12 kantor Kas dan 109 jaringan ATM, serta bekerja sama dengan ATM BERSAMA, dan ATM PRIMA/BCA sehingga total ATM yang dapat digunakan oleh nasabah sebanyak 76.129 ATM serta kartu ATM Bank Mayapada dapat digunakan sebagai debit card di lebih 338.724 merchant jaringan PRIMA yang tersebar di berbagai wilayah Indonesia.</p>\r\n\r\n<p>Bank Mayapada terus meningkatkan kompetensi, melakukan inovasi serta mengembangkan produk dan jasa perbankan bagi semua segmen bisnis. Inovasi dilakukan dengan mengikuti perkembangan teknologi informasi yang modern.</p>\r\n\r\n<p>Pada tanggal 08 Februari 2001, Bank Mayapada menerima sertifikat ISO 9002 yang merupakan sertifikasi Sistem Manajemen Mutu (Quality Management System) dalam bidang operasional perbankan. Sertfikasi tersebut berhasil dipertahankan hingga sekarang dan telah berubah nama menjadi ISO 9001:2008.</p>\r\n','GP-BNK-IDN-MYPD HEAD OFFICE.jpg','Mayapada Tower, Ground Floor - 3rd Floor\n, Jl. Jend. Sudirman Kav. 28\n, Jakarta 12920 - Indonesia\r\n','https://www.bankmayapada.com/\r\n','PT Mayapada Karunia','No','N/A','N/A','No','Indonesia','Indonesia','PT. Bank Mayapada International, Tbk\r\n','Yes','https://ibank.mayapadaonline.com/\r\n','N/A','1-500-029\r\n','N/A','https://www.bankmayapada.com/id/hubungan-investor/laporan-tahunan\r\n',100,1),(12,'GP-BNK-IDN-MEGA','111BNK','Bank Mega\r\n','GP-BNK-IDN-MEGA LOGO.png','<p>Berawal dari sebuah usaha milik keluarga bernama Bank Karman yang didirikan pada tanggal 15 April 1969 dan berkedudukan di Surabaya, selanjutnya pada tahun 1992 berubah nama menjadi Mega Bank dan melakukan relokasi kantor pusat ke Jakarta.</p>\r\n\r\n<p>Seiring dengan perkembangannya Mega Bank pada tahun 1996 diambil alih oleh CT Corp (d/h Para Group) (PT Para Global Investindo dan PT. Para Rekan Investama). Untuk lebih meningkatkan citra Mega Bank.</p>\r\n\r\n<p>Pada bulan Juni 1997 melakukan perubahan logo dengan tujuan bahwa sebagai lembaga keuangan kepercayaan masyarakat dengan akan lebih mudah dikenal melalui logo perusahaan yang baru berubah nama menjadi Bank Mega.</p>\r\n\r\n<p>Dalam rangka memperkuat struktur permodalan maka pada tahun yang sama Bank Mega melaksanakan Initial Public Offering dan listed di BEJ maupun BES. Dengan demikian sebagian saham Bank Mega dimiliki oleh publik dan berubah namanya menjadi PT. Bank Mega Tbk.</p>\r\n\r\n<p>Pada saat krisis ekonomi, Bank Mega mencuat sebagai salah satu bank yang tidak terpengaruh oleh krisis dan tumbuh terus tanpa bantuan pemerintah bersama dengan Citibank, Deutsche Bank dan HSBC.</p>\r\n\r\n<p>Bank Mega merupakan satu-satunya bank di Indonesia yang mobil operasionalnya menggunakan Livery Bank Mega. Dan strategi ini berhasil menanamkan image yang begitu kuat dikalangan gross root Bangsa Indonesia.</p>\r\n\r\n<p>Dan hingga kini Bank Mega masih merupakan bank yang kepemilikannya 100% milik warga Indonesia, saat mayoritas usaha di sektor keuangan Indonesia dimonopoli oleh asing.</p>','GP-BNK-IDN-MEGA HEAD OFFICE.jpg','Menara Bank Mega, Jl. Kapten P. Tendean No.12-14A\n, Jakarta 12790','https://www.bankmega.com/\r\n','PT Mega Corpora','No','N/A','N/A','No','Indonesia','Indonesia','PT Bank Mega Tbk\r\n','Yes','https://ibank.bankmega.com/business/common2/login.do?action=loginRequest\r\n','https://ibank.bankmega.com/corp/common2/login.do?action=loginRequest\r\n','1-500-010\r\n','N/A','https://www.bankmega.com/laporan_keuangan4.php\r\n',60,1),(13,'GP-BNK-IDN-UOBI','111BNK','Bank UOB Indonesia\r\n','GP-BNK-IDN-UOBI LOGO.png','<p>UOB Indonesia didirikan pada tanggal 31 Agustus 1956 dengan nama PT Bank Buana Indonesia. Pada bulan Mei 2011, berganti nama menjadi PT Bank UOB Indonesia.</p>\r\n<p>Jaringan layanan UOB Indonesia mencakup 41 kantor cabang, 168 kantor cabang pembantu dan 191 ATM yang tersebar di 54 kota di 18 provinsi. Layanan UOB Indonesia juga dapat dinikmati melalui jaringan ATM regional UOB, ATM Prima, ATM bersama serta jaringan VISA.</p>\r\n<p>UOB Indonesia dikenal sebagai Bank dengan fokus pada layanan Usaha Kecil Menengah (UKM), layanan kepada nasabah retail, serta mengembangkan bisnis consumer dan corporate banking melalui layanan tresuri dan cash management.</p>\r\n<p>Dengan jaringan layanan yang luas, sistem teknologi informasi, struktur permodalan yang sehat dan sumber daya manusia yang berkualitas, UOB Indonesia bertujuan menciptakan manfaat jangka panjang yang berkesinambungan bagi para pemangku kepentingan. Fokus UOB Indonesia senantiasa mengarah pada pembaharuan untuk menjadi Bank Premier melalui pertumbuhan yang berdisiplin dan stabilitas bisnis.</p>\r\n<p>UOB Indonesia melalui program Corporate Social Responsibility turut berpartisipasi aktif membangun komunitas. Kegiatan CSR UOBI fokus kepada dunia seni, pendidikan dan anak-anak. UOBI mengadakan kompetisi seni secara regular di Indonesia melalui UOB Painting of The Year. UOBI juga mendorong karyawannya untuk ikut serta di kegiatan sukarela, antara lain melalui UOB Heartbeat, Kegiatan Donor Darah dan Donasi Buku.</p>','GP-BNK-IDN-UOBI HEAD OFFICE.jpg','UOB Plaza, Jl M.H. Thamrin No. 10 Jakarta Pusat 10230','http://www.uob.co.id/about-us/index.page\r\n','UOB International Investment Private Limited','Yes','No','N/A','Yes','Singapore\r\n','Indonesia','PT Bank UOB Indonesia\r\n','Yes','https://pib.uob.co.id/PIBLogin/Public/processPreCapture.do?keyId=lpc&lang=in_ID\r\n','https://bibplus.uob.co.id/BIB/public\r\n','14008\r\n','UOBCare@uob.co.id\r\n','http://www.uob.co.id/about-us/investor-relations/laporan-keuangan/annual-report.page\r\n',100,1),(14,'GP-BNK-IDN-KEBH','111BNK','Bank KEB Hana\r\n','GP-BNK-IDN-KEBH LOGO.png','<p>PT Bank KEB Hana Indonesia (”Bank”) didirikan dengan nama PT Bank Pasar Pagi Madju berdasarkan Akta Pendirian No. 25 tanggal 27 April 1971 dengan pengesahan Menteri Kehakiman sesuai Keputusan No. Y.A.5/189/25 tanggal 25 Mei 1974.</p>\r\n<p>Seiring dengan perubahan status dari Bank Pasar menjadi Bank Umum, nama Bank Pasar Pagi Madju berubah menjadi PT Bank Bintang Manunggal (Bank Bima) berdasarkan Keputusan Menteri Keuangan Republik Indonesia No. 1306/KMK.013/1989 tanggal 30 November 1989. Pada tahun 2007, Hana Financial Group mengakuisisi Bank Bima sehingga berubah menjadi PT Bank Hana sesuai Keputusan Gubernur Bank Indonesia No. 10/20/KEP.BI/2008 tanggal 18 Maret 2008. PT Bank Hana kemudian melakukan penggabungan usaha dengan PT Bank KEB Indonesia pada tahun 2013 sehingga berubah menjadi PT Bank KEB Hana.</p>\r\n<p>Selanjutnya pada tahun 2014, nama PT Bank KEB Hana diubah menjadi PT Bank KEB Hana Indonesia dan disetujui oleh Otoritas Jasa Keuangan sesuai dengan Surat Keputusan Dewan Komisioner Otoritas Jasa Keuangan No.13/KDK.03/2014 tanggal 27 Juni 2014 tentang Penetapan Penggunaan Izin Usaha Atas Nama PT Bank Hana menjadi Izin Usaha Atas Nama PT Bank KEB Hana Indonesia.</p>','GP-BNK-IDN-KEBH HEAD OFFICE.jpg','Mangkuluhur City Tower One, Jend. Gatot Subroto Kav. 1-3, Jakarta 12930','https://www.kebhana.co.id/\r\n','KEB Hana Bank','Yes','No','N/A','Yes','South Korea','Indonesia','PT Bank KEB Hana Indonesia\r\n','Yes','https://myhana.co.id/gibPT/intn/main?lang=in\r\n','https://myhana.co.id/cbsPT/icbs/index\r\n','1-500-021\r\n','N/A','http://www.hanabank.co.id/about/ir/annualReport\r\n',100,1),(15,'GP-BNK-IDN-WRSD','111BNK','Bank Woori Saudara\r\n','GP-BNK-IDN-WRSD LOGO.png','<p>1906 - Himpoenan Soedara berdiri atas prakarsa 10 saudagar Pasar Baru</p>\r\n\r\n<p>1913 - Disahkan sebagai Badan Hukum berstatus “Vereeniging”</p>\r\n\r\n<p>1975- Menjadi Badan Hukum dengan nama “PT. Bank Tabungan Himpunan Saudara 1906”</p>\r\n\r\n<p>1991 - Medco Group masuk menjadi Pemegang Saham Pengendali</p>\r\n\r\n<p>1993 - Beroperasi sebagai Bank Umum dengan nama “PT. Bank HS 1906” yang diikuti perubahan logo</p>\r\n\r\n<p>2006 - Identitas korporat berubah dari Bank HS 1906 menjadi Bank Saudara sekaligus menjadi Perusahaan publik/terbuka</p>\r\n\r\n<p>2007 - Perubahan susunan Pengurus Perseroan serta penambahan layanan menjadi salah satu Bank Kustodian</p>\r\n\r\n<p>2008 - Izin beroperasi menjadi Bank Devisa</p>\r\n\r\n<p>2009 - Bank Saudara melakukan Penawaran Umun Terbatas-I (PUT-I) dengan Hak Memesan Efek Terlebih Dahulu (HMETD) sejumlah 750.000.000 (Tujuh ratus lima puluh juta) saham, dengan nilai nominal Rp. 100 (Seratus rupiah)</p>\r\n\r\n<p>2011 - Penerbitan Obligasi Bank Saudara I Tahun 2011 Dalam rangka mengembangkan pasar kredit di Indonesia, Bank Saudara menerbitkan Obligasi Bank Saudara I Tahun 2011 senilai Rp 250 miliar yang listing di Bursa Efek Indonesia tanggal 2 Desember 2011.</p>\r\n\r\n<p>2012 - Pada 29 Oktober 2012 Bank Saudara menerbitkan Obligasi Subordinasi Bank Saudara I dan Obligasi Bank Saudara II tahun 2012 sebesar Rp 300 Miliar.</p>\r\n\r\n<p>2013 - Grand Opening Gedung Bank Saudara sekaligus bertepatan dengan HUT Bank Saudara ke 107 pada 18 April 2013. Kantor Pusat Bank Saudara yang semula berlokasi di Jalan Buah Batu No. 58 Bandung kemudian pindah ke Gedung Bank Saudara di Jalan Diponegoro No. 28 Bandung.<br />\r\n- PT Bank Himpunan Saudara 1906, Tbk, telah memperoleh persetujuan dari Bank Indonesia melalui surat tertanggal 30 Desember 2013 terkait pembelian 33% (tiga puluh tiga persen) saham Bank Saudara oleh Woori Bank Korea.</p>','GP-BNK-IDN-WRSD HEAD OFFICE.jpg','Treasury Tower, Distric 8 lot 28 SCBD, Jl. Jend. Sudirman Kav 52-53, Jakarta 12190','http://www.bankwoorisaudara.com/\r\n','Woori Bank Korea','Yes','No','N/A','Yes','South Korea','Indonesia','PT. Bank Woori Saudara Indonesia 1906, Tbk\r\n','Yes','https://ib.bankwoorisaudara.com/individual/login\r\n','https://ib.bankwoorisaudara.com/bisnis/login\r\n','1-500-008\r\n','customercare@bankwoorisaudara.com\r\n','http://www.bankwoorisaudara.com/content/12\r\n',100,1),(16,'GP-BNK-IDN-SHHN','111BNK','Bank Shinhan\r\n','GP-BNK-IDN-SHHN LOGO.png','<p>Launched on March 4th, 2016. Following the acquisition of BME and CNB, Shinhan Bank Indonesia becomes a subsidiary of Shinhan Bank. Guided by the Group mission of Compassionate Finance, Shinhan Bank Indonesia will deliver outstanding results and become the number one foreign bank in Indonesia</p>','GP-BNK-IDN-SHHN HEAD OFFICE.jpg','International Financial Center 2. Jalan Jenderal Sudirman Kav. 22-23, Jakarta Selatan 12920','https://www.shinhan.co.id/\r\n','Shinhan Bank, Co. Ltd.','Yes','No','N/A','Yes','South Korea','Indonesia','PT. Bank Shinhan Indonesia\r\n','Yes','https://online.shinhan.co.id/global.shinhan\r\n','https://online.shinhan.co.id/global.shinhan\r\n','62-21-2975-1500\r\n','N/A','https://www.shinhan.co.id/annual-report\r\n',100,1),(17,'GP-BNK-IDN-DINR','111BNK','Bank Dinar\r\n','GP-BNK-IDN-DINR LOGO.png','<p>PT Bank Dinar Indonesia Tbk merupakan salah satu Bank Umum Swasta Nasional Non-Devisa yang didirikan di Jakarta pada tanggal 15 Agustus 1990 dengan Akta Notaris James Herman Rahardjo, SH. No. 99. Ijin operasi sebagai Bank Umum ditetapkan melalui surat Bank Indonesia tertanggal 22 November 1991. Pada awal berdirinya Bank ini bernama PT. Bank Liman International terhitung sejak tanggal 8 November 2012 dilakukan rebranding dari PT Bank Liman International menjadi PT Bank Dinar Indonesia (Bank Dinar).</p>','GP-BNK-IDN-DINR HEAD OFFICE.jpg','Ir. H. Juanda No 12 Jakarta Pusat','https://bankdinar.co.id/\r\n','APRO Financial Co., Ltd.','Yes','No','N/A','Yes','South Korea','Indonesia','PT Bank Dinar Indonesia Tbk\r\n','No','N/A','N/A','62-21-231-2633\r\n','cs@bankdinar.co.id\r\n','https://bankdinar.co.id/laporan-tahunan/\r\n',100,1),(18,'GP-BNK-IDN-AMAR','111BNK','Bank Amar\r\n','GP-BNK-IDN-AMAR LOGO.png','<p>PT Bank Amar Indonesia lebih dikenal Amar Bank dahulu PT Anglomas International Bank lebih dikenal Amin Bank didirikan pada 15 Maret 1991 oleh keluarga almarhum Noto Suhardjo Wibisono (Lioe Kiem Tjauw) dan Hartini Wibisono (Tan Sioe Ing) di Surabaya. Kepercayaan yang diberikan nasabah selama ini menjadi pilar utama untuk terus tumbuh dan berkembang dalam menjalankan fungsinya sebagai lembaga intermediasi.</p>\r\n\r\n<p>Sejak awal beroperasi, Bank berusaha mengukuhkan eksistensinya dalam dunia perbankan nasional dan pada akhir 2014 Bank telah memiliki 4 jaringan kantor yang siap melayani nasabah di wilayah Jawa Timur dan DKI Jakarta. Dengan fokus pada segmen retail, Bank berusaha memenuhi kebutuhan finansial nasabah</p>','GP-BNK-IDN-AMAR HEAD OFFICE.jpg','Jl. Basuki Rahmad No. 109 \nSurabaya 60271','https://amarbank.co.id/\r\n','Tolaram Group Inc.','No','N/A','N/A','No','Indonesia','Indonesia','PT Bank Amar Indonesia\r\n','No','N/A','N/A','62-21-4000-5859\r\n','tanya@amarbank.co.id \r\n','https://amarbank.co.id/about-us\r\n',100,1),(19,'GP-BNK-IDN-RSPD','111BNK','Bank Resona Perdania\r\n','GP-BNK-IDN-RSPD LOGO.png','<p>Pada tanggal 1 Februari 1958, Bank Resona Perdania, yang saat itu bernama Bank Perdania, memulai beroperasi di industri perbankan Indonesia berdasarkan Surat Keputusan Menteri Keuangan No. 260486/U.M.II tanggal 23 Desember 1957 mengenai izin untuk melakukan usaha sebagai Bank Umum. Berkantor pusat di Jakarta, Bank Resona Perdania merupakan bank joint venture pertama di Indonesia yang menjadi bukti nyata usaha Indonesia dan Jepang untuk menciptakan kerja sama ekonomi yang lebih baik, melalui pemberian pelayanan terbaik (service excellence) dalam memenuhi setiap kebutuhan perbankan nasabah. Dalam rangka mengoptimalkan perannya, pada bulan April 1969, Bank resmi beroperasi sebagai bank devisa untuk dapat melakukan kegiatan usaha perbankan dalam valuta asing.</p>\r\n\r\n<p>Mengikuti tantangan zaman dan tuntutan industri yang semakin kompleks, agresif, serta kompetitif, Bank pun melakukan beberapa kali pergantian nama. Setelah berdiri dengan nama Bank Perdania, Bank mengubah namanya menjadi Daiwa Perdania Bank pada tahun 1994. Kemudian pada tahun 1999, Bank kembali mengganti namanya menjadi Bank Daiwa Perdania. Terakhir, pada tahun 2003, nama Bank berganti menjadi Bank Resona Perdania seperti yang dikenal sampai saat ini.</p>\r\n\r\n<p>Berlandaskan semangat untuk menjadi “Your Real Partner”, Bank Resona Perdania memiliki kesungguhan untuk selalu maju dan berkembang bersama nasabah. Komitmen ini diwujudkan dengan memberikan beragam produk dan layanan yang menjadi solusi keuangan nasabah, seperti kredit, pendanaan, tresuri, impor, ekspor, bank garansi dan kegiatan transaksi perbankan lainnya.</p>\r\n\r\n<p>Berbekal dukungan penuh dari para pemegang saham yang memiliki reputasi baik dan berpengalaman di bidangnya, Bank Resona Perdania siap melaju meneruskan kinerja cemerlangnya sebagai bank yang kredibel dan dapat selalu diandalkan dengan menyediakan kualitas layanan keuangan terbaik.</p>','GP-BNK-IDN-RSPD HEAD OFFICE.jpg','Menara Mulia, Jend. Gatot Subroto Kav. 9 - 11, Karet Semanggi,Setiabudi Jakarta 12930, Indonesia','http://www.perdania.co.id/\r\n','RESONA BANK, LTD., JEPANG','Yes','No','N/A','No','Indonesia','Indonesia','PT. Bank Resona Perdania\r\n','Yes','https://pdirect.perdania.co.id/sslvpn/Login/Login\r\n','N/A','62-21-570-1445 \r\n','N/A','http://www.perdania.co.id/corporate-secretary/annual-report/\r\n',100,1),(20,'GP-MFN-IDN-CNAF','211MFN','CIMB Niaga Auto Finance\r\n','GD-MFN-IDN-CNAF LOGO.png','<p>CIMB NIAGA AUTO FINANCE ADALAH PERUSAHAAN\r\nPEMBIAYAAN TERBESAR KE-7 DI INDONESIA</p>\r\n\r\n<p>PT CIMB Niaga Auto Finance (“CNAF” atau “Perusahaan”) didirikan pada 10 Desember 1981 dengan nama PT Saseka Gelora Leasing. Pada Agustus 1993, Perusahaan kemudian berganti nama menjadi PT Saseka Gelora Finance dengan fokus bisnis adalah sewa guna usaha.</p>\r\n\r\n<p>Pada tahun 1996, PT Bank CIMB Niaga Tbk (dahulu PT Bank Niaga Tbk) menjadi pemegang saham mayoritas Perusahaan dengan 79.65% kepemilikan saham dan pada tahun 2007 PT Bank CIMB Niaga Tbk (“CIMB Niaga”) kembali menambah porsi kepemilikannya menjadi 95,91%. Pada Oktober 2009, seiring dengan rencana PT Bank CIMB Niaga Tbk untuk lebih serius menggarap bisnis bisnis pembiayaan, Perusahaan melakukan transformasi dengan melakukan perubahan pada fokus bisnis dari sewa guna usaha menjadi pembiayaan konsumen, khususnya kendaraan bermotor. Pada Agustus 2010, Perusahaan berganti nama menjadi PT CIMB Niaga Auto Finance. Perubahan nama ini juga disertai dengan perubahan logo Perusahaan.</p>\r\n\r\n<p>Pada tengah tahun 2015, CIMB Niaga selaku pemegang saham mayoritas dari CNAF dan PT Kencana Internusa Artha Finance (“KITAF”) telah memutuskan untuk melakukan penggabungan kedua bisnis kendaraan bermotor dengan segmen usaha yang sama tersebut, dimana CNAF bertindak sebagai perusahaan penerima penggabungan. Rencana tersebut mendapat persetujuan dari Otoritas Jasa Keuangan (OJK) pada 19 November 2015, kemudian disusul oleh persetujuan pemegang saham lewat Rapat Umum Pemegang Saham (RUPS) Luar Biasa pada 23 Desember 2015 dan dari Kementrian Hukum dan Hak Asasi Manusia pada 23 Desember 2015. Penggabungan antara CNAF dan KITAF berlaku efektif per 1 Januari 2016.</p>','GD-MFN-IDN-CNAF HEAD OFFICE.jpg','Menara Sentraya Lt. 28.\nJl. Iskandarsyah Raya No.1 A Kebayoran Baru, Jakarta Selatan 12160.\nIndonesia','https://www.cnaf.co.id/\r\n','PT. Bank CIMB Niaga TBk','Yes','Yes','GP-BNK-IDN-CMNG\r\n','No','Indonesia','Indonesia','PT CIMB Niaga Auto Finance\r\n','N/A','N/A','N/A','N/A','N/A','https://www.cnaf.co.id/publikasi/laporan-tahunan\r\n',100,1),(21,'GP-SCB-IDN-BRKS','431SCB','Bareksa\r\n','GP-SCB-IDN-BRKS LOGO.png','<p>Bareksa.com adalah marketplace reksa dana online terintegrasi pertama di Indonesia, yang bernaung di bawah PT Bareksa Portal Investasi yang didirikan pada tanggal 17 Februari 2013. Portal ini, selain menyediakan platform untuk melakukan jual-beli reksa dana secara online, juga memberikan layanan data, informasi, dan alat investasi reksa dana, saham, obligasi, dan lainnya, untuk memudahkan masyarakat berinvestasi.</p>\r\n\r\n<p>Didirikan dan diawaki anak-anak muda yang berpengalaman di bidang pasar modal, teknologi informatika dan media digital, Bareksa.com memiliki visi dan misi untuk ikut menumbuh kembangkan dunia investasi di kalangan masyarakat Indonesia dengan memanfaatkan teknologi informatika dan Internet. Bareksa.com juga berambisi turut serta semakin meningkatkan eksposur dunia investasi nasional ke pasar global.</p>\r\n\r\n<p>Dalam Bahasa Sansekerta, \'bareksa\' berarti pohon. Adalah cita-cita kami, sebagaimana namanya, kehadiran Bareksa.com dapat turut membuat dunia investasi nasional semakin tumbuh rindang dan berbuah lebat bagi kesejahteraan masyarakat luas.</p>','GP-SCB-IDN-BRKS HEAD OFFICE.jpg','Wisma Lembawai\n, Jl. Bangka Raya No.27 G-H, \nKemang, Jakarta 12720','https://www.bareksa.com/\r\n','N/A','N/A','N/A','N/A','No','Indonesia','Indonesia','PT. Bareksa Portal Investasi\r\n','Yes','https://www.bareksa.com/en/member/login\r\n','N/A','62-21-717-909-70\r\n','cs@bareksa.com\r\n','N/A',80,1),(22,'GP-SCB-IDN-PNSK','431SCB','Panin Sekuritas\r\n','GP-SCB-IDN-PNSK LOGO.png','<p>Panin Sekuritas merupakan salah satu Perusahaan Efek terkemuka yang hadir untuk jawab kebutuhan investasi para Investor, baik perorangan, perusahaan swasta, maupun lembaga pemerintahan. Solusi cerdas yang kami berikan didasari oleh pengetahuan yang mendalam atas kebutuhan investasi para Nasabah kami.</p>\r\n\r\n<p>Panin Sekuritas berkomitmen untuk untuk melebarkan sayap pelayanannya kepada Nasabah melalui media internet. Dalam era keuangan global dan revolusi teknologi, Perusahaan kami akan tetap berinovasi, baik dari segi pengetahuan maupun ketrampilan teknis serta dalam berbagai sisi kegiatan usaha. Manajemen dan staf kami yang berpengalaman di bidangnya dengan bangga melayani segala kebutuhan investasi para Nasabah.</p>','GP-SCB-IDN-PNSK HEAD OFFICE.jpg','Gedung Bursa Efek Indonesia, Tower II Suite 1705. Jl. Jendral Sudirman Kav. 52-53. Jakarta Selatan 12190, Indonesia','https://www.pans.co.id/\r\n','PT Bank Panin','Yes','Yes','GP-BNK-IDN-PANN\r\n','No','Indonesia','Indonesia','PT. Panin Sekuritas Tbk\r\n','Yes','https://www.post-pro.co.id/mi3/index.html\r\n','N/A','62-21-2977-3655\r\n','info@pans.co.id\r\n','https://www.pans.co.id/info-investor/laporan-tahunan\r\n',100,1),(23,'GP-IRB-IDN-PRMR','551IRB','Premiro\r\n','GP-IRB-IDN-PRMR LOGO.png','<p>Premiro dikelola di bawah PT Mitra Ibisnis Terapan (MIT) , pialang Asuransi berbasis teknologi yang sepenuhnya dimiliki oleh PT Mitra Iswara & Rorimpandey, pialang asuransi terkemuka yang telah berdiri lebih dari 40 tahun. Sejalan dengan pengalaman itu kami dapat membantu anda dalam menemukan, membandingkan dan membeli asuransi dengan kualitas terbaik.</p>\r\n\r\n<p>Kami memberdayakan Anda, para pelanggan, dengan memberi kesempatan kepada mitra asuransi rekanan kami untuk bersaing sehingga Anda dapat memilih asuransi perjalanan atau kendaraan bermotor yang paling sesuai. Cepat dan handal, kami membuat proses memilih dan membeli asuransi sesederhana mungkin. Sangat mudah, seluruh proses dapat dilakukan dalam waktu lima menit.</p>\r\n\r\n<p>Kami ingin Anda memegang kendali. Dengan memberikan kebebasan memilih asuransi yang paling sesuai dengan kebutuhan Anda.</p>','GP-IRB-IDN-PRMR HEAD OFFICE.jpg','Wisma Slipi 7th Floor, Suite 710. Jl. S.Parman Kav 12, Slipi\n, Jakarta Barat 11480','https://www.premiro.com/\r\n','PT Mitra Iswara & Rorimpandey','Yes','No','N/A','No','Indonesia','Indonesia','PT Mitra Ibisnis Terapan\r\n','Yes','https://www.premiro.com/\r\n','N/A','62-21-395-00123\r\n','cs@premiro.com\r\n','N/A',100,1),(24,'GP-IRB-IDN-KBRU','551IRB','KBRU\r\n','GP-IRB-IDN-KBRU LOGO.png','<p>We offer total service, catering for all of our client\'s requirement \"\"Using our knowledge and experience, we will address any products which may be useful for our client</p>\r\n\r\n<p>A response is provided by return fax whenever possible \"\"Our Golden Rule is that everything must be in writing, whenever possible, to avoid misunderstanding and doing business professionally and responsively</p>\r\n\r\n<p>Flexible, Innovative, Conscious, and Highly Responsible \"\"Our services are responsive and creative, we work dynamically but always concerns with price, innovation and responsibility to gain the best coverage available in the market. This is done by our wide range of insurance network both locally and overseas</p>','GP-IRB-IDN-KBRU HEAD OFFICE.jpg','Komplek Mega Grosir Cempaka Mas Blok E nomor 1-6, Jl. Letjen Suprapto, Jakarta 10640','http://kbru.co.id/kbru/home\r\n','N/A','N/A','N/A','N/A','No','Indonesia','Indonesia','PT. Kalibesar Raya Utama\r\n','Yes','N/A','https://www.kbru.co.id/\r\n','62-21-4288-3707\r\n','info@kbru.co.id\r\n','N/A',60,1),(25,'GP-FRX-IDN-VIPR','711FRX','VIT\r\n','GP-FRX-IDN-VIPR LOGO.png','<p>VIP Money Changer adalah salah satu pedagang valuta asing terkemuka di Jakarta yang berdiri sejak tahun 1995. Mempertahankan reputasi yang baik bukanlah hal yang mudah, kami selalu berusaha yang terbaik untuk memuaskan Anda, pelanggan kami. Usaha kami beradaptasi sesuai dengan kemajuan zaman untuk memastikan bahwa kinerja kami sesuai dengan harapan Anda. Alhasil, dengan bangga kami telah melayani Anda lebih dari satu dekade and akan terus melayani Anda untuk ke depannya.</p>\r\n\r\n<p>VIP Money Changer adalah Pedagang Valuta Asing (PVA) Berizin sesuai dengan ketentuan bank Indonesia. Kami mengenal keaslian dan menjaga kualitas produk kami sehingga pelanggan kami terlindungi.</p>','GP-FRX-IDN-VIPR HEAD OFFICE.jpg','Menteng Raya No. 23\nJakarta Pusat, DKI Jakarta 10340\nIndonesia','https://vip.co.id/\r\n','N/A','N/A','N/A','N/A','No','Indonesia','Indonesia','PT. Valuta Inti Prima\r\n','No','N/A','N/A','62-21-3190-7777\r\n','support@vip.co.id\r\n','N/A',100,1),(26,'GP-FRX-IDN-PENT','711FRX','Peniti\r\n','GP-FRX-IDN-PENT LOGO.png','<p>We established since 1997 as a foreign trading company (PT Peniti Valasindo) which then expand to travel service company (PT Peniti Travel Services) and money transfer company (PT Peniti Money Remittance).</p>','GP-FRX-IDN-PENT HEAD OFFICE.jpg','Gedung Graha Kencana Lt. Dasar\n, Jl. Raya Perjuangan Kav 88\n, Jakarta 11530','http://peniti.co.id/beta/\r\n','N/A','N/A','N/A','N/A','No','Indonesia','Indonesia','PT Peniti Valasindo \r\n','No','N/A','N/A','62-21-535-9056\r\n','N/A','N/A',70,1),(27,'GP-RMT-IDN-WSTU','721RMT','Western Union\r\n','GP-RMT-IDN-WSTU LOGO.png','<p>We are a global leader in cross-border, cross-currency money movement. From small businesses and global corporations, to families near and far away, to NGOs in the most remote communities on Earth, Western Union helps people and businesses move money - to help grow economies and realize a better world. In 2018, we completed more than 800 million transactions for our consumer and business clients. We continue to innovate, developing new ways to send money through digital, mobile, and retail channels, with an array of convenient pay-out options to meet business and consumer needs.</p>','GP-RMT-IDN-WSTU HEAD OFFICE.jpg','7001 E. Belleview, Denver, CO 80237','https://westernunion.co.id \r\n','N/A','N/A','N/A','N/A','Yes','USA','Indonesia','N/A\r\n','No','N/A','N/A','62-21-3040-5730 \r\n','CustomerService.Asia@westernunion.com\r\n','N/A',100,1),(28,'GP-RMT-IDN-XNDT','721RMT','Xendit\r\n','GP-RMT-IDN-XNDT LOGO.png','<p>Sejak pertengahan 2015 tim Xendit telah berfokus pada pembuatan dampak di dunia dengan membuat proses pengiriman uang yang sederhana dan user-friendly. Tujuan kami adalah untuk melakukan pembayaran mudah, untuk teman-teman, keluarga atau bisnis-bisnis. Kami sudah berbincang dengan orang-orang dari setiap benua dan dari setiap jalan kehidupan, mencoba untuk memahami bagaimana kita dapat menggunakan pengetahuan kita tentang infrastruktur teknologi pembayaran untuk membangun sesuatu yang memberikan nilai nyata bagi pelanggan kami.</p>\r\n\r\n<p>Sebagai hasil dari penemuan kami, kami fokus pada produk bangunan untuk Asia Tenggara dan memilih Indonesia sebagai market pertama kami. Sebuah market dimana e-commerce dan lingkungan online berkembang pesat , tim kami telah bersatu di sekitar tujuan untuk menyederhanakan sistem yang harus lebih nyaman dan user-friendly - mengirim uang sepenuhnya dengan otomatis dan real-time! Dengan inovasi dalam keamanan, teknologi pembayaran, dan perangkat, kami percaya transaksi pembayaran dapat dilakukan dengan mudah dan aman. Menerima dan mengirim uang tidak akan pernah menjadi seperti waktu cosuming seperti sebelumnya. Kami membebaskan transaksi bisnis dari token.</p>\r\n\r\n<p>Kami adalah keluarga: kami tertawa, kami berdebat, kami berjuang untuk tujuan yang sama: Just Xendit (send it) di mana saja dan kapan saja.</p>','GP-RMT-IDN-XNDT HEAD OFFICE.jpg','Rukan Grand Panglima Polim Kav.78\n, Jl. Panglima Polim Raya\nPulo, Kebayoran Baru\n, Jakarta Selatan 12160','https://www.xendit.co/id/\r\n','N/A','N/A','N/A','N/A','Yes','Indonesia','Indonesia','PT. Sinar Digital Terdepan\r\n','Yes','https://dashboard.xendit.co/index.html\r\n','N/A','62-855-7467-0343\r\n','help@xendit.co \r\n','N/A',100,1),(29,'GP-EWP-IDN-LNKJ','622EWP','LinkAja\r\n','GP-EWP-IDN-LNKJ LOGO.png','<p>LinkAja adalah layanan atau produk uang elektronik sinergi layanan keuangan elektronik milik PT Telekomunikasi Selular (“Telkomsel”) dengan merek TCASH, PT Bank Mandiri (Persero) Tbk., dengan merek E-Cash, PT. Bank Negara Indonesia (Persero), Tbk. dengan merek UnikQu, dan PT Bank Rakyat Indonesia (Persero) Tbk., dengan merek T-Bank menjadi satu produk uang elektronik yang diterbitkan dan dioperasikan oleh PT. Fintek Karya Nusantara (Finarya).</p>','GP-EWP-IDN-LNKJ HEAD OFFICE.jpg','Telkom Landmark Tower, Jl. Jenderal Gatot Subroto Kav. 52, Jakarta – Indonesia','https://www.linkaja.id/\r\n','N/A','N/A','N/A','N/A','No','Indonesia','Indonesia','PT. Fintek Karya Nusantara \r\n','Yes','https://www.linkaja.id/linkaja-app\r\n','N/A','150-911\r\n','N/A','N/A',40,1),(30,'GP-EWP-IDN-JENS','622EWP','Jenius\r\n','GP-EWP-IDN-JENS LOGO.png','<p>Jenius adalah aplikasi perbankan revolusioner yang dilengkapi dengan kartu debit Visa untuk membantu kamu melakukan aktivitas finansial seperti menabung, bertransaksi, atau mengatur keuangan dengan lebih aman, cerdas, dan simpel. Semua dilakukan dari satu tempat, semua dari smartphone.</p>','GP-EWP-IDN-JENS HEAD OFFICE.jpg','Menara BTPN - LG, CBD Mega Kuningan, Jl. Dr. Ide Anak Agung Gde Agung Kav. 5.5 – 5.6, Jakarta 12950','https://www.jenius.com/\r\n','PT Bank BTPN Tbk','Yes','Yes','GP-BNK-IDN-BTPN\r\n','No','Indonesia','Indonesia','PT Bank BTPN Tbk\r\n','Yes','https://2secure.jenius.co.id/#/authentication/login\r\n','N/A','1-500-365\r\n','jenius-help@btpn.com\r\n','N/A',80,1),(31,'GP-EWP-IDN-VOVO','622EWP','Ovo\r\n','GP-EWP-IDN-VOVO LOGO.png','<p>OVO adalah aplikasi smart  yang memberikan Anda kemudahan dalam bertransaksi (OVO Cash) dan juga kesempatan yang lebih besar untuk mengumpulkan poin di banyak tempat (OVO Points). OVO, aplikasi pembayaran Serba Bisa, Simpel, Instan dan Aman yang siap buat segala transaksi finansial Anda jadi lebih menyenangkan.</p>','GP-EWP-IDN-VOVO HEAD OFFICE.jpg','Lippo Kuningan Lt. 20,\nJl. HR. Rasuna Said Kav. B-12 Setiabudi, Jakarta 12940','https://www.ovo.id/\r\n','N/A','N/A','N/A','N/A','No','Indonesia','Indonesia','PT. Visionet Internasional\r\n','Yes','https://apps.apple.com/ID/app/id1142114207?mt=8\r\n','N/A','1-500-696\r\n','cs@ovo.id\r\n','N/A',100,1),(32,'GP-EWP-IDN-DANA','622EWP','Dana\r\n','GP-EWP-IDN-DANA LOGO.png','<p>DANA merupakan bentuk baru pembayaran dalam genggaman Anda. Dengan menggunakan DANA, anda bisa melakukan berbagai macam transaksi elektronik melalui berbagai layanan yang tersedia, seperti Saldo DANA, Transfer Bank, Kartu Kredit, dan juga Setor Tunai ke minimarket.</p>','GP-EWP-IDN-DANA HEAD OFFICE.jpg','Capital Place Lantai 18, Jl. Gatot Subroto, RT.6/RW.1, Kuningan Barat, Mampang Prapatan., Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12790','https://dana.id/\r\n','N/A','N/A','N/A','N/A','No','Indonesia','Indonesia',' PT. Espay Debit Indonesia Koe\r\n','Yes','https://dana.id/download\r\n','N/A','1-500-445\r\n','help@dana.id \r\n','N/A',100,1),(33,'GP-BNK-IDN-MYBN','111BNK','Bank Maybank','GP-BNK-IDN-MYBN LOGO.png','<p>PT Bank Maybank Indonesia Tbk (“Maybank Indonesia” atau “Bank”) adalah salah satu bank swasta terkemuka di Indonesia yang merupakan bagian dari grup Malayan Banking Berhad (Maybank), salah satu  grup penyedia layanan keuangan terbesar di ASEAN. Sebelumnya, Maybank Indonesia bernama PT Bank Internasional Indonesia Tbk (BII) yang didirikan pada 15 Mei 1959, mendapatkan ijin sebagai bank devisa pada 1988 dan mencatatkan sahamnya sebagai perusahaan terbuka di Bursa Efek Jakarta dan Surabaya (sekarang telah merger menjadi Bursa Efek Indonesia) pada 1989.</p>\r\n<p>Maybank Indonesia menyediakan serangkaian produk dan jasa komprehensif bagi nasabah individu maupun korporasi melalui layanan Community Financial Services (Perbankan Ritel dan Perbankan Non-Ritel) dan Perbankan Global, serta pembiayaan otomotif melalui entitas anak yaitu WOM Finance untuk kendaraan roda dua dan Maybank Finance untuk kendaraan roda empat. Maybank Indonesia juga terus mengembangkan layanan dan kapasitas digital banking melalui Mobile Banking, Internet Banking, Maybank2U (mobile banking berbasis internet banking dan berbagai saluran lainnya.</p>\r\n<p>Per 31 Desember 2018, Maybank Indonesia memiliki 386 cabang termasuk cabang Syariah yang tersebar di Indonesia serta satu cabang luar negeri (Mumbai, India), 21 Mobil Kas Keliling dan 1.609 ATM termasuk CDM (Cash Deposit Machine) yang terkoneksi dengan lebih dari 20.000 ATM tergabung dalam jaringan ATM PRIMA, ATM BERSAMA, ALTO, CIRRUS dan terhubung dengan 3.500 ATM Maybank di Singapura, Malaysia dan Brunei. Hingga akhir tahun 2018, Maybank Indonesia mengelola simpanan nasabah sebesar Rp116,8 triliun dan memiliki total aset senilai Rp177,5 triliun.</p>','GP-BNK-IDN-MYBN HEAD OFFICE.jpg','\"Sentral Senayan III\nJl. Asia Afrika No.8\nKelurahan Gelora Kecamatan Tanah Abang\nJakarta Pusat\nDKI Jakarta Raya 10270\"\r\n','https://www.maybank.co.id/Pages/Home.aspx\r\n','Malayan Banking Bhd\r\n','Yes','No','N/A','Yes','Malaysia\r\n','Indonesia','PT Bank Maybank Indonesia Tbk\r\n','Yes','https://m2u.maybank.co.id/common/Login.do\r\n','https://m2e.maybank.co.id/m2e/portal/portal.view\r\n','69811\r\n','customercare@maybank.co.id\r\n','https://www.maybank.co.id/investor/annual0report/Pages/Annual-Report.aspx\r\n',100,1);
/*!40000 ALTER TABLE `principals` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `principals_categories`
--

DROP TABLE IF EXISTS `principals_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `principals_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `is_eligible` enum('in_scope','out_scope') DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `principals_categories`
--

LOCK TABLES `principals_categories` WRITE;
/*!40000 ALTER TABLE `principals_categories` DISABLE KEYS */;
INSERT INTO `principals_categories` VALUES (1,'111BNK','Bank','Bank','Logo Principal Category - Bank.png','in_scope',1),(2,'121CCD','Credit card issuer\r\n','Credit card issuer\r\n','Logo Principal Category - Credit card issuer.png','in_scope',1),(3,'211MFN','Multifinance companies\r\n','Multifinance companies\r\n','Logo Principal Category - Multifinance companies.png','in_scope',1),(4,'631P2P','P2P Platform\r\n','P2P Platform\r\n','Logo Principal Category - P2P Platform.png','in_scope',1),(5,'431SCB','Securities broker \r\n','Securities broker \r\n','Logo Principal Category - Securities broker.png','in_scope',1),(6,'421AMG','Asset management \r\n','Asset management \r\n','Logo Principal Category - Asset management.png','in_scope',1),(7,'441MFA','Mutual fund agent\r\n','Mutual fund agent\r\n','Logo Principal Category - Mutual fund agent','in_scope',1),(8,'511LIC','Life insurance companies\r\n','Life insurance companies\r\n','Logo Principal Category - Life insurance companies.png','in_scope',1),(9,'512GIC','Non-life insurance companies\r\n','Non-life insurance companies\r\n','Logo Principal Category - Non-life insurance companies.png','in_scope',1),(10,'551IRB','Insurance broker companies\r\n','Insurance broker companies\r\n','Logo Principal Category - Insurance broker companies.png','in_scope',1),(11,'621EMP','E-money platform\r\n','E-money platform\r\n','Logo Principal Category - E-money platform.png','in_scope',1),(12,'622EWP','E-wallet platform\r\n','E-wallet platform\r\n','Logo Principal Category - E-wallet platform.png','in_scope',1),(13,'711FRX','Forex companies\r\n','Forex companies\r\n','Logo Principal Category - Forex companies.png','in_scope',1),(14,'721RMT','Remittance companies\r\n','Remittance companies\r\n','Logo Principal Category - Remittance companies.png','in_scope',1),(15,'821IVA','Investment advisor\r\n','Investment advisor\r\n','Logo Principal Category - Investment advisor.png','in_scope',1),(16,'831SCP','Securities portal\r\n','Securities portal\r\n','Logo Principal Category - Securities portal.png','in_scope',1),(17,'221CRU','Credit Union\r\n','Credit Union\r\n',NULL,'out_scope',1),(18,'311IBK','Investment banking\r\n','Investment banking\r\n',NULL,'out_scope',1),(19,'411SCU','Securities underwriter\r\n','Securities underwriter\r\n',NULL,'out_scope',1),(20,'412CUB','Custodian Bank\r\n','Custodian Bank\r\n',NULL,'out_scope',1),(21,'321VCP','Venture Capital\r\n','Venture Capital\r\n',NULL,'out_scope',1),(22,'341IFR','Infrastructure Financing\r\n','Infrastructure Financing\r\n',NULL,'out_scope',1),(23,'351EXF','Export Financing\r\n','Export Financing\r\n',NULL,'out_scope',1),(24,'731PWN','Pawn companies\r\n','Pawn companies\r\n',NULL,'out_scope',1),(25,'521PNF','Pension Fund \r\n','Pension Fund \r\n',NULL,'out_scope',1),(26,'531MNI','Mandatory insurance companies\r\n','Mandatory insurance companies\r\n',NULL,'out_scope',1),(27,'531SCI','Social Insurance companies\r\n','Social Insurance companies\r\n',NULL,'out_scope',1),(28,'541CRG','Credit guarantee companies\r\n','Credit guarantee companies\r\n',NULL,'out_scope',1),(29,'513RIC','Re-insurance companies\r\n','Re-insurance companies\r\n',NULL,'out_scope',1),(30,'552REB','Re-insurance broker\r\n','Re-insurance broker\r\n',NULL,'out_scope',1),(31,'561LSA','Loss adjuster\r\n','Loss adjuster\r\n',NULL,'out_scope',1);
/*!40000 ALTER TABLE `principals_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `promo_images`
--

DROP TABLE IF EXISTS `promo_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `promo_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `promo_id` int(10) unsigned NOT NULL,
  `photo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `promo_id` (`promo_id`),
  CONSTRAINT `promo_images_ibfk_1` FOREIGN KEY (`promo_id`) REFERENCES `promos` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `promo_images`
--

LOCK TABLES `promo_images` WRITE;
/*!40000 ALTER TABLE `promo_images` DISABLE KEYS */;
INSERT INTO `promo_images` VALUES (1,15,'1568896479068_124910_1568002605524_124910_blog5.jpg'),(2,16,'1568002606768_10566_Logo-Pialang-Sekuritas-Bareksa.jpg');
/*!40000 ALTER TABLE `promo_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `promos`
--

DROP TABLE IF EXISTS `promos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `promos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` int(1) NOT NULL DEFAULT '1',
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `img` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `promos`
--

LOCK TABLES `promos` WRITE;
/*!40000 ALTER TABLE `promos` DISABLE KEYS */;
INSERT INTO `promos` VALUES (12,1,'Promo Murmer','1568002606920_76273_PLACEHOLDER PROMO IMAGE - BANK HSBC.jpg','2019-09-18','2019-09-30','good','2019-09-17 09:05:32','2019-09-17 09:05:32'),(15,1,'Promo Kecee','1568896479068_5255_1568002607580_5255_vimeo-1.jpg','2019-09-25','2019-09-29','aaaa','2019-09-17 09:38:04','2019-09-19 20:50:44'),(16,1,'Promo juoss','1568002606752_28889_Logo-Bank-HSBC.jpg','2019-09-28','2019-09-25','sdfdsf','2019-09-19 20:55:59','2019-09-19 20:55:59');
/*!40000 ALTER TABLE `promos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question`
--

DROP TABLE IF EXISTS `question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `subject` varchar(100) DEFAULT NULL,
  `message` text,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question`
--

LOCK TABLES `question` WRITE;
/*!40000 ALTER TABLE `question` DISABLE KEYS */;
INSERT INTO `question` VALUES (2,'s','d@mail.com','sdfsf','sdfsdfsd','2019-09-19 17:37:42');
/*!40000 ALTER TABLE `question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reff_provinces`
--

DROP TABLE IF EXISTS `reff_provinces`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reff_provinces` (
  `id_propinsi` int(11) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_propinsi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reff_provinces`
--

LOCK TABLES `reff_provinces` WRITE;
/*!40000 ALTER TABLE `reff_provinces` DISABLE KEYS */;
INSERT INTO `reff_provinces` VALUES (11,'ACEH'),(12,'SUMATERA UTARA'),(13,'SUMATERA BARAT'),(14,'RIAU'),(15,'JAMBI'),(16,'SUMATERA SELATAN'),(17,'BENGKULU'),(18,'LAMPUNG'),(19,'KEPULAUAN BANGKA BELITUNG'),(21,'KEPULAUAN RIAU'),(31,'DKI JAKARTA'),(32,'JAWA BARAT'),(33,'JAWA TENGAH'),(34,'DI YOGYAKARTA'),(35,'JAWA TIMUR'),(36,'BANTEN'),(51,'BALI'),(52,'NUSA TENGGARA BARAT'),(53,'NUSA TENGGARA TIMUR'),(61,'KALIMANTAN BARAT'),(62,'KALIMANTAN TENGAH'),(63,'KALIMANTAN SELATAN'),(64,'KALIMANTAN TIMUR'),(65,'KALIMANTAN UTARA'),(71,'SULAWESI UTARA'),(72,'SULAWESI TENGAH'),(73,'SULAWESI SELATAN'),(74,'SULAWESI TENGGARA'),(75,'GORONTALO'),(76,'SULAWESI BARAT'),(81,'MALUKU'),(82,'MALUKU UTARA'),(91,'PAPUA'),(92,'PAPUA BARAT');
/*!40000 ALTER TABLE `reff_provinces` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_desc` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_by` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_id_unique` (`id`),
  KEY `roles_id_index` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'','','','',NULL,NULL);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `avaibility_for` varchar(50) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services`
--

LOCK TABLES `services` WRITE;
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
INSERT INTO `services` VALUES (1,'Bank Rate Display\r\n','Understand the interest rates available with our bank partners\r\n','Logo Services - Bank Rate Display.png','Public\r\n',1),(2,'Exchange Rate Display\r\n','Be informed on currency exchange rate available with our money changer partnes\r\n','Logo Services - Exchange Rate Display.png','Public\r\n',1),(3,'GeldStadt Member Counter\r\n','Register as a GeldStadt Member and enjoy special services and amenities\r\n','Logo Services - GeldStadt Member Counter.png','Public\r\n',1),(4,'Bank Office\r\n','Conduct your bank service at a bank office located in our financial center\r\n','Logo Services - Bank Office.png','Visitor\r\n',1),(5,'ATM\r\n','Withdraw or deposit funds at ATMs in our financial center\r\n','Logo Services - ATM.png','Visitor\r\n',1),(6,'High Capacity ATM\r\n','Enjoy ATM with higher transaction withdrawal and deposit limit\r\n','Logo Services - High Capacity ATM.png','Member\r\n',1),(7,'Private ATM\r\n','Conduct your ATM activities in your own private space\r\n','Logo Services - Private ATM.png','Member\r\n',1),(8,'Money Changer\r\n','Have the convinience of buying and selling currency at our financial center\r\n','Logo Services - Money Changer.png','Visitor\r\n',1),(9,'Remittance\r\n','Deliver and receive cash to international and domestic counterparts at our financial center\r\n','Logo Services - Remittance.png','Visitor',1),(10,'Financial Product Promo\r\n','Catch the latest promo from our partners\r\n','Logo Services - Financial Product Promo.png','Visitor\r\n',1),(11,'Financial Product Display\r\n','Discover, compare, and evaluate financial products with our state of the art equipments\r\n','Logo Services - Financial Product Display.png','Visitor\r\n',1),(12,'Financial Product Consultation\r\n','Inquire, understand, and decide how available financial products can help you\r\n','Logo Services - Financial Product Consultation.png','Visitor\r\n',1),(13,'Credit Card Application\r\n','Apply for a credit card from the issuer of your choice\r\n','Logo Services - Credit Card Application.png','Visitor\r\n',1),(14,'Individual Loan Application\r\n','Submit an application for a personal loan such as house or car financing or refinancing\r\n','Logo Services - Individual Loan Application.png','Visitor\r\n',1),(15,'MSE Loan Application\r\n','Submit an application for your micro and small business\r\n','Logo Services - MSE Loan Application.png','Visitor\r\n',1),(16,'Investment Brokerage\r\n','Enjoy the service of our partner investment broker\r\n','Logo Services - Investment Brokerage.png','Visitor\r\n',1),(17,'Insurance Brokerage\r\n','Enjoy the service of our partner insurance broker\r\n','Logo Services - Insurance Brokerage.png','Visitor\r\n',1),(18,'Financial Product Purchase\r\n','Purchase a financial product that you decide is right for you\r\n','Logo Services - Financial Product Purchase.png','Visitor\r\n',1),(19,'P2P Lender Registration\r\n','Register as a lender for P2P and help others get the funding they need\r\n','Logo Services - P2P Lender Registration.png','Visitor\r\n',1),(20,'E-Money Registration & Top Up\r\n','Register and top up your E-Money account at our financial center\r\n','Logo Services - E-Money Registration & Top Up.png','Visitor\r\n',1),(21,'KYC Verification\r\n','Complete your KYC Verification, stored it as digital ID, and use it multiple times\r\n','Logo Services - KYC Verification.png','Member\r\n',1),(25,'ok1','okoko','LogoServices_c626d4e255a7acc3b606dd246d196132_ttd.png','Member',1);
/*!40000 ALTER TABLE `services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stores`
--

DROP TABLE IF EXISTS `stores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) DEFAULT NULL,
  `photo` varchar(200) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `province` varchar(50) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `building_name` varchar(50) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `postal` int(6) DEFAULT NULL,
  `phone_number` varchar(50) DEFAULT NULL,
  `open_date` varchar(50) DEFAULT NULL,
  `opening_date` varchar(200) DEFAULT NULL,
  `opening_hour` varchar(200) DEFAULT NULL,
  `status` int(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `code` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stores`
--

LOCK TABLES `stores` WRITE;
/*!40000 ALTER TABLE `stores` DISABLE KEYS */;
INSERT INTO `stores` VALUES (1,'GS-IDN-JKT-JKTPST-GRID-1910A','Hero-Image-Grand-Indo-GS-IDN-JKT-JKTPST-GRID-1910A.jpg','Grand Indonesia East Mall\r\n','Indonesia','DKI Jakarta','Jakarta Pusat','Grand Indonesia\r\n','Jl. M.H. Thamrin No.1, RT.1/RW.5, Kb. Melati, Kec. Menteng',10310,'+62 21 3518 336','01-Oct-19\r\n','Mon, Tue, Wed, Thurs, Frid, Sat, Sun','10.00 to 22.00\r\n',1),(2,'GS-IDN-JKT-JKTSLT-POIN-1911A','Hero-Image-Pondok-Indah-Mall-2-GS-IDN-JKT-JKTSLT-POIN-1911A.jpg','Mall Pondok Indah 2','Indonesia','DKI Jakarta','Jakarta Selatan','Pondok Indah Mall 2','Jl. Metro Pondok Indah No.Kav. IV, RT.1/RW.16, Pd. Pinang, Kec. Kby. Lama',12310,'+62 21 3518 335','01-Nov-19','Mon, Tue, Wed, Thurs, Frid, Sat, Sun','10.00 to 22.00',1),(3,'GS-IDN-JKT-JKTBRT-PUIN-1912A','1566525695363_121146_666194.jpg','Mall Puri Indah','Indonesia','DKI Jakarta','Jakarta Barat','Puri Indah Mall','Jl. Puri Agung No.1, RT.1/RW.2, Kembangan Sel., Kec. Kembangan',11610,'+62 21 3518 381','01-Dec-19','Mon, Tue, Wed, Thurs, Frid, Sat, Sun','10.00 to 22.00',1),(4,'GS-IDN-JKT-JKTURT-KEGA-1910A','Hero-Image-Mall-Kelapa-Gading-GS-IDN-JKT-JKTURT-KEGA-1910A.jpg','Mall Kelapa Gading\r\n','Indonesia','DKI Jakarta','Jakarta Utara','Mall Kelapa Gading\r\n','Jl. Boulevard Raya, Kec. Klp. Gading',14240,'+62 21 3518 382','01-Oct-19\r\n','Mon, Tue, Wed, Thurs, Frid, Sat, Sun','10.00 to 22.00\r\n',1),(5,'GS-IDN-JKT-JKTBRT-CEPA-1910A','1563530443000_47641_IMG-20190719-WA0020.jpg','Mall Central Park','Indonesia','DKI Jakarta','Jakarta Barat','Central Park','Jl. Letjen S. Parman No.28, RT.12/RW.6, Tj. Duren Sel., Kec. Grogol petamburan',11470,'+62 21 3518 383','01-Oct-19','Mon, Tue, Wed, Thurs, Frid, Sat, Sun','10.00 to 22.00',1),(6,'GS-IDN-JKT-JKTURT-PIAV-1911A','Hero-Image-PIK-Avenue-GS-IDN-JKT-JKTURT-PIAV-1911A.jpg','PIK Avenue\r\n','Indonesia','DKI Jakarta','Jakarta Utara','PIK Avenue\r\n','Jl. Pantai Indah Kapuk No.6, RT.6/RW.2, Kamal Muara, Kec. Penjaringan',14470,'+62 21 3518 384','01-Nov-19\r\n','Mon, Tue, Wed, Thurs, Frid, Sat, Sun','10.00 to 22.00\r\n',1),(7,'GS-IDN-JTM-SURBAY-SUTO-1911A','1566525695363_121146_666194.jpg','Surabaya Town Square','Indonesia','Jawa Timur','Surabaya','Surabaya Town Square','Jl. Hayam Wuruk No.6, Sawunggaling, Kec. Wonokromo',60242,'+62 31 5610 222','01-Nov-19','Mon, Tue, Wed, Thurs, Frid, Sat, Sun','10.00 to 22.00',1);
/*!40000 ALTER TABLE `stores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stores_amenities`
--

DROP TABLE IF EXISTS `stores_amenities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stores_amenities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store_code` varchar(50) DEFAULT NULL,
  `amenities_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_stores_amenities_ibfk_2` (`amenities_id`),
  KEY `FK_stores_amenities_ibfk_1` (`store_code`),
  CONSTRAINT `FK_stores_amenities_ibfk_1` FOREIGN KEY (`store_code`) REFERENCES `stores` (`code`) ON DELETE CASCADE,
  CONSTRAINT `FK_stores_amenities_ibfk_2` FOREIGN KEY (`amenities_id`) REFERENCES `amenities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1845 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stores_amenities`
--

LOCK TABLES `stores_amenities` WRITE;
/*!40000 ALTER TABLE `stores_amenities` DISABLE KEYS */;
INSERT INTO `stores_amenities` VALUES (1,'GS-IDN-JKT-JKTPST-GRID-1910A',1),(2,'GS-IDN-JKT-JKTPST-GRID-1910A',2),(3,'GS-IDN-JKT-JKTPST-GRID-1910A',3),(4,'GS-IDN-JKT-JKTPST-GRID-1910A',4),(5,'GS-IDN-JKT-JKTPST-GRID-1910A',5),(6,'GS-IDN-JKT-JKTPST-GRID-1910A',6),(7,'GS-IDN-JKT-JKTPST-GRID-1910A',7),(8,'GS-IDN-JKT-JKTPST-GRID-1910A',8),(9,'GS-IDN-JKT-JKTPST-GRID-1910A',9),(10,'GS-IDN-JKT-JKTPST-GRID-1910A',10),(11,'GS-IDN-JKT-JKTPST-GRID-1910A',11),(12,'GS-IDN-JKT-JKTPST-GRID-1910A',12),(13,'GS-IDN-JKT-JKTPST-GRID-1910A',13),(14,'GS-IDN-JKT-JKTPST-GRID-1910A',14),(15,'GS-IDN-JKT-JKTPST-GRID-1910A',15),(16,'GS-IDN-JKT-JKTPST-GRID-1910A',16),(17,'GS-IDN-JKT-JKTPST-GRID-1910A',17),(48,'GS-IDN-JKT-JKTURT-KEGA-1910A',2),(49,'GS-IDN-JKT-JKTURT-KEGA-1910A',3),(50,'GS-IDN-JKT-JKTURT-KEGA-1910A',4),(51,'GS-IDN-JKT-JKTURT-KEGA-1910A',5),(52,'GS-IDN-JKT-JKTURT-KEGA-1910A',6),(53,'GS-IDN-JKT-JKTURT-KEGA-1910A',7),(54,'GS-IDN-JKT-JKTURT-KEGA-1910A',8),(55,'GS-IDN-JKT-JKTURT-KEGA-1910A',9),(56,'GS-IDN-JKT-JKTURT-KEGA-1910A',10),(57,'GS-IDN-JKT-JKTURT-KEGA-1910A',11),(58,'GS-IDN-JKT-JKTURT-KEGA-1910A',12),(59,'GS-IDN-JKT-JKTURT-KEGA-1910A',13),(60,'GS-IDN-JKT-JKTURT-KEGA-1910A',16),(61,'GS-IDN-JKT-JKTURT-KEGA-1910A',18),(77,'GS-IDN-JKT-JKTURT-PIAV-1911A',2),(78,'GS-IDN-JKT-JKTURT-PIAV-1911A',3),(79,'GS-IDN-JKT-JKTURT-PIAV-1911A',4),(80,'GS-IDN-JKT-JKTURT-PIAV-1911A',5),(81,'GS-IDN-JKT-JKTURT-PIAV-1911A',6),(82,'GS-IDN-JKT-JKTURT-PIAV-1911A',7),(83,'GS-IDN-JKT-JKTURT-PIAV-1911A',8),(84,'GS-IDN-JKT-JKTURT-PIAV-1911A',9),(85,'GS-IDN-JKT-JKTURT-PIAV-1911A',10),(86,'GS-IDN-JKT-JKTURT-PIAV-1911A',11),(87,'GS-IDN-JKT-JKTURT-PIAV-1911A',12),(88,'GS-IDN-JKT-JKTURT-PIAV-1911A',13),(89,'GS-IDN-JKT-JKTURT-PIAV-1911A',14),(90,'GS-IDN-JKT-JKTURT-PIAV-1911A',16),(511,'GS-IDN-JKT-JKTBRT-CEPA-1910A',2),(512,'GS-IDN-JKT-JKTBRT-CEPA-1910A',3),(513,'GS-IDN-JKT-JKTBRT-CEPA-1910A',4),(514,'GS-IDN-JKT-JKTBRT-CEPA-1910A',5),(515,'GS-IDN-JKT-JKTBRT-CEPA-1910A',6),(516,'GS-IDN-JKT-JKTBRT-CEPA-1910A',7),(517,'GS-IDN-JKT-JKTBRT-CEPA-1910A',8),(518,'GS-IDN-JKT-JKTBRT-CEPA-1910A',9),(519,'GS-IDN-JKT-JKTBRT-CEPA-1910A',10),(520,'GS-IDN-JKT-JKTBRT-CEPA-1910A',11),(521,'GS-IDN-JKT-JKTBRT-CEPA-1910A',12),(522,'GS-IDN-JKT-JKTBRT-CEPA-1910A',13),(523,'GS-IDN-JKT-JKTBRT-CEPA-1910A',14),(524,'GS-IDN-JKT-JKTBRT-CEPA-1910A',16),(525,'GS-IDN-JKT-JKTBRT-CEPA-1910A',18),(526,'GS-IDN-JTM-SURBAY-SUTO-1911A',1),(527,'GS-IDN-JTM-SURBAY-SUTO-1911A',2),(528,'GS-IDN-JTM-SURBAY-SUTO-1911A',3),(529,'GS-IDN-JTM-SURBAY-SUTO-1911A',4),(530,'GS-IDN-JTM-SURBAY-SUTO-1911A',5),(531,'GS-IDN-JTM-SURBAY-SUTO-1911A',6),(532,'GS-IDN-JTM-SURBAY-SUTO-1911A',7),(533,'GS-IDN-JTM-SURBAY-SUTO-1911A',8),(534,'GS-IDN-JTM-SURBAY-SUTO-1911A',9),(535,'GS-IDN-JTM-SURBAY-SUTO-1911A',10),(536,'GS-IDN-JTM-SURBAY-SUTO-1911A',11),(537,'GS-IDN-JTM-SURBAY-SUTO-1911A',13),(538,'GS-IDN-JTM-SURBAY-SUTO-1911A',14),(539,'GS-IDN-JTM-SURBAY-SUTO-1911A',16),(540,'GS-IDN-JTM-SURBAY-SUTO-1911A',17),(1186,'GS-IDN-JKT-JKTBRT-PUIN-1912A',2),(1187,'GS-IDN-JKT-JKTBRT-PUIN-1912A',3),(1188,'GS-IDN-JKT-JKTBRT-PUIN-1912A',4),(1189,'GS-IDN-JKT-JKTBRT-PUIN-1912A',5),(1190,'GS-IDN-JKT-JKTBRT-PUIN-1912A',6),(1191,'GS-IDN-JKT-JKTBRT-PUIN-1912A',7),(1192,'GS-IDN-JKT-JKTBRT-PUIN-1912A',8),(1193,'GS-IDN-JKT-JKTBRT-PUIN-1912A',9),(1194,'GS-IDN-JKT-JKTBRT-PUIN-1912A',10),(1195,'GS-IDN-JKT-JKTBRT-PUIN-1912A',11),(1196,'GS-IDN-JKT-JKTBRT-PUIN-1912A',12),(1197,'GS-IDN-JKT-JKTBRT-PUIN-1912A',13),(1198,'GS-IDN-JKT-JKTBRT-PUIN-1912A',14),(1199,'GS-IDN-JKT-JKTBRT-PUIN-1912A',16),(1200,'GS-IDN-JKT-JKTBRT-PUIN-1912A',19),(1771,'GS-IDN-JKT-JKTSLT-POIN-1911A',2),(1772,'GS-IDN-JKT-JKTSLT-POIN-1911A',3),(1773,'GS-IDN-JKT-JKTSLT-POIN-1911A',4),(1774,'GS-IDN-JKT-JKTSLT-POIN-1911A',5),(1775,'GS-IDN-JKT-JKTSLT-POIN-1911A',6),(1776,'GS-IDN-JKT-JKTSLT-POIN-1911A',7),(1777,'GS-IDN-JKT-JKTSLT-POIN-1911A',8),(1778,'GS-IDN-JKT-JKTSLT-POIN-1911A',9),(1779,'GS-IDN-JKT-JKTSLT-POIN-1911A',10),(1780,'GS-IDN-JKT-JKTSLT-POIN-1911A',11),(1781,'GS-IDN-JKT-JKTSLT-POIN-1911A',12),(1782,'GS-IDN-JKT-JKTSLT-POIN-1911A',13),(1783,'GS-IDN-JKT-JKTSLT-POIN-1911A',15),(1784,'GS-IDN-JKT-JKTSLT-POIN-1911A',16),(1785,'GS-IDN-JKT-JKTSLT-POIN-1911A',19);
/*!40000 ALTER TABLE `stores_amenities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stores_events`
--

DROP TABLE IF EXISTS `stores_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stores_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(10) unsigned NOT NULL,
  `stores_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `event_id` (`event_id`),
  KEY `stores_id` (`stores_id`),
  CONSTRAINT `stores_events_ibfk_1` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE CASCADE,
  CONSTRAINT `stores_events_ibfk_2` FOREIGN KEY (`stores_id`) REFERENCES `stores` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=173 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stores_events`
--

LOCK TABLES `stores_events` WRITE;
/*!40000 ALTER TABLE `stores_events` DISABLE KEYS */;
/*!40000 ALTER TABLE `stores_events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stores_images`
--

DROP TABLE IF EXISTS `stores_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stores_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store_code` varchar(50) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stores_images`
--

LOCK TABLES `stores_images` WRITE;
/*!40000 ALTER TABLE `stores_images` DISABLE KEYS */;
INSERT INTO `stores_images` VALUES (1,'GS-IDN-JKT-JKTPST-GRID-1910A','Image A Grand Indo - GS-IDN-JKT-JKTPST-GRID-1910A.jpg'),(2,'GS-IDN-JKT-JKTPST-GRID-1910A','Image B Grand Indo - GS-IDN-JKT-JKTPST-GRID-1910A.jpg'),(3,'GS-IDN-JKT-JKTSLT-POIN-1911A','Image A Pondok Indah Mall 2 - GS-IDN-JKT-JKTSLT-POIN-1911A.jpg'),(4,'GS-IDN-JKT-JKTSLT-POIN-1911A','Image B Pondok Indah Mall 2 - GS-IDN-JKT-JKTSLT-POIN-1911A.jpg'),(7,'GS-IDN-JKT-JKTURT-KEGA-1910A','Image A Mall Kelapa Gading - GS-IDN-JKT-JKTURT-KEGA-1910A.jpg'),(8,'GS-IDN-JKT-JKTURT-KEGA-1910A','Image B Mall Kelapa Gading - GS-IDN-JKT-JKTURT-KEGA-1910A.jpg'),(9,'GS-IDN-JKT-JKTBRT-CEPA-1910A','Image A Mall Central Park - GS-IDN-JKT-JKTBRT-CEPA-1910A.jpg'),(10,'GS-IDN-JKT-JKTBRT-CEPA-1910A','Image B Mall Central Park - GS-IDN-JKT-JKTBRT-CEPA-1910A.jpg'),(11,'GS-IDN-JKT-JKTURT-PIAV-1911A','Image A PIK Avenue GS-IDN-JKT-JKTURT-PIAV-1911A.jpg'),(12,'GS-IDN-JKT-JKTURT-PIAV-1911A','Image B PIK Avenue GS-IDN-JKT-JKTURT-PIAV-1911A.jpg'),(13,'GS-IDN-JTM-SURBAY-SUTO-1911A','Image A Surabaya Town Square - GS-IDN-JTM-SURBAY-SUTO-1911A.jpg'),(14,'GS-IDN-JTM-SURBAY-SUTO-1911A','Image B Surabaya Town Square - GS-IDN-JTM-SURBAY-SUTO-1911A.jpg'),(17,'GS-IDN-JKT-JKTBRT-PUIN-1912A','Image A Mall Puri Indah - GS-IDN-JKT-JKTBRT-PUIN-1912A.jpg'),(18,'GS-IDN-JKT-JKTBRT-PUIN-1912A','Image B Mall Puri Indah - GS-IDN-JKT-JKTBRT-PUIN-1912A.jpg'),(90,'test-01','1566525695363_121146_666194.jpg'),(91,'test-01','1566462748744_135787_WhatsApp Image 2019-08-22 at 3.29.33 PM.jpeg'),(92,'testtt','1568712936428_411760_1568696926698_411760_1568471368865_411760_1568002606980_411760_HERO PHOTO CAROUSEL 001 - CopyRight Trast Digital.jpg'),(93,'testtt','1568711275611_218569_1568696311127_218569_1568611415628_218569_1568185287939_218569_1566462746000_218569_WhatsApp Image 2019-08-22 at 3.27.40 PM.jpeg'),(94,'r','1568002607540_1992224_Image B PIK Avenue GS-IDN-JKT-JKTURT-PIAV-1911A.jpg'),(95,'bejo-0123','1568002607240_588033_Image A Grand Indo - GS-IDN-JKT-JKTPST-GRID-1910A.jpg');
/*!40000 ALTER TABLE `stores_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stores_principals_atms`
--

DROP TABLE IF EXISTS `stores_principals_atms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stores_principals_atms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store_code` varchar(50) DEFAULT NULL,
  `principal_code` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_stores_principals_atms_ibfk_1` (`store_code`),
  KEY `FK_stores_principals_atms_ibfk_2` (`principal_code`),
  CONSTRAINT `FK_stores_principals_atms_ibfk_1` FOREIGN KEY (`store_code`) REFERENCES `stores` (`code`) ON DELETE CASCADE,
  CONSTRAINT `FK_stores_principals_atms_ibfk_2` FOREIGN KEY (`principal_code`) REFERENCES `principals` (`code`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=912 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stores_principals_atms`
--

LOCK TABLES `stores_principals_atms` WRITE;
/*!40000 ALTER TABLE `stores_principals_atms` DISABLE KEYS */;
INSERT INTO `stores_principals_atms` VALUES (1,'GS-IDN-JKT-JKTPST-GRID-1910A','GP-BNK-IDN-CMNG'),(2,'GS-IDN-JKT-JKTPST-GRID-1910A','GP-BNK-IDN-DNMN'),(3,'GS-IDN-JKT-JKTPST-GRID-1910A','GP-BNK-IDN-MYBN'),(4,'GS-IDN-JKT-JKTPST-GRID-1910A','GP-BNK-IDN-PRMT'),(5,'GS-IDN-JKT-JKTPST-GRID-1910A','GP-BNK-IDN-DBSI'),(6,'GS-IDN-JKT-JKTPST-GRID-1910A','GP-BNK-IDN-MYPD'),(7,'GS-IDN-JKT-JKTPST-GRID-1910A','GP-BNK-IDN-MEGA'),(8,'GS-IDN-JKT-JKTPST-GRID-1910A','GP-BNK-IDN-UOBI'),(24,'GS-IDN-JKT-JKTURT-KEGA-1910A','GP-BNK-IDN-CMNG'),(25,'GS-IDN-JKT-JKTURT-KEGA-1910A','GP-BNK-IDN-BTPN'),(26,'GS-IDN-JKT-JKTURT-KEGA-1910A','GP-BNK-IDN-PRMT'),(27,'GS-IDN-JKT-JKTURT-KEGA-1910A','GP-BNK-IDN-JBBT'),(28,'GS-IDN-JKT-JKTURT-KEGA-1910A','GP-BNK-IDN-BKPN'),(29,'GS-IDN-JKT-JKTURT-KEGA-1910A','GP-BNK-IDN-MYPD'),(30,'GS-IDN-JKT-JKTURT-KEGA-1910A','GP-BNK-IDN-UOBI'),(31,'GS-IDN-JKT-JKTURT-KEGA-1910A','GP-BNK-IDN-KEBH'),(40,'GS-IDN-JKT-JKTURT-PIAV-1911A','GP-BNK-IDN-CMNG'),(41,'GS-IDN-JKT-JKTURT-PIAV-1911A','GP-BNK-IDN-PANN'),(42,'GS-IDN-JKT-JKTURT-PIAV-1911A','GP-BNK-IDN-DNMN'),(43,'GS-IDN-JKT-JKTURT-PIAV-1911A','GP-BNK-IDN-HSBC'),(44,'GS-IDN-JKT-JKTURT-PIAV-1911A','GP-BNK-IDN-MYPD'),(45,'GS-IDN-JKT-JKTURT-PIAV-1911A','GP-BNK-IDN-MEGA'),(46,'GS-IDN-JKT-JKTURT-PIAV-1911A','GP-BNK-IDN-UOBI'),(47,'GS-IDN-JKT-JKTURT-PIAV-1911A','GP-BNK-IDN-SHHN'),(264,'GS-IDN-JKT-JKTBRT-CEPA-1910A','GP-BNK-IDN-PANN'),(265,'GS-IDN-JKT-JKTBRT-CEPA-1910A','GP-BNK-IDN-OCNI'),(266,'GS-IDN-JKT-JKTBRT-CEPA-1910A','GP-BNK-IDN-JBBT'),(267,'GS-IDN-JKT-JKTBRT-CEPA-1910A','GP-BNK-IDN-MEGA'),(268,'GS-IDN-JKT-JKTBRT-CEPA-1910A','GP-BNK-IDN-WRSD'),(269,'GS-IDN-JKT-JKTBRT-CEPA-1910A','GP-BNK-IDN-AMAR'),(270,'GS-IDN-JKT-JKTBRT-CEPA-1910A','GP-BNK-IDN-RSPD'),(271,'GS-IDN-JKT-JKTBRT-CEPA-1910A','GP-BNK-IDN-MYBN'),(272,'GS-IDN-JTM-SURBAY-SUTO-1911A','GP-BNK-IDN-PANN'),(273,'GS-IDN-JTM-SURBAY-SUTO-1911A','GP-BNK-IDN-OCNI'),(274,'GS-IDN-JTM-SURBAY-SUTO-1911A','GP-BNK-IDN-BTPN'),(275,'GS-IDN-JTM-SURBAY-SUTO-1911A','GP-BNK-IDN-JBBT'),(276,'GS-IDN-JTM-SURBAY-SUTO-1911A','GP-BNK-IDN-DBSI'),(277,'GS-IDN-JTM-SURBAY-SUTO-1911A','GP-BNK-IDN-KEBH'),(278,'GS-IDN-JTM-SURBAY-SUTO-1911A','GP-BNK-IDN-WRSD'),(279,'GS-IDN-JTM-SURBAY-SUTO-1911A','GP-BNK-IDN-AMAR'),(605,'GS-IDN-JKT-JKTBRT-PUIN-1912A','GP-BNK-IDN-CMNG'),(606,'GS-IDN-JKT-JKTBRT-PUIN-1912A','GP-BNK-IDN-OCNI'),(607,'GS-IDN-JKT-JKTBRT-PUIN-1912A','GP-BNK-IDN-DBSI'),(608,'GS-IDN-JKT-JKTBRT-PUIN-1912A','GP-BNK-IDN-MEGA'),(609,'GS-IDN-JKT-JKTBRT-PUIN-1912A','GP-BNK-IDN-WRSD'),(610,'GS-IDN-JKT-JKTBRT-PUIN-1912A','GP-BNK-IDN-SHHN'),(611,'GS-IDN-JKT-JKTBRT-PUIN-1912A','GP-BNK-IDN-RSPD'),(878,'GS-IDN-JKT-JKTSLT-POIN-1911A','GP-BNK-IDN-PANN'),(879,'GS-IDN-JKT-JKTSLT-POIN-1911A','GP-BNK-IDN-DNMN'),(880,'GS-IDN-JKT-JKTSLT-POIN-1911A','GP-BNK-IDN-BTPN'),(881,'GS-IDN-JKT-JKTSLT-POIN-1911A','GP-BNK-IDN-JBBT'),(882,'GS-IDN-JKT-JKTSLT-POIN-1911A','GP-BNK-IDN-HSBC'),(883,'GS-IDN-JKT-JKTSLT-POIN-1911A','GP-BNK-IDN-BKPN'),(884,'GS-IDN-JKT-JKTSLT-POIN-1911A','GP-BNK-IDN-KEBH');
/*!40000 ALTER TABLE `stores_principals_atms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stores_principals_banks`
--

DROP TABLE IF EXISTS `stores_principals_banks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stores_principals_banks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store_code` varchar(50) DEFAULT NULL,
  `principal_code` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `stores_principals_banks_ibfk_1` (`store_code`),
  KEY `stores_principals_banks_ibfk_2` (`principal_code`),
  CONSTRAINT `stores_principals_banks_ibfk_1` FOREIGN KEY (`store_code`) REFERENCES `stores` (`code`) ON DELETE CASCADE,
  CONSTRAINT `stores_principals_banks_ibfk_2` FOREIGN KEY (`principal_code`) REFERENCES `principals` (`code`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=277 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stores_principals_banks`
--

LOCK TABLES `stores_principals_banks` WRITE;
/*!40000 ALTER TABLE `stores_principals_banks` DISABLE KEYS */;
INSERT INTO `stores_principals_banks` VALUES (1,'GS-IDN-JKT-JKTPST-GRID-1910A','GP-BNK-IDN-CMNG'),(2,'GS-IDN-JKT-JKTPST-GRID-1910A','GP-BNK-IDN-DNMN'),(7,'GS-IDN-JKT-JKTURT-KEGA-1910A','GP-BNK-IDN-BTPN'),(8,'GS-IDN-JKT-JKTURT-KEGA-1910A','GP-BNK-IDN-PRMT'),(11,'GS-IDN-JKT-JKTURT-PIAV-1911A','GP-BNK-IDN-DNMN'),(12,'GS-IDN-JKT-JKTURT-PIAV-1911A','GP-BNK-IDN-HSBC'),(67,'GS-IDN-JKT-JKTBRT-CEPA-1910A','GP-BNK-IDN-JBBT'),(68,'GS-IDN-JKT-JKTBRT-CEPA-1910A','GP-BNK-IDN-MYBN'),(69,'GS-IDN-JTM-SURBAY-SUTO-1911A','GP-BNK-IDN-PANN'),(70,'GS-IDN-JTM-SURBAY-SUTO-1911A','GP-BNK-IDN-DBSI'),(233,'GS-IDN-JKT-JKTSLT-POIN-1911A','GP-BNK-IDN-PANN'),(234,'GS-IDN-JKT-JKTSLT-POIN-1911A','GP-BNK-IDN-DNMN');
/*!40000 ALTER TABLE `stores_principals_banks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stores_principals_others`
--

DROP TABLE IF EXISTS `stores_principals_others`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stores_principals_others` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store_code` varchar(50) DEFAULT NULL,
  `principal_code` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `stores_principals_others_ibfk_1` (`store_code`),
  KEY `stores_principals_others_ibfk_2` (`principal_code`),
  CONSTRAINT `stores_principals_others_ibfk_1` FOREIGN KEY (`store_code`) REFERENCES `stores` (`code`) ON DELETE CASCADE,
  CONSTRAINT `stores_principals_others_ibfk_2` FOREIGN KEY (`principal_code`) REFERENCES `principals` (`code`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=527 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stores_principals_others`
--

LOCK TABLES `stores_principals_others` WRITE;
/*!40000 ALTER TABLE `stores_principals_others` DISABLE KEYS */;
INSERT INTO `stores_principals_others` VALUES (1,'GS-IDN-JKT-JKTPST-GRID-1910A','GP-SCB-IDN-BRKS'),(2,'GS-IDN-JKT-JKTPST-GRID-1910A','GP-IRB-IDN-KBRU'),(3,'GS-IDN-JKT-JKTPST-GRID-1910A','GP-FRX-IDN-PENT'),(4,'GS-IDN-JKT-JKTPST-GRID-1910A','GP-RMT-IDN-WSTU'),(5,'GS-IDN-JKT-JKTPST-GRID-1910A','GP-EWP-IDN-LNKJ'),(6,'GS-IDN-JKT-JKTPST-GRID-1910A','GP-EWP-IDN-JENS'),(18,'GS-IDN-JKT-JKTURT-KEGA-1910A','GP-SCB-IDN-PNSK'),(19,'GS-IDN-JKT-JKTURT-KEGA-1910A','GP-IRB-IDN-KBRU'),(20,'GS-IDN-JKT-JKTURT-KEGA-1910A','GP-FRX-IDN-VIPR'),(21,'GS-IDN-JKT-JKTURT-KEGA-1910A','GP-RMT-IDN-WSTU'),(22,'GS-IDN-JKT-JKTURT-KEGA-1910A','GP-EWP-IDN-JENS'),(28,'GS-IDN-JKT-JKTURT-PIAV-1911A','GP-SCB-IDN-PNSK'),(29,'GS-IDN-JKT-JKTURT-PIAV-1911A','GP-IRB-IDN-KBRU'),(30,'GS-IDN-JKT-JKTURT-PIAV-1911A','GP-FRX-IDN-PENT'),(31,'GS-IDN-JKT-JKTURT-PIAV-1911A','GP-RMT-IDN-XNDT'),(32,'GS-IDN-JKT-JKTURT-PIAV-1911A','GP-EWP-IDN-LNKJ'),(33,'GS-IDN-JKT-JKTURT-PIAV-1911A','GP-EWP-IDN-VOVO'),(174,'GS-IDN-JKT-JKTBRT-CEPA-1910A','GP-SCB-IDN-BRKS'),(175,'GS-IDN-JKT-JKTBRT-CEPA-1910A','GP-IRB-IDN-PRMR'),(176,'GS-IDN-JKT-JKTBRT-CEPA-1910A','GP-FRX-IDN-PENT'),(177,'GS-IDN-JKT-JKTBRT-CEPA-1910A','GP-RMT-IDN-WSTU'),(178,'GS-IDN-JKT-JKTBRT-CEPA-1910A','GP-EWP-IDN-DANA'),(179,'GS-IDN-JTM-SURBAY-SUTO-1911A','GP-SCB-IDN-BRKS'),(180,'GS-IDN-JTM-SURBAY-SUTO-1911A','GP-IRB-IDN-KBRU'),(181,'GS-IDN-JTM-SURBAY-SUTO-1911A','GP-FRX-IDN-PENT'),(182,'GS-IDN-JTM-SURBAY-SUTO-1911A','GP-RMT-IDN-XNDT'),(183,'GS-IDN-JTM-SURBAY-SUTO-1911A','GP-EWP-IDN-DANA'),(269,'GS-IDN-JKT-JKTBRT-PUIN-1912A','GP-SCB-IDN-BRKS'),(460,'GS-IDN-JKT-JKTSLT-POIN-1911A','GP-SCB-IDN-PNSK'),(461,'GS-IDN-JKT-JKTSLT-POIN-1911A','GP-IRB-IDN-PRMR'),(462,'GS-IDN-JKT-JKTSLT-POIN-1911A','GP-FRX-IDN-VIPR'),(463,'GS-IDN-JKT-JKTSLT-POIN-1911A','GP-RMT-IDN-WSTU'),(464,'GS-IDN-JKT-JKTSLT-POIN-1911A','GP-EWP-IDN-LNKJ');
/*!40000 ALTER TABLE `stores_principals_others` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stores_promos`
--

DROP TABLE IF EXISTS `stores_promos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stores_promos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `promo_id` int(10) unsigned NOT NULL,
  `stores_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `promo_id` (`promo_id`),
  KEY `stores_id` (`stores_id`),
  CONSTRAINT `stores_promos_ibfk_1` FOREIGN KEY (`promo_id`) REFERENCES `promos` (`id`) ON DELETE CASCADE,
  CONSTRAINT `stores_promos_ibfk_2` FOREIGN KEY (`stores_id`) REFERENCES `stores` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=175 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stores_promos`
--

LOCK TABLES `stores_promos` WRITE;
/*!40000 ALTER TABLE `stores_promos` DISABLE KEYS */;
INSERT INTO `stores_promos` VALUES (13,12,3),(14,15,3),(32,12,2);
/*!40000 ALTER TABLE `stores_promos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stores_services`
--

DROP TABLE IF EXISTS `stores_services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stores_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store_code` varchar(50) DEFAULT NULL,
  `service_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `stores_services_ibfk_1` (`store_code`),
  KEY `stores_services_ibfk_2` (`service_id`),
  CONSTRAINT `stores_services_ibfk_1` FOREIGN KEY (`store_code`) REFERENCES `stores` (`code`) ON DELETE CASCADE,
  CONSTRAINT `stores_services_ibfk_2` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=502 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stores_services`
--

LOCK TABLES `stores_services` WRITE;
/*!40000 ALTER TABLE `stores_services` DISABLE KEYS */;
INSERT INTO `stores_services` VALUES (1,'GS-IDN-JKT-JKTPST-GRID-1910A',1),(2,'GS-IDN-JKT-JKTPST-GRID-1910A',2),(3,'GS-IDN-JKT-JKTPST-GRID-1910A',3),(4,'GS-IDN-JKT-JKTPST-GRID-1910A',4),(5,'GS-IDN-JKT-JKTPST-GRID-1910A',5),(6,'GS-IDN-JKT-JKTPST-GRID-1910A',6),(7,'GS-IDN-JKT-JKTPST-GRID-1910A',7),(8,'GS-IDN-JKT-JKTPST-GRID-1910A',8),(9,'GS-IDN-JKT-JKTPST-GRID-1910A',9),(10,'GS-IDN-JKT-JKTPST-GRID-1910A',10),(11,'GS-IDN-JKT-JKTPST-GRID-1910A',11),(12,'GS-IDN-JKT-JKTPST-GRID-1910A',12),(13,'GS-IDN-JKT-JKTPST-GRID-1910A',13),(14,'GS-IDN-JKT-JKTPST-GRID-1910A',14),(15,'GS-IDN-JKT-JKTPST-GRID-1910A',15),(16,'GS-IDN-JKT-JKTPST-GRID-1910A',16),(17,'GS-IDN-JKT-JKTPST-GRID-1910A',17),(18,'GS-IDN-JKT-JKTPST-GRID-1910A',18),(19,'GS-IDN-JKT-JKTPST-GRID-1910A',19),(20,'GS-IDN-JKT-JKTPST-GRID-1910A',20),(21,'GS-IDN-JKT-JKTSLT-POIN-1911A',1),(22,'GS-IDN-JKT-JKTSLT-POIN-1911A',2),(23,'GS-IDN-JKT-JKTSLT-POIN-1911A',3),(24,'GS-IDN-JKT-JKTSLT-POIN-1911A',4),(25,'GS-IDN-JKT-JKTSLT-POIN-1911A',5),(26,'GS-IDN-JKT-JKTSLT-POIN-1911A',6),(27,'GS-IDN-JKT-JKTSLT-POIN-1911A',7),(28,'GS-IDN-JKT-JKTSLT-POIN-1911A',8),(29,'GS-IDN-JKT-JKTSLT-POIN-1911A',10),(30,'GS-IDN-JKT-JKTSLT-POIN-1911A',11),(31,'GS-IDN-JKT-JKTSLT-POIN-1911A',13),(32,'GS-IDN-JKT-JKTSLT-POIN-1911A',14),(33,'GS-IDN-JKT-JKTSLT-POIN-1911A',15),(34,'GS-IDN-JKT-JKTSLT-POIN-1911A',18),(35,'GS-IDN-JKT-JKTSLT-POIN-1911A',19),(36,'GS-IDN-JKT-JKTSLT-POIN-1911A',20),(37,'GS-IDN-JKT-JKTSLT-POIN-1911A',21),(52,'GS-IDN-JKT-JKTURT-KEGA-1910A',1),(53,'GS-IDN-JKT-JKTURT-KEGA-1910A',2),(54,'GS-IDN-JKT-JKTURT-KEGA-1910A',3),(55,'GS-IDN-JKT-JKTURT-KEGA-1910A',4),(56,'GS-IDN-JKT-JKTURT-KEGA-1910A',5),(57,'GS-IDN-JKT-JKTURT-KEGA-1910A',7),(58,'GS-IDN-JKT-JKTURT-KEGA-1910A',8),(59,'GS-IDN-JKT-JKTURT-KEGA-1910A',10),(60,'GS-IDN-JKT-JKTURT-KEGA-1910A',11),(61,'GS-IDN-JKT-JKTURT-KEGA-1910A',13),(62,'GS-IDN-JKT-JKTURT-KEGA-1910A',14),(63,'GS-IDN-JKT-JKTURT-KEGA-1910A',15),(64,'GS-IDN-JKT-JKTURT-KEGA-1910A',18),(78,'GS-IDN-JKT-JKTURT-PIAV-1911A',1),(79,'GS-IDN-JKT-JKTURT-PIAV-1911A',2),(80,'GS-IDN-JKT-JKTURT-PIAV-1911A',3),(81,'GS-IDN-JKT-JKTURT-PIAV-1911A',4),(82,'GS-IDN-JKT-JKTURT-PIAV-1911A',5),(83,'GS-IDN-JKT-JKTURT-PIAV-1911A',8),(84,'GS-IDN-JKT-JKTURT-PIAV-1911A',9),(85,'GS-IDN-JKT-JKTURT-PIAV-1911A',10),(86,'GS-IDN-JKT-JKTURT-PIAV-1911A',11),(87,'GS-IDN-JKT-JKTURT-PIAV-1911A',13),(88,'GS-IDN-JKT-JKTURT-PIAV-1911A',14),(89,'GS-IDN-JKT-JKTURT-PIAV-1911A',15),(90,'GS-IDN-JKT-JKTURT-PIAV-1911A',16),(91,'GS-IDN-JKT-JKTURT-PIAV-1911A',18),(92,'GS-IDN-JKT-JKTURT-PIAV-1911A',19),(93,'GS-IDN-JKT-JKTURT-PIAV-1911A',20),(94,'GS-IDN-JKT-JKTURT-PIAV-1911A',21),(422,'GS-IDN-JKT-JKTBRT-PUIN-1912A',1),(423,'GS-IDN-JKT-JKTBRT-PUIN-1912A',2),(424,'GS-IDN-JKT-JKTBRT-PUIN-1912A',3),(425,'GS-IDN-JKT-JKTBRT-PUIN-1912A',4),(426,'GS-IDN-JKT-JKTBRT-PUIN-1912A',5),(427,'GS-IDN-JKT-JKTBRT-PUIN-1912A',7),(428,'GS-IDN-JKT-JKTBRT-PUIN-1912A',8),(429,'GS-IDN-JKT-JKTBRT-PUIN-1912A',10),(430,'GS-IDN-JKT-JKTBRT-PUIN-1912A',11),(431,'GS-IDN-JKT-JKTBRT-PUIN-1912A',13),(432,'GS-IDN-JKT-JKTBRT-PUIN-1912A',14),(433,'GS-IDN-JKT-JKTBRT-PUIN-1912A',15),(434,'GS-IDN-JKT-JKTBRT-PUIN-1912A',18),(435,'GS-IDN-JKT-JKTBRT-PUIN-1912A',19),(449,'GS-IDN-JKT-JKTBRT-CEPA-1910A',1),(450,'GS-IDN-JKT-JKTBRT-CEPA-1910A',2),(451,'GS-IDN-JKT-JKTBRT-CEPA-1910A',3),(452,'GS-IDN-JKT-JKTBRT-CEPA-1910A',4),(453,'GS-IDN-JKT-JKTBRT-CEPA-1910A',5),(454,'GS-IDN-JKT-JKTBRT-CEPA-1910A',7),(455,'GS-IDN-JKT-JKTBRT-CEPA-1910A',9),(456,'GS-IDN-JKT-JKTBRT-CEPA-1910A',10),(457,'GS-IDN-JKT-JKTBRT-CEPA-1910A',11),(458,'GS-IDN-JKT-JKTBRT-CEPA-1910A',13),(459,'GS-IDN-JKT-JKTBRT-CEPA-1910A',14),(460,'GS-IDN-JKT-JKTBRT-CEPA-1910A',15),(461,'GS-IDN-JKT-JKTBRT-CEPA-1910A',18),(462,'GS-IDN-JTM-SURBAY-SUTO-1911A',1),(463,'GS-IDN-JTM-SURBAY-SUTO-1911A',2),(464,'GS-IDN-JTM-SURBAY-SUTO-1911A',3),(465,'GS-IDN-JTM-SURBAY-SUTO-1911A',4),(466,'GS-IDN-JTM-SURBAY-SUTO-1911A',5),(467,'GS-IDN-JTM-SURBAY-SUTO-1911A',7),(468,'GS-IDN-JTM-SURBAY-SUTO-1911A',8),(469,'GS-IDN-JTM-SURBAY-SUTO-1911A',9),(470,'GS-IDN-JTM-SURBAY-SUTO-1911A',10),(471,'GS-IDN-JTM-SURBAY-SUTO-1911A',11),(472,'GS-IDN-JTM-SURBAY-SUTO-1911A',13),(473,'GS-IDN-JTM-SURBAY-SUTO-1911A',14),(474,'GS-IDN-JTM-SURBAY-SUTO-1911A',15),(475,'GS-IDN-JTM-SURBAY-SUTO-1911A',16),(476,'GS-IDN-JTM-SURBAY-SUTO-1911A',18),(477,'GS-IDN-JTM-SURBAY-SUTO-1911A',19),(478,'GS-IDN-JTM-SURBAY-SUTO-1911A',20);
/*!40000 ALTER TABLE `stores_services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Administrator','admin','admin@geld.id',NULL,'$2y$10$zO0idVVS0UF29xzpkDJG0.N5lcRmDmYYzbIm8uVR3Btg28KmWwwHq','BEduAW3nLE9yX3p5AfLg3xhxVhh9c07ZcuWHYyUsQ1Mhm1zKcdrmqCqpfjlz','2019-08-02 16:44:01','2019-09-08 02:17:47');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-09-20 13:33:12
