<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
// use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\Route;
// use Illuminate\Foundation\Auth;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username()
    {
        return 'username';
    }

    public function login2(Request $request)
    {
        $this->validateLogin($request);

        $credentials = $request->only('username', 'password');
        // $credentials['password'] = \Hash::make($credentials['password']);
        if (Auth::attempt($credentials)) {
            // Authentication passed...
            // return true;
            //var_dump($this->sendLoginResponse($request));
            return redirect()->intended('dashboard');
            print_r(true);die;
            // return redirect()->intended('dashboard');
        }else{
            var_dump($this->sendFailedLoginResponse($request));die;
        }
        // print_r($credentials);
        die;
        // $params = $request->all();

        // $this->validateLogin($request);
        // return 123;
    }
}
