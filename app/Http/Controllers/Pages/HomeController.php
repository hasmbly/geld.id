<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Stores;
use App\PageHome;
use App\Principals;
use App\Promo;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{



    public function index()
    {	

    	$home 	= PageHome::where(['is_publish' => 1])->get();

    	if ($home->isNotEmpty()) {

    	$id 	= $home[0]->id;
    	$photo 	= DB::table('page_homes_images')->where('pages_homes_id', $id)->pluck('photo');


    	return view('pages/home', [

        	'homes' 		=> $home[0],
        	'photos'		=> $photo,
            'stores' 		=> Stores::all(),
            'principals' 	=> Principals::all(),
            'promos'		=> Promo::all(),
            'contact_us'    => DB::table('contact_us')->get()
            ]);

    	} else {

    		abort(403, 'Something Problem.');

    	}
    }
}
