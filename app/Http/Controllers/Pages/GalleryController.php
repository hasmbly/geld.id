<?php

namespace App\Http\Controllers\Pages;

// use App\PageGallery;
use App\Gallery;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class GalleryController extends Controller
{

    public function index()
    {
        return view('pages/gallery', [
            'galleries' => Gallery::where(['is_publish' => 1])->get(),
            'contact_us' => DB::table('contact_us')->get()
            ]);
    }

}
