<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\PageGallery;
use Illuminate\Support\Facades\DB;

class PromoController extends Controller
{

    public function index(Request $request)
    {

        if(isset($request['name'])){
	        return view('pages/promoDetail', [
	            'promos' 		=> DB::table('promos')->where('name', $request['name'])->first(),
	            'contact_us'    => DB::table('contact_us')->get()
	            ]);
	    }else {    	
        return view('pages/promo', [
            'galleries' => PageGallery::where(['is_publish' => 1])->get(),
            'contact_us'    => DB::table('contact_us')->get(),
            'promos' 		=> DB::table('promos')->get()
            ]);
    	}
    }
}
