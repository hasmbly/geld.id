<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request ;
use App\Http\Requests ;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

use App\Stores;
use App\StoresPrincipalsBank;
use Illuminate\Support\Facades\DB;


class StoresController extends Controller
{

    public function index(Request $request)
    {
        if(isset($request['code'])){
            return view('pages/storesDetail', [
                'store' => Stores::where(['code' => $request['code']])->first(),
                'contact_us' => DB::table('contact_us')->get()
                ]);
        }else {
            return view('pages/stores', [
                'stores' => Stores::all(),
                'contact_us' => DB::table('contact_us')->get()
                ]);
        }

    }

    public function detail(Request $request)
    {
        return view('pages/storesDetail', [
            'stores' => Stores::all(),
            'contact_us' => DB::table('contact_us')->get()
            ]);
    }
}
