<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\PrincipalsCategory;
use App\Principals;
use App\StoresPrincipalsAtm;
use App\StoresPrincipalsBank;
use App\StoresPrincipalsOther;
use App\Stores;
use Illuminate\Support\Facades\DB;

class PrincipalsController extends Controller
{

    public function index(Request $request)
    {
        if(isset($request['code'])){
            // get refence stores
            // stores_principals_atms
            $stores = [];
            $atms = StoresPrincipalsAtm::where(['principal_code' => $request['code']])->get();
            foreach ($atms as $atm) {
                $stores[] = $atm->store_code;
            }

            $banks = StoresPrincipalsBank::where(['principal_code' => $request['code']])->get();
            foreach ($banks as $bank) {
                $stores[] = $bank->store_code;
            }

            $others = StoresPrincipalsOther::where(['principal_code' => $request['code']])->get();
            foreach ($others as $other) {
                $stores[] = $other->store_code;
            }
            $stores = array_unique($stores);

            return view('pages/principalsDetail', [
                'principal' => Principals::where(['principals.code' => $request['code']])
                ->join('principals_categories', 'principals.category_code', '=', 'principals_categories.code')
                ->first(),
                'stores' => Stores::whereIn('code', $stores)->get(),
                'contact_us' => DB::table('contact_us')->get()
                ]);
        }else {
            return view('pages/principals', [
                'principals' => PrincipalsCategory::where(['is_eligible' => 'in_scope'])->get(),
                'contact_us' => DB::table('contact_us')->get()
                ]);
        }
    }
}
