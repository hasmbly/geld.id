<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\PageGallery;
use Illuminate\Support\Facades\DB;

class ContactController extends Controller
{

    public function index()
    {	

    	

        return view('pages/contact', [
            'galleries' => PageGallery::where(['is_publish' => 1])->get(),
            'contact_us' => DB::table('contact_us')->get()
            ]);
    }
}

