<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Stores;
use App\Principals;
use App\PageBlogs;
use Illuminate\Support\Facades\DB;

class BlogsController extends Controller
{

    public function index()
    {
        return view('pages/blogs', [
            'blogs' => PageBlogs::where(['is_publish' => 1])->get(),
            'contact_us' => DB::table('contact_us')->get()
            ]);
    }
}
