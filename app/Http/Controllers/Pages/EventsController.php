<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\PageBlogs;
use App\Principals;
use Illuminate\Support\Facades\DB;

class EventsController extends Controller
{

    public function index(Request $request)
    {	
        if(isset($request['title'])){
	        return view('pages/eventsDetail', [
	            'events' 		=> DB::table('events')->where('title', $request['title'])->first(),
	            'contact_us'    => DB::table('contact_us')->get()
	            ]);
	    }else {
	        return view('pages/events', [
	            'blogs' 		=> PageBlogs::where(['is_publish' => 1])->get(),
	            'principals' 	=> Principals::all(),
	            'contact_us'    => DB::table('contact_us')->get(),
	            'events' 		=> DB::table('events')->get()
	            ]);
	       }
    }
}
