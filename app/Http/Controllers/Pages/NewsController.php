<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\PageNews;
use Illuminate\Support\Facades\DB;

class NewsController extends Controller
{

    public function index()
    {
        return view('pages/news', [
            'news' => PageNews::where(['is_publish' => 1])->get(),
            'contact_us' => DB::table('contact_us')->get()
            ]);
    }
}
