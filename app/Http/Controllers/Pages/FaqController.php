<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Faq;
use Illuminate\Support\Facades\DB;

class FaqController extends Controller
{

    public function index()
    {
        return view('pages/faq', [
            'faq' 			=> Faq::all(),
            'faq_question'	=> DB::table('faqs_question')->get(),
            'contact_us' 	=> DB::table('contact_us')->get()
            ]);
    }
}
